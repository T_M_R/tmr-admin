<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\articles;
use App\master;
use Carbon\Carbon;
use Redirect;
use Session;
use DB;
use Config;

class articlesController extends Controller
{
    public function __construct(){
        $this->articles = new articles();
        $this->master = new master();
    }

    public function getArticlesData(Request $request){
        if (!session('username')) {
	    	return redirect('/');
        } 
        $search_key = '';
        if(isset($_GET['search_key'])){
            $search_key=$_GET['search_key'];
        }
        $getArticlesData = $this->articles->getArticlesData($search_key);
        $getArticlesData = $getArticlesData->appends(['search_key'=>$search_key]);

        return view('articles.list')->with('getArticlesData',$getArticlesData)->with('search_key',$search_key);
    }

    public function addNewArticle(){
        if (!session('username')) {
	    	return redirect('/');
        }

        $getCategories = $this->master->getCategories();
        $getSubCategories = $this->master->getSubCategories();
        $getAuthors = $this->master->getAuthors();

        return view('articles.add')->with('getCategories',$getCategories)->with('getSubCategories',$getSubCategories)->with('getAuthors',$getAuthors);
    }

    public function save_article(Request $request){
        if (!session('username')) {
	    	return redirect('/');
        } 
        
        $data = $request->all();
        $save_article = $this->articles->save_article($data);
        return $save_article;
    }

    public function edit_article($id){
        if (!session('username')) {
	    	return redirect('/');
        }

        $getCategories = $this->master->getCategories();
        $getSubCategories = $this->master->getSubCategories();
        $getAuthors = $this->master->getAuthors();
        $edit_article = $this->articles->getArticleDataById($id);

        return view('articles.edit')->with('getSubCategories',$getSubCategories)->with('getAuthors',$getAuthors)->with('edit_article',$edit_article)->with('getCategories',$getCategories);
    }

    public function update_article(Request $request){
        if (!session('username')) {
	    	return redirect('/');
        } 
        
        $data = $request->all();
        $update_article = $this->articles->update_article($data);
        return $update_article;
    }

    public function update_article_status(Request $request){
        if (!session('username')) {
	    	return redirect('/');
        } 
        
        $data = $request->all();
        $update_article_status = $this->articles->update_article_status($data);
        return $update_article_status;
    }

    public function view_article($id){
        if (!session('username')) {
	    	return redirect('/');
        }

        $getCategories = $this->master->getCategories();
        $getSubCategories = $this->master->getSubCategories();
        $getAuthors = $this->master->getAuthors();
        $view_article = $this->articles->getArticleDataById($id);

        return view('articles.view')->with('getSubCategories',$getSubCategories)->with('getAuthors',$getAuthors)->with('view_article',$view_article);
    }
}
