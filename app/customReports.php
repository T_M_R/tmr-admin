<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Session;
use Config;
use Carbon\Carbon;

class customReports extends Model
{
    public function getCustomReports($search_key){
        $query = DB::table('tbl_custom_reports')
                    ->orderBy('rep_id','DESC');
        if($search_key != ''){
            $query->where('rep_name','like','%'.$search_key.'%');
        }

        $getCustomReports = $query->paginate(30);

        if(sizeof($getCustomReports)>0){
            return $getCustomReports;
        }
        else{
            return [];
        }
    } 

    public function getCustomReports_For_Download($search_key){
        $query = DB::table('tbl_custom_reports')
                    ->orderBy('rep_id','DESC');
        if($search_key != ''){
            $query->where('rep_name','like','%'.$search_key.'%');
        }

        $getCustomReports_For_Download = $query->get();

        if(sizeof($getCustomReports_For_Download)>0){
            return $getCustomReports_For_Download;
        }
        else{
            return [];
        }
    } 


    public function save_custom_report($data){
        $date = Carbon::now();

        $save_custom_report = DB::table('tbl_custom_reports')->insert([
            'rep_name' => $data['custom_report_name'],
            'rep_url' => $data['custom_report_url'],  
            'rep_pub_date' => $data['custom_report_publish_date'],      
            'rep_price_el' => $data['custom_report_el'],
            'rep_price_sul' => $data['custom_report_sul'],
            'rep_price_cul' => $data['custom_report_cul'],
            'cat_id' => $data['custom_report_category_id']
            // 'rep_type' => $data['custom_report_type'],
            // 'added_date' => $date
        ]);
        
        if($save_custom_report == 1){
            return 1;
        }
        else{
            return 0;
        }
    }

    public function getCustomReportDataById($id){
        $getCustomReportDataById = DB::table('tbl_custom_reports')
                                ->where('rep_id','=',$id)
                                ->get();
        if(sizeof($getCustomReportDataById)>0){
            return $getCustomReportDataById;
        }
        else{
            return [];
        }
    }

    public function update_custom_report($data){
        $date = Carbon::now();

        $update_custom_report = DB::table('tbl_custom_reports')->where('rep_id','=',$data['custom_report_id'])->update([
            'rep_name' => $data['custom_report_name'],
            'rep_url' => $data['custom_report_url'],
            'rep_pub_date' => $data['custom_report_publish_date'],
            'rep_price_el' => $data['custom_report_el'],
            'rep_price_sul' => $data['custom_report_sul'],
            'rep_price_cul' => $data['custom_report_cul'],
            'cat_id' => $data['custom_report_category_id']
            // 'update_date' => $date
        ]);
        
        if($update_custom_report == 1){
            return 1;
        }
        else{
            return 0;
        }
    }

    public function update_custom_report_status($data){
        $date = Carbon::now();
        $status='';
        if($data['hidden_status'] == 1){
            $status='Y';
        }
        else{
            $status='N';
        }
        $update_custom_report_status = DB::table('tbl_custom_reports')->where('rep_id','=',$data['custom_report_id'])->update([
            'rep_status' => $status,
            // 'update_date' => $date
        ]);
        
        if($update_custom_report_status == 1){
            return 1;
        }
        else{
            return 0;
        }
    }
}
