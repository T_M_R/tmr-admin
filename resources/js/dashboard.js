const { constant } = require("lodash");

if ($('#funnelcontainer').length) {
    getLeadStatus();
    getMnthWiseleadData();
    getMnthWiseOrdersData();
    getMnthWiseReportData();
    // getMnthWiseArticlesData();
}

$('.get-custom-funnel-leads').click(function () {
    getLeadStatus();
    getMnthWiseleadData();
    getMnthWiseOrdersData();
    getMnthWiseReportData();
    // getMnthWiseArticlesData();
})

function getLastMonths(n) {
    var monthNames = ["", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    var months = new Array();
    var today = new Date();
    // var currentMonth = monthNames[date.getMonth()];
    // var currentDate = date.getDate();
    var year = today.getFullYear();
    var month = today.getMonth() + 1;
    // console.log(month);
    var i = 0;
    // do {
    //     months.push(monthNames[parseInt((month > 9 ? "" : "0") + month)]);
    //     if (month == 1) {
    //         month = 12;
    //         year--;
    //     } else {
    //         month--;
    //     }
    //     i++;
    // } while (i < n);

    do {
        months.push(year + '-' + (month > 9 ? "" : "0") + month);
        if (month == 1) {
            month = 12;
            year--;
        } else {
            month--;
        }
        i++;
    } while (i < n);
    return months;
}

function getLeadStatus() {
    var user_id = $('#user').val();
    var category_id = $('#category_id').val();
    var from_date = $('.from_date').val();
    var to_date = $('.to_date').val();
    // console.log(from_date);
    // console.log(to_date);
    $.ajax({
        url: 'getLeadStatus',
        type: "GET",
        data: {
            from_date: from_date,
            to_date: to_date,
            user_id: user_id,
            category_id: category_id,
        },
        success: function (result) {
            console.log(result);
            // return false;
            result = JSON.parse(result)
            if (result['status'] === true) {
                response = result.leadStatus[0];
                if (response.active == null && response.pipeline == null && response.inprocess == null && response.closed == null && response.sale == null) {
                    getLeadFilterStatus(0, 0, 0, 0, 0);
                }
                else {
                    getLeadFilterStatus(parseInt(response.active), parseInt(response.pipeline), parseInt(response.inprocess), parseInt(response.closed), parseInt(response.sale))
                }
            }
            else {
                getLeadFilterStatus(0, 0, 0, 0, 0);
            }
        }
    });
}

function getLeadFilterStatus(active, pipeline, inprocess, closed, sale) {
    $('.loadingio-spinner-eclipse-awp1yaa5q94').css({ 'display': 'block' });
    var chart_active = !isNaN(active) ? active : 0;
    var chart_pipeline = !isNaN(pipeline) ? pipeline : 0;
    var chart_inprocess = !isNaN(inprocess) ? inprocess : 0;
    var chart_closed = !isNaN(closed) ? closed : 0;
    var chart_sale = !isNaN(sale) ? sale : 0;
    // console.log(chart_active);
    // console.log(chart_pipeline);
    // console.log(chart_inprocess);
    // console.log(chart_closed);
    // console.log(chart_sale);
    if (chart_active == 0 && chart_pipeline == 0 && chart_inprocess == 0 && chart_closed == 0 && chart_sale == 0) {
        $('.loadingio-spinner-eclipse-awp1yaa5q94').css({ 'display': 'none' });
        Highcharts.chart('funnelcontainer', {
            title: {
                text: '<b>No Data</b>'
            },
        });
    }
    else {
        $('.loadingio-spinner-eclipse-awp1yaa5q94').css({ 'display': 'none' });
        Highcharts.chart('funnelcontainer', {
            chart: {
                type: 'funnel'
            },
            title: {
                text: '<b>Leads By Status</b>'
            },
            plotOptions: {
                series: {
                    dataLabels: {
                        enabled: true,
                        format: '<b>{point.name}</b> ({point.y:,.0f})',
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black',
                        softConnector: true
                    },
                    center: ['40%', '50%'],
                    neckWidth: '20%',
                    neckHeight: '25%',
                    width: '70%'
                }
            },
            legend: {
                enabled: false
            },
            series: [{
                name: 'Leads',
                data: [
                    ['Active', chart_active],
                    ['Pipeline', chart_pipeline],
                    ['Inprocess', chart_inprocess],
                    ['Closed', chart_closed],
                    ['Sale', chart_sale]
                ]
            }],
            exporting: { enabled: false },
            credits: { enabled: false }
        }
        );
    }

}

function getMnthWiseleadData() {
    var user_id = $('#user').val();
    var category_id = $('#category_id').val();
    var from_date = $('.from_date').val();
    var to_date = $('.to_date').val();
    console.log(from_date);
    console.log(to_date);
    $.ajax({
        url: 'get_mnth_wise_lead_data',
        type: "GET",
        data: {
            from_date: from_date,
            to_date: to_date,
            user_id: user_id,
            category_id: category_id,
        },
        success: function (result) {
            console.log(result);
            if (result.length != 0) {
                result = JSON.parse(result);
                console.log(result);
                // return false;
                if (result['status'] === true) {
                    response = result.get_mnth_wise_lead_data;
                    // let months = result.monthNamesArray;
                    // let no_of_months = result.no_of_months;
                    let dates_array = result.dates_array;
                    // console.log(response);
                    // return false;
                    // if (response.active == null && response.pipeline == null && response.inprocess == null && response.closed == null && response.sale == null) {
                    //     getLeadFilterStatus(0, 0, 0, 0, 0);
                    //     getLeadLineChart();
                    // }
                    // else {
                    // getLeadFilterStatus(parseInt(response.active), parseInt(response.pipeline), parseInt(response.inprocess), parseInt(response.closed), parseInt(response.sale))
                    getLeadLineChart(response, dates_array);
                    // getReportLineChart(response);
                    // }
                }
                else {
                    // getLeadFilterStatus(0, 0, 0, 0, 0);
                    getLeadLineChart(0);
                    // getReportLineChart(0, 0, 0, 0);
                }
            }
            else {
                getLeadLineChart(0);
            }
        }
    });
}

function getLeadLineChart(response, dates_array) {
    if (response === 0) {
        $('.loadingio-spinner-eclipse-awp1yaa5q94').css({ 'display': 'none' });
        Highcharts.chart('lineChart', {
            title: {
                text: '<b>No Data</b>'
            },
        });
    }
    else {
        $('.loadingio-spinner-eclipse-awp1yaa5q94').css({ 'display': 'none' });
        console.log(response);
        console.log(dates_array);
        // return false;
        // console.log(no_of_months);
        // var countsArray = [];
        // console.log(response[0].month_year);
        // let month1 = response[0].count;
        // let month2 = response[1].count;
        // let month3 = response[2].count;
        // var last3Months = getLastMonths(no_of_months);
        // last3Months = last3Months.reverse();
        // console.log(response);
        // console.log(last3Months);
        // $.each(response, function (i) {
        //     // $.each(response[i], function (key, val) {
        //     $.each(last3Months, function (mkey, mval) {
        //         console.log(response[i].month_year);
        //         console.log(mval);
        //         if (response[i].month_year == mval) {
        //             countsArray.push(response[i].count);
        //             // return false;
        //             return;
        //         }
        //         // else {
        //         //     countsArray.push(0);
        //         // }
        //     })
        //     // });
        // });
        // countsArray = countsArray.reverse();

        Highcharts.chart('lineChart', {
            chart: {
                type: 'spline'
            },
            title: {
                text: 'Leads Month Wise Data'
            },
            // subtitle: {
            //     text: 'Source: WorldClimate.com'
            // },
            xAxis: {
                categories: dates_array
            },
            yAxis: {
                title: {
                    text: 'Count'
                },
                labels: {
                    formatter: function () {
                        return this.value;
                    }
                }
            },
            tooltip: {
                crosshairs: true,
                shared: true
            },
            plotOptions: {
                spline: {
                    marker: {
                        radius: 4,
                        lineColor: '#666666',
                        lineWidth: 1
                    }
                }
            },
            series: [{
                name: 'Leads',
                marker: {
                    symbol: 'circle'
                },
                data: response
            }],
            credits: { enabled: false }
        });
    }
}

// For Reports
function getMnthWiseReportData() {
    var user_id = $('#user').val();
    var category_id = $('#category_id').val();
    var from_date = $('.from_date').val();
    var to_date = $('.to_date').val();
    console.log(from_date);
    console.log(to_date);
    $.ajax({
        url: 'get_mnth_wise_report_data',
        type: "GET",
        data: {
            from_date: from_date,
            to_date: to_date,
            user_id: user_id,
            category_id: category_id,
        },
        success: function (result) {
            console.log(result);
            // return false;
            result = JSON.parse(result);
            console.log(result);
            // return false;
            if (result['status'] === true) {
                response = result.get_mnth_wise_report_data;
                // let months = result.monthNamesArray;
                // let no_of_months = result.no_of_months;
                let dates_array = result.dates_array;
                // let no_of_months = result.no_of_months;
                // console.log(response);
                // return false;
                // if (response.active == null && response.pipeline == null && response.inprocess == null && response.closed == null && response.sale == null) {
                //     getLeadFilterStatus(0, 0, 0, 0, 0);
                //     getLeadLineChart();
                // }
                // else {
                // getLeadFilterStatus(parseInt(response.active), parseInt(response.pipeline), parseInt(response.inprocess), parseInt(response.closed), parseInt(response.sale))
                getReportLineChart(response, dates_array);
                // }
            }
            else {
                getReportLineChart(0);
            }
        }
    });
}

function getReportLineChart(response, dates_array) {
    if (response === 0) {
        $('.loadingio-spinner-eclipse-awp1yaa5q94').css({ 'display': 'none' });
        Highcharts.chart('reportLineChart', {
            title: {
                text: '<b>No Data</b>'
            },
        });
    }
    else {
        $('.loadingio-spinner-eclipse-awp1yaa5q94').css({ 'display': 'none' });
        console.log(response);
        console.log(dates_array);
        Highcharts.chart('reportLineChart', {
            chart: {
                type: 'spline'
            },
            title: {
                text: 'Reports Month Wise Data'
            },
            // subtitle: {
            //     text: 'Source: WorldClimate.com'
            // },
            xAxis: {
                categories: dates_array,
                crosshair: true
            },
            yAxis: {
                title: {
                    text: 'Count'
                },
                labels: {
                    formatter: function () {
                        return this.value;
                    }
                }
            },
            tooltip: {
                crosshairs: true,
                shared: true
            },
            plotOptions: {
                spline: {
                    marker: {
                        radius: 4,
                        lineColor: '#666666',
                        lineWidth: 1
                    }
                }
            },
            series: [{
                name: 'Reports',
                marker: {
                    symbol: 'circle'
                },
                data: response
            }],
            credits: { enabled: false }
        });
    }
}

// For Orders
function getMnthWiseOrdersData() {
    var user_id = $('#user').val();
    var category_id = $('#category_id').val();
    var from_date = $('.from_date').val();
    var to_date = $('.to_date').val();
    console.log(from_date);
    console.log(to_date);
    $.ajax({
        url: 'get_mnth_wise_orders_data',
        type: "GET",
        data: {
            from_date: from_date,
            to_date: to_date,
            user_id: user_id,
            category_id: category_id,
        },
        success: function (result) {
            console.log(result);
            // return false;
            result = JSON.parse(result);
            console.log(result);
            // return false;
            if (result['status'] === true) {
                response = result.get_mnth_wise_orders_data;
                let dates_array = result.dates_array;
                // console.log(response);
                // return false;
                // if (response.active == null && response.pipeline == null && response.inprocess == null && response.closed == null && response.sale == null) {
                //     getLeadFilterStatus(0, 0, 0, 0, 0);
                //     getLeadLineChart();
                // }
                // else {
                // getLeadFilterStatus(parseInt(response.active), parseInt(response.pipeline), parseInt(response.inprocess), parseInt(response.closed), parseInt(response.sale))
                getorderLineChart(response, dates_array);
                // }
            }
            else {
                getorderLineChart(0);
            }
        }
    });
}

function getorderLineChart(response, dates_array) {
    if (response === 0) {
        $('.loadingio-spinner-eclipse-awp1yaa5q94').css({ 'display': 'none' });
        Highcharts.chart('ordersLineChart', {
            title: {
                text: '<b>No Data</b>'
            },
        });
    }
    else {
        $('.loadingio-spinner-eclipse-awp1yaa5q94').css({ 'display': 'none' });
        console.log(response);
        console.log(dates_array);
        Highcharts.chart('ordersLineChart', {
            chart: {
                type: 'spline'
            },
            title: {
                text: 'Orders Month Wise Data'
            },
            // subtitle: {
            //     text: 'Source: WorldClimate.com'
            // },
            xAxis: {
                categories: dates_array
            },
            yAxis: {
                title: {
                    text: 'Count'
                },
                labels: {
                    formatter: function () {
                        return this.value;
                    }
                }
            },
            tooltip: {
                crosshairs: true,
                shared: true
            },
            plotOptions: {
                spline: {
                    marker: {
                        radius: 4,
                        lineColor: '#666666',
                        lineWidth: 1
                    }
                }
            },
            series: [{
                name: 'Orders',
                marker: {
                    symbol: 'circle'
                },
                data: response
            }],
            credits: { enabled: false }
        });
    }
}

// For Articles
function getMnthWiseArticlesData() {
    var user_id = $('#user').val();
    var category_id = $('#category_id').val();
    var from_date = $('.from_date').val();
    var to_date = $('.to_date').val();
    console.log(from_date);
    console.log(to_date);
    $.ajax({
        url: 'get_mnth_wise_articles_data',
        type: "GET",
        data: {
            from_date: from_date,
            to_date: to_date,
            user_id: user_id,
            category_id: category_id,
        },
        success: function (result) {
            console.log(result);
            // return false;
            result = JSON.parse(result);
            console.log(result);
            return false;
            if (result['status'] === true) {
                response = result.get_mnth_wise_articles_data;
                let dates_array = result.dates_array;
                // console.log(response);
                // return false;
                // if (response.active == null && response.pipeline == null && response.inprocess == null && response.closed == null && response.sale == null) {
                //     getLeadFilterStatus(0, 0, 0, 0, 0);
                //     getLeadLineChart();
                // }
                // else {
                // getLeadFilterStatus(parseInt(response.active), parseInt(response.pipeline), parseInt(response.inprocess), parseInt(response.closed), parseInt(response.sale))
                getArticleLineChart(response, dates_array);
                // }
            }
            else {
                getArticleLineChart(0);
            }
        }
    });
}

function getArticleLineChart(response, dates_array) {
    if (response === 0) {
        $('.loadingio-spinner-eclipse-awp1yaa5q94').css({ 'display': 'none' });
        Highcharts.chart('articlesLineChart', {
            title: {
                text: '<b>No Data</b>'
            },
        });
    }
    else {
        $('.loadingio-spinner-eclipse-awp1yaa5q94').css({ 'display': 'none' });
        console.log(response);
        console.log(dates_array);

        Highcharts.chart('articlesLineChart', {
            chart: {
                type: 'spline'
            },
            title: {
                text: 'Articles Month Wise Data'
            },
            // subtitle: {
            //     text: 'Source: WorldClimate.com'
            // },
            xAxis: {
                categories: dates_array
            },
            yAxis: {
                title: {
                    text: 'Count'
                },
                labels: {
                    formatter: function () {
                        return this.value;
                    }
                }
            },
            tooltip: {
                crosshairs: true,
                shared: true
            },
            plotOptions: {
                spline: {
                    marker: {
                        radius: 4,
                        lineColor: '#666666',
                        lineWidth: 1
                    }
                }
            },
            series: [{
                name: 'Articles',
                marker: {
                    symbol: 'circle'
                },
                data: response
            }],
            credits: { enabled: false }
        });
    }
}