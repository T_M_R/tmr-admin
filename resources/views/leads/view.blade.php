@extends('layouts.header')
@section('body-content')
<section class="regions-list">
    <div class="block-header">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 my-auto">
                <h4 class="page-title m-0">View Lead Details</h4>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <a href="{{URL:: to('/leads')}}">
                    <button type="button" class="btn btn-info float-right">Back</button>
                </a>
            </div>
        </div>
    </div>
    <div class="row clearfix mt-3">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="card project_widget">
                <div class="body mb-4">
                    @if(isset($getLeadDetails) && sizeof($getLeadDetails)>0)
                        @foreach($getLeadDetails as $ldetails)
                        <div class="row">
                            <div class="col-md-12 border-bottom">
                                <h5>{{$ldetails->rep_name}}</h5>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4 pb-4 mb-2">
                                <label class="" style="color:#000;"><b>Name :</b> <span>{{$ldetails->name}}</span></label>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4 pb-4">
                                <label class="" style="color:#000;"><b>Mobile :</b> <span>{{$ldetails->phoneNo}}</span></label>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4 pb-4">
                                <label class="" style="color:#000;"><b>Email :</b> <span>{{$ldetails->emailId}}</span></label>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4 pb-4">
                                <label class="" style="color:#000;"><b>Assigned To :</b> <span>{{$ldetails->username}}</span></label>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4 pb-4">
                                <label class="" style="color:#000;"><b>Status :</b> 
                                    <span>
                                        @if($ldetails->leadStatus == 'N' )    
                                            {{$ldetails->leadStatus}}
                                        @else
                                            {{$ldetails->leadStatus}}
                                        @endif
                                    </span>
                                </label>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4 pb-4">
                                <label class="" style="color:#000;"><b>Country :</b> <span>{{$ldetails->country}}</span></label>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4 pb-4">
                                <label class="" style="color:#000;"><b>Company :</b> <span>{{$ldetails->company}}</span></label>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4 pb-4">
                                <label class="" style="color:#000;"><b>Catrgory :</b> <span>{{$ldetails->cat_name}}</span></label>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4 pb-4">
                                <label class="" style="color:#000;"><b>Rep Type :</b> 
                                    <span>
                                        @if($ldetails->rep_type == 'N' )    
                                            Publish
                                        @else
                                            Upcomoing
                                        @endif
                                    </span>
                                </label>
                            </div>
                        </div>
                        @endforeach
                    @else
                        <div class="row">
                            <div class="col-md-12">
                                <label class="" style="color:#414244;"><b>Comments</b><span class="text-danger">*</span></label>
                                <div class="form-group mt-5"> 
                                    <div class="form-line">  
                                        <textarea class="form-control lead_comments p-3" name="lead_comments" rows="4" placeholder="Enter Your Comment Here"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="row clearfix mt-3">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="card project_widget">
                <div class="body">
                    <div class="row border-bottom">
                        <div class="col-md-12 mb-0">
                            <label class="" style="color:#414244;"><b>Comments</b><span class="text-danger">*</span></label>
                            <div class="form-group mt-4"> 
                                <div class="form-line">  
                                    <textarea class="form-control lead_comments p-3" name="lead_comments" rows="4" placeholder="Enter Your Comment Here"></textarea>
                                </div>
                            </div>
                        </div>
                        <input type="hidden" class="lead_id" value="{{$getLeadDetails[0]->contactId}}">
                        <div class="leads_error_msg col-red center w-100 mb-3" style="display:none"></div>
                        <div class="leads_success_msg col-green center w-100 mb-3" style="display:none"></div>
                        <div class="col-md-12 center">
                            <button type="submit" class="btn btn-primary center waves-effect save_lead_comment">SAVE COMMENT</button>  
                            <button type="button" class="btn btn-primary center waves-effect" data-toggle="modal" data-target="#add_lead_remainder">Add Reminder</button>                                                              
                        </div>
                    </div>
                    <div class="row mt-4 ml-1 mr-1">
                        @if(isset($getLeadComments) && sizeof($getLeadComments)>0)
                            @foreach($getLeadComments as $comment)
                            <div class="col-md-12">
                                <div class="row border" style="border-radius:5px;">
                                    <div class="col-md-12">
                                        <div class="row comments-data-bg">
                                            <div class="col-md-10 col-lg-10 mx-auto my-auto p-2">
                                                <div class="preview">
                                                    @if($comment->comment_type == 1)
                                                        <div class="icon-preview pr-3" style="display:inline-block;"><i class="fas fa-comments" style="font-size:18px"></i></div>
                                                    @elseif($comment->comment_type == 2)
                                                        <div class="icon-preview pr-3" style="display:inline-block;"><i class="fas fa-exchange-alt" style="font-size:18px"></i></div>
                                                    @elseif($comment->comment_type == 3)
                                                        <div class="icon-preview pr-3" style="display:inline-block;"><i class="fas fa-users-cog" style="font-size:18px"></i></div>
                                                    @else
                                                        <div class="icon-preview pr-3" style="display:inline-block;"><i class="fas fa-user-clock" style="font-size:18px"></i></div>
                                                    @endif
                                                    
                                                    <span class="my-auto"><b>{{$comment->username}}</b>  </span>
                                                </div>
                                            </div>
                                            <div class="col-md-9 col-lg-9 mx-auto my-auto p-2 d-none">
                                                <b>{{$comment->username}}</b>  
                                            </div>
                                            <div class="col-md-2 col-lg-2 mx-auto my-auto p-2">
                                                <?php $created_at = date("d-m-Y h:m", strtotime($comment->created_at));?>
                                                <b>{{$created_at}}</b>  
                                            </div>
                                        </div>
                                        <div clas="row">
                                            <div class="col-md-12 my-auto pt-3 pl-0 pr-0">
                                                <p class="m-0">{{$comment->comment}}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- For remainder -->
<div class="modal fade add_lead_remainder" id="add_lead_remainder" tabindex="-1" role="dialog" aria-labelledby="formModal" aria-hidden="true">    
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title modal-title" id="formModal">Add Remainder</h5>
                <button type="button" class="close modal-close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12 p-0">
                        <div class="input-group">  
                            <input type="text" class="datetimepicker remainder_date" autocomplete="off" value="" required/>
                            <span style="position: absolute;top: 8px;right: 0;bottom: 0;"><i class="icon-calendar" style="font-size: 26px;"></i></span>
                            <label>Publish Date <span class="text-danger">*</span></label>
                        </div>
                        <div class="form-group">
                            <div class="input-group">  
                                <input type="text" class="remainder_title" autocomplete="off" value="" required/>
                                <label>Title <span class="text-danger">*</span></label>
                            </div>
                        </div>
                        <div class="form-group mt-4"> 
                            <div class="form-line">  
                                <textarea class="form-control remainder_comments p-3" name="remainder_comments" rows="4" placeholder="Enter Your Message Here"></textarea>
                            </div>
                        </div>
                        <input type="hidden" class="lead_id" value="{{$getLeadDetails[0]->contactId}}">
                        <p class="remainder_success_msg mb-0 text-success text-center"></p>
                        <p class="remainder_error_msg mb-0 text-danger text-center"></p>
                    </div>
                </div>
            </div>
            <div class="modal-footer center">
                <button type="button" class="btn btn-danger modal-close center" data-dismiss="modal">CLOSE</button>
                <button type="button" class="btn btn-primary center save_remainder">SUBMIT</button>
            </div>
        </div>
    </div>
</div>
@endsection