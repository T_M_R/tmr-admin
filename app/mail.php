<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Session;
use Config;
use Carbon\Carbon;

class mail extends Model
{
    public function mail_config($search_key){
        $mail_config = DB::table('emails')->select('email_id','email_cat','email_to','email_status')->orderBy('email_id','DESC')->get();

        if(sizeof($mail_config)>0){
            return $mail_config;
        }
        else{
            return [];
        }
    }

    public function save_mail_config($data){
        $date = Carbon::now();
        
        $save_mail_config = DB::table('emails')->insert([
            'email_cat' => $data['mail_category_name'],
            'email_to' => $data['mail_to'],
            'email_cc' => $data['mail_cc'],
            'email_bcc' => $data['mail_bcc'],
            'email_from' => $data['mail_from'],
            'email_subject' => $data['mail_subject'],
            'email_type' => 'text/plain',
            'email_body' => $data['mail_body'],
            'email_status' => 'Y'
        ]);
        
        if($save_mail_config == 1){
            return 1;
        }
        else{
            return 0;
        }
    }

    public function edit_email_config($id){
        $edit_email_config = DB::table('emails')
                            ->where('email_id','=',$id)
                            ->get();

        if(sizeof($edit_email_config)>0){
            return $edit_email_config;
        }
        else{
            return [];
        }
    }

    public function update_mail_config($data){
        $date = Carbon::now();

        $update_mail_config = DB::table('emails')->where('email_id',$data['email_id'])->update([
            'email_cat' => $data['mail_category_name'],
            'email_to' => $data['mail_to'],
            'email_cc' => $data['mail_cc'],
            'email_bcc' => $data['mail_bcc'],
            'email_from' => $data['mail_from'],
            'email_subject' => $data['mail_subject'],
            'email_type' => 'text/plain',
            'email_body' => $data['mail_body'],
            'email_status' => $data['email_status']
        ]);
        
        if($update_mail_config == 1){
            return 1;
        }
        else{
            return 0;
        }
    }

    public function update_email_status($data){
        $date = Carbon::now();
        $status='';
        if($data['hidden_status'] == 1){
            $status='Y';
        }
        else{
            $status='N';
        }
        $update_email_status = DB::table('emails')->where('email_id',$data['email_id'])->update([
            'email_status' => $status,
        ]);
        
        if($update_email_status == 1){
            return 1;
        }
        else{
            return 0;
        }
    }

    public function getMailUsers()
    {
        $getMailUsers = DB::table('emails')
                    // ->where('users.id','=',9)
                    // ->where('users.id','=',159)
                    // ->where('users.unit_id','=','UN0')
                    // ->where('users.id','=',174)
                    ->where('email_to','!=','')
                    ->where('email_status','=','Y')
                    ->get();
        return $getMailUsers;
    }
}
