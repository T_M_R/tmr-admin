$('.save_custom_report').on('click', function () {
    var custom_report_name = $('.custom_report_name').val();
    var custom_report_url = $('.custom_report_url').val();
    var custom_report_sul = $('.custom_report_sul').val();
    var custom_report_el = $('.custom_report_el').val();
    var custom_report_cul = $('.custom_report_cul').val();
    var custom_report_category_id = $('.custom_report_category_id option:selected').val();
    var custom_report_publish_date = $('.custom_report_publish_date').val();
    console.log(custom_report_name);
    console.log(custom_report_url);
    console.log(custom_report_sul);
    console.log(custom_report_el);
    console.log(custom_report_cul);
    if (custom_report_name != '' && custom_report_name != null && custom_report_name != undefined &&
        custom_report_url != '' && custom_report_url != null && custom_report_url != undefined &&
        custom_report_sul != '' && custom_report_sul != null && custom_report_sul != undefined &&
        custom_report_el != '' && custom_report_el != null && custom_report_el != undefined &&
        custom_report_cul != '' && custom_report_cul != null && custom_report_cul != undefined &&
        custom_report_category_id != '' && custom_report_category_id != null && custom_report_category_id != undefined &&
        custom_report_publish_date != '' && custom_report_publish_date != null && custom_report_publish_date != undefined) {
        $.ajax({
            url: '/save_custom_report',
            type: "POST",
            data: {
                custom_report_name: custom_report_name,
                custom_report_url: custom_report_url,
                custom_report_sul: custom_report_sul,
                custom_report_el: custom_report_el,
                custom_report_cul: custom_report_cul,
                custom_report_category_id: custom_report_category_id,
                custom_report_publish_date: custom_report_publish_date
            },
            success: function (result) {
                console.log(result);
                // return false
                if (result == 1) {
                    $('.custom_report_success_msg').css({ 'display': 'block' });
                    $('.custom_report_success_msg').text('Report Saved Successfully.');
                    setTimeout(() => {
                        $('.custom_report_success_msg').css({ 'display': 'none' });
                        window.location.href = document.location.origin + '/custom_reports';
                    }, 1500);
                }
                else {
                    $('.custom_report_error_msg').css({ 'display': 'block' });
                    $('.custom_report_error_msg').text('Something Went Wrong.Pls try again later..!!');
                    setTimeout(() => {
                        $('.custom_report_error_msg').css({ 'display': 'none' });
                    }, 3000);
                    return false;
                }
            }
        });
    }
    else {
        $('.custom_report_error_msg').css({ 'display': 'block' });
        $('.custom_report_error_msg').text('All Fields Are Required.');
        setTimeout(() => {
            $('.custom_report_error_msg').css({ 'display': 'none' });
            // location.reload();
        }, 3000);
        return false;
    }

})

$('.update_custom_report').on('click', function () {
    var custom_report_id = $('.custom_report_id').val();
    var custom_report_name = $('.custom_report_name').val();
    var custom_report_url = $('.custom_report_url').val();
    var custom_report_sul = $('.custom_report_sul').val();
    var custom_report_el = $('.custom_report_el').val();
    var custom_report_cul = $('.custom_report_cul').val();
    var custom_report_category_id = $('.custom_report_category_id option:selected').val();
    var custom_report_publish_date = $('.custom_report_publish_date').val();
    console.log(custom_report_id);
    console.log(custom_report_name);
    console.log(custom_report_url);
    console.log(custom_report_sul);
    console.log(custom_report_el);
    console.log(custom_report_cul);
    console.log(custom_report_category_id);
    if (custom_report_id != '' && custom_report_id != null && custom_report_id != undefined &&
        custom_report_name != '' && custom_report_name != null && custom_report_name != undefined &&
        custom_report_url != '' && custom_report_url != null && custom_report_url != undefined &&
        custom_report_sul != '' && custom_report_sul != null && custom_report_sul != undefined &&
        custom_report_el != '' && custom_report_el != null && custom_report_el != undefined &&
        custom_report_cul != '' && custom_report_cul != null && custom_report_cul != undefined &&
        custom_report_publish_date != '' && custom_report_publish_date != null && custom_report_publish_date != undefined &&
        custom_report_category_id != '' && custom_report_category_id != null && custom_report_category_id != undefined) {
        $.ajax({
            url: '/update_custom_report',
            type: "POST",
            data: {
                custom_report_id: custom_report_id,
                custom_report_name: custom_report_name,
                custom_report_url: custom_report_url,
                custom_report_sul: custom_report_sul,
                custom_report_el: custom_report_el,
                custom_report_cul: custom_report_cul,
                custom_report_publish_date: custom_report_publish_date,
                custom_report_category_id: custom_report_category_id
            },
            success: function (result) {
                console.log(result);
                // return false
                if (result == 1) {
                    $('.custom_report_success_msg').css({ 'display': 'block' });
                    $('.custom_report_success_msg').text('Report Updated Successfully.');
                    setTimeout(() => {
                        $('.custom_report_success_msg').css({ 'display': 'none' });
                        window.location.href = document.location.origin + '/custom_reports';
                    }, 1500);
                }
                else {
                    $('.custom_report_error_msg').css({ 'display': 'block' });
                    $('.custom_report_error_msg').text('Something Went Wrong.Pls try again later..!!');
                    setTimeout(() => {
                        $('.custom_report_error_msg').css({ 'display': 'none' });
                    }, 3000);
                    return false;
                }
            }
        });
    }
    else {
        $('.custom_report_error_msg').css({ 'display': 'block' });
        $('.custom_report_error_msg').text('All Fields Are Required.');
        setTimeout(() => {
            $('.custom_report_error_msg').css({ 'display': 'none' });
            // location.reload();
        }, 3000);
        return false;
    }

})

$('.custom_report_toggle_event').change(function (e) {
    // $('.console-event').html('Toggle: ' $(this).prop('checked'))
    var status = $(this).prop('checked');
    console.log(status)
    if (status == true) {
        var hidden_status = $(this).parent().parent().find('.hidden_status').val(1);
    }
    else {
        var hidden_status = $(this).parent().parent().find('.hidden_status').val(0);
    }

    var custom_report_id = $(this).parent().parent().find('.custom_report_id').val();
    var hidden_status = $(this).parent().parent().find('.hidden_status').val();
    console.log(custom_report_id);
    console.log(hidden_status);
    if (custom_report_id != '' && custom_report_id != undefined && custom_report_id != null) {
        $.ajax({
            url: '/update_custom_report_status',
            type: "POST",
            data: {
                custom_report_id: custom_report_id,
                hidden_status: hidden_status,
            },
            success: function (result) {
                console.log(result);
                // return false
                if (result == 1) {
                    showSuccessMessage();
                }
                else {
                    showCancelMessage();
                }
            }
        });
    } else {
        $('.status_change_msg').css({ 'display': 'block' });
        $('.status_change_msg').text('Record id not found');
        setTimeout(() => {
            $('.status_change_msg').css({ 'display': 'none' });
            // location.reload();
        }, 3000);
        return false;
    }
})

function showSuccessMessage() {
    swal("Status Updated Successfully!", "", "success");
}

function showCancelMessage() {
    swal("Oops", "Something went wrong!", "error")
}