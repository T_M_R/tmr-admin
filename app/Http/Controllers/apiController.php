<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\master;
use App\reports;
use App\api;
use Carbon\Carbon;
use Redirect;
use Session;
use DB;
use Config;

class apiController extends Controller
{
    public function __construct(){
        $this->master = new master();
        $this->api = new api();
        $this->reports = new reports();
    }

    public function getReports(Request $request,$date){
        $data = $request->all();
       
        $publish_status = 'N';
        // $pub_date = '';
        // if(isset($_GET['publish_status'])){
        //     $publish_status=$_GET['publish_status'];
        // }

        // if(isset($_GET['pub_date'])){
        //     $pub_date=$_GET['pub_date'];
        // }
        // print_r($date);
        // exit;
        $getReports = $this->api->getReports($publish_status,$date);
       
        return response()->json($getReports);
        // return view('reports.list')->with('getReports',$getReports)->with('search_key',$search_key);
    } 
}
