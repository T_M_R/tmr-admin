<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Session;
use Config;
use Carbon\Carbon;

class news extends Model
{
    public function getNewsData($search_key){
        $query = DB::table('gnews')
                ->leftjoin('category','category.category_id','=','gnews.category_id')
                ->select('gnews.id','gnews.gnews_title','gnews.gnews_url','gnews.category_id','gnews.status','gnews.news_by','gnews.publish_date','gnews.status','gnews.status','category.cat_name')
                ->orderBy('gnews.id','DESC');

        if($search_key != ''){
            $query->where('gnews.gnews_title','like','%'.$search_key.'%');
        }

        $getNewsData = $query->paginate(30);
        if(sizeof($getNewsData)>0){
            return $getNewsData;
        }
        else{
            return [];
        }
    }

    public function save_news($data,$img_name){
        $date = Carbon::now();

        $save_news = DB::table('gnews')->insert([
            'gnews_title' => $data['news_name'],
            'gnews_url' => $data['news_url'],
            'news_by' => $data['news_by'],
            'publish_date' => $data['news_publish_date'],
            'short_description' => $data['news_short_desc'],
            'gnews_image' => $img_name,
            'category_id' => $data['news_category_id'],
            'meta_title' => $data['meta_title'],
            'meta_keyword' => $data['meta_keywords'],
            'meta_description' => $data['meta_desc'],
            'status' => $data['news_status'],
            'created_at' => $date
        ]);
        
        if($save_news == 1){
            return 1;
        }
        else{
            return 0;
        }
    }

     public function getNewsDataById($id){
        $getNewsDataById = DB::table('gnews')
                            ->where('id','=',$id)
                            ->get();
        if(sizeof($getNewsDataById)>0){
            return $getNewsDataById;
        }
        else{
            return [];
        }
    }
    
    public function update_news($data,$img_name){
        $date = Carbon::now();

        $update_news = DB::table('gnews')->where('id',$data['news_id'])->update([
            'gnews_title' => $data['news_name'],
            'gnews_url' => $data['news_url'],
            'news_by' => $data['news_by'],
            'publish_date' => $data['news_publish_date'],
            'short_description' => $data['news_short_desc'],
            'gnews_image' => $img_name,
            'category_id' => $data['news_category_id'],
            'meta_title' => $data['meta_title'],
            'meta_keyword' => $data['meta_keywords'],
            'meta_description' => $data['meta_desc'],
            'status' => $data['news_status'],
            'updated_at' => $date
        ]);
        
        if($update_news == 1){
            return 1;
        }
        else{
            return 0;
        }
    }

    public function update_news_status($data){
        $date = Carbon::now();
        $status='';
        if($data['hidden_status'] == 1){
            $status='Y';
        }
        else{
            $status='N';
        }
        $update_news_status = DB::table('gnews')->where('id',$data['news_id'])->update([
            'status' => $status,
            'updated_at' => $date
        ]);
        
        if($update_news_status == 1){
            return 1;
        }
        else{
            return 0;
        }
    }
}
