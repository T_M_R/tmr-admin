$('.region_id, .country_id').prop('disabled',true);
$('.region_id, .country_id').addClass('input-disabled');
$('.category_id').on('change',function(){
    var category_id = $('.category_id option:selected').val();
    console.log(category_id);
    if(category_id != '' && category_id != null && category_id != undefined){
        $.ajax({
            url : '/get_regions_by_category',
            method : 'GET',
            data : {
                category_id : category_id
            },
            success : function(result){
                console.log(result);
                if(result == 1){
                    $('.region_id').prop('disabled',false);
                    $('.region_id').removeClass('input-disabled');
                }
                else{
                    $('.category_error_msg').css({'display':'block'});
                    $('.category_error_msg').text('Something Went Wrong.Pls try again.');
                    // setTimeout(() => {
                    //     $('.category_error_msg').css({'display':'none'});
                    // }, 3000);
                    return false;
                }
            }
        });
    }
    else{
        $('.region_id').prop('disabled',true);
        $('.region_id').addClass('input-disabled');
        $('.category_error_msg').css({'display':'block'});
        $('.category_error_msg').text('Please select category');
        setTimeout(() => {
            $('.category_error_msg').css({'display':'none'});
        }, 3000);
        return false;
    }
})

$('.region_id').on('change',function(){
    var category_id = $('.category_id option:selected').val();
    var region_id = $('.region_id option:selected').val();
    var userid = $('.userid').val();
    console.log(region_id);
    if(region_id != '' && region_id != null && region_id != undefined){
        $.ajax({
            url : '/get_countries_by_region',
            method : 'GET',
            data : {
                userid : userid,
                category_id : category_id,
                region_id : region_id
            },
            success : function(result){
                console.log(result);
                if(result != '' ){
                    $('#country_id').find('option').remove();
                    var result_array = [];
                    // console.log('len-',result.length);
                    if(result != 0){
                        result_array.push('<option value="0">All Below Countries</option>');
                        for (i = 0; i < result.length; i++) {
                            result_array.push('<option value="' + result[i].id + '">' + result[i].name + '</option>');
                        }
                        $("#country_id").append(result_array);
                        $('.country_id').prop('disabled',false);
                        $('.country_id').removeClass('input-disabled');
                    }
                    else{
                        $('.country_error_msg').css({'display':'block'});
                        $('.country_error_msg').text('Un-Allocated country data not available based on your category and region selection.');
                        setTimeout(() => {
                            $('.country_error_msg').css({'display':'none'});
                        }, 3000);
                        return false;
                    }
                }
                else{
                    $('.region_error_msg').css({'display':'block'});
                    $('.region_error_msg').text('Something Went Wrong.Pls try again.');
                    setTimeout(() => {
                        $('.region_error_msg').css({'display':'none'});
                    }, 3000);
                    return false;
                }
            }
        });
    }
    else{
        $('.country_id').prop('disabled',true);
        $('.country_id').addClass('input-disabled');
        $('.region_error_msg').css({'display':'block'});
        $('.region_error_msg').text('Please select region');
        setTimeout(() => {
            $('.region_error_msg').css({'display':'none'});
        }, 3000);
        return false;
    }
})

$('.add_allocation').click(function(){
    $('.loadingio-spinner-blocks').css({'display':'block'});
    var cat_id = $('#category_id option:selected').val();
    var cat_name = $('#category_id option:selected').text();
    var region_id = $('#region_id option:selected').val();
    var region_name = $('#region_id option:selected').text();
    var country_id = $('#country_id').val();
    var country_name = $('#country_id option:selected').text();
    // var rowCount = $('.table-lead-allocations tr').length+1;
    var userid = $('#userid').val();
    console.log(userid);
    if($.isNumeric(cat_id) && $.isNumeric(region_id) && cat_id != '' && cat_id != undefined && cat_id != null && region_id != '' && region_id != undefined && region_id != null && country_id != '' && country_id != undefined && country_id != null ){
        $.ajax({
            url: '/updateLeadAllocations',
            type: 'POST',
            data: { 
                userid : userid, 
                cat_id : cat_id, 
                region_id : region_id, 
                country_id : country_id 
            },
            success: function (response) {
                console.log(response)
                // return false;
                $('.loadingio-spinner-blocks').css({'display':'none'});
                if(response == 1){
                    console.log(response);
                    $('.message').html('<p class="text-success mb-0" style="font-size:18px;">Lead allocations added successfully</p>');
                    $('#exampleModal').modal('show');
                }
                else{
                    $('.message').html('<p class="text-danger mb-0" style="font-size:18px;">Something went wrong, Please try again.</p>');
                    $('#exampleModal').modal('show');
                }
            }
        })
    }
    else{
        alert('Please select category, region and country');
    }
})

$('.close-la-modal').click(function(){
    location.reload();
})

$(document).ready(function() {
    $(".js-example-basic-multiple").select2({
        maximumSelectionLength: 7,
        placeholder: 'Select Countries',
        allowClear: true,
        dropdownCssClass : 'bigdrop'
    });
});

$(".js-example-basic-multiple").on("change", function () { 
    country_id = $('#country_id').val();
    if(jQuery.inArray("0", $('#country_id').val()) === 0){
        $('.js-example-basic-multiple').val(null).trigger('change');
        $('.js-example-basic-multiple').val('0');
        // $(".js-example-basic-multiple").prop("disabled", true);
    }
});

$('.btn_lead_allocation').click(function(){
    $('.loadingio-spinner-blocks').css({'display':'block'});
    var country_id = $(this).parent().parent().find('.allocated_country_id').text();
    var category_id = $(this).parent().parent().find('.allocated_category_id').text();
    var region_id = $(this).parent().parent().find('.allocated_region_id').text();
    var user_id = $(this).parent().parent().find('.allocated_user_id').text();
    console.log(country_id);
    console.log(category_id);
    console.log(region_id);
    console.log(user_id);
    // return;
    $.ajax({
        url: '/deleteLeadAllocations',
        type: 'GET',
        data: { 
            country_id : country_id,
            category_id : category_id,
            region_id : region_id,
            user_id : user_id, 
        },
        success: function (response) {
            console.log(response);
            // return false;
            $('.loadingio-spinner-blocks').css({'display':'none'});
            if(response == 1){
                $('.message').html('<p class="text-success mb-0" style="font-size:18px;">Lead allocation deleted successfully</p>');
                $('#exampleModal').modal('show');
            }
            else{
                $('.message').html('<p class="text-danger mb-0" style="font-size:18px;">Something went wrong, Please try again.</p>');
                $('#exampleModal').modal('show');
            }
        }
    })
})