<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Session;
use Config;
use Carbon\Carbon;

class infographic extends Model
{
    public function getInfographicData($search_key){
        $query = DB::table('infographics')
                ->join('category','category.category_id','=','infographics.category_id')
                ->select('infographics.infographic_id','infographics.infographic_title','infographics.infographic_url','infographics.category_id','infographics.status','category.cat_name')
                ->orderBy('infographics.infographic_id','DESC');

        if($search_key != ''){
            $query->where('infographics.infographic_title','like','%'.$search_key.'%');
        }

        $getInfographicData = $query->paginate(30);
        if(sizeof($getInfographicData)>0){
            return $getInfographicData;
        }
        else{
            return [];
        }
    }

    public function save_infographic($data,$img_name,$infograhic_thumb_image){
        $date = Carbon::now();

        $save_infographic = DB::table('infographics')->insert([
            'infographic_title' => $data['infograhic_name'],
            'infographic_url' => $data['infograhic_url'],
            'category_id' => $data['infograhic_category_id'],
            'infographic_short_descr' => $data['infograhic_short_desc'],
            'infographic_descr' => $data['infograhic_long_desc'],
            'infographic_image' => $img_name,
            'infographic_thumb_image' => $infograhic_thumb_image,
            'meta_title' => $data['meta_title'],
            'meta_keywords' => $data['meta_keywords'],
            'meta_description' => $data['meta_desc'],
            'status' => $data['infograhic_status'],
            'created_at' => $date
        ]);
        
        if($save_infographic == 1){
            return 1;
        }
        else{
            return 0;
        }
    }

    public function getInfographicDataById($id){
        $getInfographicDataById = DB::table('infographics')
                            ->where('infographic_id','=',$id)
                            ->get();
        if(sizeof($getInfographicDataById)>0){
            return $getInfographicDataById;
        }
        else{
            return [];
        }
    }

    public function update_infographic($data,$img_name,$infograhic_thumb_image){
        $date = Carbon::now();

        $update_infographic = DB::table('infographics')->where('infographic_id',$data['infographic_id'])->update([
            'infographic_title' => $data['infograhic_name'],
            'infographic_url' => $data['infograhic_url'],
            'category_id' => $data['infograhic_category_id'],
            'infographic_short_descr' => $data['infograhic_short_desc'],
            'infographic_descr' => $data['infograhic_long_desc'],
            'infographic_image' => $img_name,
            'infographic_thumb_image' => $infograhic_thumb_image,
            'meta_title' => $data['meta_title'],
            'meta_keywords' => $data['meta_keywords'],
            'meta_description' => $data['meta_desc'],
            'status' => $data['infograhic_status'],
            'updated_at' => $date
        ]);
        
        if($update_infographic == 1){
            return 1;
        }
        else{
            return 0;
        }
    }

    public function update_infographic_status($data){
        $date = Carbon::now();
        $status='';
        if($data['hidden_status'] == 1){
            $status='Y';
        }
        else{
            $status='N';
        }
        $update_infographic_status = DB::table('infographics')->where('infographic_id',$data['infographic_id'])->update([
            'status' => $status,
            'updated_at' => $date
        ]);
        
        if($update_infographic_status == 1){
            return 1;
        }
        else{
            return 0;
        }
    }
}
