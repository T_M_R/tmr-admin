<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\leads;
use App\leadsAllocation;
use App\master;
use App\users;
use Carbon\Carbon;
use Redirect;
use Session;
use DB;
use Config;

class leadsController extends Controller
{
    public function __construct(){
        $this->leads = new leads();
        $this->leadsAllocation = new leadsAllocation();
        $this->master = new master();
        $this->users = new users();
    }

     public function getLeads(Request $request){
        if (!session('username')) {
	    	return redirect('/');
        }  

        $user_id='';
        $category_id='';
        $country_id='';
        $filterFlag= 0;
        $search_key='';

        if(isset($_GET['user'])){
            $filterFlag= 1;
            $user_id=$_GET['user'];
        }
        if(isset($_GET['category_id'])){
            $filterFlag= 1;
            $category_id=$_GET['category_id'];
        }
        if(isset($_GET['country_id'])){
            $filterFlag= 1;
            $country_id=$_GET['country_id'];
        }
        if(isset($_GET['search_key'])){
            $search_key=$_GET['search_key'];
        }

        $get_my_team = $this->users->get_my_team();

        $getCountriesList = $this->leadsAllocation->getCountriesList();
        $getCategories = $this->leadsAllocation->getCategories();
        $getUsers = $this->leadsAllocation->getUsersForFilter();
        
        $users = $this->leadsAllocation->getUsers($get_my_team,$user_id);
        
        $getLeads = $this->leads->getLeads($get_my_team,$user_id,$category_id,$country_id,$filterFlag);
        
        // for Excel
        $getLeads_For_Excel = $this->leads->getLeads($get_my_team,$user_id,$category_id,$country_id,$filterFlag);
        
        if(sizeof($getLeads)>0){
            $getLeads= $getLeads->appends(['user'=>$user_id,'category_id'=>$category_id,'country_id'=>$country_id,'search_key'=>$search_key]);
        }
        // else{
        //     $getLeads= $getLeads->appends(['user'=>$user_id,'category_id'=>$category_id,'country_id'=>$country_id,'search_key'=>$search_key]);
        // }

        Session::put('getLeads_For_Excel',$getLeads_For_Excel);
        Session::put('getUsers',$getUsers);
        return view('leads.list')->with('getLeads',$getLeads)->with('getCountriesList',$getCountriesList)->with('getCategories',$getCategories)->with('getUsers',$getUsers);
    }

    public function getLeadDetails($id){
        if (!session('username')) {
	    	return redirect('/');
        }

        $getLeadDetails = $this->leads->getLeadDetails($id);
        $getLeadComments = $this->leads->getLeadComments($id);
        return view('leads.view')->with('getLeadDetails',$getLeadDetails)->with('getLeadComments',$getLeadComments);
    }

    public function save_lead_comment(Request $request){
        if (!session('username')) {
	    	return redirect('/');
        }

        $data =$request->all();
        $save_lead_comment = $this->leads->save_lead_comment($data);
        return $save_lead_comment;
    }

    public function save_remainder(Request $request){
        if (!session('username')) {
	    	return redirect('/');
        }

        $data =$request->all();
        $save_remainder = $this->leads->save_remainder($data);
        return $save_remainder;
    }

    public function change_assignedUser_LeadStatus(Request $request){
        if (!session('username')) {
	    	return redirect('/');
        }

        $data =$request->all();
        $change_assignedUser_LeadStatus = $this->leads->change_assignedUser_LeadStatus($data);
        return $change_assignedUser_LeadStatus;
    }
}
