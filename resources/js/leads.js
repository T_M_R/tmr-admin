$('.save_lead_comment').on('click', function () {
    let leadId = $('.lead_id').val();
    let comment = $('.lead_comments').val();

    console.log(leadId, comment);
    if (leadId != '' && leadId != undefined && leadId != null && comment != null && comment != '' && comment != undefined) {
        $.ajax({
            url: '/save_lead_comment',
            type: "POST",
            data: {
                leadId: leadId,
                comment: comment
            },
            success: function (result) {
                console.log(result);
                // return false;
                if (result == 1) {
                    $('.leads_success_msg').css({ 'display': 'block' });
                    $('.leads_success_msg').text('Comment Saved successfully.');
                    setTimeout(() => {
                        $('.leads_success_msg').css({ 'display': 'none' });
                        location.reload();
                    }, 1500);
                }
                else {
                    $('.leads_error_msg').css({ 'display': 'block' });
                    $('.leads_error_msg').text('Something Went Wrong.Pls try again later..!!');
                    setTimeout(() => {
                        $('.leads_error_msg').css({ 'display': 'none' });
                    }, 3000);
                    return false;
                }
            }
        });
    }
    else {
        $('.leads_error_msg').css({ 'display': 'block' });
        $('.leads_error_msg').text('Pleas enter your message in textarea');
        setTimeout(() => {
            $('.leads_error_msg').css({ 'display': 'none' });
            // location.reload();
        }, 3000);
        return false;
    }
})

$('.save_remainder').on('click', function () {
    let leadId = $('.lead_id').val();
    let remainder_date = $('.remainder_date').val();
    let remainder_title = $('.remainder_title').val();
    let remainder_comments = $('.remainder_comments').val();

    console.log(leadId, remainder_date, remainder_title, remainder_comments);
    if (leadId != '' && leadId != undefined && leadId != null && remainder_date != null && remainder_date != '' && remainder_date != undefined && remainder_comments != null && remainder_comments != '' && remainder_comments != undefined) {
        $.ajax({
            url: '/save_remainder',
            type: "POST",
            data: {
                leadId: leadId,
                remainder_date: remainder_date,
                remainder_title: remainder_title,
                remainder_comments: remainder_comments
            },
            success: function (result) {
                console.log(result);
                // return false;
                if (result == 1) {
                    $('.remainder_success_msg').css({ 'display': 'block' });
                    $('.remainder_success_msg').text('Remainder Saved successfully.');
                    setTimeout(() => {
                        $('.remainder_success_msg').css({ 'display': 'none' });
                        location.reload();
                    }, 1500);
                }
                else {
                    $('.remainder_error_msg').css({ 'display': 'block' });
                    $('.remainder_error_msg').text('Something Went Wrong.Pls try again later..!!');
                    setTimeout(() => {
                        $('.remainder_error_msg').css({ 'display': 'none' });
                    }, 3000);
                    return false;
                }
            }
        });
    }
    else {
        $('.remainder_error_msg').css({ 'display': 'block' });
        $('.remainder_error_msg').text('All fields are Mandatory');
        setTimeout(() => {
            $('.remainder_error_msg').css({ 'display': 'none' });
            // location.reload();
        }, 3000);
        return false;
    }
})