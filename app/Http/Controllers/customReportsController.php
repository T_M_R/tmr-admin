<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\master;
use App\customReports;
use Carbon\Carbon;
use Redirect;
use Session;
use DB;
use Config;

class customReportsController extends Controller
{
    public function __construct(){
        $this->master = new master();
        $this->customReports = new customReports();
    }

    public function getCustomReports(Request $request){
        if (!session('username')) {
	    	return redirect('/');
        } 
        $search_key = '';
        if(isset($_GET['search_key'])){
            $search_key=$_GET['search_key'];
        }
        $getCustomReports = $this->customReports->getCustomReports($search_key);
        $getCustomReports = $getCustomReports->appends(['search_key'=>$search_key]);

        $getCustomReports_For_Download = $this->customReports->getCustomReports_For_Download($search_key);
        Session::put('getCustomReports',$getCustomReports_For_Download);
        return view('customReports.list')->with('getCustomReports',$getCustomReports)->with('search_key',$search_key);
    } 

     public function addNewCustomReport(Request $request){
        if (!session('username')) {
	    	return redirect('/');
        } 
        $getCategories = $this->master->getCategories();
        return view('customReports.add')->with('getCategories',$getCategories);
    }

    public function save_custom_report(Request $request){
        if (!session('username')) {
	    	return redirect('/');
        } 
        
        $data = $request->all();
        $save_custom_report = $this->customReports->save_custom_report($data);
        return $save_custom_report;
    }

    public function edit_custom_report(Request $request,$id){
        if (!session('username')) {
	    	return redirect('/');
        } 
        
        $data = $request->all();
        $getCategories = $this->master->getCategories();
        $edit_custom_report = $this->customReports->getCustomReportDataById($id);
        return view('customReports.edit')->with('edit_custom_report',$edit_custom_report)->with('getCategories',$getCategories);
    }

    public function update_custom_report(Request $request){
        if (!session('username')) {
	    	return redirect('/');
        } 
        
        $data = $request->all();
        $update_custom_report = $this->customReports->update_custom_report($data);
        return $update_custom_report;
    }

    public function view_custom_report(Request $request,$id){
        if (!session('username')) {
	    	return redirect('/');
        } 
        
        $data = $request->all();
        $getCategories = $this->master->getCategories();
        $view_custom_report = $this->customReports->getCustomReportDataById($id);
        return view('customReports.view')->with('view_custom_report',$view_custom_report)->with('getCategories',$getCategories);
    }

    public function update_custom_report_status(Request $request){
        if (!session('username')) {
	    	return redirect('/');
        } 
        
        $data = $request->all();
        $update_custom_report_status = $this->customReports->update_custom_report_status($data);
        return $update_custom_report_status;
    }
}
