@extends('layouts.header')
@section('body-content')
<section class="sub-category-list">
    <div class="block-header">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 my-auto">
                <h4 class="page-title m-0">View Aritcle</h4>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <a href="{{URL:: to('/articles')}}">
                    <button type="button" class="btn btn-info float-right">Back</button>
                </a>
            </div>
        </div>
    </div>
    <div class="row clearfix mt-3">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="card project_widget">
                <div class="body mt-4 mb-4">
                @if(isset($view_article) && sizeof($view_article)>0)
                    @foreach($view_article as $value)
                    <div class="row">
                        <div class="col-12 col-md-6 mb-0">
                            <div class="input-group">  
                                <input type="text" class="article_name" autocomplete="off" value="{{$value->name}}" required disabled/>
                                <label>Title <span class="text-danger">*</span></label>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 mb-0">
                            <div class="input-group">  
                                <input type="text" class="article_url" autocomplete="off" value="{{$value->url}}" required disabled/>
                                <label>URL <span class="text-danger">*</span></label>
                            </div>
                        </div>
                        <div class="col-12 col-md-12 mb-0">
                            <div class="input-group">
                                <select class="floating-select article_category_id" onclick="this.setAttribute('value', this.value);" required disabled>
                                    @if(sizeof($getSubCategories)>0)
                                        <option value=""></option>
                                        @foreach($getSubCategories as $val)
                                            @if($val->category_id == $value->category_id)
                                                <option value="{{$val->category_id}}" selected>{{$val->cat_name}}</option>
                                            @else
                                                <option value="{{$val->category_id}}">{{$val->cat_name}}</option>
                                            @endif
                                        @endforeach
                                    @endif
                                </select>
                                <label>Category <span class="text-danger">*</span></label>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 mb-0 d-none">
                            <div class="input-group">
                                <select class="floating-select article_author_id" onclick="this.setAttribute('value', this.value);" required disabled>
                                    @if(sizeof($getSubCategories)>0)
                                        <option value=""></option>
                                        @foreach($getSubCategories as $val)
                                            @if($val->category_id == $value->category_id)
                                                <option value="{{$val->category_id}}" selected>{{$val->cat_name}}</option>
                                            @else
                                                <option value="{{$val->category_id}}">{{$val->cat_name}}</option>
                                            @endif
                                        @endforeach
                                    @endif
                                </select>
                                <label>Author <span class="text-danger">*</span></label>
                            </div>
                        </div>
                        <div class="col-12 col-md-12 mb-0">
                            <label class="" style="color:#414244;"><b>Short  Description</b><span class="text-danger">*</span></label>
                            <div class="input-group mt-5">
                                <textarea class="article_short_desc ckeditor" name="article_short_desc" id="article_short_desc" >{{$value->short_desc   }}  </textarea>
                            </div>
                        </div>
                        <div class="col-12 col-md-12 mb-0">
                            <label class="" style="color:#414244;"><b>Long Description</b><span class="text-danger">*</span></label>
                            <div class="input-group mt-5">  
                                <textarea class="article_long_desc ckeditor" id="article_long_desc" name="article_long_desc"> {{$value->full_desc}} </textarea>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 mb-0">
                            <div class="input-group">  
                                <input type="text" class="meta_title" autocomplete="off" value="{{$value->meta_title}}" required disabled/>
                                <label>Meta Title <span class="text-danger">*</span></label>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 mb-0">
                            <div class="input-group">  
                                <input type="text" class="meta_keywords" autocomplete="off" value="{{$value->meta_keywords}}" required disabled/>
                                <label>Meta Keywords<span class="text-danger">*</span></label>
                            </div>
                        </div>
                        <div class="col-12 col-md-12 mb-0">
                            <label class="" style="color:#414244;"><b>Meta Description</b><span class="text-danger">*</span></label>
                            <div class="form-group mt-5"> 
                                <div class="form-line">  
                                    <textarea class="form-control meta_desc p-3" name="meta_desc" rows="4" placeholder="Meta Description"> {{$value->meta_description}} </textarea>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 mb-0">
                            <div class="input-group">  
                                <input type="text" class="datepicker article_publish_date" autocomplete="off" value="{{$value->upload_date}}" required disabled/>
                                <span style="position: absolute;top: 8px;right: 0;bottom: 0;"><i class="icon-calendar" style="font-size: 26px;"></i></span>
                                <label>Publish Date <span class="text-danger">*</span></label>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 mb-0">
                            <div class="input-group">
                                <select class="floating-select article_status" onclick="this.setAttribute('value', this.value);" required disabled>
                                    <option value=""></option>
                                    @if($value->status == 'Y')
                                       <option value="Y" selected>Active</option>
                                        <option value="N">In-Active</option>
                                    @elseif($value->status == 'N')
                                        <option value="Y">Active</option>
                                        <option value="N" selected>In-Active</option>
                                    @else
                                        <option value="Y">Active</option>
                                        <option value="N">In-Active</option>
                                    @endif
                                </select>
                                <label>Status <span class="text-danger">*</span></label>
                            </div>
                        </div>
                        <input type="hidden" class="article_id" value="{{$value->id}}">
                        <div class="col-12 col-md-4 mb-0">
                            <p class="text-danger mb-0">* All fields are Mandatory</p>
                        </div>
                    </div>
                    @endforeach
                @else
                    <div class="row">
                        <div class="col-md-12">
                            <h5 class="Center">No Data</h5>
                        </div>
                    </div>
                @endif   
                </div>
            </div>
        </div>
    </div>
</section>
@endsection