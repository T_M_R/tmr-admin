require('./bootstrap');

domain =  window.location.origin;
url_path = window.location.pathname;

import ('./master.js');
import ('./lead_allocation.js');
import ('./roles.js');
import ('./users.js');
