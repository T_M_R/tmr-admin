<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Excel;
use Session;
use DB;
use Carbon\Carbon;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Writer\Xls;
use Config;

class downloadController extends Controller
{
    // For Reports Download ==========================================
    public function reports_csv_download()
    {
        if(Session::has('getReports')){
            $raw_Records = session('getReports');
            
            $row[0] = [];
            $row[0]['Report']='';
            $row[0]['Report URL']='';
            $row[0]['Report Type']='';
            $row[0]['Publish Date']='';
            $row[0]['Status']='';
            
            $resultCount=0;
            foreach ($raw_Records as $key => $value) 
            {
                $row[$resultCount]['Report']=$value->rep_name;
                $row[$resultCount]['Report URL']=$value->rep_url;
                if($value->rep_type == 'Y')
                {
                    $row[$resultCount]['Report Type']='Publish';
                }
                else
                {
                    $row[$resultCount]['Report Type']='Upcoming';
                }
                if($value->rep_pub_date !='')
                {
                    $rep_pub_date = date("d-m-Y", strtotime($value->rep_pub_date));
                    $row[$resultCount]['Publish Date']=$rep_pub_date;
                }
                else
                {
                    $row[$resultCount]['Publish Date']='';
                }
                if($value->rep_status == 'Y')
                {
                    $row[$resultCount]['Status']='Active';
                }
                else
                {
                    $row[$resultCount]['Status']='In-Active';
                }
                
                $resultCount++;
            }
          
            $un_inserted_records= $row;

            if(sizeof($un_inserted_records)>0){
                $rep_data = $un_inserted_records;
                $fileName = "Reports.xls";
                $this->download_report_as_excel($rep_data,$fileName);
                Session::forget('getReports');
                Session::save();
            }
            else
            {
                return redirect()->back();
            }
        }
        else{
            return redirect()->back();
        }
    }

    // For Custom Reports Download =====================================
    public function custom_reports_csv_download()
    {
        if(Session::has('getCustomReports')){
            $raw_Records = session('getCustomReports');
            
            $row[0] = [];
            $row[0]['Report']='';
            $row[0]['Report URL']='';
            $row[0]['Report Type']='';
            $row[0]['Publish Date']='';
            $row[0]['Status']='';
            
            $resultCount=0;
            foreach ($raw_Records as $key => $value) 
            {
                $row[$resultCount]['Report']=$value->rep_name;
                $row[$resultCount]['Report URL']=$value->rep_url;
                if($value->rep_type == 'P')
                {
                    $row[$resultCount]['Report Type']='Publish';
                }
                else
                {
                    $row[$resultCount]['Report Type']='Upcoming';
                }
                if($value->added_date !='')
                {
                    $added_date = date("d-m-Y", strtotime($value->added_date));
                    $row[$resultCount]['Publish Date']=$added_date;
                }
                else
                {
                    $row[$resultCount]['Publish Date']='';
                }
                if($value->rep_status == 'Y')
                {
                    $row[$resultCount]['Status']='Active';
                }
                else
                {
                    $row[$resultCount]['Status']='In-Active';
                }
                
                $resultCount++;
            }
          
            $un_inserted_records= $row;

            if(sizeof($un_inserted_records)>0){
                $rep_data = $un_inserted_records;
                $fileName = "Custom Reports.xls";
                $this->download_report_as_excel($rep_data,$fileName);
                Session::forget('getCustomReports');
                Session::save();
            }
            else
            {
                return redirect()->back();
            }
        }
        else{
            return redirect()->back();
        }
    }

    // For Leads Download ==========================================
    public function leads_csv_download()
    {
        if(Session::has('getLeads_For_Excel')){
            $raw_Records = session('getLeads_For_Excel');
            $getUsers = session('getUsers');
            
            $row[0] = [];
            $row[0]['Report']='';
            $row[0]['Category']='';
            $row[0]['Assigned To']='';
            $row[0]['Rep Type']='';
            $row[0]['User Name']='';
            $row[0]['Mobile']='';
            $row[0]['Email']='';
            $row[0]['Country']='';
            $row[0]['Company']='';
            
            $resultCount=0;
            foreach ($raw_Records as $key => $value) 
            {
                $row[$resultCount]['Report']=$value->rep_name;
                $row[$resultCount]['Category']=$value->cat_master_name;
                if($value->userId != ''){
                    foreach($getUsers as $user){
                        if($user->id == $value->userId){
                            $row[$resultCount]['Assigned To']=$user->name;
                        }
                    }
                }else{
                    $row[$resultCount]['Assigned To']='';
                }
                if($value->rep_type == 'N')
                {
                    $row[$resultCount]['Rep Type']='Publish';
                }
                else
                {
                    $row[$resultCount]['Rep Type']='Upcoming';
                }

                $row[$resultCount]['User Name']=$value->name;
                
                if($value->phoneNo != '' )
                {
                    $row[$resultCount]['Mobile']=$value->phoneNo;
                }
                else
                {
                    $row[$resultCount]['Mobile']=$value->phoneNo;
                }

                if($value->emailId !='')
                {
                   
                    $row[$resultCount]['Email']=$value->emailId;
                }
                else
                {
                    $row[$resultCount]['Email']='';
                }
                if($value->country != '')
                {
                    $row[$resultCount]['Country']=$value->country;
                }
                else
                {
                    $row[$resultCount]['Country']='';
                }

                if($value->company != '')
                {
                    $row[$resultCount]['Company']=$value->company;
                }
                else
                {
                    $row[$resultCount]['Company']='';
                }
                
                $resultCount++;
            }
          
            $un_inserted_records= $row;

            if(sizeof($un_inserted_records)>0){
                $rep_data = $un_inserted_records;
                $fileName = "Leads.xls";
                $this->download_report_as_excel($rep_data,$fileName);
                Session::forget('getLeads_For_Excel');
                Session::forget('getUsers');
                Session::save();
            }
            else
            {
                return redirect()->back();
            }
        }
        else{
            return redirect()->back();
        }
    }

    //for downloading excel
    public function download_report_as_excel($rep_data,$fileName){
        if(sizeof($rep_data)>0){
            if(sizeof($rep_data[0])>0){
                $spreadsheet = new Spreadsheet();
                $sheet = $spreadsheet->getActiveSheet();
                $rows=1; $col=1;
                foreach($rep_data[0] as $key => $val){
                    $sheet->setCellValueByColumnAndRow($col, $rows, $key);
                    $col++;
                }
                $rows = 2;
                foreach($rep_data as $val){
                    $col=1;
                    foreach($val as $key => $value){
                        $sheet->setCellValueByColumnAndRow($col, $rows, $val[$key]);
                        $col++;
                    }
                    $rows++;
                }
                // for Xlsx use - new Xlsx($spreadsheet);
                $objWriter = new Xls($spreadsheet);
                header("Content-Type: application/vnd.ms-excel");
                header('Content-Disposition: attachment; filename='."$fileName");
                $objWriter->save('php://output');
            }
        }
    }
}
