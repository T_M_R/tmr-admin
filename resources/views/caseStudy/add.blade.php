@extends('layouts.header')
@section('body-content')
<section class="sub-category-list">
    <div class="block-header">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 my-auto">
                <h4 class="page-title m-0">Add New Case Study</h4>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <a href="{{URL:: to('/case_studies')}}">
                    <button type="button" class="btn btn-info float-right">Back</button>
                </a>
            </div>
        </div>
    </div>
    <div class="row clearfix mt-3">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="card project_widget">
                <div class="body mt-4 mb-4">
                    <form method="POST" action="{{url('save_case_study')}}" enctype="multipart/form-data">   
                        <div class="row">
                            <div class="col-12 col-md-6 mb-0">
                                <div class="input-group">  
                                    <input type="text" class="cs_name" name="cs_name" autocomplete="off" value="" required/>
                                    <label>Title <span class="text-danger">*</span></label>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 mb-0">
                                <div class="input-group">  
                                    <input type="text" class="cs_url" name="cs_url" autocomplete="off" value="" required/>
                                    <label>URL <span class="text-danger">*</span></label>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 mb-0">
                                <div class="input-group">
                                    <select class="floating-select cs_category_id" name="cs_category_id" onclick="this.setAttribute('value', this.value);" value="" required>
                                        @if(sizeof($getCategories)>0)
                                            <option value=""></option>
                                            @foreach($getCategories as $val)
                                                <option value="{{$val->category_id}}">{{$val->cat_name}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                    <label>Category <span class="text-danger">*</span></label>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 mb-0">
                                <div class="input-group">  
                                    <input type="file" class="cs_image" name="cs_image" autocomplete="off" value="" required/>
                                    <!-- <label>Image <span class="text-danger">*</span></label> -->
                                </div>
                            </div>
                            <div class="col-12 col-md-12 mb-0">
                                <label class="" style="color:#414244;"><b>Challenge</b><span class="text-danger">*</span></label>
                                <div class="input-group mt-5">
                                    <textarea class="cs_challenge ckeditor" name="cs_challenge" id="cs_challenge" >  </textarea>
                                </div>
                            </div>
                            <div class="col-12 col-md-12 mb-0">
                                <label class="" style="color:#414244;"><b>Insight</b><span class="text-danger">*</span></label>
                                <div class="input-group mt-5">  
                                    <textarea class="cs_insight ckeditor" id="cs_insight" name="cs_insight">  </textarea>
                                </div>
                            </div>
                            <div class="col-12 col-md-12 mb-0">
                                <label class="" style="color:#414244;"><b>Solution</b><span class="text-danger">*</span></label>
                                <div class="input-group mt-5">
                                    <textarea class="cs_solution ckeditor" name="cs_solution" id="cs_solution" >  </textarea>
                                </div>
                            </div>
                            <div class="col-12 col-md-12 mb-0">
                                <label class="" style="color:#414244;"><b>Result</b><span class="text-danger">*</span></label>
                                <div class="input-group mt-5">
                                    <textarea class="cs_result ckeditor" name="cs_result" id="cs_result" >  </textarea>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 mb-0">
                                <div class="input-group">  
                                    <input type="text" class="meta_title" name="meta_title" autocomplete="off" value="">
                                    <label>Meta Title </label>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 mb-0">
                                <div class="input-group">  
                                    <input type="text" class="meta_keywords" name="meta_keywords" autocomplete="off" value="">
                                    <label>Meta Keywords</label>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 mb-0">
                                <label class="" style="color:#414244;"><b>Meta Description</b></label>
                                <div class="form-group mt-5"> 
                                    <div class="form-line">  
                                        <textarea class="form-control meta_desc p-3" name="meta_desc" rows="4" placeholder="Meta Description">  </textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 mb-0">
                                <label class="" style="color:#414244;"><b>DC Description</b></label>
                                <div class="form-group mt-5"> 
                                    <div class="form-line">  
                                        <textarea class="form-control dc_desc p-3" name="dc_desc" rows="4" placeholder="DC Description">  </textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 mb-0">
                                <div class="input-group">  
                                    <input type="text" class="seo_title" name="seo_title" autocomplete="off" value=""/>
                                    <label>Seo Title </label>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 mb-0">
                                <div class="input-group">
                                    <select class="floating-select cs_status" name="cs_status" onclick="this.setAttribute('value', this.value);" value="" required>
                                        <option value=""></option>
                                        <option value="1">Active</option>
                                        <option value="0">In-Active</option>
                                    </select>
                                    <label>Status <span class="text-danger">*</span></label>
                                </div>
                            </div>
                            <div class="col-12 col-md-4 mb-0">
                                <p class="text-danger mb-0">* All fields are Mandatory</p>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb-0 text-center">
                                <div class="form-group">
                                    <div class="form-line">
                                        <div class="article_error_msg col-red w-100" style="display:none"></div>
                                        <div class="article_success_msg col-green w-100" style="display:none"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 text-center">
                                <button type="submit" class="btn btn-primary waves-effect save_article">SUBMIT</button>                                                                
                            </div>
                        </div> 
                    </form>   
                </div>
            </div>
        </div>
    </div>
</section>
@endsection