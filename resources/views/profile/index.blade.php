@extends('layouts.header')
@section('body-content')
<section class="regions-list">
    <div class="block-header">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 my-auto">
                <h4 class="page-title m-0">Profile</h4>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <button type="button" class="btn btn-primary float-right" data-toggle="modal" data-target="#changePassword">Change Password</button>
            </div>
        </div>
    </div>
    <div class="row clearfix mt-3">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <p class="center status_change_msg text-danger"></p>
            <div class="card">
                <div class="body">
                    @foreach($getProfileData as $value)
                    <div class="row">
                        <div class="col-md-8 mb-0">
                            <div class="row">
                                <div class="col-md-4">
                                    <strong>Full Name</strong>
                                </div>
                                <div class="col-md-1">:</div>
                                <div class="col-md-7">
                                    <p class="text-muted">{{$value->name}}</p>
                                </div>
                                <div class="col-md-4">
                                    <strong>Role</strong>
                                </div>
                                <div class="col-md-1">:</div>
                                <div class="col-md-7">
                                    <p class="text-muted">{{$value->role_name}}</p>
                                </div>
                                <div class="col-md-4">
                                    <strong>Email</strong>
                                </div>
                                <div class="col-md-1">:</div>
                                <div class="col-md-7">
                                    <p class="text-muted">{{$value->email}}</p>
                                </div>
                                <div class="col-md-4">
                                    <strong>Mobile</strong>
                                </div>
                                <div class="col-md-1">:</div>
                                <div class="col-md-7">
                                    <p class="text-muted">{{$value->mobile_phone}}</p>
                                </div>
                                @if($value->supervisor_id !='')
                                    <div class="col-md-4">
                                        <strong>Supervisor</strong>
                                    </div>
                                    <div class="col-md-1">:</div>
                                    <div class="col-md-7">
                                        <p class="text-muted">{{$user->supervisor}}</p>
                                    </div>
                                @endif
                            </div>
                        </div>
                        <input type="number" class="d-none profile-id" value="{{$value->id}}">
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</section>

<div class="modal fade changePassword" id="changePassword" tabindex="-1" role="dialog" aria-labelledby="formModal" aria-hidden="true">    
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title modal-title" id="formModal">Change Password</h5>
                <button type="button" class="close modal-close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12 p-0">
                        <div class="col-12 col-md-12 mb-0">
                            <div class="input-group">  
                                <input type="text" class="current_pw" autocomplete="off" value="" required/>
                                <label>Current Password <span class="text-danger">*</span></label>
                            </div>
                        </div>
                        <div class="col-12 col-md-12 mb-0">
                            <div class="input-group">  
                                <input type="text" class="new_pw" autocomplete="off" value="" required/>
                                <label>New Password <span class="text-danger">*</span></label>
                            </div>
                        </div>
                        <div class="col-12 col-md-12 mb-0">
                            <div class="input-group">  
                                <input type="text" class="re_enter_new_pw" autocomplete="off" value="" required/>
                                <label>Re-Enter New Password <span class="text-danger">*</span></label>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 mb-0">
                            <p class="text-danger mb-0">* All fields are Mandatory</p>
                        </div>
                        <p class="pw_change_success_msg mb-0 text-success text-center"></p>
                        <p class="pw_change_error_msg mb-0 text-danger text-center"></p>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger modal-close" data-dismiss="modal">CLOSE</button>
                <button type="button" class="btn btn-primary update_password">Update</button>
            </div>
        </div>
    </div>
</div>
@endsection