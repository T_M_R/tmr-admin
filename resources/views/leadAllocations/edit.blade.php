@extends('layouts.header')
<style>
    td{
        background: #fff !important;
    }
</style>
@section('body-content')
<section class="regions-list">
    <div class="block-header">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 my-auto">
                <h4 class="page-title m-0">Update Lead Allocations</h4>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <a href="{{URL:: to('/lead_allocations')}}">
                    <button type="button" class="btn btn-info float-right">Back</button>
                </a>
            </div>
        </div>
    </div>
    <div class="row clearfix mt-3">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="card project_widget">
                <div class="card-header">
                    <h4 styles="width:fit-content;" class="m-0"><i class="fa fa-user-circle"></i> Lead Allocations <span> for </span> <span style="text-transform: capitalize;"><?php echo $getUserName[0]->name; ?></span></h4>
                </div><br>
                <div class="body mb-4">
                    <div class="row">
                        <div class="col-12 col-md-6 mb-0">
                            <div class="input-group">
                                <select class="floating-select category_id" id="category_id" onclick="this.setAttribute('value', this.value);" value="" required>
                                    @if(sizeof($getCategories)>0)
                                        <option value=""></option>
                                        @foreach($getCategories as $val)
                                            <option value="{{$val->category_id}}">{{$val->cat_name}}</option>
                                        @endforeach
                                    @endif
                                </select>
                                <label>Category <span class="text-danger">*</span></label>
                                <p class="mb-0 text-danger category_error_msg pt-2"></p>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 mb-0">
                            <div class="input-group">
                                <select class="floating-select region_id" id="region_id" onclick="this.setAttribute('value', this.value);" value="" required>
                                    @if(sizeof($getRegionsList)>0)
                                        <option value=""></option>
                                        @foreach($getRegionsList as $val)
                                            <option value="{{$val->id}}">{{$val->name}}</option>
                                        @endforeach
                                    @endif
                                </select>
                                <label>Region <span class="text-danger">*</span></label>
                                <p class="mb-0 text-danger region_error_msg pt-2"></p>
                            </div>
                        </div>
                        <div class="col-12 col-md-12 mb-0">
                            <div class="input-group">
                                <select class="floating-select js-example-placeholder-multiple country_id" id="country_id" name="country_id[]" multiple="multiple" required>
                                </select>
                                <label>Country <span class="text-danger">*</span></label>
                                <p class="mb-0 text-danger country_error_msg pt-2"></p>
                            </div>
                        </div>
                        <input type="hidden" class="userid" id="userid" value="{{$getUserName[0]->id}}">
                        <div class="col-12 col-md-4 mb-0">
                            <p class="text-danger mb-0">* All fields are Mandatory</p>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb-0 text-center">
                            <div class="form-group">
                                <div class="form-line">
                                    <div class="region_error_msg col-red w-100" style="display:none"></div>
                                    <div class="region_success_msg col-green w-100" style="display:none"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 text-center">
                            <button type="submit" class="btn btn-primary waves-effect add_allocation">Add Allocation</button>                                                                
                        </div>
                    </div>    
                </div>
            </div>
        </div>
    </div>

    <div class="row clearfix mt-3">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="card project_widget">
                <div class="card-header">
                    <h4 styles="width:fit-content;" class="m-0">Allocations</h4>
                </div><br>
                <div class="body mb-4">
                    <div class="table-responsive">     
                        <table  class="table table-striped table-bordered table-condensed table-lead-allocations" >
                            <thead>
                                <th>Category</th>
                                <th>Region</th>
                                <th>Country</th>
                                <th>Delete</th>
                            </thead>
                            <tbody class="tbody-lead-allocations">
                                @if(sizeof($lead_allocations)>0)
                                    @foreach ($lead_allocations as $name => $category)
                                    <tr class="colored">
                                        <td class="align-middle" rowspan="{{$category->total_country_count}}">{{$category->cat_name}}</td>
                                        @foreach ($category->regions as $region)
                                            <td class="align-middle" rowspan="{{$region->temp_country_count}}">{{$region->region_name}}</td>
                                            @foreach ($region->countries as $country)
                                                    <td rowspan="1" class="align-middle">{{$country->country_name}}</td>
                                                    <td rowspan="1" class="align-middle"><button class="border-0 bg-transparent btn_lead_allocation"><i class="material-icons text-danger"><b>cancel</b></i></button></td>
                                                    <td class="d-none allocated_country_id">{{$country->country_id}}</td>
                                                    <td class="d-none allocated_category_id">{{$category->category_id}}</td>
                                                    <td class="d-none allocated_region_id">{{$region->region_id}}</td>
                                                    <td class="d-none allocated_user_id">{{$category->userid}}</td>
                                                </tr>
                                            @endforeach
                                        @endforeach
                                    @endforeach
                                @else
                                    <tr><td colspan="4" class="text-center"><b>No Allocations</b></td></tr>
                                @endif
                            </tbody>
                        </table>
                    </div>   
                </div>
            </div>
        </div>
    </div>
</section>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Message</h5>
        <button type="button" class="close text-right close-la-modal" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
            <p class="message"></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary close-la-modal" data-dismiss="modal">Close</button>
        <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
      </div>
    </div>
  </div>
</div>
@endsection