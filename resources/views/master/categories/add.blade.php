@extends('layouts.header')
@section('body-content')
<section class="regions-list">
    <div class="block-header">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 my-auto">
                <h4 class="page-title m-0">Add Country</h4>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <a href="{{URL:: to('/categories')}}">
                    <button type="button" class="btn btn-info float-right">Back</button>
                </a>
            </div>
        </div>
    </div>
    <div class="row clearfix mt-3">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="card project_widget">
                <div class="body mt-4 mb-4">
                    <!-- <form id="save_category" enctype="multipart/form-data"> -->
                        <div class="row">
                            <div class="col-12 col-md-6 mb-0">
                                <div class="input-group">  
                                    <input type="text" class="category_name" name="category_name" autocomplete="off" value="" required/>
                                    <label>Category Name <span class="text-danger">*</span></label>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 mb-0">
                                <div class="input-group">  
                                    <input type="text" class="category_path" name="category_path" autocomplete="off" value="" required/>
                                    <label>Category Path <span class="text-danger">*</span></label>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 mb-0">
                                <div class="input-group">  
                                    <input type="text" class="category_heading" name="category_heading" autocomplete="off" value="" required/>
                                    <label>Category Page Heading<span class="text-danger">*</span></label>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 mb-0">
                                <div class="input-group">  
                                    <input type="text" class="seo_pagename" name="seo_pagename" autocomplete="off" value="" required/>
                                    <label>SEO Page Name<span class="text-danger">*</span></label>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 mb-0 d-none">
                                <label class="" style="color:#414244;"><b>Thumb Image</b></label>
                                <div class="input-group mt-5">  
                                    <input type="file" class="thumb_fileToUpload" name="thumb_fileToUpload" autocomplete="off" value="" />
                                </div>
                            </div>
                            <div class="col-12 col-md-6 mb-0 d-none">
                                <label class="" style="color:#414244;"><b>Media Image</b></label>
                                <div class="input-group mt-5">  
                                    <input type="file" class="media_fileToUpload" name="media_fileToUpload" autocomplete="off" value="" />
                                </div>
                            </div>
                            <div class="col-12 col-md-6 mb-0">
                                <div class="input-group">  
                                    <input type="text" class="thumb_alt_text" name="thumb_alt_text" autocomplete="off" value="" required/>
                                    <label>Thumb Alt Text<span class="text-danger">*</span></label>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 mb-0">
                                <div class="input-group">  
                                    <input type="text" class="med_alt_text" name="med_alt_text" autocomplete="off" value="" required/>
                                    <label>Media Alt Text<span class="text-danger">*</span></label>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 mb-0">
                                <div class="input-group">  
                                    <input type="text" class="thumb_title_text" name="thumb_title_text" autocomplete="off" value="" required/>
                                    <label>Thumb Title Text<span class="text-danger">*</span></label>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 mb-0">
                                <div class="input-group">  
                                    <input type="text" class="med_title_text" name="med_title_text" autocomplete="off" value="" required/>
                                    <label>Media Title Text<span class="text-danger">*</span></label>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 mb-0">
                                <div class="input-group">
                                    <select class="floating-select thumb_no_follow" name="thumb_no_follow" onclick="this.setAttribute('value', this.value);" value="" required>
                                        <option value=""></option>
                                        <option value="Y">Yes</option>
                                        <option value="N">No</option>
                                    </select>
                                    <label>Thumb No Flow<span class="text-danger">*</span></label>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 mb-0">
                                <div class="input-group">
                                    <select class="floating-select med_no_follow" name="med_no_follow" onclick="this.setAttribute('value', this.value);" value="" required>
                                        <option value=""></option>
                                        <option value="Y">Yes</option>
                                        <option value="N">No</option>
                                    </select>
                                    <label>Thumb No Flow<span class="text-danger">*</span></label>
                                </div>
                            </div>
                            <div class="col-12 col-md-12 mb-0">
                                <label class="" style="color:#414244;"><b>Top  Description</b><span class="text-danger">*</span></label>
                                <div class="input-group mt-5">
                                    <textarea class="cat_top_descr ckeditor" name="cat_top_descr" id="cat_top_descr" >  </textarea>
                                </div>
                            </div>
                            <div class="col-12 col-md-12 mb-0">
                                <label class="" style="color:#414244;"><b>Bottom Description</b><span class="text-danger">*</span></label>
                                <div class="input-group mt-5">  
                                    <textarea class="cat_bottom_descr ckeditor" id="cat_bottom_descr" name="cat_bottom_descr">  </textarea>
                                </div>
                            </div>
                            <div class="col-12 col-md-12 mb-0">
                                <label class="" style="color:#414244;"><b>Home Description</b><span class="text-danger">*</span></label>
                                <div class="input-group mt-5">  
                                    <textarea class="cat_home_descr ckeditor" id="cat_home_descr" name="cat_home_descr">  </textarea>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 mb-0">
                                <div class="input-group">  
                                    <input type="text" class="meta_title" name="meta_title" autocomplete="off" value="" required/>
                                    <label>Meta Title </label>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 mb-0">
                                <div class="input-group">  
                                    <input type="text" class="meta_keywords" name="meta_keywords" autocomplete="off" value="" required/>
                                    <label>Meta Keywords</label>
                                </div>
                            </div>
                            <div class="col-12 col-md-12 mb-0">
                                <label class="" style="color:#414244;"><b>Meta Description</b></label>
                                <div class="form-group mt-5"> 
                                    <div class="form-line">  
                                        <textarea class="form-control meta_desc p-3" name="meta_desc" rows="4" placeholder="Meta Description">  </textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 mb-0">
                                <div class="input-group">  
                                    <input type="text" class="seo_header" name="seo_header" autocomplete="off" value="" required/>
                                    <label>SEO Header<span class="text-danger">*</span></label>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 mb-0">
                                <div class="input-group">  
                                    <input type="text" class="seo_footer" name="seo_footer" autocomplete="off" value="" required/>
                                    <label>SEO Footer<span class="text-danger">*</span></label>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 mb-0">
                                <div class="input-group">
                                    <select class="floating-select is_homepage" name="is_homepage" onclick="this.setAttribute('value', this.value);" value="">
                                        <option value=""></option>
                                        <option value="Y">Yes</option>
                                        <option value="N">No</option>
                                    </select>
                                    <label>Is Homepage</label>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 mb-0">
                                <div class="input-group">  
                                    <input type="text" class="link_title" name="link_title" autocomplete="off" value=""/>
                                    <label>Link Title</label>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 mb-0">
                                <div class="input-group">  
                                    <input type="text" class="qweb_logo_code" name="qweb_logo_code" autocomplete="off" value=""/>
                                    <label>Qweb Logo Code</label>
                                </div>
                            </div>
                            <div class="col-12 col-md-12 mb-0">
                                <p class="text-danger mb-0">* All fields are Mandatory</p>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb-0 text-center">
                                <div class="form-group">
                                    <div class="form-line">
                                        <div class="cat_error_msg col-red w-100" style="display:none"></div>
                                        <div class="cat_success_msg col-green w-100" style="display:none"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 text-center">
                                <button type="submit" class="btn btn-primary waves-effect save_category">SUBMIT</button>
                                <!-- <button type="submit" class="btn btn-primary waves-effect">SUBMIT</button> -->
                            </div>
                        </div>
                    <!-- </form>      -->
                </div>
            </div>
        </div>
    </div>
</section>
@endsection