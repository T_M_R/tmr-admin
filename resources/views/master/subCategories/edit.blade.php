@extends('layouts.header')
@section('body-content')
<section class="sub-category-list">
    <div class="block-header">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 my-auto">
                <h4 class="page-title m-0">Edit SubCategory</h4>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <a href="{{URL:: to('/sub-categories')}}">
                    <button type="button" class="btn btn-info float-right">Back</button>
                </a>
            </div>
        </div>
    </div>
    <div class="row clearfix mt-3">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="card project_widget">
                <div class="body mt-4 mb-4">
                    @foreach($edit_sub_category as $value)
                    <div class="row">
                        <div class="col-12 col-md-3 mb-0"></div>
                        <div class="col-12 col-md-6 mb-0">
                            <div class="input-group">
                                <select class="floating-select category_id" onclick="this.setAttribute('value', this.value);" value="" required>
                                    @if(sizeof($getCategories)>0)
                                        <option value=""></option>
                                        @foreach($getCategories as $val)
                                            @if($val->category_id == $value->category_id)
                                                <option value="{{$val->category_id}}" selected>{{$val->cat_name}}</option>
                                            @else
                                                <option value="{{$val->category_id}}">{{$val->cat_name}}</option>
                                            @endif
                                        @endforeach
                                    @endif
                                </select>
                                <label>Category <span class="text-danger">*</span></label>
                            </div>
                        </div>
                        <div class="col-12 col-md-3 mb-0"></div>
                        <div class="col-12 col-md-12 mb-0">
                            <div class="input-group">  
                                <input type="text" class="sub_category_name" autocomplete="off" value="{{$value->sub_category_name}}" required/>
                                <label>SubCategory Name <span class="text-danger">*</span></label>
                            </div>
                        </div>
                        <div class="col-12 col-md-12 mb-0">
                            <div class="input-group">  
                                <input type="text" class="sub_category_url" autocomplete="off" value="{{$value->sub_category_url}}" required/>
                                <label>SubCategory URL <span class="text-danger">*</span></label>
                            </div>
                        </div>
                        <div class="col-12 col-md-12 mb-0">
                            <div class="input-group">  
                                <input type="text" class="sub_category_heading_h2" autocomplete="off" value="{{$value->sub_category_heading}}" required/>
                                <label>SubCategory Page Heading H2 <span class="text-danger">*</span></label>
                            </div>
                        </div>
                        <div class="col-12 col-md-12 mb-0">
                            <label class="" style="color:#414244;"><b>Short  Description</b><span class="text-danger">*</span></label>
                            <div class="input-group mt-4">
                                <textarea class="sub_category_short_desc ckeditor" name="sub_category_short_desc" id="sub_category_short_desc" >{{$value->short_description}}  </textarea>
                            </div>
                        </div>
                        <div class="col-12 col-md-12 mb-0">
                            <label class="" style="color:#414244;"><b>Long Description</b><span class="text-danger">*</span></label>
                            <div class="input-group mt-4">  
                                <textarea class="sub_category_desc ckeditor" id="sub_category_desc" name="sub_category_desc"> {{$value->description}}</textarea>
                            </div>
                        </div>
                        <div class="col-12 col-md-12 mb-0">
                            <div class="input-group">  
                                <input type="text" class="meta_title" autocomplete="off" value="{{$value->meta_title}}" required/>
                                <label>Meta Title <span class="text-danger">*</span></label>
                            </div>
                        </div>
                        <div class="col-12 col-md-12 mb-0">
                            <div class="input-group">  
                                <input type="text" class="meta_keywords" autocomplete="off" value="{{$value->meta_keyword}}" required/>
                                <label>Meta Keywords<span class="text-danger">*</span></label>
                            </div>
                        </div>
                        <div class="col-12 col-md-12 mb-0">
                            <label class="" style="color:#414244;"><b>Meta Description</b><span class="text-danger">*</span></label>
                            <div class="form-group mt-4"> 
                                <div class="form-line">  
                                    <textarea class="form-control meta_desc p-3" name="meta_desc" rows="4" placeholder="Meta Description">{{$value->meta_descr}}  </textarea>
                                </div>
                            </div>
                        </div>
                        <input type="hidden" class="sub_category_id" value="{{$value->sub_category_id}}">
                        <div class="col-12 col-md-4 mb-0">
                            <p class="text-danger mb-0">* All fields are Mandatory</p>
                        </div>
                    </div>
                    @endforeach
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb-0 text-center">
                            <div class="form-group">
                                <div class="form-line">
                                    <div class="cat_error_msg col-red w-100" style="display:none"></div>
                                    <div class="cat_success_msg col-green w-100" style="display:none"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 text-center">
                            <button type="submit" class="btn btn-primary waves-effect update_sub_category">UPDATE</button>                                                                
                        </div>
                    </div>    
                </div>
            </div>
        </div>
    </div>
</section>
@endsection