<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        'validate_login','save_country','update_country','save_region','update_region','updateLeadAllocations','deleteLeadAllocations','save_category','update_category','save_user','update_user','save_sub_category','update_sub_category','save_company','update_company','save_role_permissions','update_role_permissions','update_region_status','update_country_status','update_category_status','update_sub_category_status','update_company_status','update_user_status','save_report','update_report','update_report_status','save_custom_report','update_custom_report','update_custom_report_status','save_lead_comment','save_remainder','save_article','update_article','update_article_status','save_case_study','update_case_study','update_cs_status','save_infographic','update_infographic','update_infographic_status','save_news','update_news','update_news_status','update_password','update_mail_config','update_email_status','save_mail_config','save_press_release','update_press_release','update_press_release_status'
    ];
}
