<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\mail;
use App\master;
use Carbon\Carbon;
use Redirect;
use Session;
use DB;
use Config;

class mailController extends Controller
{
    public function __construct(){
        $this->mail = new mail();
        $this->master = new master();
    }

    public function mail_config(Request $request){
        if (!session('username')) {
	    	return redirect('/');
        } 
        $search_key = '';
        if(isset($_GET['search_key'])){
            $search_key=$_GET['search_key'];
        }
        $mail_config = $this->mail->mail_config($search_key);
        
        return view('mailConfig.list')->with('mail_config',$mail_config)->with('search_key',$search_key);
    }

    public function add_mail_config(){
        if (!session('username')) {
	    	return redirect('/');
        } 
        return view('mailConfig.add');
    }

    public function save_mail_config(Request $request){
        if (!session('username')) {
	    	return redirect('/');
        } 
        $data = $request->all();
        $save_mail_config = $this->mail->save_mail_config($data);
        
        if($save_mail_config == 1){
            return redirect('/mail_config')->with('message','Configuration Added Successfully');
        }
        else{
            return redirect('/mail_config')->with('error','Something went wrong..!!');
        }
    }

    public function edit_email_config($id){
        if (!session('username')) {
	    	return redirect('/');
        } 

        $edit_email_config = $this->mail->edit_email_config($id);
        return view('mailConfig.edit')->with('edit_email_config',$edit_email_config);
    }

    public function update_mail_config(Request $request){
        if (!session('username')) {
	    	return redirect('/');
        } 
        $data = $request->all();
        $update_mail_config = $this->mail->update_mail_config($data);

        if($update_mail_config == 1){
            return redirect('/mail_config')->with('message','Configuration Updated Successfully');
        }
        else{
            return redirect('/mail_config')->with('error','Something went wrong..!!');
        }
    }

    public function update_email_status(Request $request){
        if (!session('username')) {
	    	return redirect('/');
        } 
        $data = $request->all();
        $update_email_status = $this->mail->update_email_status($data);
        return $update_email_status;
    }

    public function mail_template_new(Request $request){
        set_time_limit(300);
        $user_id = Session::get('user_id');
        $role_id = Session::get('roleid');

        $loopCount = 0;
        $success_Mails_Array = [];
        $failed_MailsArray = [];
        $getMailUsers = $this->mail->getMailUsers();
        $length = sizeof($getMailUsers);
        
        foreach($getMailUsers as $user){
            $email_id = $user->email_id;
            $email_cat = $user->email_cat;
            $email_subject = $user->email_subject;
            $email_type = $user->email_type;    
            $email_body = $user->email_body;
            $email_from = $user->email_from; 
            $email_to = $user->email_to; 
            $email_from = "rakeshm@auroraelabs.com";

            if($email_to != '' || $email_to != null){
                // $get_my_team = $this->Mail->get_my_team($user_id);
                // $getMasterUnits = $this->Mail->getMasterUnits($user_unit);

                // $last7days = [];
                // $m = date("m"); $de= date("d"); $y= date("Y");
                // for($i=1; $i<=7; $i++){
                //     $last7days[] = date('Y-m-d', mktime(0,0,0,$m,($de-$i),$y)); 
                // }
                // $getPatientData = $this->Mail->getPatientData($get_my_team,$user_global_view,$user_unit,$last7days);

                // if(sizeof($getPatientData)>0){
                //     $patientDataResult = $this->prepareMailDataNew($getMasterUnits,$getPatientData);
                // }
                // else{
                //     $patientDataResult = [];
                // }
                
                // $last7days = array_reverse($last7days);
                // $fromDate = date('d M Y',strtotime(reset($last7days)));
                // $toDate = date('d M Y',strtotime(end($last7days)));
                // $mailTemplate = view('mailTemplate', compact('getMasterUnits','fromDate','toDate','user_name','patientDataResult'))->render();

                    
                $mailToArray = explode(',', $email_to);
                
                $mailto="";
                foreach($mailToArray as $mailTo){
                    $mailto="$mailTo";

                    if (filter_var($mailto, FILTER_VALIDATE_EMAIL)) 
                    {
                        $email = new \SendGrid\Mail\Mail(); 
                        $email->setFrom($email_from, "TMR");
                        $email->setSubject($email_subject);
                        $email->addTo($mailto);
                        // $email->addContent("text/plain", "and easy to do anywhere, even with PHP");
                        $email->addContent(
                            "text/html", "$email_body"
                        );
                
                        $sendgrid = new \SendGrid(getenv('SENDGRID_API_KEY'));
                        try {
                            $response = $sendgrid->send($email);
                            array_push($success_Mails_Array,$email_id);
                            // $response = $sendgrid->CRMemailalerts($email);
                            // print $response->statusCode() . "\n";
                            // print_r($response->headers());
                            // print $response->body() . "\n";
                        } catch (Exception $e) {
                            array_push($failed_MailsArray,$email_id);
                            echo 'Caught exception: '. $e->getMessage() ."\n";
                        }
                    } 
                    else{
                        array_push($failed_MailsArray,$email_id);
                    }
                }
            }
            else{
                array_push($failed_MailsArray,$email_id);
            }
            $loopCount++;
        }
        if($loopCount == $length){
            $successCount = sizeof($success_Mails_Array);
            $failedCount = sizeof($failed_MailsArray);
            // $mailTemplate = view('mailLog', compact('success_Mails_Array','failed_MailsArray','successCount','failedCount'))->render();
            
            $mailTemplate = "Mail sent Successully";

            $subject = "Log For TMR";
            
            $mailto="rakeshm@auroraelabs.com";
            $email = new \SendGrid\Mail\Mail(); 
            $email->setFrom($email_from, "TMR");
            $email->setSubject($subject);
            $email->addTo($mailto);
            // $email->addTo("anilteja@auroraelabs.com");
            // $res = "Mails Sent - " .$loopCount;
            // $email->addContent("text/plain", "$res");
            $email->addContent(
                "text/html","$mailTemplate"
            );
        
            $sendgrid = new \SendGrid(getenv('SENDGRID_API_KEY'));
            try {
                $response = $sendgrid->send($email);
                // $response = $sendgrid->CRMemailalerts($email);
            } catch (Exception $e) {
                echo 'Caught exception: '. $e->getMessage() ."\n";
            }
            echo 'successCount - '.$successCount;
            echo 'failedCount - '.$failedCount;
            // return 'Success';
            // return view('mailLog')->with('success_Mails_Array',$success_Mails_Array)->with('failed_MailsArray',$failed_MailsArray)->with('successCount',$successCount)->with('failedCount',$failedCount);
        }
        else{
            return 'Error';
        }
    }
}
