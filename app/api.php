<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Session;
use Config;
use Carbon\Carbon;

class api extends Model
{
    protected $table = 'reports';
    
    public function getReports($publish_status,$date){
        $getReports = DB::table('reports')
                    // ->select('rep_id','rep_name','rep_url','rep_type','rep_pub_date','rep_status')
                    ->where('rep_type','=',$publish_status)
                    ->whereDate('rep_pub_date','=',$date)
                    // ->whereDate('rep_pub_date','=', '2011-11-09')
                    ->orderBy('rep_id','DESC')
                    ->limit(1000)
                    ->get();

        if(sizeof($getReports)>0){
            return $getReports;
        }
        else{
            return [];
        }
    }
}
