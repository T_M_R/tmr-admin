// for regions=====================================

$('.add_region_modal_btn').click(function () {
    $('.region_name').val('');
    // $('.add_continent_short_name').val('');
    $('.region_id').val('');
    $('.add_update_flag').val(1);
    $('#addRegionModal').modal('show');
})

$('.save_region').on('click', function () {
    var region_name = $('.region_name').val();
    // var region_short_name = $('.region_short_name').val();
    var add_update_flag = 1;
    var region_id = $('.region_id').val();

    console.log(region_id);
    console.log(region_name);
    // console.log(region_short_name);
    console.log(add_update_flag);
    // return false;
    if (region_name != '' && region_name != undefined && region_name != null) {
        $.ajax({
            url: '/region_duplicate_check',
            type: "GET",
            data: {
                region_name: region_name,
                region_id: region_id,
                add_update_flag: add_update_flag
            },
            success: function (result) {
                console.log(result);
                // return false;
                if (result == 0) {
                    $.ajax({
                        url: '/save_region',
                        type: "POST",
                        data: {
                            region_name: region_name,
                            // region_short_name : region_short_name,
                            add_update_flag: add_update_flag
                        },
                        success: function (result) {
                            console.log(result);
                            // return false
                            if (result == 1) {
                                $('.region_success_msg').css({ 'display': 'block' });
                                $('.region_success_msg').text('Region Saved successfully.');
                                setTimeout(() => {
                                    $('.region_success_msg').css({ 'display': 'none' });
                                    window.location.href = document.location.origin + '/regions';
                                }, 1500);
                            }
                            else {
                                $('.region_error_msg').css({ 'display': 'block' });
                                $('.region_error_msg').text('Something Went Wrong.Pls try again later..!!');
                                setTimeout(() => {
                                    $('.region_error_msg').css({ 'display': 'none' });
                                }, 3000);
                                return false;
                            }
                        }
                    });
                }
                else {
                    $('.region_error_msg').css({ 'display': 'block' });
                    $('.region_error_msg').text('Region Name already exists.');
                    setTimeout(() => {
                        $('.region_error_msg').css({ 'display': 'none' });
                    }, 3000);
                    return false;
                }
            }
        });
    } else {
        $('.region_error_msg').css({ 'display': 'block' });
        $('.region_error_msg').text('All Fields Are Required.');
        setTimeout(() => {
            $('.region_error_msg').css({ 'display': 'none' });
            // location.reload();
        }, 3000);
        return false;
    }
})

$('.edit_region_modal_btn').click(function () {
    var region_id = $(this).val();
    var region_name = $(this).parent().find('.region_name').val();
    // var continent_short_name = $(this).parent().find('.continent_short_name').val();
    console.log(region_id);
    console.log($(this).parent().find('.region_name').val());
    console.log(region_name);
    $('.region_name').val('');
    if (region_id != '' && region_name != '') {
        $('.modal-title').text('Edit Region');
        $('.region_name').val(region_name);
        $('.region_id').val(region_id);
        $('.add_update_flag').val(2);
        $('.addRegionModal').modal('show');
    }
    else {

    }
})

$('.update_region').on('click', function () {
    var region_name = $('.region_name').val();
    // var region_short_name = $('.region_short_name').val();
    var add_update_flag = 2;
    var region_id = $('.region_id').val();

    console.log(region_id);
    console.log(region_name);
    // console.log(region_short_name);
    console.log(add_update_flag);
    // return false;
    if (region_id != '' && region_id != undefined && region_id != null && region_name != '' && region_name != undefined && region_name != null) {
        $.ajax({
            url: '/region_duplicate_check',
            type: "GET",
            data: {
                region_name: region_name,
                region_id: region_id,
                add_update_flag: add_update_flag
            },
            success: function (result) {
                console.log(result);
                // return false;
                if (result == 0) {
                    $.ajax({
                        url: '/update_region',
                        type: "POST",
                        data: {
                            region_name: region_name,
                            // region_short_name : region_short_name,
                            region_id: region_id,
                            add_update_flag: add_update_flag
                        },
                        success: function (result) {
                            console.log(result);
                            // return false
                            if (result == 1) {
                                $('.region_success_msg').css({ 'display': 'block' });
                                $('.region_success_msg').text('Region Updated successfully.');
                                setTimeout(() => {
                                    $('.region_success_msg').css({ 'display': 'none' });
                                    window.location.href = document.location.origin + '/regions';
                                }, 1500);
                            }
                            else {
                                $('.region_error_msg').css({ 'display': 'block' });
                                $('.region_error_msg').text('Something Went Wrong.Pls try again later..!!');
                                setTimeout(() => {
                                    $('.region_error_msg').css({ 'display': 'none' });
                                }, 3000);
                                return false;
                            }
                        }
                    });
                }
                else {
                    $('.region_error_msg').css({ 'display': 'block' });
                    $('.region_error_msg').text('Region Name already exists.');
                    setTimeout(() => {
                        $('.region_error_msg').css({ 'display': 'none' });
                    }, 3000);
                    return false;
                }
            }
        });
    } else {
        $('.region_error_msg').css({ 'display': 'block' });
        $('.region_error_msg').text('All Fields Are Required.');
        setTimeout(() => {
            $('.region_error_msg').css({ 'display': 'none' });
            // location.reload();
        }, 3000);
        return false;
    }
})

$('.region_toggle_event').change(function (e) {
    // $('.console-event').html('Toggle: ' $(this).prop('checked'))
    var status = $(this).prop('checked');
    console.log(status)
    if (status == true) {
        var hidden_status = $(this).parent().parent().find('.hidden_status').val(1);
    }
    else {
        var hidden_status = $(this).parent().parent().find('.hidden_status').val(0);
    }

    var region_id = $(this).parent().parent().find('.region_id').val();
    var hidden_status = $(this).parent().parent().find('.hidden_status').val();
    console.log(region_id);
    console.log(hidden_status);
    if (region_id != '' && region_id != undefined && region_id != null) {
        $.ajax({
            url: '/update_region_status',
            type: "POST",
            data: {
                region_id: region_id,
                hidden_status: hidden_status,
            },
            success: function (result) {
                console.log(result);
                // return false
                if (result == 1) {
                    // $('.region_success_msg').css({'display':'block'});
                    // $('.region_success_msg').text('Region Updated successfully.');
                    showSuccessMessage();
                    // setTimeout(() => {
                    //     $('.region_success_msg').css({'display':'none'});
                    //     window.location.href = document.location.origin+'/regions';
                    // }, 1500);
                }
                else {
                    // $('.region_error_msg').css({'display':'block'});
                    // $('.region_error_msg').text('Something Went Wrong.Pls try again later..!!');
                    showCancelMessage();
                    setTimeout(() => {
                        $('.region_error_msg').css({ 'display': 'none' });
                    }, 3000);
                    return false;
                }
            }
        });
    } else {
        $('.status_change_msg').css({ 'display': 'block' });
        $('.status_change_msg').text('Record id not found');
        setTimeout(() => {
            $('.status_change_msg').css({ 'display': 'none' });
            // location.reload();
        }, 3000);
        return false;
    }
})

// For Country =============================
$('.save_country').on('click', function () {
    var country_name = $('.country_name').val();
    var country_code = $('.country_code').val();
    var region_id = $('.country_region_id option:selected').val();
    var add_update_flag = 1;
    var country_id = $('.country_id').val();

    console.log(country_id);
    console.log(country_name);
    console.log(region_id);
    console.log(add_update_flag);
    // return false;
    if (country_name != '' && country_name != undefined && country_name != null && region_id != '' && region_id != undefined && region_id != null) {
        $.ajax({
            url: '/country_duplicate_check',
            type: "GET",
            data: {
                country_name: country_name,
                country_id: country_id,
                add_update_flag: add_update_flag
            },
            success: function (result) {
                console.log(result);
                // return false;
                if (result == 0) {
                    $.ajax({
                        url: '/save_country',
                        type: "POST",
                        data: {
                            country_name: country_name,
                            country_code: country_code,
                            region_id: region_id,
                            add_update_flag: add_update_flag
                        },
                        success: function (result) {
                            console.log(result);
                            // return false
                            if (result == 1) {
                                $('.country_success_msg').css({ 'display': 'block' });
                                $('.country_success_msg').text('Country Saved successfully.');
                                setTimeout(() => {
                                    $('.country_success_msg').css({ 'display': 'none' });
                                    window.location.href = document.location.origin + '/countries';
                                }, 1500);
                            }
                            else {
                                $('.country_error_msg').css({ 'display': 'block' });
                                $('.country_error_msg').text('Something Went Wrong.Pls try again later..!!');
                                setTimeout(() => {
                                    $('.country_error_msg').css({ 'display': 'none' });
                                }, 3000);
                                return false;
                            }
                        }
                    });
                }
                else {
                    $('.country_error_msg').css({ 'display': 'block' });
                    $('.country_error_msg').text('Country Name already exists.');
                    setTimeout(() => {
                        $('.country_error_msg').css({ 'display': 'none' });
                    }, 3000);
                    return false;
                }
            }
        });
    } else {
        $('.country_error_msg').css({ 'display': 'block' });
        $('.country_error_msg').text('All Fields Are Required.');
        setTimeout(() => {
            $('.country_error_msg').css({ 'display': 'none' });
            // location.reload();
        }, 3000);
        return false;
    }
})

$('.update_country').on('click', function () {
    var country_name = $('.country_name').val();
    var country_code = $('.country_code').val();
    var region_id = $('.country_region_id option:selected').val();
    var add_update_flag = $('.add_update_flag').val();
    var country_id = $('.country_id').val();

    console.log(country_id);
    console.log(country_name);
    console.log(region_id);
    console.log(add_update_flag);
    // return false;
    if (country_name != '' && country_name != undefined && country_name != null && region_id != '' && region_id != undefined && region_id != null) {
        $.ajax({
            url: '/country_duplicate_check',
            type: "GET",
            data: {
                country_name: country_name,
                country_id: country_id,
                add_update_flag: add_update_flag
            },
            success: function (result) {
                console.log(result);
                // return false;
                if (result == 0) {
                    $.ajax({
                        url: '/update_country',
                        type: "POST",
                        data: {
                            country_name: country_name,
                            country_code: country_code,
                            region_id: region_id,
                            country_id: country_id,
                            add_update_flag: add_update_flag
                        },
                        success: function (result) {
                            console.log(result);
                            // return false
                            if (result == 1) {
                                $('.country_success_msg').css({ 'display': 'block' });
                                $('.country_success_msg').text('Country Updated Successfully.');
                                setTimeout(() => {
                                    $('.country_success_msg').css({ 'display': 'none' });
                                    window.location.href = document.location.origin + '/countries';
                                }, 1500);
                            }
                            else {
                                $('.country_error_msg').css({ 'display': 'block' });
                                $('.country_error_msg').text('Something Went Wrong.Pls try again later..!!');
                                setTimeout(() => {
                                    $('.country_error_msg').css({ 'display': 'none' });
                                }, 3000);
                                return false;
                            }
                        }
                    });
                }
                else {
                    $('.country_error_msg').css({ 'display': 'block' });
                    $('.country_error_msg').text('Country Name already exists.');
                    setTimeout(() => {
                        $('.country_error_msg').css({ 'display': 'none' });
                    }, 3000);
                    return false;
                }
            }
        });
    } else {
        $('.country_error_msg').css({ 'display': 'block' });
        $('.country_error_msg').text('All Fields Are Required.');
        setTimeout(() => {
            $('.country_error_msg').css({ 'display': 'none' });
            // location.reload();
        }, 2000);
        return false;
    }
})

$('.country_toggle_event').change(function (e) {
    // $('.console-event').html('Toggle: ' $(this).prop('checked'))
    var status = $(this).prop('checked');
    console.log(status)
    if (status == true) {
        var hidden_status = $(this).parent().parent().find('.hidden_status').val(1);
    }
    else {
        var hidden_status = $(this).parent().parent().find('.hidden_status').val(0);
    }

    var country_id = $(this).parent().parent().find('.country_id').val();
    var hidden_status = $(this).parent().parent().find('.hidden_status').val();
    console.log(country_id);
    console.log(hidden_status);
    if (country_id != '' && country_id != undefined && country_id != null) {
        $.ajax({
            url: '/update_country_status',
            type: "POST",
            data: {
                country_id: country_id,
                hidden_status: hidden_status,
            },
            success: function (result) {
                console.log(result);
                // return false
                if (result == 1) {
                    showSuccessMessage();
                }
                else {
                    showCancelMessage();
                }
            }
        });
    } else {
        $('.status_change_msg').css({ 'display': 'block' });
        $('.status_change_msg').text('Record id not found');
        setTimeout(() => {
            $('.status_change_msg').css({ 'display': 'none' });
            // location.reload();
        }, 3000);
        return false;
    }
})

// For Categories==================

$('.add_category_modal_btn').click(function () {
    $('.category_name').val('');
    $('.category_id').val('');
    $('.add_update_flag').val(1);
    $('#addCategoryModal').modal('show');
})

$('.save_category').on('click', function () {
    var category_name = $('.category_name').val();
    var category_path = $('.category_path').val();
    var category_heading = $('.category_heading').val();
    var seo_pagename = $('.seo_pagename').val();
    var thumb_img = $('.thumb_img').val();
    var thumb_alt_text = $('.thumb_alt_text').val();
    var thumb_title_text = $('.thumb_title_text').val();
    var thumb_no_follow = $('.thumb_no_follow').val();
    var med_img = $('.med_img').val();
    var med_title_text = $('.med_title_text').val();
    var med_alt_text = $('.med_alt_text').val();
    var med_no_follow = $('.med_no_follow').val();
    var cat_top_descr = CKEDITOR.instances['cat_top_descr'].getData();
    var cat_bottom_descr = CKEDITOR.instances['cat_bottom_descr'].getData();
    var cat_home_descr = CKEDITOR.instances['cat_home_descr'].getData();
    var meta_title = $('.meta_title').val();
    var meta_keywords = $('.meta_keywords').val();
    var meta_desc = $('.meta_desc').val();
    var seo_header = $('.seo_header').val();
    var seo_footer = $('.seo_footer').val();
    var is_homepage = $('.is_homepage option:selected').val();
    var link_title = $('.link_title').val();
    var qweb_logo_code = $('.qweb_logo_code').val();
    var add_update_flag = 1;
    var category_id = '';

    if (category_name != '' && category_name != undefined && category_name != null && category_heading != undefined && category_heading != null && category_heading != '' && seo_pagename != undefined && seo_pagename != null && seo_pagename != '' && thumb_alt_text != '' && thumb_alt_text != undefined && thumb_alt_text != null && thumb_title_text != '' && thumb_title_text != undefined && thumb_title_text != null && thumb_no_follow != '' && thumb_no_follow != undefined && thumb_no_follow != null && med_alt_text != '' && med_alt_text != undefined && med_alt_text != null && med_title_text != '' && med_title_text != undefined && med_title_text != null && med_no_follow != '' && med_no_follow != undefined && med_no_follow != null && cat_home_descr != '' && cat_home_descr != undefined && cat_home_descr != null) {
        $.ajax({
            url: '/category_duplicate_check',
            type: "GET",
            data: {
                category_name: category_name,
                category_id: category_id,
                add_update_flag: add_update_flag
            },
            success: function (result) {
                console.log(result);
                if (result == 0) {
                    $.ajax({
                        url: '/save_category',
                        type: "POST",
                        data: {
                            category_name: category_name,
                            category_path: category_path,
                            category_heading: category_heading,
                            seo_pagename: seo_pagename,
                            thumb_img: thumb_img,
                            thumb_alt_text: thumb_alt_text,
                            thumb_title_text: thumb_title_text,
                            thumb_no_follow: thumb_no_follow,
                            med_img: med_img,
                            med_title_text: med_title_text,
                            med_alt_text: med_alt_text,
                            med_no_follow: med_no_follow,
                            cat_top_descr: cat_top_descr,
                            cat_bottom_descr: cat_bottom_descr,
                            cat_home_descr: cat_home_descr,
                            meta_title: meta_title,
                            meta_keywords: meta_keywords,
                            meta_desc: meta_desc,
                            seo_header: seo_header,
                            seo_footer: seo_footer,
                            is_homepage: is_homepage,
                            link_title: link_title,
                            qweb_logo_code: qweb_logo_code,
                            add_update_flag: add_update_flag
                        },
                        success: function (result) {
                            console.log(result);
                            // return false;
                            if (result == 1) {
                                $('.cat_success_msg').css({ 'display': 'block' });
                                $('.cat_success_msg').text('Category Saved successfully.');
                                setTimeout(() => {
                                    $('.cat_success_msg').css({ 'display': 'none' });
                                    window.location.href = document.location.origin + '/categories';
                                }, 1500);
                            }
                            else {
                                $('.cat_error_msg').css({ 'display': 'block' });
                                $('.cat_error_msg').text('Something Went Wrong.Pls try again later..!!');
                                setTimeout(() => {
                                    $('.cat_error_msg').css({ 'display': 'none' });
                                }, 3000);
                                return false;
                            }
                        }
                    });
                }
                else {
                    $('.cat_error_msg').css({ 'display': 'block' });
                    $('.cat_error_msg').text('Category Name already exists.');
                    setTimeout(() => {
                        $('.cat_error_msg').css({ 'display': 'none' });
                    }, 3000);
                    return false;
                }
            }
        });
    } else {
        $('.cat_error_msg').css({ 'display': 'block' });
        $('.cat_error_msg').text('All Fields Are Required.');
        setTimeout(() => {
            $('.cat_error_msg').css({ 'display': 'none' });
            // location.reload();
        }, 3000);
        return false;
    }
})

$('#save_category').on('submit', function (e) {
    e.preventDefault();
    $("#save_category").validate({
        ignore: [],
        rules: {
            category_name: "required",
            category_path: "required",
            category_heading: "required",
            seo_pagename: {
                required: true,
            },
            thumb_alt_text: {
                required: true
            },
            med_alt_text: {
                required: true
            },
            thumb_title_text: {
                required: true
            },
            med_title_text: {
                required: true
            },
            thumb_no_follow: {
                required: true
            },
            med_no_follow: {
                required: true
            },
            cat_top_descr: {
                required: true
            },
            cat_bottom_descr: {
                required: true
            },
            cat_home_descr: {
                required: true
            },
            seo_header: {
                required: true
            },
            seo_footer: {
                required: true
            }
        },
        messages: {
            category_name: "Category Name must not be empty",
            category_path: "Category Path must not be empty",
            category_heading: "Please enter the Category Heading",
            seo_pagename: {
                required: "Please enter the SEO Pagename",
            },
            thumb_alt_text: {
                required: "Please enter Thumb Alt Text",
            },
            med_alt_text: {
                required: "Please enter Media Alt Text",
            },
            thumb_title_text: {
                required: "Please enter Thumb Title Text",
            },
            med_title_text: {
                required: "Please enter Media Title Text",
            },
            thumb_no_follow: {
                required: "Please enter Thumb No Flow",
            },
            med_no_follow: {
                required: "Please enter Media No Flow",
            },
            cat_top_descr: {
                required: "Please enter Category Top Description",
            },
            cat_bottom_descr: {
                required: "Please enter Category Bottom Description",
            },
            cat_home_descr: {
                required: "Please enter Category Home Description",
            },
            seo_header: {
                required: "Please enter SEO Header",
            },
            seo_footer: {
                required: "Please enter SEO Footer",
            }
        },
        submitHandler: function (form) {
            $.ajax({
                type: "POST",
                url: "save_category",
                data: $(form).serialize(),
                success: function (result) {
                    // console.log(result);
                    // return false;
                    if (result == 1) {
                        $('.cat_success_msg').css({ 'display': 'block' });
                        $('.cat_success_msg').text('Category Saved successfully.');
                        setTimeout(() => {
                            $('.cat_success_msg').css({ 'display': 'none' });
                            window.location.href = document.location.origin + '/categories';
                        }, 1500);
                    }
                    else {
                        $('.cat_error_msg').css({ 'display': 'block' });
                        $('.cat_error_msg').text('Something Went Wrong.Pls try again later..!!');
                        setTimeout(() => {
                            $('.cat_error_msg').css({ 'display': 'none' });
                        }, 3000);
                        return false;
                    }
                    // $(form).html("<div id='message'></div>");
                    // if (result['error'] === 0) {
                    //     $('.registration-modal-msg').html('');
                    //     $('.registration-modal-msg').addClass('d-none');
                    //     $('#message').html("<h4 class='text-center text-success'>" + result['message'] + "</h4>")
                    //         .hide()
                    //         .fadeIn(1500, function () {
                    //             $('#message').append("<div class='text-right'><button type='button' class='btn btn-secondary modal-close' data-dismiss='modal' onClick='window.location.reload();'>Close</button><div>");
                    //         });
                    // }
                    // else {
                    //     $('.registration-modal-msg').html('');
                    //     $('.registration-modal-msg').addClass('d-none');
                    //     $('#message').html("<h4 class='text-center text-danger'>" + result['message'] + "</h4>")
                    //         .hide()
                    //         .fadeIn(0, function () {
                    //             $('#message').append("<div class='text-right'><button type='button' class='btn btn-secondary modal-close' data-dismiss='modal' onClick='window.location.reload();'>Close</button><div>");
                    //         });
                    // }
                }
            });
            return false; // required to block normal submit since you used ajax
        }
    });
})

$('.update_category').on('click', function () {
    var category_name = $('.category_name').val();
    var category_path = $('.category_path').val();
    var category_heading = $('.category_heading').val();
    var seo_pagename = $('.seo_pagename').val();
    var thumb_img = $('.thumb_img').val();
    var thumb_alt_text = $('.thumb_alt_text').val();
    var thumb_title_text = $('.thumb_title_text').val();
    var thumb_no_follow = $('.thumb_no_follow').val();
    var med_img = $('.med_img').val();
    var med_title_text = $('.med_title_text').val();
    var med_alt_text = $('.med_alt_text').val();
    var med_no_follow = $('.med_no_follow').val();
    var cat_top_descr = CKEDITOR.instances['cat_top_descr'].getData();
    var cat_bottom_descr = CKEDITOR.instances['cat_bottom_descr'].getData();
    var cat_home_descr = CKEDITOR.instances['cat_home_descr'].getData();
    var meta_title = $('.meta_title').val();
    var meta_keywords = $('.meta_keywords').val();
    var meta_desc = $('.meta_desc').val();
    var seo_header = $('.seo_header').val();
    var seo_footer = $('.seo_footer').val();
    var is_homepage = $('.is_homepage option:selected').val();
    var link_title = $('.link_title').val();
    var qweb_logo_code = $('.qweb_logo_code').val();
    var add_update_flag = 2;
    var category_id = $('.category_id').val();

    console.log(category_id);
    console.log(category_name);
    console.log(add_update_flag);
    if (category_id != '' && category_id != undefined && category_id != null && category_name != '' && category_name != undefined && category_name != null && seo_pagename != '' && seo_pagename != undefined && seo_pagename != null) {
        $.ajax({
            url: '/category_duplicate_check',
            type: "GET",
            data: {
                category_name: category_name,
                category_id: category_id,
                add_update_flag: add_update_flag
            },
            success: function (result) {
                console.log(result);
                if (result == 0) {
                    $.ajax({
                        url: '/update_category',
                        type: "POST",
                        data: {
                            category_name: category_name,
                            category_path: category_path,
                            category_heading: category_heading,
                            seo_pagename: seo_pagename,
                            thumb_img: thumb_img,
                            thumb_alt_text: thumb_alt_text,
                            thumb_title_text: thumb_title_text,
                            thumb_no_follow: thumb_no_follow,
                            med_img: med_img,
                            med_title_text: med_title_text,
                            med_alt_text: med_alt_text,
                            med_no_follow: med_no_follow,
                            cat_top_descr: cat_top_descr,
                            cat_bottom_descr: cat_bottom_descr,
                            cat_home_descr: cat_home_descr,
                            meta_title: meta_title,
                            meta_keywords: meta_keywords,
                            meta_desc: meta_desc,
                            seo_header: seo_header,
                            seo_footer: seo_footer,
                            is_homepage: is_homepage,
                            link_title: link_title,
                            qweb_logo_code: qweb_logo_code,
                            category_id: category_id,
                            add_update_flag: add_update_flag
                        },
                        success: function (result) {
                            console.log(result);
                            if (result == 1) {
                                $('.cat_success_msg').css({ 'display': 'block' });
                                $('.cat_success_msg').text('Category Updated successfully.');
                                setTimeout(() => {
                                    $('.cat_success_msg').css({ 'display': 'none' });
                                    window.location.href = document.location.origin + '/categories';
                                }, 1500);
                            }
                            else {
                                $('.cat_error_msg').css({ 'display': 'block' });
                                $('.cat_error_msg').text('Something Went Wrong.Pls try again later..!!');
                                setTimeout(() => {
                                    $('.cat_error_msg').css({ 'display': 'none' });
                                }, 3000);
                                return false;
                            }
                        }
                    });
                }
                else {
                    $('.cat_error_msg').css({ 'display': 'block' });
                    $('.cat_error_msg').text('Category Name already exists.');
                    setTimeout(() => {
                        $('.cat_error_msg').css({ 'display': 'none' });
                    }, 3000);
                    return false;
                }
            }
        });
    } else {
        $('.cat_error_msg').css({ 'display': 'block' });
        $('.cat_error_msg').text('All Fields Are Required.');
        setTimeout(() => {
            $('.cat_error_msg').css({ 'display': 'none' });
            // location.reload();
        }, 3000);
        return false;
    }
})

$('.category_toggle_event').change(function (e) {
    // $('.console-event').html('Toggle: ' $(this).prop('checked'))
    var status = $(this).prop('checked');
    console.log(status)
    if (status == true) {
        var hidden_status = $(this).parent().parent().find('.hidden_status').val('Y');
    }
    else {
        var hidden_status = $(this).parent().parent().find('.hidden_status').val('N');
    }

    var category_id = $(this).parent().parent().find('.category_id').val();
    var hidden_status = $(this).parent().parent().find('.hidden_status').val();
    console.log(category_id);
    console.log(hidden_status);
    if (category_id != '' && category_id != undefined && category_id != null) {
        $.ajax({
            url: '/update_category_status',
            type: "POST",
            data: {
                category_id: category_id,
                hidden_status: hidden_status,
            },
            success: function (result) {
                console.log(result);
                // return false
                if (result == 1) {
                    showSuccessMessage();
                }
                else {
                    showCancelMessage();
                }
            }
        });
    } else {
        $('.status_change_msg').css({ 'display': 'block' });
        $('.status_change_msg').text('Record id not found');
        setTimeout(() => {
            $('.status_change_msg').css({ 'display': 'none' });
            // location.reload();
        }, 3000);
        return false;
    }
})

// $('.region_toggle_event').change(function(e) {
//     // $('.console-event').html('Toggle: ' $(this).prop('checked'))
//     var status = $(this).prop('checked');
//     console.log(status)
//     if(status == true)
//     {
//         var hidden_status = $(this).parent().parent().find('.hidden_status').val(1);
//     }
//     else
//     {
//         var hidden_status = $(this).parent().parent().find('.hidden_status').val(0);
//     }
// })

// For Sub Category=======================
$('.save_sub_category').on('click', function () {
    var category_id = $('.category_id option:selected').val();
    var sub_category_name = $('.sub_category_name').val();
    var sub_category_url = $('.sub_category_url').val();
    var sub_category_heading_h2 = $('.sub_category_heading_h2').val();
    var sub_category_short_desc = CKEDITOR.instances['sub_category_short_desc'].getData();
    var sub_category_desc = CKEDITOR.instances['sub_category_desc'].getData();
    var meta_title = $('.meta_title').val();
    var meta_keywords = $('.meta_keywords').val();
    var meta_desc = $('.meta_desc').val();
    var add_update_flag = 1;
    console.log(category_id);
    console.log(sub_category_name);
    console.log(sub_category_url);
    console.log(sub_category_heading_h2);
    console.log(sub_category_short_desc);
    console.log(sub_category_desc);
    console.log(meta_title);
    console.log(meta_keywords);
    console.log(meta_desc);
    // return false;
    if (category_id != '' && category_id != undefined && category_id != null && sub_category_name != '' && sub_category_name != undefined && sub_category_name != null && sub_category_url != '' && sub_category_url != undefined && sub_category_url != null && sub_category_heading_h2 != '' && sub_category_heading_h2 != undefined && sub_category_heading_h2 != null && sub_category_short_desc != '' && sub_category_short_desc != undefined && sub_category_short_desc != null && sub_category_desc != '' && sub_category_desc != undefined && sub_category_desc != null && meta_title != '' && meta_title != undefined && meta_title != null && meta_keywords != '' && meta_keywords != undefined && meta_keywords != null && meta_desc != '' && meta_desc != undefined && meta_desc != null) {
        $.ajax({
            url: '/save_sub_category',
            type: "POST",
            data: {
                category_id: category_id,
                sub_category_name: sub_category_name,
                sub_category_url: sub_category_url,
                sub_category_heading_h2: sub_category_heading_h2,
                sub_category_short_desc: sub_category_short_desc,
                sub_category_desc: sub_category_desc,
                meta_title: meta_title,
                meta_keywords: meta_keywords,
                meta_desc: meta_desc
            },
            success: function (result) {
                console.log(result);
                if (result == 1) {
                    $('.cat_success_msg').css({ 'display': 'block' });
                    $('.cat_success_msg').text('SubCategory Saved successfully.');
                    setTimeout(() => {
                        $('.cat_success_msg').css({ 'display': 'none' });
                        window.location.href = document.location.origin + '/sub-categories';
                    }, 1500);
                }
                else {
                    $('.cat_error_msg').css({ 'display': 'block' });
                    $('.cat_error_msg').text('Something Went Wrong.Pls try again later..!!');
                    setTimeout(() => {
                        $('.cat_error_msg').css({ 'display': 'none' });
                    }, 3000);
                    return false;
                }
            }
        });
    } else {
        $('.cat_error_msg').css({ 'display': 'block' });
        $('.cat_error_msg').text('All Fields Are Required.');
        setTimeout(() => {
            $('.cat_error_msg').css({ 'display': 'none' });
            // location.reload();
        }, 3000);
        return false;
    }
})

$('.update_sub_category').on('click', function () {
    var sub_category_id = $('.sub_category_id').val();
    var category_id = $('.category_id option:selected').val();
    var sub_category_name = $('.sub_category_name').val();
    var sub_category_url = $('.sub_category_url').val();
    var sub_category_heading_h2 = $('.sub_category_heading_h2').val();
    var sub_category_short_desc = CKEDITOR.instances['sub_category_short_desc'].getData();
    var sub_category_desc = CKEDITOR.instances['sub_category_desc'].getData();
    var meta_title = $('.meta_title').val();
    var meta_keywords = $('.meta_keywords').val();
    var meta_desc = $('.meta_desc').val();
    var add_update_flag = 2
    console.log(sub_category_id);
    console.log(category_id);
    console.log(sub_category_name);
    console.log(sub_category_url);
    console.log(sub_category_heading_h2);
    console.log(sub_category_short_desc);
    console.log(sub_category_desc);
    console.log(meta_title);
    console.log(meta_keywords);
    console.log(meta_desc);
    // return false;
    if (sub_category_id != '' && sub_category_id != undefined && sub_category_id != null && category_id != '' && category_id != undefined && category_id != null && sub_category_name != '' && sub_category_name != undefined && sub_category_name != null && sub_category_url != '' && sub_category_url != undefined && sub_category_url != null && sub_category_heading_h2 != '' && sub_category_heading_h2 != undefined && sub_category_heading_h2 != null && sub_category_short_desc != '' && sub_category_short_desc != undefined && sub_category_short_desc != null && sub_category_desc != '' && sub_category_desc != undefined && sub_category_desc != null && meta_title != '' && meta_title != undefined && meta_title != null && meta_keywords != '' && meta_keywords != undefined && meta_keywords != null && meta_desc != '' && meta_desc != undefined && meta_desc != null) {
        $.ajax({
            url: '/update_sub_category',
            type: "POST",
            data: {
                sub_category_id: sub_category_id,
                category_id: category_id,
                sub_category_name: sub_category_name,
                sub_category_url: sub_category_url,
                sub_category_heading_h2: sub_category_heading_h2,
                sub_category_short_desc: sub_category_short_desc,
                sub_category_desc: sub_category_desc,
                meta_title: meta_title,
                meta_keywords: meta_keywords,
                meta_desc: meta_desc
            },
            success: function (result) {
                console.log(result);
                if (result == 1) {
                    $('.cat_success_msg').css({ 'display': 'block' });
                    $('.cat_success_msg').text('SubCategory Updated successfully.');
                    setTimeout(() => {
                        $('.cat_success_msg').css({ 'display': 'none' });
                        window.location.href = document.location.origin + '/sub-categories';
                    }, 1500);
                }
                else {
                    $('.cat_error_msg').css({ 'display': 'block' });
                    $('.cat_error_msg').text('Something Went Wrong.Pls try again later..!!');
                    setTimeout(() => {
                        $('.cat_error_msg').css({ 'display': 'none' });
                    }, 3000);
                    return false;
                }
            }
        });
    } else {
        $('.cat_error_msg').css({ 'display': 'block' });
        $('.cat_error_msg').text('All Fields Are Required.');
        setTimeout(() => {
            $('.cat_error_msg').css({ 'display': 'none' });
            // location.reload();
        }, 3000);
        return false;
    }
})

$('.sub_category_toggle_event').change(function (e) {
    // $('.console-event').html('Toggle: ' $(this).prop('checked'))
    var status = $(this).prop('checked');
    console.log(status)
    if (status == true) {
        var hidden_status = $(this).parent().parent().find('.hidden_status').val(1);
    }
    else {
        var hidden_status = $(this).parent().parent().find('.hidden_status').val(0);
    }

    var sub_category_id = $(this).parent().parent().find('.sub_category_id').val();
    var hidden_status = $(this).parent().parent().find('.hidden_status').val();
    console.log(sub_category_id);
    console.log(hidden_status);
    if (sub_category_id != '' && sub_category_id != undefined && sub_category_id != null) {
        $.ajax({
            url: '/update_sub_category_status',
            type: "POST",
            data: {
                sub_category_id: sub_category_id,
                hidden_status: hidden_status,
            },
            success: function (result) {
                console.log(result);
                // return false
                if (result == 1) {
                    showSuccessMessage();
                }
                else {
                    showCancelMessage();
                }
            }
        });
    } else {
        $('.status_change_msg').css({ 'display': 'block' });
        $('.status_change_msg').text('Record id not found');
        setTimeout(() => {
            $('.status_change_msg').css({ 'display': 'none' });
            // location.reload();
        }, 3000);
        return false;
    }
})

// Companies==============================================
// For Sub Category=======================
$('.save_company').on('click', function () {
    var company_name = $('.company_name').val();
    var company_desc = CKEDITOR.instances['company_desc'].getData();
    var meta_title = $('.meta_title').val();
    var meta_keywords = $('.meta_keywords').val();
    var meta_desc = $('.meta_desc').val();
    var add_update_flag = 1;
    var company_id = '';

    console.log(company_name);
    console.log(company_desc);
    console.log(meta_title);
    console.log(meta_keywords);
    console.log(meta_desc);
    // return false;
    if (company_name != '' && company_name != undefined && company_name != null && company_desc != '' && company_desc != undefined && company_desc != null) {
        $.ajax({
            url: '/company_duplicate_check',
            type: "GET",
            data: {
                company_name: company_name,
                company_id: company_id,
                add_update_flag: add_update_flag
            },
            success: function (result) {
                console.log(result);
                // return false;
                if (result == 0) {
                    $.ajax({
                        url: '/save_company',
                        type: "POST",
                        data: {
                            company_name: company_name,
                            company_desc: company_desc,
                            meta_title: meta_title,
                            meta_keywords: meta_keywords,
                            meta_desc: meta_desc
                        },
                        success: function (result) {
                            console.log(result);
                            if (result == 1) {
                                $('.company_success_msg').css({ 'display': 'block' });
                                $('.company_success_msg').text('Company Saved successfully.');
                                setTimeout(() => {
                                    $('.company_success_msg').css({ 'display': 'none' });
                                    window.location.href = document.location.origin + '/companies';
                                }, 1500);
                            }
                            else {
                                $('.company_error_msg').css({ 'display': 'block' });
                                $('.company_error_msg').text('Something Went Wrong.Pls try again later..!!');
                                setTimeout(() => {
                                    $('.company_error_msg').css({ 'display': 'none' });
                                }, 3000);
                                return false;
                            }
                        }
                    });
                }
                else {
                    $('.company_error_msg').css({ 'display': 'block' });
                    $('.company_error_msg').text('Company Name already exists.');
                    setTimeout(() => {
                        $('.company_error_msg').css({ 'display': 'none' });
                    }, 3000);
                    return false;
                }
            }
        });
    } else {
        $('.company_error_msg').css({ 'display': 'block' });
        $('.company_error_msg').text('All Fields Are Required.');
        setTimeout(() => {
            $('.company_error_msg').css({ 'display': 'none' });
            // location.reload();
        }, 3000);
        return false;
    }
})

$('.update_company').on('click', function () {
    var company_id = $('.compnay_id').val();
    var company_name = $('.company_name').val();
    var company_desc = CKEDITOR.instances['company_desc'].getData();
    var meta_title = $('.meta_title').val();
    var meta_keywords = $('.meta_keywords').val();
    var meta_desc = $('.meta_desc').val();
    var add_update_flag = 2;

    console.log(company_id);
    console.log(company_name);
    console.log(company_desc);
    console.log(meta_title);
    console.log(meta_keywords);
    console.log(meta_desc);
    // return false;
    if (company_id != '' && company_id != undefined && company_id != null && company_name != '' && company_name != undefined && company_name != null && company_desc != '' && company_desc != undefined && company_desc != null) {
        $.ajax({
            url: '/company_duplicate_check',
            type: "GET",
            data: {
                company_name: company_name,
                company_id: company_id,
                add_update_flag: add_update_flag
            },
            success: function (result) {
                console.log(result);
                // return false;
                if (result == 0) {
                    $.ajax({
                        url: '/update_company',
                        type: "POST",
                        data: {
                            company_id: company_id,
                            company_name: company_name,
                            company_desc: company_desc,
                            meta_title: meta_title,
                            meta_keywords: meta_keywords,
                            meta_desc: meta_desc
                        },
                        success: function (result) {
                            console.log(result);
                            if (result == 1) {
                                $('.company_success_msg').css({ 'display': 'block' });
                                $('.company_success_msg').text('Company Updated successfully.');
                                setTimeout(() => {
                                    $('.company_success_msg').css({ 'display': 'none' });
                                    window.location.href = document.location.origin + '/companies';
                                }, 1500);
                            }
                            else {
                                $('.company_error_msg').css({ 'display': 'block' });
                                $('.company_error_msg').text('Something Went Wrong.Pls try again later..!!');
                                setTimeout(() => {
                                    $('.company_error_msg').css({ 'display': 'none' });
                                }, 3000);
                                return false;
                            }
                        }
                    });
                }
                else {
                    $('.company_error_msg').css({ 'display': 'block' });
                    $('.company_error_msg').text('Company Name already exists.');
                    setTimeout(() => {
                        $('.company_error_msg').css({ 'display': 'none' });
                    }, 3000);
                    return false;
                }
            }
        });
    } else {
        $('.company_error_msg').css({ 'display': 'block' });
        $('.company_error_msg').text('All Fields Are Required.');
        setTimeout(() => {
            $('.company_error_msg').css({ 'display': 'none' });
            // location.reload();
        }, 3000);
        return false;
    }
})

$('.company_toggle_event').change(function (e) {
    // $('.console-event').html('Toggle: ' $(this).prop('checked'))
    var status = $(this).prop('checked');
    console.log(status)
    if (status == true) {
        var hidden_status = $(this).parent().parent().find('.hidden_status').val(1);
    }
    else {
        var hidden_status = $(this).parent().parent().find('.hidden_status').val(0);
    }

    var company_id = $(this).parent().parent().find('.company_id').val();
    var hidden_status = $(this).parent().parent().find('.hidden_status').val();
    console.log(company_id);
    console.log(hidden_status);
    if (company_id != '' && company_id != undefined && company_id != null) {
        $.ajax({
            url: '/update_company_status',
            type: "POST",
            data: {
                company_id: company_id,
                hidden_status: hidden_status,
            },
            success: function (result) {
                console.log(result);
                // return false
                if (result == 1) {
                    showSuccessMessage();
                }
                else {
                    showCancelMessage();
                }
            }
        });
    } else {
        $('.status_change_msg').css({ 'display': 'block' });
        $('.status_change_msg').text('Record id not found');
        setTimeout(() => {
            $('.status_change_msg').css({ 'display': 'none' });
            // location.reload();
        }, 3000);
        return false;
    }
});

function showSuccessMessage() {
    swal("Status Updated Successfully!", "", "success");
}

function showCancelMessage() {
    swal("Oops", "Something went wrong!", "error")
}
