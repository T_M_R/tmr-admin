@extends('layouts.header')
@section('body-content')
<section class="sub-category-list">
    <div class="block-header">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 my-auto">
                <h4 class="page-title m-0">Edit Custom Report</h4>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <a href="{{URL:: to('/custom_reports')}}">
                    <button type="button" class="btn btn-info float-right">Back</button>
                </a>
            </div>
        </div>
    </div>
    <div class="row clearfix mt-3">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="card project_widget">
                <div class="body mt-4 mb-4">
                    @if(isset($edit_custom_report) && sizeof($edit_custom_report)>0)
                        @foreach($edit_custom_report as $creport)
                            <div class="row">
                                <div class="col-12 col-md-12 mb-0">
                                    <div class="input-group">  
                                        <input type="text" class="custom_report_name" autocomplete="off" value="{{$creport->rep_name}}" required/>
                                        <label>Report Name <span class="text-danger">*</span></label>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6 mb-0">
                                    <div class="input-group">  
                                        <input type="text" class="custom_report_url" autocomplete="off" value="{{$creport->rep_url}}" required/>
                                        <label>Report URL <span class="text-danger">*</span></label>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6 mb-0">
                                    <div class="input-group">  
                                        <input type="number" class="custom_report_sul" autocomplete="off" value="{{$creport->rep_price_sul}}" required/>
                                        <label>Price-SUL <span class="text-danger">*</span></label>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6 mb-0">
                                    <div class="input-group">  
                                        <input type="number" class="custom_report_el" autocomplete="off" value="{{$creport->rep_price_el}}" required/>
                                        <label>Price-EL<span class="text-danger">*</span></label>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6 mb-0">
                                    <div class="input-group">  
                                        <input type="number" class="custom_report_cul" autocomplete="off" value="{{$creport->rep_price_cul}}" required/>
                                        <label>Price-CUL <span class="text-danger">*</span></label>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6 mb-0">
                                    <div class="input-group">
                                        <select class="floating-select custom_report_category_id" onclick="this.setAttribute('value', this.value);" value="" required>
                                            @if(sizeof($getCategories)>0)
                                                <option value=""></option>
                                                @foreach($getCategories as $val)
                                                    @if($val->category_id == $creport->cat_id)
                                                        <option value="{{$val->category_id}}" selected>{{$val->cat_name}}</option>
                                                    @else
                                                        <option value="{{$val->category_id}}">{{$val->cat_name}}</option>
                                                    @endif
                                                @endforeach
                                            @endif
                                        </select>
                                        <label>Category <span class="text-danger">*</span></label>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6 mb-0">
                                    <div class="input-group">  
                                        <input type="text" class="datepicker custom_report_publish_date" autocomplete="off" value="{{$creport->rep_pub_date}}" required/>
                                        <span style="position: absolute;top: 8px;right: 0;bottom: 0;"><i class="icon-calendar" style="font-size: 26px;"></i></span>
                                        <label>Publish Date <span class="text-danger">*</span></label>
                                    </div>
                                </div>
                                <input type="hidden" class="custom_report_id" value="{{$creport->rep_id}}">
                                <div class="col-12 col-md-12 mb-0">
                                    <p class="text-danger mb-0">* All fields are Mandatory</p>
                                </div>
                            </div>
                        @endforeach
                    @else
                        <div class="row">
                            <div class="col-md-12">
                                <h5 class="text-danger">Something went wrong while fetching Data from DB</h5>
                            </div>
                        </div>
                    @endif
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb-0 text-center">
                            <div class="form-group">
                                <div class="form-line">
                                    <div class="custom_report_error_msg col-red w-100" style="display:none"></div>
                                    <div class="custom_report_success_msg col-green w-100" style="display:none"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 text-center">
                            <button type="submit" class="btn btn-primary waves-effect update_custom_report">UPDATE</button>                                                                
                        </div>
                    </div>    
                </div>
            </div>
        </div>
    </div>
</section>
@endsection