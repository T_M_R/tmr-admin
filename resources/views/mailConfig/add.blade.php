@extends('layouts.header')
@section('body-content')
<section class="sub-category-list">
    <div class="block-header">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 my-auto">
                <h4 class="page-title m-0">Add Email configuration</h4>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <a href="{{URL:: to('/mail_config')}}">
                    <button type="button" class="btn btn-info float-right">Back</button>
                </a>
            </div>
        </div>
    </div>
    <div class="row clearfix mt-3">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="card project_widget">
                <div class="body mt-4 mb-4">
                    <form method="POST" action="{{url('save_mail_config')}}" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-12 col-md-12 mb-0">
                                <div class="input-group">  
                                    <input type="text" class="mail_category_name" name="mail_category_name" autocomplete="off" value="" required/>
                                    <label>Category <span class="text-danger">*</span></label>
                                </div>
                            </div>
                            <div class="col-12 col-md-12 mb-0">
                                <div class="input-group">  
                                    <input type="text" class="mail_to" name="mail_to" autocomplete="off" value="" required/>
                                    <label>Mail To <span class="text-danger">*</span></label>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 mb-0">
                                <div class="input-group">  
                                    <input type="text" class="mail_cc" name="mail_cc"  autocomplete="off" value=""/>
                                    <label>Mail CC </label>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 mb-0">
                                <div class="input-group">  
                                    <input type="text" class="mail_bcc" name="mail_bcc" autocomplete="off" value=""/>
                                    <label>Mail BCC </label>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 mb-0">
                                <div class="input-group">  
                                    <input type="text" class="mail_from" name="mail_from" autocomplete="off" value="" required/>
                                    <label>Mai From <span class="text-danger">*</span></label>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 mb-0">
                                <div class="input-group">  
                                    <input type="text" class="mail_subject" name="mail_subject" autocomplete="off" value="" required/>
                                    <label>Mail Subject<span class="text-danger">*</span></label>
                                </div>
                            </div>
                            <div class="col-12 col-md-12 mb-0">
                                <label class="" style="color:#414244;"><b>Mail Body</b><span class="text-danger">*</span></label>
                                <div class="input-group mt-5">
                                    <textarea class="mail_body ckeditor" name="mail_body" id="mail_body" ></textarea>
                                </div>
                            </div>
                            <div class="col-12 col-md-12 mb-0">
                                <p class="text-danger mb-0">* All fields are Mandatory</p>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb-0 text-center">
                                <div class="form-group">
                                    <div class="form-line">
                                        <div class="mail_error_msg col-red w-100" style="display:none"></div>
                                        <div class="mail_success_msg col-green w-100" style="display:none"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 text-center">
                                <button type="submit" class="btn btn-primary waves-effect">SUBMIT</button>                                                                
                            </div>
                        </div>    
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection