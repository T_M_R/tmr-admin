$('.cs_toggle_event').change(function (e) {
    // $('.console-event').html('Toggle: ' $(this).prop('checked'))
    var status = $(this).prop('checked');
    console.log(status)
    if (status == true) {
        var hidden_status = $(this).parent().parent().find('.hidden_status').val(1);
    }
    else {
        var hidden_status = $(this).parent().parent().find('.hidden_status').val(0);
    }

    var cs_id = $(this).parent().parent().find('.cs_id').val();
    var hidden_status = $(this).parent().parent().find('.hidden_status').val();
    console.log(cs_id);
    console.log(hidden_status);
    if (cs_id != '' && cs_id != undefined && cs_id != null) {
        $.ajax({
            url: '/update_cs_status',
            type: "POST",
            data: {
                cs_id: cs_id,
                hidden_status: hidden_status,
            },
            success: function (result) {
                console.log(result);
                // return false
                if (result == 1) {
                    showSuccessMessage();
                }
                else {
                    showCancelMessage();
                }
            }
        });
    } else {
        $('.cs_change_msg').css({ 'display': 'block' });
        $('.cs_change_msg').text('Record id not found');
        setTimeout(() => {
            $('.cs_change_msg').css({ 'display': 'none' });
            // location.reload();
        }, 3000);
        return false;
    }
})

function showSuccessMessage() {
    swal("Status Updated Successfully!", "", "success");
}

function showCancelMessage() {
    swal("Oops", "Something went wrong!", "error")
}