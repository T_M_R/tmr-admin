@extends('layouts.header')
@section('body-content')
<section class="regions-list">
    <div class="block-header">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 my-auto">
                <h4 class="page-title m-0">Lead Allocations</h4>
            </div>
             <?php 
                $user_id='';
                $category_id='';
                $country_id='';
                
                if(isset($_GET['user'])){
                    $user_id=$_GET['user'];
                }
                if(isset($_GET['category_id'])){
                    $category_id=$_GET['category_id'];
                }
                if(isset($_GET['country_id'])){
                    $country_id=$_GET['country_id'];
                }
            ?>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <div class="row">
                    <div class="col-sm-12 col-md-4 col-lg-4">
                        <div class="input-group m-0">
                            <select class="floating-select onchange-select" onchange="location = this.value;" required >
                                <option value="" disabled selected>User</option>
                                    @foreach($getUsers as $user)
                                        @if($user->id == $user_id)
                                            <option value="/lead_allocations?user={{$user->id}}&category_id={{$category_id}}&country_id={{$country_id}}" selected>{{$user->name}}</option>
                                        @else
                                            <option value="/lead_allocations?user={{$user->id}}&category_id={{$category_id}}&country_id={{$country_id}}">{{$user->name}}</option>
                                        @endif
                                    @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-4 col-lg-4">
                        <div class="input-group m-0">
                            <select class="floating-select onchange-select" onchange="location = this.value;" required >
                                <option value="" disabled selected>Category</option>
                                    @foreach($getCategories as $val)
                                        @if($val->category_id == $category_id)
                                            <option value="/lead_allocations?user={{$user_id}}&category_id={{$val->category_id}}&country_id={{$country_id}}" selected>{{$val->cat_name}}</option>
                                        @else
                                            <option value="/lead_allocations?user={{$user_id}}&category_id={{$val->category_id}}&country_id={{$country_id}}">{{$val->cat_name}}</option>
                                        @endif
                                    @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-4 col-lg-4 d-none">
                        <div class="input-group m-0">
                            <select class="floating-select onchange-select" onchange="location = this.value;" required >
                                <option value="" disabled selected>Country</option>
                                    @foreach($getCountriesList as $country)
                                        @if($country->id == $country_id)
                                            <option value="/lead_allocations?user={{$user_id}}&category_id={{$category_id}}&country_id={{$country->id}}" selected>{{$country->name}}</option>
                                        @else
                                            <option value="/lead_allocations?user={{$user_id}}&category_id={{$category_id}}&country_id={{$country->id}}">{{$country->name}}</option>
                                        @endif
                                    @endforeach
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 pl-0 my-auto">
                <form method="GET" action="{{url('/lead_allocations')}}" enctype="multipart/form-data" role="form" autocomplete="off">
                    <button type="submit" class="btn btn-clear">Clear</button>
                </form>
            </div>
        </div>
    </div>
    <?php 
        $sessionUser=Session::get('subMenuArray');  
        $val=[];
        $allRoutes=[];
        $addButtonStatus=0;
        $editButtonStatus=0;
        $viewButtonStatus=0;
        foreach($sessionUser as $PagePermission)
        {
            if(isset($PagePermission['route'])){
                if($PagePermission['route'] == "lead_allocations")
                {
                    $addButtonStatus=$PagePermission['is_add'];
                    $editButtonStatus=$PagePermission['is_edit'];
                    $viewButtonStatus=$PagePermission['is_view'];
                }
            }
        }
    ?>
    <div class="row clearfix mt-3">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            @if(isset($leadAllocations))
                @if(sizeof($leadAllocations)>0)
                <div class="card">
                    <div class="body p-0">
                        <div class="table-responsive">
                            <!-- <table class="table table-bordered table-striped table-hover js-basic-example dataTable"> -->
                            <table class="table table-bordered mb-0">
                                <thead>
                                    <tr>
                                        <th>S.No</th>
                                        <th>User Name</th>
                                        <th>Category</th>
                                        @if($editButtonStatus == 1 || $viewButtonStatus == 1)
                                        <th>Action</th>
                                        @endif
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i=1;?>
                                    @foreach($leadAllocations as $value)
                                        <tr>
                                            <td>{{$i}}</td>
                                            <td>{{$value->name}}</td>
                                            <td>
                                                @if($value->allocations != '')
                                                    {{$value->allocations}}
                                                @else
                                                    -----
                                                @endif    
                                            </td>
                                            @if($editButtonStatus == 1 || $viewButtonStatus == 1)
                                            <td>
                                                @if($editButtonStatus == 1)
                                                <a href="{{URL:: to('/leads_allocation_edit/'.$value->id)}}" class="text-decoration-none">
                                                    <button class="btn tblActnBtn">
                                                        <i class="material-icons">mode_edit</i>
                                                    </button>
                                                </a>
                                                @endif
                                                @if($viewButtonStatus == 1)
                                                <a href="{{URL:: to('/leads_allocation_view/'.$value->id)}}" class="text-decoration-none">
                                                    <button class="btn tblActnBtn">
                                                        <i class="fa fa-eye" aria-hidden="true"></i>
                                                    </button>
                                                </a>
                                                @endif
                                            </td>
                                            @endif
                                        </tr>
                                        <?php $i++;?>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                @else
                    <div class="card">
                        <p class="text-center py-5" style="font-size:25px">No Data Available To Show</p>  
                    </div>
                @endif
            @else
                <div class="card">
                    <p class="text-center py-5" style="font-size:25px">No Data Available To Show</p>  
                </div>
            @endif
        </div>
    </div>
</section>
@endsection