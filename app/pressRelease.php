<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Session;
use Config;
use Carbon\Carbon;

class pressRelease extends Model
{
    public function getPressReleases($search_key){
        $query = DB::table('tbl_press_release')
                ->select('id','name','url','status')
                ->orderBy('id','DESC');

        if($search_key != ''){
            $query->where('name','like','%'.$search_key.'%');
        }

        $getPressReleases = $query->paginate(30);
        if(sizeof($getPressReleases)>0){
            return $getPressReleases;
        }
        else{
            return [];
        }
    }

    public function save_press_release($data){
        $date = Carbon::now();
        $month_year = $date->format("Y-m");
        $t=time();
        // print_r($data);
        // exit;
        // $upload_date = strtotime( $data['article_publish_date']);
        $save_press_release = DB::table('tbl_press_release')->insert([
            'rep_id' => 0,
            'name' => $data['press_release_name'],
            'heading_h1' => $data['press_release_heading'],
            'url' => $data['press_release_url'],
            'category_id' => $data['press_release_category_id'],
            'short_desc' => $data['press_release_short_desc'],
            'full_desc' => $data['press_release_long_desc'],
            'upload_date' => $data['press_release_publish_date'],
            'meta_title' => $data['meta_title'],
            'meta_keywords' => $data['meta_keywords'],
            'meta_description' => $data['meta_desc'],
            'status' => $data['press_release_status'],
            'creation' => $t,
            'created_date' => $date,
            'modification' => $t,
            'modified_date' => $date,
        ]);
        
        if($save_press_release == 1){
            return 1;
        }
        else{
            return 0;
        }
    }

    public function getPressReleaseDataById($id){
        $getPressReleaseDataById = DB::table('tbl_press_release')
                            ->where('id','=',$id)
                            ->get();
        if(sizeof($getPressReleaseDataById)>0){
            return $getPressReleaseDataById;
        }
        else{
            return [];
        }
    }

     public function update_press_release($data){
        $date = Carbon::now();
        $t=time();
        // $upload_date = strtotime( $data['article_publish_date']);
        // $upload_date = date('Y-m-d H:i:s',$data['article_publish_date']);
        // print_r($upload_date);
        // exit;
        $update_press_release = DB::table('tbl_press_release')->where('id',$data['press_release_id'])->update([
            'rep_id' => 0,
            'name' => $data['press_release_name'],
            'heading_h1' => $data['press_release_heading'],
            'url' => $data['press_release_url'],
            'category_id' => $data['press_release_category_id'],
            'short_desc' => $data['press_release_short_desc'],
            'first_desc' => $data['press_release_first_desc'],
            'full_desc' => $data['press_release_long_desc'],
            'upload_date' => $data['press_release_publish_date'],
            'meta_title' => $data['meta_title'],
            'meta_keywords' => $data['meta_keywords'],
            'meta_description' => $data['meta_desc'],
            'status' => $data['press_release_status'],
            'modification' => $t,
            'modified_date' => $date,
        ]);
        
        if($update_press_release == 1){
            return 1;
        }
        else{
            return 0;
        }
    }

    public function update_press_release_status($data){
        $date = Carbon::now();
        $status='';
        if($data['hidden_status'] == 1){
            $status='Y';
        }
        else{
            $status='N';
        }
        $update_press_release_status = DB::table('tbl_press_release')->where('id','=',$data['press_release_id'])->update([
            'status' => $status,
            // 'updated_at' => $date
        ]);
        
        if($update_press_release_status == 1){
            return 1;
        }
        else{
            return 0;
        }
    }
}
