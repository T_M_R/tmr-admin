<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Session;
use Config;
use Carbon\Carbon;

class master extends Model
{
    public function getRegionsList($search_key){
        if($search_key != ''){
            $getRegionsList = DB::table('region')
                        ->where('name','like','%'.$search_key.'%')
                        ->orderBy('id','DESC')
                        ->paginate(30);
        }
        else{
            $getRegionsList = DB::table('region')
                        ->orderBy('id','DESC')
                        ->paginate(30);
        }
        
        if($getRegionsList){
            return $getRegionsList;
        }
        else{
            return [];
        }
    }

    public function region_duplicate_check($data){

        $region_name = $data['region_name'];
        $add_update_flag = $data['add_update_flag'];
        $region_id = $data['region_id'];

        if($add_update_flag == 1){
            $region_duplicate_check = DB::table('region')
                                    ->where('name','like','%'.$region_name.'%')
                                    ->count();
        }
        else{
            $region_duplicate_check = DB::table('region')
                                    ->where('name','like','%'.$region_name.'%')
                                    ->where('id','!=',$region_id)
                                    ->count();
        }
        if($region_duplicate_check == 0){
            return 0;
        }
        else{
            return 1;
        }
    }

    public function save_region($data){
        $date = Carbon::now();

        $save_region = DB::table('region')->insert([
            'name' => $data['region_name'],
            // 'continent_short_name' => $data['region_short_name'],
            'status' => 'Y',
            'created_at' => $date
        ]);
        
        if($save_region == 1){
            return 1;
        }
        else{
            return 0;
        }
    }

    public function getregionDataById($id) {
        $getregionDataById = DB::table('region')
                        ->where('id','=',$id)
                        ->get();
        if($getregionDataById){
            return $getregionDataById;
        }
        else{
            return [];
        }
    }

    public function update_region($data){
        $date = Carbon::now();

        $update_region = DB::table('region')->where('id','=',$data['region_id'])->update([
            'name' => $data['region_name'],
            // 'continent_short_name' => $data['region_short_name'],
            'updated_at' => $date
        ]);
        
        if($update_region == 1){
            return 1;
        }
        else{
            return 0;
        }
    }

    public function update_region_status($data){
        $date = Carbon::now();
        $status='';
        if($data['hidden_status'] == 1){
            $status='Y';
        }
        else{
            $status='N';
        }
        $update_region_status = DB::table('region')->where('id','=',$data['region_id'])->update([
            'status' => $status,
            'updated_at' => $date
        ]);
        
        if($update_region_status == 1){
            return 1;
        }
        else{
            return 0;
        }
    }

    // Countries ======================================
    public function getCountriesList($search_key){
        if($search_key != ''){
            $getCountriesList = DB::table('countries')
                        ->join('region','region.id','=','countries.region_id')
                        ->select('countries.id','countries.name','countries.country_code','countries.id','countries.country_status','region.name as region_name')
                        ->where('countries.name','like','%'. $search_key.'%')
                        ->orderBy('countries.id','DESC')
                        ->paginate(30);
        }
        else{
            $getCountriesList = DB::table('countries')
                        ->join('region','region.id','=','countries.region_id')
                        ->select('countries.id','countries.name','countries.country_code','countries.id','countries.country_status','region.name as region_name')
                        ->orderBy('countries.id','DESC')
                        ->paginate(30);
        }
        
        if($getCountriesList){
            return $getCountriesList;
        }
        else{
            return [];
        }
    }

    public function getRegionsListForCountries(){
        $getRegionsList = DB::table('region')
                        ->where('status','=','Y')
                        ->orderBy('id','DESC')
                        ->get();
        if($getRegionsList){
            return $getRegionsList;
        }
        else{
            return [];
        }
    }

    public function country_duplicate_check($data){

        $country_name = $data['country_name'];
        $add_update_flag = $data['add_update_flag'];
        $country_id = $data['country_id'];

        if($add_update_flag == 1){
            $country_duplicate_check = DB::table('countries')
                                    ->where('name','like','%'.$country_name.'%')
                                    ->count();
        }
        else{
            $country_duplicate_check = DB::table('countries')
                                    ->where('name','like','%'.$country_name.'%')
                                    ->where('id','!=',$country_id)
                                    ->count();
        }
        if($country_duplicate_check == 0){
            return 0;
        }
        else{
            return 1;
        }
    }

    public function save_country($data){
        $date = Carbon::now();

        $save_country = DB::table('countries')->insert([
            'name' => $data['country_name'],
            'country_code' => $data['country_code'],
            'region_id' => $data['region_id'],
            'country_status' => 'Y',
            'created_at' => $date
        ]);
        
        if($save_country == 1){
            return 1;
        }
        else{
            return 0;
        }

        // if($save_country){
        //     $save_country = DB::table('countries')->where('id','=',$save_country)->update(['country_unique_id' => $save_country]);
        //     if($save_country == 1){
        //         return 1;
        //     }
        //     else{
        //         return 0;
        //     }
        // }
        // else{
        //     return 0;
        // }
    }

    public function getcountryDataById($id) {
        $getcountryDataById = DB::table('countries')
                        ->join('region','region.id','=','countries.region_id')
                        ->select('countries.id','countries.name','countries.country_code','countries.region_id','countries.country_status','region.name as region_name')
                        ->where('countries.id','=',$id)
                        ->get();
        if($getcountryDataById){
            return $getcountryDataById;
        }
        else{
            return [];
        }
    }

    public function update_country($data){
        $date = Carbon::now();

        $update_country = DB::table('countries')->where('id','=',$data['country_id'])->update([
            'name' => $data['country_name'],
            'country_code' => $data['country_code'],
            'region_id' => $data['region_id'],
            'updated_at' => $date
        ]);
        
        if($update_country == 1){
            return 1;
        }
        else{
            return 0;
        }
    }

    public function update_country_status($data){
        $date = Carbon::now();
        $status='';
        if($data['hidden_status'] == 1){
            $status='Y';
        }
        else{
            $status='N';
        }
        $update_country_status = DB::table('countries')->where('id','=',$data['country_id'])->update([
            'country_status' => $status,
            'updated_at' => $date
        ]);
        
        if($update_country_status == 1){
            return 1;
        }
        else{
            return 0;
        }
    }
    // Categories=================
    public function getCategoriesList($search_key){
        if($search_key != ''){
            $getCategoriesList = DB::table('category')
                                ->where('cat_name','like','%'.$search_key.'%')
                                ->orderBy('category_id','DESC')
                                ->paginate(30);
        }
        else{
            $getCategoriesList = DB::table('category')
                                ->orderBy('category_id','DESC')
                                ->paginate(30);
        }
        
        if($getCategoriesList){
            return $getCategoriesList;
        }
        else{
            return [];
        }
    }

    public function category_duplicate_check($data){

        $category_name = $data['category_name'];
        $add_update_flag = $data['add_update_flag'];
        $category_id = $data['category_id'];

        if($add_update_flag == 1){
            $category_duplicate_check = DB::table('category')
                                    ->where('cat_name','like','%'.$category_name.'%')
                                    ->count();
        }
        else{
            $category_duplicate_check = DB::table('category')
                                    ->where('cat_name','like','%'.$category_name.'%')
                                    ->where('category_id','!=',$category_id)
                                    ->count();
        }
        if($category_duplicate_check == 0){
            return 0;
        }
        else{
            return 1;
        }
    }

    public function save_category($data){
        $date = Carbon::now();
        $save_category = DB::table('category')->insert([
            'cat_path' => $data['category_path'],
            'cat_name' => $data['category_name'],
            'cat_page_heading' => $data['category_heading'],
            'seo_pagename' => $data['seo_pagename'],
            'thumb_alt_text' => $data['thumb_alt_text'],
            'thumb_title_text' => $data['thumb_title_text'],
            'thumb_no_follow' => $data['thumb_no_follow'],
            'med_alt_text' => $data['med_alt_text'],
            'med_title_text' => $data['med_title_text'],
            'med_no_follow' => $data['med_no_follow'],
            'cat_top_descr' => $data['cat_top_descr'],
            'cat_bottom_descr' => $data['cat_bottom_descr'],
            'cat_home_descr' => $data['cat_home_descr'],
            'meta_title' => $data['meta_title'],
            'meta_keyword' => $data['meta_keywords'],
            'meta_descr' => $data['meta_desc'],
            'seo_header' => $data['seo_header'],
            'seo_footer' => $data['seo_footer'],
            'is_homepage' => $data['is_homepage'],
            'link_title' => $data['link_title'],
            'qweb_logo_code' => $data['qweb_logo_code'],
            'cat_status' => 'Y'
        ]);
        
        if($save_category == 1){
            return 1;
        }
        else{
            return 0;
        }
    }

    public function getCategoryDataById($id) {
        $getCategoryDataById = DB::table('category')
                            ->where('category_id','=',$id)
                            ->get();
        if($getCategoryDataById){
            return $getCategoryDataById;
        }
        else{
            return [];
        }
    }

    public function update_category($data){
        $date = Carbon::now();

        $update_category = DB::table('category')->where('category_id','=',$data['category_id'])->update([
            'cat_path' => $data['category_path'],
            'cat_name' => $data['category_name'],
            'cat_page_heading' => $data['category_heading'],
            'seo_pagename' => $data['seo_pagename'],
            'thumb_alt_text' => $data['thumb_alt_text'],
            'thumb_title_text' => $data['thumb_title_text'],
            'thumb_no_follow' => $data['thumb_no_follow'],
            'med_alt_text' => $data['med_alt_text'],
            'med_title_text' => $data['med_title_text'],
            'med_no_follow' => $data['med_no_follow'],
            'cat_top_descr' => $data['cat_top_descr'],
            'cat_bottom_descr' => $data['cat_bottom_descr'],
            'cat_home_descr' => $data['cat_home_descr'],
            'meta_title' => $data['meta_title'],
            'meta_keyword' => $data['meta_keywords'],
            'meta_descr' => $data['meta_desc'],
            'seo_header' => $data['seo_header'],
            'seo_footer' => $data['seo_footer'],
            'is_homepage' => $data['is_homepage'],
            'link_title' => $data['link_title'],
            'qweb_logo_code' => $data['qweb_logo_code']
        ]);
        
        if($update_category == 1){
            return 1;
        }
        else{
            return 0;
        }
    }

    public function update_category_status($data){
        $date = Carbon::now();
        $status='';
        if($data['hidden_status'] == 'Y'){
            $status='Y';
        }
        else{
            $status='N';
        }
        $update_category_status = DB::table('category')->where('category_id','=',$data['category_id'])->update([
            'cat_status' => $status
        ]);
        
        if($update_category_status == 1){
            return 1;
        }
        else{
            return 0;
        }
    }

    // For Sub category================================================
    public function getSubCategoriesList($search_key){
        if($search_key != ''){
            $getSubCategoriesList = DB::table('sub_category')
                            ->join('category','category.category_id','=','sub_category.category_id')
                            ->select('sub_category.*','category.cat_name as cat_master_name')
                            ->where('category.cat_name','like','%'.$search_key.'%')
                            ->orderBy('sub_category.sub_category_id','DESC')
                            ->paginate(30);
        }
        else{
            $getSubCategoriesList = DB::table('sub_category')
                            ->join('category','category.category_id','=','sub_category.category_id')
                            ->select('sub_category.*','category.cat_name as cat_master_name')
                            ->orderBy('sub_category.sub_category_id','DESC')
                            ->paginate(30);
        }
        
        if($getSubCategoriesList){
            return $getSubCategoriesList;
        }
        else{
            return [];
        }
    }

    public function getCategories(){
        $getCategories = DB::table('category')
                            ->where('cat_master_id','=',0)
                            ->where('cat_status','=','Y')
                            ->orderBy('category_id','DESC')
                            ->get();
        
        if($getCategories){
            return $getCategories;
        }
        else{
            return [];
        }
    }

    public function save_sub_category($data){
        $date = Carbon::now();

        $save_sub_category = DB::table('sub_category')->insert([
            'category_id' => $data['category_id'],
            'sub_category_name' => $data['sub_category_name'],
            'sub_category_heading' => $data['sub_category_heading_h2'],
            'sub_category_url' => $data['sub_category_url'],
            'short_description' => $data['sub_category_short_desc'],
            'description' => $data['sub_category_desc'],
            'meta_title' => $data['meta_title'],
            'meta_keyword' => $data['meta_keywords'],
            'meta_descr' => $data['meta_desc'],
            'status' => 'Y',
        ]);
        
        if($save_sub_category == 1){
            return 1;
        }
        else{
            return 0;
        }
    }

    public function getSubCategoryDataById($id) {
        $getSubCategoryDataById = DB::table('sub_category')
                            ->where('sub_category_id','=',$id)
                            ->get();
        if($getSubCategoryDataById){
            return $getSubCategoryDataById;
        }
        else{
            return [];
        }
    }

    public function update_sub_category($data){
        $date = Carbon::now();

        $update_sub_category = DB::table('sub_category')->where('sub_category_id','=',$data['sub_category_id'])->update([
            'category_id' => $data['category_id'],
            'sub_category_name' => $data['sub_category_name'],
            'sub_category_heading' => $data['sub_category_heading_h2'],
            'sub_category_url' => $data['sub_category_url'],
            'short_description' => $data['sub_category_short_desc'],
            'description' => $data['sub_category_desc'],
            'meta_title' => $data['meta_title'],
            'meta_keyword' => $data['meta_keywords'],
            'meta_descr' => $data['meta_desc'],
        ]);
        
        if($update_sub_category == 1){
            return 1;
        }
        else{
            return 0;
        }
    }

    public function update_sub_category_status($data){
        $date = Carbon::now();
        $status='';
        if($data['hidden_status'] == 1){
            $status='Y';
        }
        else{
            $status='N';
        }
        $update_sub_category_status = DB::table('sub_category')->where('sub_category_id','=',$data['sub_category_id'])->update([
            'status' => $status
        ]);
        
        if($update_sub_category_status == 1){
            return 1;
        }
        else{
            return 0;
        }
    }

    // Companies ==============================================
    public function getCompaniesList($search_key){
        if($search_key != ''){
            $getCompaniesList = DB::table('companies')
                                ->where('company_name','like','%'.$search_key.'%')
                                ->orderBy('company_id','DESC')
                                ->paginate(30);
        }
        else{
            $getCompaniesList = DB::table('companies')
                                ->orderBy('company_id','DESC')
                                ->paginate(30);
        }
        
        if($getCompaniesList){
            return $getCompaniesList;
        }
        else{
            return [];
        }
    }

    public function company_duplicate_check($data){
        $company_name = $data['company_name'];
        $add_update_flag = $data['add_update_flag'];
        $company_id = $data['company_id'];

        if($add_update_flag == 1){
            $company_duplicate_check = DB::table('companies')
                                    ->where('company_name','like','%'.$company_name.'%')
                                    ->count();
        }
        else{
            $company_duplicate_check = DB::table('companies')
                                    ->where('company_name','like','%'.$company_name.'%')
                                    ->where('company_id','!=',$company_id)
                                    ->count();
        }
        if($company_duplicate_check == 0){
            return 0;
        }
        else{
            return 1;
        }
    }

    public function save_company($data){
        $date = Carbon::now();

        $save_company = DB::table('companies')->insert([
            'company_name' => $data['company_name'],
            'company_description' => $data['company_desc'],
            'meta_title' => $data['meta_title'],
            'meta_keyword' => $data['meta_keywords'],
            'meta_description' => $data['meta_desc'],
            'on_search' => 'Y',
            'status' => 'Y',
            'created_at' => $date
        ]);
        
        if($save_company == 1){
            return 1;
        }
        else{
            return 0;
        }
    }

    public function getCompanyDataById($id) {
        $getCompanyDataById = DB::table('companies')
                            ->where('company_id','=',$id)
                            ->get();
        if($getCompanyDataById){
            return $getCompanyDataById;
        }
        else{
            return [];
        }
    }

    public function update_company($data){
        $date = Carbon::now();

        $update_comapny_status = DB::table('companies')->where('company_id','=',$data['company_id'])->update([
            'company_name' => $data['company_name'],
            'company_description' => $data['company_desc'],
            'meta_title' => $data['meta_title'],
            'meta_keyword' => $data['meta_keywords'],
            'meta_description' => $data['meta_desc'],
            'on_search' => 'Y',
            'updated_at' => $date
        ]);
        
        if($update_comapny_status == 1){
            return 1;
        }
        else{
            return 0;
        }
    }

    public function update_company_status($data){
        $date = Carbon::now();
        $status='';
        if($data['hidden_status'] == 1){
            $status='Y';
        }
        else{
            $status='N';
        }
        $update_company_status = DB::table('companies')->where('company_id','=',$data['company_id'])->update([
            'status' => $status,
            'updated_at' => $date
        ]);
        
        if($update_company_status == 1){
            return 1;
        }
        else{
            return 0;
        }
    }

    public function getSubCategories(){
        $getSubCategories = DB::table('sub_category')
                            ->where('status','=','Y')
                            ->orderBy('sub_category_id','ASC')
                            ->get();
        
        if($getSubCategories){
            return $getSubCategories;
        }
        else{
            return [];
        }
    }

    public function getCatSubcategories($data){
        $getCatSubcategories = DB::table('sub_category')
                            ->where('category_id','=',$data['report_category_id'])
                            // ->where('status','=','Y')
                            ->orderBy('sub_category_id','ASC')
                            ->get();
        
        if($getCatSubcategories){
            return $getCatSubcategories;
        }
        else{
            return [];
        }
    }

    public function getCompanies(){
        $getCompanies = DB::table('companies')
                            ->where('status','=','Y')
                            ->orderBy('company_id','ASC')
                            ->get();
        
        if($getCompanies){
            return $getCompanies;
        }
        else{
            return [];
        }
    }

    public function getAuthors(){
        $getAuthors = DB::table('authors')
                    ->select('author_name','author_id')
                    ->where('status','=','Y')
                    ->orderBy('author_id','ASC')
                    ->get();
        
        if($getAuthors){
            return $getAuthors;
        }
        else{
            return [];
        }
    }
}
