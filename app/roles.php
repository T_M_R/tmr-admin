<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use DB;
use Session;
use Config;
use Carbon\Carbon;

class roles extends Model
{
    public function getRoles(){
        $getRoles = DB::table('roles_new')
                    ->leftjoin('roles_new as rep_role','rep_role.role_id','=','roles_new.reporting_role_id')
                    ->select('roles_new.*','rep_role.role_name as rep_role_name')
                    ->orderBy('roles_new.id','ASC')
                    ->paginate(30);
        
        if($getRoles){
            return $getRoles;
        }
        else{
            return [];
        }
    }

    public function getPagesList(){
        $getpagesinfo = DB::table('master_pages')
                        ->select('id','name','route','is_view','is_edit','is_add','is_delete','is_download','parent_page_id')
                        ->get();

        if($getpagesinfo){
            return $getpagesinfo;  
        }
        else{
            return [];
        }
    }

    public function addNewRole($role_name,$reporting_role_id){
        $date = Carbon::now();
        $str_random = Str::random(6);
        $role_id = 'RL';
        $role_id = $role_id.'_'.$str_random;
        
        $addNewRole = DB::table('roles_new')->insertGetId([
                        'role_name'=>$role_name,
                        'reporting_role_id'=>$reporting_role_id,
                        'role_id'=>$role_id,
                        'created_at'=>$date
                    ]);
        if($addNewRole){
            return $addNewRole;
        }
        else{
            return 0;
        }
    }

    public function getRole_id($addNewRole){
        $getRole_id = DB::table('roles_new')
                    ->select('role_id')
                    ->where('id','=',$addNewRole)
                    ->get();
        if($getRole_id){
            return $getRole_id;
        }
        else{
            return 0;
        }
    }

    public function updateRolePermissions($mainArray){
        $updateRolePermissions = DB::table('role_permissions')->insert($mainArray);        
        if($updateRolePermissions){
            return $updateRolePermissions;
        }
        else{
            return $updateRolePermissions = [];
        }
    }

    public function getRoleName($id){
        $getRoleName = DB::table('roles_new')
                        ->select('role_name','role_id','reporting_role_id')
                        ->where('role_id','=',$id)
                        ->get();

        if(sizeof($getRoleName) > 0 ){
            return $getRoleName; 
        }
        else{
            return []; 
        }
    }

    public function editPermissions($id){
        $editPermissions = DB::table('role_permissions')
                            ->where('role_id','=',$id)
                            ->get();
        if(sizeof($editPermissions) > 0 ){
            return $editPermissions; 
        }
        else{
            return []; 
        }
    }

    public function updateRole($id,$reporting_role_id){
        $date = Carbon::now();
        $updateRole = DB::table('roles_new')->where('role_id','=',$id)->update( [
            'reporting_role_id'=>$reporting_role_id,
            'updated_at'=>$date 
        ] );
        if($updateRole){
            return 1;
        }
        else{
            return 0;
        }
    }

    public function deleteExistingPermissions($id){
        $deleteExistingPermissions = DB::table('role_permissions')->where('role_id','=',$id)->delete();
        return $deleteExistingPermissions;
    }
}
