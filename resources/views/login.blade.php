<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta content="width=device-width, initial-scale=1" name="viewport" />
	<title>TMR Admin</title>
	<!-- Favicon-->
	<link rel="icon" href="{{URL::asset('assets/images/project/favicon.ico')}}" type="image/x-icon">
	<!-- Plugins Core Css -->
	<link href="{{URL::asset('assets/css/app.min.css')}}" rel="stylesheet">
	<!-- Custom Css -->
	<link href="{{URL::asset('assets/css/style.css')}}" rel="stylesheet" />
    <link href="{{URL::asset('css/app.css')}}" rel="stylesheet">
	<link href="{{URL::asset('assets/css/pages/extra_pages.css')}}" rel="stylesheet" />
	<style>
		.login100-form-btn{
			background-color:#00bcd4 !important;
			border : none !important;
		}

		.login100-form-btn:hover{
			background-color:none !important;
		}

		.container-login100{
			background-image: url("../../../assets/images/project/tmr-login-bg.jpg");
			background-position:bottom;
		}

		.wrap-login100{
			display: block;
		}

		.login-form{
			float:right;
			/* width: 453px; */
			/* display: block; */
			background-color: #ffffff;
			padding: 20px 40px;
			/* margin: 30px; */
			border-radius: 20px;
		}
	</style>
</head>

<body>
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<form action="{{URL::to('/validate_login')}}" method="POST" class="login-form validate-form" enctype="multipart/form-data">
					<span class="login100-form-title p-b-45">
						Login
					</span>
                    @if(session()->has('error'))
                        <div class="badge col-red w-100 text-danger">
                            {{ session()->get('error') }}
                            <?php session()->forget('error'); ?>
                        </div>
                    @endif
                    <div class="col-12 col-md-12 mb-0 pl-0">
                        <div class="input-group">  
                            <input type="text" class="username" name="username" autocomplete="off" value="" required/>
                            <span class="focus-input100"></span>
                            <label>Email<span class="text-danger">*</span></label>
                        </div>
                    </div>
                    <div class="col-12 col-md-12 mb-0 pl-0">
                        <div class="input-group">  
                            <input type="password" class="password" name="password" autocomplete="off" value="" required/>
                            <span class="focus-input100"></span>
                            <label>Password<span class="text-danger">*</span></label>
                            <div class="float-right pt-3">
                                <a href="#" class="txt1">
                                    Forgot Password?
                                </a>
                            </div>
                        </div>
                    </div>
					<div class="container-login100-form-btn">
						<button class="login100-form-btn">
							Login
						</button>
					</div>
				</form>

				<div class="login100-more" style="background-image: url('../../assets/images/pages/bg-01.png');">
				</div>
			</div>
		</div>
	</div>

	<!-- Plugins Js -->
	<script src="{{URL::asset('assets/js/app.min.js')}}"></script>
	<!-- Extra page Js -->
	<script src="{{URL::asset('assets/js/pages/examples/pages.js')}}"></script>
</body>

</html>