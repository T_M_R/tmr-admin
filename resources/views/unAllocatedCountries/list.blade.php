@extends('layouts.header')
@section('body-content')
<section class="unallocated-list">
    <div class="block-header">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 my-auto">
                <h4 class="page-title m-0">Un-Allocated Country</h4>
            </div>
            <?php 
                $category_id='';
                if(isset($_GET['category_id'])){
                    $category_id=$_GET['category_id'];
                }
            ?>
            <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5 my-auto">
                <div class="input-group m-0">
                    <select class="floating-select onchange-select" name="category_id" id="category_id" onchange="location = this.value;" placeholder="Select Category" required >
                        <option value="" disabled selected> Select Category</option>
                            @foreach($getCategories as $val)
                            @if($val->category_id == $category_id)
                                <option value="/un_allocated_countries?category_id={{$val->category_id}}" selected>{{$val->cat_name}}</option>
                            @else
                                <option value="/un_allocated_countries?category_id={{$val->category_id}}">{{$val->cat_name}}</option>
                            @endif
                            @endforeach
                    </select>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-1 col-lg-1 pl-0 my-auto">
                <form method="GET" action="{{url('/un_allocated_countries')}}" enctype="multipart/form-data" role="form" autocomplete="off">
                    <button type="submit" class="btn btn-clear">Clear</button>
                </form>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3"></div>
        </div>
    </div>
    <div class="row clearfix mt-3">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            @if(isset($get_un_allocated_countries) && sizeof($get_un_allocated_countries)>0)
            <div class="card">
                <div class="body p-0">
                    <div class="table-responsive">
                        <!-- <table class="table table-bordered table-striped table-hover js-basic-example dataTable"> -->
                        <table class="table table-bordered mb-0">
                            <thead>
                                <tr>
                                    <th>S.No</th>
                                    <th>Category</th>
                                    <th>Region</th>
                                    <th>Country</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $i=1;?>
                                @foreach($get_un_allocated_countries as $value)
                                    <tr>
                                        <td>{{$i}}</td>
                                        <td>{{$value->category}}</td>
                                        <td>{{$value->continent_name}}</td>
                                        <td>
                                            @if($value->countries)    
                                                {{$value->countries}}
                                            @else
                                                -----
                                            @endif
                                        </td>
                                    </tr>
                                    <?php $i++;?>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            @else
                <div class="card">
                    <p class="text-center py-5" style="font-size:25px">No Data Available To Show</p>  
                </div>
            @endif
        </div>
    </div>
</section>
@endsection