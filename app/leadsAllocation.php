<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Session;
use Config;
use Carbon\Carbon;

class leadsAllocation extends Model
{
    public function __construct(){
        $this->adminRole = Config::get('constants.ADMIN_ROLE_ID');
        $this->bedRole = Config::get('constants.BDE_ROLE_ID');
    }

    public function getUsers($get_my_team,$user_id){
        $myteam = "" . implode(", ", $get_my_team) . "";

        $query = DB::table('users')
                    ->select('id','name')
                    ->where('status','=','Y')
                    ->groupBy('id','name');

        if($user_id != ''){
            $query->where('id','=',$user_id);
        }
        else{
            $query->whereIn('id',$get_my_team);
        }

        $getUsers = $query->get();
        
        if(sizeof($getUsers)>0){
            return $getUsers;
        }
        else{
            return [];
        }
    }

    public function getLeadAllocations($get_my_team,$user_id,$category_id,$country_id,$filterFlag){
        $adminRole = $this->adminRole;
        $bdeRole = $this->bedRole;
        $myteam = "" . implode(", ", $get_my_team) . "";
            // AND u.role_id Not IN ('$adminRole')
            //  WHERE u.role_id IN (2,3,4,17)
        // $query = ("SELECT u.id, u.name as username, max(ua.name) as supervisor, cat.cat_master_name as allocations FROM lead_allocations la
        // JOIN category_master cat ON cat.cat_master_id = la.category_id
        // JOIN users u ON u.id = la.user_id
        // JOIN users ua ON ua.id = u.supervisor_id
        // WHERE u.id IN ($myteam)
        // AND la.status=1
        // GROUP BY u.id, u.name, cat.cat_master_name");
        $query = DB::table('lead_allocations as la')
                ->join('category as cat','cat.category_id','=','la.category_id')
                ->join('users as u','u.id','=','la.user_id')
                ->select('u.id','u.name as username','cat.cat_name as allocations')
                ->where('la.status','=',1)
                ->groupBy('u.id','u.name','cat.cat_name');
                
        if($user_id != ''){
            $query->where('la.user_id','=',$user_id);
        }
        else{
            $query->whereIn('u.id',$get_my_team);
        }

        if($category_id != ''){
            $query->where('la.category_id','=',$category_id);
        }
        else{
            $query->whereIn('u.id',$get_my_team);
        }

        if($country_id != ''){
            $query->where('la.country_id','=',$country_id);
        }
        else{
            $query->whereIn('u.id',$get_my_team);
        }

        if($user_id == '' && $category_id == '' && $country_id == ''){
            $query->whereIn('u.id',$get_my_team);
        }
        
        $getLeadAllocations = $query->get();
        $length = sizeof($getLeadAllocations);
        if($filterFlag == 1 && sizeof($getLeadAllocations)>0){
            $arr = array();
            $check_userid = [];
            foreach($getLeadAllocations as $key => $allocations){
                if(in_array($allocations->id, $check_userid)){
                    $arr[$allocations->id]->allocations = $arr[$allocations->id]->allocations.','.$getLeadAllocations[$key]->allocations;
                }
                else{
                    array_push($check_userid, $allocations->id);
                    $arr[$allocations->id] = $allocations;
                }
            }
            return $arr;
        }
        else if(($filterFlag == 0 && $length == 0) || ($filterFlag == 0 && $length > 0)){
            $arr = array();
            $check_userid = [];
            $getLeadAllocations = $query->get();
            foreach($getLeadAllocations as $key => $allocations){
                if(in_array($allocations->id, $check_userid)){
                    $arr[$allocations->id]->allocations = $arr[$allocations->id]->allocations.','.$getLeadAllocations[$key]->allocations;
                }
                else{
                    array_push($check_userid, $allocations->id);
                    $arr[$allocations->id] = $allocations;
                }
            }
            return $arr;
        }
        else{
            return [];
        }
    }

    public function prepareAllocationsList($leadAllocations, $users){
        foreach($users as $key => $user){
            if(array_key_exists($user->id, $leadAllocations)){
                $users[$key]->allocations = $leadAllocations[$user->id]->allocations;
            }
            else{
                $users[$key]->allocations = '';
            }
        }
        return $users;
    }

    public function getUserName($id){
        $getUserName = DB::table('users')
                        ->leftjoin('roles_new as roles','roles.role_id','=','users.role_id')
                        ->leftjoin('users as u','u.id','=','users.supervisor_id')
                        ->select('users.id','users.username','users.role_id','users.name','users.mobile_phone','users.supervisor_id','roles.role_name','u.name as supervisor_name')
                        ->where('users.id',$id)
                        ->get();
        return $getUserName;
    }

    public function get_regions_by_category($data){
        $get_regions_by_category = DB::table('region')
                                ->where('status','=','Y')
                                ->orderBy('id','ASC')
                                ->get();
        if(sizeof($get_regions_by_category) >0){
            return 1;
        }
        else{
            return 0;
        }
    }

    public function get_countries_by_region($data){
        $userid = $data['userid'];
        $region_id = $data['region_id'];
        $category_id = $data['category_id'];

        $query =("SELECT id, name FROM countries
        WHERE id NOT IN (SELECT country_id FROM lead_allocations WHERE category_id= $category_id AND region_id= $region_id AND status=1) 
        AND region_id is NOT null AND region_id = $region_id
        AND country_status='Y'");

        $get_countries_by_region = DB::select($query);

        if(sizeof($get_countries_by_region) >0){
            return $get_countries_by_region;
        }
        else{
            return 0;
        }
    }

    public function getRegionsList(){
        $getRegionsList = DB::table('region')
                        ->where('status','=','Y')
                        ->orderBy('id','ASC')
                        ->get();
        if($getRegionsList){
            return $getRegionsList;
        }
        else{
            return [];
        }
    }

    public function getCountriesList(){
        $getCountriesList = DB::table('countries')
                        ->where('country_status','=','Y')
                        ->orderBy('id','ASC')
                        ->get();
        if($getCountriesList){
            return $getCountriesList;
        }
        else{
            return [];
        }
    }

    // public function getCategories(){
    //     $getCategories = DB::table('category_master')
    //                     ->where('status','=',1)
    //                     ->orderBy('cat_master_id','ASC')
    //                     ->get();
    //     if($getCategories){
    //         return $getCategories;
    //     }
    //     else{
    //         return [];
    //     }
    // }
    public function getCategories(){
        $getCategories = DB::table('category')
                        ->where('cat_master_id','=',0)
                        ->where('cat_status','=','Y')
                        ->orderBy('category_id','ASC')
                        ->get();
        if($getCategories){
            return $getCategories;
        }
        else{
            return [];
        }
    }

    public function getUsersForFilter(){
        $getUsers = DB::table('users')
                    ->select('id','name')
                    ->whereNotIn('id',[1])
                    ->where('status','=','Y')
                    ->groupBy('id','name')
                    ->get();
        
        if(sizeof($getUsers)>0){
            return $getUsers;
        }
        else{
            return [];
        }
    }

    public function getAllocationsGroupByRegion($id){
        $query = ("SELECT la.id, u.id as userid, cat.category_id, cat.cat_name, r.id as region_id, r.name as region_name, mc.id as country_id, mc.name as country_name FROM lead_allocations la
        JOIN category cat ON cat.category_id = la.category_id
        JOIN region r ON r.id = la.region_id
        JOIN countries mc ON mc.id = la.country_id
        JOIN users u ON u.id = la.user_id
        WHERE u.id = $id
        AND la.status=1");
        
        $data = DB::select($query);
        
        $arr = array();
        $check_ids = array();
        $cat = array();
        $region = array();

        foreach ($data as $key => $item) {
            if (!in_array($item->id, $check_ids)) {
                // $obj = new stdClass();
                $obj=(object)array();
                $obj->id = $item->id;
                $obj->userid = $item->userid;
                $obj->category_id = $item->category_id;
                $obj->cat_name = $item->cat_name;
                $temp_region = array();
                $total_region_count = 0;
                $total_country_count = 0;

                foreach ($data as $reg_key => $reg_item) {
                    if($item->category_id == $reg_item->category_id){
                        if (!in_array($reg_item->id, $check_ids)) {
                            // $temp_reg_obj = new stdClass();
                            $temp_reg_obj=(object)array();
                            $temp_reg_obj->region_id = $reg_item->region_id;
                            $temp_reg_obj->region_name = $reg_item->region_name;
                            $temp_country = array();
                            $temp_country_count = 0;
                            $countries_for_view = '';
                            foreach ($data as $reg_key => $country_item) {
                                if($item->category_id == $country_item->category_id && $reg_item->region_id == $country_item->region_id ){
                                    // $temp_country_obj = new stdClass();
                                    $temp_country_obj=(object)array();
                                    $temp_country_obj->id = $country_item->id;
                                    $temp_country_obj->country_id = $country_item->country_id;
                                    $temp_country_obj->country_name = $country_item->country_name;
                                    $countries_for_view .= $country_item->country_name . ', ';

                                    array_push($temp_country, $temp_country_obj);
                                    array_push($check_ids, $country_item->id);
                                    $total_country_count = $total_country_count+1;
                                    $temp_country_count = $temp_country_count+1;
                                }
                            }
                            $temp_reg_obj->countries = $temp_country;
                            $temp_reg_obj->countries_for_view = $countries_for_view;
                            $temp_reg_obj->temp_country_count = $temp_country_count;
                            array_push($temp_region, $temp_reg_obj);
                            $total_region_count = $total_region_count+1;
                        }
                    }
                }
                array_push($check_ids, $item->id);
                $obj->regions = $temp_region;
                $obj->total_region_count = $total_region_count;
                $obj->total_country_count = $total_country_count;
                array_push($arr, $obj);
            }
        }
        
        return $arr;
    }

    public function updateLeadAllocations($userid, $cat_id, $region_id, $country_id){
        $date= Carbon::now();
        if (in_array('0', $country_id) ){
            $query = ("SELECT id FROM countries WHERE region_id = $region_id AND region_id IS NOT NULL AND id NOT IN (SELECT country_id FROM lead_allocations WHERE category_id=$cat_id AND region_id=$region_id AND status=1)");

            $country_ids = DB::select($query);
            
            $i = 0;
            $length = sizeof($country_ids);
            
            foreach($country_ids as $ids){
                $temp = array(
                    'user_id' => $userid,
                    'category_id' => $cat_id,
                    'region_id' => $region_id,
                    'country_id' => $ids->id,
                    'status' => 1,
                    'created_at'=>$date
                );

                $updateLeadAllocations = DB::table('lead_allocations')->insert($temp);
                $i++;
            }
            if($length == $i){
                return 1;
            }
            else{
                return 0;
            }
        }
        else{
            $country_ids = $country_id;
            $i = 0;
            $length = sizeof($country_ids);
            foreach($country_ids as $ids){
                $temp = array(
                    'user_id' => $userid,
                    'category_id' => $cat_id,
                    'region_id' => $region_id,
                    'country_id' => $ids,
                    'status' => 1,
                    'created_at'=>$date
                );
                $updateLeadAllocations = DB::table('lead_allocations')->insert($temp);
                // $this->db->insert('lead_allocations', $temp);
                // $id = $this->db->insert_id();
                $i++;
            }
            if($length == $i){
                return 1;
            }
            else{
                return 0;
            }
        }
    }

    public function deleteLeadAllocations($data,$country_id){
        
        $region_id = $data['region_id'];
        $category_id = $data['category_id'];
        $user_id = $data['user_id'];

        $deleteLeadAllocations = DB::table('lead_allocations')
                                ->where('country_id', $country_id)
                                ->where('region_id', $region_id)
                                ->where('category_id', $category_id)
                                ->where('user_id', $user_id)
                                ->update(['status'=>0]);
        // print_r($deleteLeadAllocations);
        // exit;
        if($deleteLeadAllocations){
            return 1;
        }
        else{
            return 0;
        }
       
    }

    public function get_un_allocated_countries($category_id){
        $getRegions = DB::table('region')
                    ->select('id as continent_id','name as continent_name')
                    ->where('status','=','Y')
                    ->get();
        $arr = [];
        foreach($getRegions as $continent){
            $query = ("SELECT cat.category_id as cat_master_id, cat.cat_name as cat_master_name, reg.id as continent_id,reg.name as continent_name, con.id as country_id, con.name as country_name FROM category AS cat 
            CROSS JOIN countries AS con 
            JOIN region reg ON reg.id = con.region_id
            WHERE cat.cat_status= 'Y' AND con.country_status='Y' AND cat.category_id=$category_id AND con.region_id=$continent->continent_id
            AND con.id NOT IN (
                SELECT con.id FROM lead_allocations la
                JOIN category cat ON cat.category_id = la.category_id
                JOIN region reg ON reg.id = la.region_id
                JOIN countries con ON con.id = la.country_id
                AND la.status=1 AND con.country_status='Y'
                WHERE cat.category_id=$category_id AND con.region_id=$continent->continent_id
            )
            ORDER BY con.id ASC");

            $get_un_allocated_countries = DB::Select($query);
            $arr = array_merge($arr, $get_un_allocated_countries);
        }

        if(count($arr) > 0){
            $category = $arr[0]->cat_master_name;
            $i = 0 ;
            foreach($getRegions as $continent){
                $html = '';
                foreach ($arr as $reg_key => $val) {
                    if( $val->continent_id == $continent->continent_id ){
                        $html .= $val->country_name . ', ';
                    }
                }
                $getRegions[$i]->category = $category;
                $getRegions[$i]->countries = $html;
                $i++;
            }
        }
        return $getRegions;
    }

    public function getUserMail($userid){
        $getUserMail = DB::table('users')
                        ->select('email')
                        ->where('id',$userid)
                        ->get();
        return $getUserMail;
    }

    public function getCatName($cat_id){
        $getCatName = DB::table('category')
                        ->select('cat_name')
                        ->where('category_id',$cat_id)
                        ->get();
        return $getCatName;
    }
}
