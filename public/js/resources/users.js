$('.save_user').on('click',function(){
    var full_name = $('.full_name').val();
    var mobile = $('.mobile').val();
    var user_name = $('.user_name').val();
    var password = $('.password').val();
    var email = $('.email').val();
    var user_role = $('.user_role option:selected').val();
    var user_supervisor = $('.user_supervisor option:selected').val();
    var user_id = $('.user_id').val();
    var add_update_flag = 1;

    console.log(full_name);
    console.log(mobile);
    console.log(user_name);
    console.log(password);
    console.log(email);
    console.log(user_role);
    console.log(user_supervisor);
    // return false;
    if(full_name !='' && full_name != undefined && full_name != null && mobile !='' && mobile != undefined && mobile != null && user_name !='' && user_name != undefined && user_name != null && password !='' && password != undefined && password != null && email !='' && email != undefined && email != null && user_role !='' && user_role != undefined && user_role != null ){
        $.ajax({
            url: '/user_duplicate_check',
            type: "GET",
            data: {
                user_name : user_name,
                mobile : mobile,
                user_id : user_id,
                add_update_flag : add_update_flag
            },
            success: function (result) {
                console.log(result);
                // return false;
                if(result == 0){
                    $.ajax({
                        url: '/save_user',
                        type: "POST",
                        data: {
                            full_name : full_name,
                            mobile : mobile,
                            user_id : user_id,
                            user_name : user_name,
                            password : password,
                            email : email,
                            user_role : user_role,
                            user_supervisor : user_supervisor,
                            add_update_flag : add_update_flag
                        },
                        success: function (result) {
                            console.log(result);
                            // return false
                            if(result == 1){
                                $('.user_success_msg').css({'display':'block'});
                                $('.user_success_msg').text('User Saved successfully.');
                                setTimeout(() => {
                                    $('.user_success_msg').css({'display':'none'});
                                    window.location.href = document.location.origin+'/users';
                                }, 3000);
                            }
                            else{
                                $('.user_error_msg').css({'display':'block'});
                                $('.user_error_msg').text('Something Went Wrong.Pls try again later..!!');
                                setTimeout(() => {
                                    $('.user_error_msg').css({'display':'none'});
                                }, 3000);
                                return false;
                            }
                        }
                    });
                }
                else{
                    $('.user_error_msg').css({'display':'block'});
                    $('.user_error_msg').text('User Name already exists.');
                    setTimeout(() => {
                        $('.user_error_msg').css({'display':'none'});
                    }, 3000);
                    return false;
                }
            }
        });
    } else {
        $('.user_error_msg').css({'display':'block'});
        $('.user_error_msg').text('All Fields Are Required.');
        setTimeout(() => {
            $('.user_error_msg').css({'display':'none'});
            // location.reload();
        }, 3000);
        return false;
    }
})

$('.update_user').on('click',function(){
    var full_name = $('.full_name').val();
    var mobile = $('.mobile').val();
    var user_name = $('.user_name').val();
    var password = $('.password').val();
    var email = $('.email').val();
    var user_role = $('.user_role option:selected').val();
    var user_supervisor = $('.user_supervisor option:selected').val();
    var user_id = $('.user_id').val();
    var add_update_flag = 2;

    console.log(full_name);
    console.log(mobile);
    console.log(user_name);
    console.log(password);
    console.log(email);
    console.log(user_role);
    console.log(user_supervisor);
    // return false;
    if(full_name !='' && full_name != undefined && full_name != null && mobile !='' && mobile != undefined && mobile != null && user_name !='' && user_name != undefined && user_name != null && password !='' && password != undefined && password != null && email !='' && email != undefined && email != null && user_role !='' && user_role != undefined && user_role != null  && user_supervisor !='' && user_supervisor != undefined && user_supervisor != null){
        $.ajax({
            url: '/user_duplicate_check',
            type: "GET",
            data: {
                user_name : user_name,
                mobile : mobile,
                user_id : user_id,
                add_update_flag : add_update_flag
            },
            success: function (result) {
                console.log(result);
                // return false;
                if(result == 0){
                    $.ajax({
                        url: '/update_user',
                        type: "POST",
                        data: {
                            full_name : full_name,
                            mobile : mobile,
                            user_id : user_id,
                            user_name : user_name,
                            password : password,
                            email : email,
                            user_role : user_role,
                            user_supervisor : user_supervisor,
                            add_update_flag : add_update_flag
                        },
                        success: function (result) {
                            console.log(result);
                            // return false
                            if(result == 1){
                                $('.user_success_msg').css({'display':'block'});
                                $('.user_success_msg').text('User Updated successfully.');
                                setTimeout(() => {
                                    $('.user_success_msg').css({'display':'none'});
                                    window.location.href = document.location.origin+'/users';
                                }, 3000);
                            }
                            else{
                                $('.user_error_msg').css({'display':'block'});
                                $('.user_error_msg').text('Something Went Wrong.Pls try again later..!!');
                                setTimeout(() => {
                                    $('.user_error_msg').css({'display':'none'});
                                }, 3000);
                                return false;
                            }
                        }
                    });
                }
                else{
                    $('.user_error_msg').css({'display':'block'});
                    $('.user_error_msg').text('User Name already exists.');
                    setTimeout(() => {
                        $('.user_error_msg').css({'display':'none'});
                    }, 3000);
                    return false;
                }
            }
        });
    } else {
        $('.user_error_msg').css({'display':'block'});
        $('.user_error_msg').text('All Fields Are Required.');
        setTimeout(() => {
            $('.user_error_msg').css({'display':'none'});
            // location.reload();
        }, 3000);
        return false;
    }
})

if(domain + url_path == domain + '/' + 'add_user'){
    $('.user_supervisor').prop('disabled',true);
    $('.user_supervisor').addClass('input-disabled');
}

$('.user_role').on('change',function(){
    var user_role = $(this).val();
    console.log(user_role);
    
    if(user_role !='' && user_role != undefined && user_role != null){
        if(user_role != 'RL_l3v74v'){
            $.ajax({
                url: '/get_role_supervisors',
                type: "GET",
                data: {
                    user_role : user_role,
                },
                success: function (result) {
                    console.log(result);
                    // return false
                    if(result == 0){
                        $('#user_supervisor').find('option').remove();
                        var result_array = [];
                        result_array.push('<option value=""></option>');
                        result_array.push('<option value="RL_l3v74v">CEO</option>');
                        console.log(result_array);
                        $("#user_supervisor").append(result_array);
                        $('.user_supervisor').prop('disabled',false);
                        $('.user_supervisor').removeClass('input-disabled');
                    }
                    else if(result.length > 0){
                        $('#user_supervisor').find('option').remove();
                        var result_array = [];
                        result_array.push('<option value=""></option>');
                        for (i = 0; i < result.length; i++) {
                            result_array.push('<option value="' + result[i].id + '">' + result[i].name + '</option>');
                        }
                        $("#user_supervisor").append(result_array);
                        $('.user_supervisor').prop('disabled',false);
                        $('.user_supervisor').removeClass('input-disabled');
                    }
                    else{
                        $('.user_error_msg').css({'display':'block'});
                        $('.user_error_msg').text('Something Went Wrong.Pls try again later..!!');
                        setTimeout(() => {
                            $('.user_error_msg').css({'display':'none'});
                        }, 3000);
                        return false;
                    }
                }
            });
        }
        else{
            $('.user_supervisor').prop('disabled',false);
            $('.user_supervisor').removeClass('input-disabled');
        }
        
    } else {
        $('.user_error_msg').css({'display':'block'});
        $('.user_error_msg').text('Please Select User Role.');
        setTimeout(() => {
            $('.user_error_msg').css({'display':'none'});
            // location.reload();
        }, 3000);
        return false;
    }
})