@extends('layouts.header')
@section('body-content')
<section class="sub-category-list">
    <div class="block-header">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 my-auto">
                <h4 class="page-title m-0">Add New Report</h4>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <a href="{{URL:: to('/reports')}}">
                    <button type="button" class="btn btn-info float-right">Back</button>
                </a>
            </div>
        </div>
    </div>
    <div class="row clearfix mt-3">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="card project_widget">
                <div class="body mt-4 mb-4">
                    <div class="row">
                        <div class="col-12 col-md-12 mb-0">
                            <div class="input-group">  
                                <input type="text" class="report_name" autocomplete="off" value="" required/>
                                <label>Report Name <span class="text-danger">*</span></label>
                            </div>
                        </div>
                        <div class="col-12 col-md-12 mb-0">
                            <div class="input-group">  
                                <input type="text" class="report_bread_crumb" autocomplete="off" value="" required/>
                                <label>Report BreadCrumb <span class="text-danger">*</span></label>
                            </div>
                        </div>
                        <div class="col-12 col-md-12 mb-0">
                            <div class="input-group">  
                                <input type="text" class="report_url" autocomplete="off" value="" required/>
                                <label>Report URL <span class="text-danger">*</span></label>
                            </div>
                        </div>
                        <div class="col-12 col-md-12 mb-0">
                            <label class="" style="color:#414244;"><b>Short  Description</b><span class="text-danger">*</span></label>
                            <div class="input-group mt-5">
                                <textarea class="report_short_desc ckeditor" name="report_short_desc" id="report_short_desc" >  </textarea>
                            </div>
                        </div>
                        <div class="col-12 col-md-12 mb-0">
                            <label class="" style="color:#414244;"><b>Long Description</b><span class="text-danger">*</span></label>
                            <div class="input-group mt-5">  
                                <textarea class="report_long_desc ckeditor" id="report_long_desc" name="report_long_desc">  </textarea>
                            </div>
                        </div>
                        <div class="col-12 col-md-12 mb-0">
                            <label class="" style="color:#414244;"><b>Table of Content</b><span class="text-danger">*</span></label>
                            <div class="input-group mt-5">
                                <textarea class="report_table_of_content ckeditor" name="report_table_of_content" id="report_table_of_content" >  </textarea>
                            </div>
                        </div>
                        <div class="col-12 col-md-12 mb-0">
                            <label class="" style="color:#414244;"><b>List Of Tables</b><span class="text-danger">*</span></label>
                            <div class="input-group mt-5">  
                                <textarea class="report_list_of_tables ckeditor" id="report_list_of_tables" name="report_list_of_tables">  </textarea>
                            </div>
                        </div>
                        <div class="col-12 col-md-12 mb-0">
                            <label class="" style="color:#414244;"><b>List Of Charts</b></label>
                            <div class="input-group mt-5">  
                                <textarea class="rep_list_chart ckeditor" id="rep_list_chart" name="rep_list_chart">  </textarea>
                            </div>
                        </div>
                        <div class="col-12 col-md-12 mb-0">
                            <label class="" style="color:#414244;"><b>Report Analysis</b><span class="text-danger">*</span></label>
                            <div class="input-group mt-5">
                                <textarea class="report_analysis ckeditor" name="report_analysis" id="report_analysis" >  </textarea>
                            </div>
                        </div>
                        <div class="col-12 col-md-12 mb-0">
                            <label class="" style="color:#414244;"><b>Report Analysis 2</b><span class="text-danger">*</span></label>
                            <div class="input-group mt-5">  
                                <textarea class="report_analysis_2 ckeditor" id="report_analysis_2" name="report_analysis_2">  </textarea>
                            </div>
                        </div>
                        <div class="col-12 col-md-12 mb-0">
                            <label class="" style="color:#414244;"><b>Report Market Overview</b><span class="text-danger">*</span></label>
                            <div class="input-group mt-5">
                                <textarea class="rep_mar_overview ckeditor" name="rep_mar_overview" id="rep_mar_overview" >  </textarea>
                            </div>
                        </div>
                        <div class="col-12 col-md-12 mb-0">
                            <label class="" style="color:#414244;"><b>Report Market Segment</b><span class="text-danger">*</span></label>
                            <div class="input-group mt-5">
                                <textarea class="report_mkt_sgmt ckeditor" name="report_mkt_sgmt" id="report_mkt_sgmt" >  </textarea>
                            </div>
                        </div>
                        <div class="col-12 col-md-12 mb-0">
                            <label class="" style="color:#414244;"><b>Rep Part2</b></label>
                            <div class="input-group mt-5">
                                <textarea class="rep_part2 ckeditor" name="rep_part2" id="rep_part2" >  </textarea>
                            </div>
                        </div>
                        <div class="col-12 col-md-12 mb-0">
                            <label class="" style="color:#414244;"><b>Rep Preview Part2</b></label>
                            <div class="input-group mt-5">
                                <textarea class="report_preview_part_2 ckeditor" name="report_preview_part_2" id="report_preview_part_2" >  </textarea>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 mb-0">
                            <div class="input-group">
                                <select class="floating-select report_category_id" onclick="this.setAttribute('value', this.value);" value="" required>
                                    @if(sizeof($getCategories)>0)
                                        <option value=""></option>
                                        @foreach($getCategories as $val)
                                            <option value="{{$val->category_id}}">{{$val->cat_name}}</option>
                                        @endforeach
                                    @endif
                                </select>
                                <label>Category <span class="text-danger">*</span></label>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 mb-0">
                            <div class="input-group">
                                <select class="floating-select report_sub_category_id" id="report_sub_category_id" onclick="this.setAttribute('value', this.value);" value="" required>
                                    @if(sizeof($getSubCategories)>0)
                                        <option value=""></option>
                                        @foreach($getSubCategories as $val)
                                            <option value="{{$val->sub_category_id}}">{{$val->sub_category_name}}</option>
                                        @endforeach
                                    @endif
                                </select>
                                <label>Sub-Category <span class="text-danger">*</span></label>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 mb-0">
                            <div class="input-group">  
                                <input type="text" class="datepicker report_publish_date" autocomplete="off" value="" required/>
                                <span style="position: absolute;top: 8px;right: 0;bottom: 0;"><i class="icon-calendar" style="font-size: 26px;"></i></span>
                                <label>Publish Date <span class="text-danger">*</span></label>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 mb-0">
                            <div class="input-group">  
                                <input type="number" class="report_sul" autocomplete="off" value="" required/>
                                <label>Price-SUL <span class="text-danger">*</span></label>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 mb-0">
                            <div class="input-group">  
                                <input type="text" class="report_el" autocomplete="off" value="" required/>
                                <label>Price-EL <span class="text-danger">*</span></label>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 mb-0">
                            <div class="input-group">  
                                <input type="number" class="report_cul" autocomplete="off" value="" required/>
                                <label>Price-CUL <span class="text-danger">*</span></label>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 mb-0">
                            <div class="input-group">  
                                <input type="number" class="report_pages" autocomplete="off" value="" required/>
                                <label>No. Of Pages <span class="text-danger">*</span></label>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 mb-0">
                            <div class="input-group">
                                <select class="floating-select report_type" onclick="this.setAttribute('value', this.value);" value="" required>
                                    <option value=""></option>
                                    <option value="P">Publish</option>
                                    <option value="U">Upcoming</option>
                                </select>
                                <label>Report Type <span class="text-danger">*</span></label>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 mb-0">
                            <div class="input-group">
                                <select class="floating-select rep_top_selling" onclick="this.setAttribute('value', this.value);" value="" required>
                                    <option value=""></option>
                                    <option value="Y">Yes</option>
                                    <option value="N">No</option>
                                </select>
                                <label>Report Top Selling<span class="text-danger">*</span></label>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 mb-0">
                            <div class="input-group">
                                <select class="floating-select rep_schema" onclick="this.setAttribute('value', this.value);" value="" required>
                                    <option value=""></option>
                                    <option value="Y">Yes</option>
                                    <option value="N">No</option>
                                </select>
                                <label>Rep Schema</label>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 mb-0">
                            <div class="input-group">
                                <select class="floating-select top_selling" onclick="this.setAttribute('value', this.value);" value="" required>
                                    <option value=""></option>
                                    <option value="Y">Yes</option>
                                    <option value="N">No</option>
                                </select>
                                <label>Top Selling</label>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 mb-0">
                            <div class="input-group">
                                <select class="floating-select is_updated" onclick="this.setAttribute('value', this.value);" value="" required>
                                    <option value=""></option>
                                    <option value="Y">Yes</option>
                                    <option value="N">No</option>
                                </select>
                                <label>Is Updated</label>
                            </div>
                        </div>
                        <div class="col-12 col-md-12 mb-0">
                            <div class="input-group">  
                                <input type="text" class="meta_title" autocomplete="off" value="" required/>
                                <label>Meta Title <span class="text-danger">*</span></label>
                            </div>
                        </div>
                        <div class="col-12 col-md-12 mb-0">
                            <div class="input-group">  
                                <input type="text" class="meta_keywords" autocomplete="off" value="" required/>
                                <label>Meta Keywords<span class="text-danger">*</span></label>
                            </div>
                        </div>
                        <div class="col-12 col-md-12 mb-0">
                            <label class="" style="color:#414244;"><b>Meta Description</b><span class="text-danger">*</span></label>
                            <div class="form-group mt-5"> 
                                <div class="form-line">  
                                    <textarea class="form-control meta_desc p-3" name="meta_desc" rows="4" placeholder="Meta Description">  </textarea>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-12 mb-0">
                            <label class="" style="color:#414244;"><b>DC Description</b></label>
                            <div class="form-group mt-5"> 
                                <div class="form-line">  
                                    <textarea class="form-control dc_desc p-3" name="dc_desc" rows="4" placeholder="DC Description">  </textarea>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-4 mb-0">
                            <p class="text-danger mb-0">* All fields are Mandatory</p>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb-0 text-center">
                            <div class="form-group">
                                <div class="form-line">
                                    <div class="report_error_msg col-red w-100" style="display:none"></div>
                                    <div class="report_success_msg col-green w-100" style="display:none"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 text-center">
                            <button type="submit" class="btn btn-primary waves-effect save_report">SUBMIT</button>                                                                
                        </div>
                    </div>    
                </div>
            </div>
        </div>
    </div>
</section>
@endsection