<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Session;
use Config;
use Carbon\Carbon;

class login extends Model
{
    public function validatelogin($data){
        // $encrypted_password_string = base64_encode($data['password']);
        $getUserDetails = DB::table('users')
                            ->join('roles_new as roles','roles.role_id','=','users.role_id')
                            ->join('role_permissions','role_permissions.role_id','=','roles.role_id')
                            ->join('master_pages','master_pages.id','=','role_permissions.page_id')
                            ->select('roles.role_id as roleid','users.id','users.name','users.email','users.mobile_phone','users.username','users.role_id','users.status','users.supervisor_id','master_pages.name as master_page_name', 'master_pages.id as master_page_id', 'master_pages.route as master_page_route','master_pages.icon as page_icon','role_permissions.is_view','role_permissions.is_edit','role_permissions.is_delete','role_permissions.is_add','role_permissions.is_download','role_permissions.page_id','master_pages.parent_page_id')
                            // ->where('users.password','=',$encrypted_password_string)
                            ->where('users.username','=',$data['username'])
                            ->where('users.password','=',$data['password'])
                            ->where('users.status','=','Y')
                            // ->orderBy('master_pages.id','ASC')
                            ->orderBy('master_pages.menu_order','ASC')
                            ->get();

        if($getUserDetails){
            if(sizeof($getUserDetails)>0){
                return $getUserDetails;
            }
            else{
                return [];
            }
        }
        else{
            //return "data-error";
            return [];
        }
    }

    public function sectionPermissions($role_id){
        // print_r($role_id);die;
        $sectionPermissions = DB::table('users')
                            ->join('roles_new as roles','roles.role_id','=','users.role_id')
                            ->join('role_permissions','role_permissions.role_id','=','roles.role_id')
                            ->join('master_pages','master_pages.id','=','role_permissions.page_id')
                            ->select('master_pages.name as master_page_name', 'master_pages.id as master_page_id', 'master_pages.route as master_page_route','master_pages.icon as page_icon','role_permissions.is_view','role_permissions.is_edit','role_permissions.is_delete','role_permissions.is_add','role_permissions.is_download','role_permissions.page_id','master_pages.parent_page_id')
                            ->where('role_permissions.role_id','=',$role_id)
                            ->orderBy('master_pages.menu_order','ASC')
                            ->get();
        if($sectionPermissions){
            if(sizeof($sectionPermissions)>0){

                return $sectionPermissions;
            }
            else{
                return [];
            }
        }
        else{
            //return "data-error";
            return [];
        }
    }

    public function getProfileData($data){
        $user_id = Session::get('user_id');
        $getProfileData = DB::table('users')
                            ->join('roles_new as roles','roles.role_id','=','users.role_id')
                            ->leftjoin('users as u','u.id','=','users.supervisor_id')
                            ->select('roles.role_id as roleid','users.id','users.name','users.email','users.mobile_phone','users.username','users.role_id','users.status','users.supervisor_id','u.name as supervisor','roles.role_name')
                            ->where('users.id','=',$user_id)
                            ->get();

        if($getProfileData){
            if(sizeof($getProfileData)>0){
                return $getProfileData;
            }
            else{
                return [];
            }
        }
        else{
            return [];
        }
    }

    public function update_password($data){
        $date = Carbon::now();
        $user_id = Session::get('user_id');

        $check_current_password = DB::table('users')
            ->where('id','=',$user_id)
            ->where('password','=',$data['current_pw'])
            ->get();
        if( sizeof( $check_current_password ) > 0 ){
            $update_password = DB::table('users')->where('id','=',$user_id)->update([
                'password'=>$data['new_pw'],
                'updated_at'=>$date
            ]);

            if($update_password == 1){
                return 1;
            }   
            else{
                return 0;
            }
        }
        else{
            return 2;
        }

    }
}
