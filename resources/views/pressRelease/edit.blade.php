@extends('layouts.header')
@section('body-content')
<section class="sub-category-list">
    <div class="block-header">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 my-auto">
                <h4 class="page-title m-0">Edit Press Release</h4>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <a href="{{URL:: to('/press-releases')}}">
                    <button type="button" class="btn btn-info float-right">Back</button>
                </a>
            </div>
        </div>
    </div>
    <div class="row clearfix mt-3">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="card project_widget">
                <div class="body mt-4 mb-4">
                @if(isset($edit_press_release) && sizeof($edit_press_release)>0)
                    @foreach($edit_press_release as $value)
                    <div class="row">
                        <div class="col-12 col-md-12 mb-0">
                            <div class="input-group">  
                                <input type="text" class="press_release_name" autocomplete="off" value="{{$value->name}}" required/>
                                <label>Title <span class="text-danger">*</span></label>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 mb-0">
                            <div class="input-group">  
                                <input type="text" class="press_release_heading" autocomplete="off" value="{{$value->heading_h1}}" required/>
                                <label>Heading (h1) <span class="text-danger">*</span></label>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 mb-0">
                            <div class="input-group">  
                                <input type="text" class="press_release_url" autocomplete="off" value="{{$value->url}}" required/>
                                <label>URL <span class="text-danger">*</span></label>
                            </div>
                        </div>
                        <div class="col-12 col-md-12 mb-0">
                            <div class="input-group">
                                <select class="floating-select press_release_category_id" onclick="this.setAttribute('value', this.value);" required>
                                    @if(sizeof($getCategories)>0)
                                        <option value=""></option>
                                        @foreach($getCategories as $val)
                                            @if($val->category_id == $value->category_id)
                                                <option value="{{$val->category_id}}" selected>{{$val->cat_name}}</option>
                                            @else
                                                <option value="{{$val->category_id}}">{{$val->cat_name}}</option>
                                            @endif
                                        @endforeach
                                    @endif
                                </select>
                                <label>Category <span class="text-danger">*</span></label>
                            </div>
                        </div>
                        <div class="col-12 col-md-12 mb-0">
                            <label class="" style="color:#414244;"><b>Short  Description</b><span class="text-danger">*</span></label>
                            <div class="input-group mt-5">
                                <textarea class="press_release_short_desc ckeditor" name="press_release_short_desc" id="press_release_short_desc" >{{$value->short_desc   }}  </textarea>
                            </div>
                        </div>
                        <div class="col-12 col-md-12 mb-0">
                            <label class="" style="color:#414244;"><b>First Description</b></label>
                            <div class="input-group mt-5">  
                                <textarea class="press_release_first_desc ckeditor" id="press_release_first_desc" name="press_release_first_desc"> {{$value->first_desc}} </textarea>
                            </div>
                        </div>
                        <div class="col-12 col-md-12 mb-0">
                            <label class="" style="color:#414244;"><b>Long Description</b><span class="text-danger">*</span></label>
                            <div class="input-group mt-5">  
                                <textarea class="press_release_long_desc ckeditor" id="press_release_long_desc" name="press_release_long_desc"> {{$value->full_desc}} </textarea>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 mb-0">
                            <div class="input-group">  
                                <input type="text" class="meta_title" autocomplete="off" value="{{$value->meta_title}}" required/>
                                <label>Meta Title <span class="text-danger">*</span></label>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 mb-0">
                            <div class="input-group">  
                                <input type="text" class="meta_keywords" autocomplete="off" value="{{$value->meta_keywords}}" required/>
                                <label>Meta Keywords<span class="text-danger">*</span></label>
                            </div>
                        </div>
                        <div class="col-12 col-md-12 mb-0">
                            <label class="" style="color:#414244;"><b>Meta Description</b><span class="text-danger">*</span></label>
                            <div class="form-group mt-5"> 
                                <div class="form-line">  
                                    <textarea class="form-control meta_desc p-3" name="meta_desc" rows="4" placeholder="Meta Description"> {{$value->meta_description}} </textarea>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 mb-0">
                            <div class="input-group">
                                <?php $uploadDate = date('d-m-Y',strtotime($value->upload_date));?>  
                                <input type="text" class="datepicker press_release_publish_date" autocomplete="off" value="{{$uploadDate}}" required/>
                                <span style="position: absolute;top: 8px;right: 0;bottom: 0;"><i class="icon-calendar" style="font-size: 26px;"></i></span>
                                <label>Publish Date <span class="text-danger">*</span></label>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 mb-0">
                            <div class="input-group">
                                <select class="floating-select press_release_status" onclick="this.setAttribute('value', this.value);" required>
                                    <option value=""></option>
                                    @if($value->status == 'Y')
                                       <option value="Y" selected>Active</option>
                                        <option value="N">In-Active</option>
                                    @elseif($value->status == 'N')
                                        <option value="Y">Active</option>
                                        <option value="N" selected>In-Active</option>
                                    @else
                                        <option value="Y">Active</option>
                                        <option value="N">In-Active</option>
                                    @endif
                                </select>
                                <label>Status <span class="text-danger">*</span></label>
                            </div>
                        </div>
                        <input type="hidden" class="press_release_id" value="{{$value->id}}">
                        <div class="col-12 col-md-4 mb-0">
                            <p class="text-danger mb-0">* All fields are Mandatory</p>
                        </div>
                    </div>
                    @endforeach
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb-0 text-center">
                            <div class="form-group">
                                <div class="form-line">
                                    <div class="press_release_error_msg col-red w-100" style="display:none"></div>
                                    <div class="press_release_success_msg col-green w-100" style="display:none"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 text-center">
                            <button type="submit" class="btn btn-primary waves-effect update_press_release">UPDATE</button>                                                                
                        </div>
                    </div> 
                @else
                    <div class="row">
                        <div class="col-md-12">
                        </div>
                    </div>
                @endif   
                </div>
            </div>
        </div>
    </div>
</section>
@endsection