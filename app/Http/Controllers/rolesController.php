<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\master;
use App\roles;
use Carbon\Carbon;
use Redirect;
use Session;
use DB;
use Config;

class rolesController extends Controller
{
    public function __construct(){
        $this->master = new master();
        $this->roles = new roles();
    }

    public function getRoles(Request $request){
        if (!session('username')) {
	    	return redirect('/');
        } 
        $search_key = '';
        if(isset($_GET['search_key'])){
            $search_key=$_GET['search_key'];
        }
        $getRoles = $this->roles->getRoles();
        $getRoles = $getRoles->appends(['search_key'=>$search_key]);
        return view('roles.list')->with('getRoles',$getRoles)->with('search_key',$search_key);
    }

    public function addNewRole(Request $request){
        if (!session('username')) {
	    	return redirect('/');
        }
        $data = $request->all();
        $roles = $this->roles->getRoles();
        $getPagesList = $this->roles->getPagesList();
        
        return view('roles.add')->with('getPagesList',$getPagesList)->with('roles',$roles);
    }

    public function save_role_permissions(Request $request){
        if($request->all()) {
            $data=$request->all();
            $role_name = $data['role_name'];
            if(isset($data['reporting_role_id'])){
                $reporting_role_id = $data['reporting_role_id'];
            }
            else{
                $reporting_role_id = '';
            }
            
            if( isset($role_name) && $role_name != '' ){
                $addNewRole = $this->roles->addNewRole($role_name,$reporting_role_id);
                $getRole_id = $this->roles->getRole_id($addNewRole);
                if($addNewRole != 0){
                    $pages=$data["page_id"];
                    $mainArray=[];
                    $count=0;
                    $fk_role_id = $request->fk_role_id;     
                    
                    foreach($pages as $pageIds){
                        $time =Carbon::now();
                        $current_time = $time->toDateTimeString();
                        
                        $details['is_edit'] = $data['edit'][$count];
                        $details['is_delete'] = $data['delete'][$count];
                        $details['is_add'] = $data['add'][$count];
                        $details['is_download'] = $data['download'][$count];
                        $details['is_view'] = $data['view'][$count];
                        $details['page_id'] = $data['page_id'][$count];
                        $details['module_name'] = $data['modulename'][$count];
                        $details['role_id'] = $getRole_id[0]->role_id;
                        // $details['parent_page_id'] = $data['parent_page_id'][$count];
                        $details['status'] = 1;
                        $details['created_by'] = Session::get('user_id');
                        $details['created_at'] = $current_time;
                        array_push($mainArray, $details);
                        $count++;
                    }
                    $updateRolePermissions =  $this->roles->updateRolePermissions($mainArray);

                    return redirect("roles")->with('message', 'Roles Added Succesfully');
                }
            }
            else{
                return redirect()->back()->with('error', 'Something broke, please try again later');
            }
        }
    }

    public function edit_role_permissions($id){
        if (!session('username')) {
	    	return redirect('/');
        }
        $roles = $this->roles->getRoles();
        $getPagesList = $this->roles->getPagesList();
        $getRoleName = $this->roles->getRoleName($id);        
        $editPermissions = $this->roles->editPermissions($id);

        return view('roles.edit')
        ->with('roles',$roles)
        ->with('getPagesList',$getPagesList)
        ->with('getRoleName',$getRoleName)
        ->with('editPermissions',$editPermissions)
        ->with('fk_role_id',$getRoleName[0]->role_id);
    }

    public function update_role_permissions(Request $request){
        if (!session('username')) {
	    	return redirect('/');
        }
        
        if($request->all()) {
            $data=$request->all();
            $pages=$data["page_id"];
            $mainArray=[];
            $count=0;
            $role_id = $request->role_id;  
            if(isset($data['reporting_role_id'])){
                $reporting_role_id = $data['reporting_role_id'];
            }
            else{
                $reporting_role_id = '';
            }
            // if( isset($reporting_role) && $reporting_role != '' ){
                $updateRole = $this->roles->updateRole($data['role_id'],$reporting_role_id);
                if($updateRole == 1){
                    foreach($pages as $pageIds){
                        $time =Carbon::now();
                        $current_time = $time->toDateTimeString();
                        
                        $details['is_edit'] = $data['edit'][$count];
                        $details['is_delete'] = $data['delete'][$count];
                        $details['is_add'] = $data['add'][$count];
                        $details['is_download'] = $data['download'][$count];
                        $details['is_view'] = $data['view'][$count];
                        $details['page_id'] = $data['page_id'][$count];
                        $details['module_name'] = $data['modulename'][$count];
                        $details['role_id'] = $data['role_id'];
                        // $details['parent_page_id'] = $data['parent_page_id'][$count];
                        $details['status'] = 1;
                        $details['updated_by'] = Session::get('user_id');
                        $details['updated_at'] = $current_time;
                        array_push($mainArray, $details);
                        $count++;
                    }
                    
                   $deleteExistingPermissions = $this->roles->deleteExistingPermissions($data['role_id']);

                    $updateRolePermissions =  $this->roles->updateRolePermissions($mainArray);                    
                    return redirect("roles")->with('message', 'Permissions Updated Successfully');
                }
                else{
                    return redirect('roles')->with('error', 'Something broke, please try again later');
                }
            // }
            // else{
            //     return redirect('roles')->with('error', 'Something broke, please try again later');
            // }
        }
    }

    public function view_role_permissions($id){
        if (!session('username')) {
	    	return redirect('/');
        }
        $roles = $this->roles->getRoles();
        $getPagesList = $this->roles->getPagesList();
        $getRoleName = $this->roles->getRoleName($id);        
        $editPermissions = $this->roles->editPermissions($id);

        return view('roles.view')
        ->with('roles',$roles)
        ->with('getPagesList',$getPagesList)
        ->with('getRoleName',$getRoleName)
        ->with('editPermissions',$editPermissions)
        ->with('fk_role_id',$getRoleName[0]->role_id);
    }
}
