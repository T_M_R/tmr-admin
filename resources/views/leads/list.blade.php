@extends('layouts.header')
@section('body-content')
<section class="regions-list">
    <div class="block-header">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 my-auto">
                <h4 class="page-title m-0">Leads</h4>
            </div>
             <?php 
                $user_id='';
                $category_id='';
                $country_id='';
                $search_key='';
                
                if(isset($_GET['user'])){
                    $user_id=$_GET['user'];
                }
                if(isset($_GET['category_id'])){
                    $category_id=$_GET['category_id'];
                }
                if(isset($_GET['country_id'])){
                    $country_id=$_GET['country_id'];
                }
                if(isset($_GET['search_key'])){
                    $search_key=$_GET['search_key'];
                }
            ?>
            <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5 my-auto">
                <div class="input-group m-0">
                    <form method="GET" action="{{url('/leads')}}" enctype="multipart/form-data" role="form" autocomplete="off">
                        <input type="search" id="search_key" name="search_key" class="search-field m-0" value="{{$search_key}}" placeholder="Search">
                        <div class="input-group-append">
                            <button type="submit" class="btn btn-primary"><i class="fa fa-search mr-1"></i> Search</button>
                        </div>
                    </form>
                </div>
            </div>
            <?php 
                $sessionUser=Session::get('subMenuArray');  
                $val=[];
                $allRoutes=[];
                $addButtonStatus=0;
                $editButtonStatus=0;
                $viewButtonStatus=0;
                $downloadButtonStatus=0;
                foreach($sessionUser as $PagePermission)
                {
                    if(isset($PagePermission['route'])){
                        if($PagePermission['route'] == "leads")
                        {
                            $addButtonStatus=$PagePermission['is_add'];
                            $editButtonStatus=$PagePermission['is_edit'];
                            $viewButtonStatus=$PagePermission['is_view'];
                            $downloadButtonStatus=$PagePermission['is_download'];
                        }
                    }
                }
            ?>
            <div class="col-md-3 col-lg-3"></div>
            <div class="col-xs-12 col-sm-12 col-md-1 col-lg-1 my-auto pl-0">
                @if($downloadButtonStatus == 1)
                <form method="GET" action="{{url('/leads_csv_download')}}" enctype="multipart/form-data" class="mb-0" role="form" autocomplete="off">
                    <button type="submit" class="btn btn-info h-100"><i data-feather="download"></i></bitton>
                </form>
                @endif
            </div>
        </div>
        <div class="row mt-4">
            <div class="col-xs-12 col-sm-12 col-md-2 col-lg-6">
                <a href="{{URL:: to('/leads')}}">
                    <button type="button" class="btn btn-clear">Clear</button>
                </a>
            </div>
            <div class="col-xs-10 col-sm-10 col-md-6 col-lg-6">
                <div class="row">
                    <div class="col-sm-12 col-md-4 col-lg-4"></div>
                    <div class="col-sm-12 col-md-4 col-lg-4">
                        <div class="form-group m-0">
                            <select class="form-control onchange-select h-36 pl-2" onchange="location = this.value;" required >
                                <option value="" disabled selected>User</option>
                                    @foreach($getUsers as $user)
                                        @if($user->id == $user_id)
                                            <option value="/leads?user={{$user->id}}&category_id={{$category_id}}&country_id={{$country_id}}&search_key={{$search_key}}" selected>{{$user->name}}</option>
                                        @else
                                            <option value="/leads?user={{$user->id}}&category_id={{$category_id}}&country_id={{$country_id}}&search_key={{$search_key}}">{{$user->name}}</option>
                                        @endif
                                    @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-4 col-lg-4">
                        <div class="form-group m-0">
                            <select class="form-control onchange-select h-36 pl-2" onchange="location = this.value;" required >
                                <option value="" disabled selected>Category</option>
                                    @foreach($getCategories as $val)
                                        @if($val->category_id == $category_id)
                                            <option value="/leads?user={{$user_id}}&category_id={{$val->category_id}}&country_id={{$country_id}}&search_key={{$search_key}}" selected>{{$val->cat_name}}</option>
                                        @else
                                            <option value="/leads?user={{$user_id}}&category_id={{$val->category_id}}&country_id={{$country_id}}&search_key={{$search_key}}">{{$val->cat_name}}</option>
                                        @endif
                                    @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-4 col-lg-4 d-none">
                        <div class="form-group m-0">
                            <select class="form-control onchange-select h-36" onchange="location = this.value;" required >
                                <option value="" disabled selected>Country</option>
                                    @foreach($getCountriesList as $country)
                                        @if($country->id == $country_id)
                                            <option value="/leads?user={{$user_id}}&category_id={{$category_id}}&country_id={{$country->id}}&search_key={{$search_key}}" selected>{{$country->name}}</option>
                                        @else
                                            <option value="/leads?user={{$user_id}}&category_id={{$category_id}}&country_id={{$country->id}}&search_key={{$search_key}}">{{$country->name}}</option>
                                        @endif
                                    @endforeach
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 pl-0 my-auto d-none">
                <form method="GET" action="{{url('/leads')}}" enctype="multipart/form-data" role="form" autocomplete="off">
                    <button type="submit" class="btn btn-clear">Clear</button>
                </form>
            </div>
        </div>
    </div>
    <div class="row clearfix mt-3">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            @if(isset($getLeads))
                @if(sizeof($getLeads)>0)
                    @foreach($getLeads as $value)
                        <div class="card">
                            <div class="card-body lead-details p-0">
                                <div class="row">
                                    <div class="col-sm-12 col-md-12 col-lg-12">
                                        <p class="float-right"><strong>Assigned To : </strong><span style="color: #aa2222;" class="assigned_to_name assigned_to_name_{{$value->contactId}}">{{$value->username}}</span> 
                                            <span class="pt-2" style="display: inline-block;"> 
                                                <select class="form-control userAssign_{{$value->contactId}} assigned_to_input_sec assigned_to_input_sec_{{$value->contactId}}" name="userAssign_{{$value->contactId}}" id="userAssign_{{$value->contactId}}" onchange ="changeAssigned_User_LeadStatus(<?php echo $value->contactId; ?>)">
                                                    <option value="" disabled selected>Select User</option>
                                                        @foreach($getUsers as $user)
                                                            <option value="{{$user->id}}">{{$user->name}}</option>
                                                        @endforeach
                                                </select>
                                            </span> 
                                            <span class="assignto-edit-btn_{{$value->contactId}}"><i class="fas fa-edit" onclick ="openAssignInput('<?php echo $value->contactId; ?>')" style="display: inline-block;"></i></span> 
                                            <span class="assignto-edit-remove-btn_{{$value->contactId}}"><i class="fas fa-times assignto-edit-remove-btn assignto-edit-remove-btn_{{$value->contactId}}" onclick ="closeAssignInput('<?php echo $value->contactId; ?>')"  style="display: inline-block;"></i></span>
                                            | <strong >Status :</strong > <span style="color: #aa2222;" class="status_name status_name_{{$value->contactId}}"><?php echo getleadStatusName($value->leadStatus) ?></span> 
                                            <span class="pt-2" style="display: inline-block;"> 
                                                <select class="form-control changeStatus_{{$value->contactId}} status_input_sec status_input_sec_{{$value->contactId}}" name="changeStatus_{{$value->contactId}}" id="changeStatus_{{$value->contactId}}" onchange ="changeAssigned_User_LeadStatus(<?php echo $value->contactId; ?>)">
                                                    <?php echo leadStatus($value->leadStatus); ?>
                                                </select>
                                            </span> 
                                            <span class="status-edit-btn_{{$value->contactId}}"><i class="fas fa-edit" onclick ="openStatusInput('<?php echo $value->contactId; ?>')" style="display: inline-block;"></i></span> 
                                            <span class="status-edit-remove-btn_{{$value->contactId}}"><i class="fas fa-times status-edit-remove-btn status-edit-remove-btn_{{$value->contactId}}" onclick ="closeStatusInput('<?php echo $value->contactId; ?>')"  style="display: inline-block;"></i></span>
                                            | <strong >Country :</strong > <span style="color: #aa2222;">{{$value->country}}</span> 
                                        </p>
                                    </div>
                                    <a href="{{URL:: to('/lead-details/'.$value->contactId)}}" class="w-100">
                                        <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
                                            <?php $report= substr("$value->rep_name",0,120)."...";?>
                                            <p><strong >Report :</strong > <Span>{{$report}}</Span></p>
                                        </div>
                                        <div class="col-sm-12 col-md-12 col-lg-12">
                                            <div class="row pt-3">
                                                <div class="col-md-12">
                                                    <div class="row">
                                                        <div class="col-xs-12 col-sm-6 col-md-4 pb-3">
                                                            <div class="row">
                                                                <div class="col-md-4">
                                                                    <label for="">Name :</label>
                                                                </div>
                                                                <div class="col-md-8">
                                                                    <p>{{$value->name}}</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-6 col-md-4 pb-3">
                                                            <div class="row">
                                                                <div class="col-md-4">
                                                                    <label for="">Mobile :</label>
                                                                </div>
                                                                <div class="col-md-8">
                                                                    <?php $phoneNo= strlen($value->phoneNo) > 12 ? substr($value->phoneNo,0,12)."..." : $value->phoneNo;?>
                                                                    <p>{{$phoneNo}}</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-6 col-md-4 pb-3">
                                                            <div class="row">
                                                                <div class="col-md-4">
                                                                    <label for="">Email :</label>
                                                                </div>
                                                                <div class="col-md-8">
                                                                    <?php $email= strlen($value->emailId) > 15 ? substr($value->emailId,0,15)."..." : $value->emailId;?>
                                                                    <p>{{$email}}</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-6 col-md-4">
                                                            <div class="row">
                                                                <div class="col-md-4">
                                                                    <label for="">Company:</label>
                                                                </div>
                                                                <div class="col-md-8">
                                                                    <p>{{$value->company}}</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-6 col-md-4">
                                                            <div class="row">
                                                                <div class="col-md-4">
                                                                    <label for="">Category:</label>
                                                                </div>
                                                                <div class="col-md-8">
                                                                    <?php $category= strlen($value->cat_name) > 15 ? substr($value->cat_name,0,15)."..." : $value->cat_name;?>
                                                                    <p>{{$category}}</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-6 col-md-4">
                                                            <div class="row">
                                                                <div class="col-md-4">
                                                                    <label for="">Rep Type:</label>
                                                                </div>
                                                                <div class="col-md-8">
                                                                    @if($value->rep_type == 'N' )    
                                                                        <p>Publish</p>
                                                                    @else
                                                                        <p>Upcomoing</p>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @else
                    <div class="card">
                        <p class="text-center py-5" style="font-size:25px">No Data Available To Show</p>  
                    </div>
                @endif
            @else
                <div class="card">
                    <p class="text-center py-5" style="font-size:25px">No Data Available To Show</p>  
                </div>
            @endif
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 paginate-sec">
            @if(sizeof($getLeads)>0)
                {{$getLeads->links()}}
            @endif
        </div>
    </div>
</section>
<?php
function leadStatus($currentStatus = "") {
    ?>
    <option <?php if ($currentStatus == "N") {
        echo "selected";
    } ?> value="N">Active</option>
    <option <?php if ($currentStatus == "C") {
        echo "selected";
    } ?> value="C">Close</option>
    <option <?php if ($currentStatus == "P") {
        echo "selected";
    } ?> value="P">Pipeline</option>
    <option <?php if ($currentStatus == "S") {
        echo "selected";
    } ?> value="S">Student</option>
    <option <?php if ($currentStatus == "J") {
        echo "selected";
    } ?> value="J">Junk</option>
    <option <?php if ($currentStatus == "NB") {
        echo "selected";
    } ?> value="NB">No Budget</option>
    <option <?php if ($currentStatus == "NI") {
        echo "selected";
    } ?> value="NI">Not Interested</option>
    <option <?php if ($currentStatus == "FU") {
        echo "selected";
    } ?> value="FU">Follow Up</option>
    <?php
}

function getleadStatusName($currentStatus) {
    if ($currentStatus == "N") {
        return "Active";
    }
    if ($currentStatus == "P") {
        return "Pipeline";
    }
    if ($currentStatus == "S") {
        return "Student";
    }
    if ($currentStatus == "J") {
        return "Junk";
    }
    if ($currentStatus == "NB") {
        return "No Budget";
    }
    if ($currentStatus == "NI") {
        return "Not Interested";
    }
    if ($currentStatus == "FU") {
        return "Follow Up";
    }
    if ($currentStatus == "IP") {
        return "In Process";
    }
    if ($currentStatus == "NR") {
        return "No Response";
    }
    if ($currentStatus == "C") {
        return "Close";
    }
    if ($currentStatus == "SALE") {
        return "Sale";
    }
}

?>
@endsection