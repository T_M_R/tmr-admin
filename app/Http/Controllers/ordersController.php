<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\orders;
use App\reports;
use Carbon\Carbon;
use Redirect;
use Session;
use DB;
use Config;

class ordersController extends Controller
{
    public function __construct(){
        $this->orders = new orders();
        $this->reports = new reports();
    }

    public function getOrdersData(Request $request){
        if (!session('username')) {
	    	return redirect('/');
        } 
        $search_key = '';
        if(isset($_GET['search_key'])){
            $search_key=$_GET['search_key'];
        }
        $getOrdersData = $this->orders->getOrdersData($search_key);
        $getOrdersData= $getOrdersData->appends(['search_key'=>$search_key]);
        Session::put('getOrdersData',$getOrdersData);
        return view('orders.list')->with('getOrdersData',$getOrdersData)->with('search_key',$search_key);
    } 
}
