<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('layouts.header');
// });

Route::get('/','LoginController@login');
Route::post('validate_login','LoginController@validatelogin');
Route::get('logout','LoginController@logout');

// Master Routes===============
Route::get('regions','masterController@getRegionsList');
Route::get('add_region','masterController@addNewRegion');
Route::get('region_duplicate_check','masterController@region_duplicate_check');
Route::post('save_region','masterController@save_region');
Route::get('edit_region/{id}','masterController@edit_region');
Route::post('update_region','masterController@update_region');
Route::post('update_region_status','masterController@update_region_status');

Route::get('countries','masterController@getCountriesList');
Route::get('add_country','masterController@addNewCountry');
Route::get('country_duplicate_check','masterController@country_duplicate_check');
Route::post('save_country','masterController@save_country');
Route::get('edit_country/{id}','masterController@edit_country');
Route::post('update_country','masterController@update_country');
Route::post('update_country_status','masterController@update_country_status');

Route::get('categories','masterController@getCategoriesList');
Route::get('add_category','masterController@addNewCategory');
Route::get('category_duplicate_check','masterController@category_duplicate_check');
Route::post('save_category','masterController@save_category');
Route::get('edit_category/{id}','masterController@edit_category');
Route::post('update_category','masterController@update_category');
Route::post('update_category_status','masterController@update_category_status');

Route::get('sub-categories','masterController@getSubCategoriesList');
Route::get('add_subcategory','masterController@addNewSubCategory');
// Route::get('category_duplicate_check','masterController@category_duplicate_check');
Route::post('save_sub_category','masterController@save_sub_category');
Route::get('edit_sub_category/{id}','masterController@edit_sub_category');
Route::post('update_sub_category','masterController@update_sub_category');
Route::post('update_sub_category_status','masterController@update_sub_category_status');

Route::get('companies','masterController@getCompaniesList');
Route::get('add_company','masterController@addNewCompany');
Route::get('company_duplicate_check','masterController@company_duplicate_check');
Route::post('save_company','masterController@save_company');
Route::get('edit_company/{id}','masterController@edit_company');
Route::post('update_company','masterController@update_company');
Route::post('update_company_status','masterController@update_company_status');

// Leads Allocation
Route::get('lead_allocations','leadsAllocationController@get_lead_allocations');
Route::get('leads_allocation_edit/{id}','leadsAllocationController@edit_lead_allocations');
Route::get('get_regions_by_category','leadsAllocationController@get_regions_by_category');
Route::get('get_countries_by_region','leadsAllocationController@get_countries_by_region');
Route::any('updateLeadAllocations','leadsAllocationController@updateLeadAllocations');
Route::any('deleteLeadAllocations','leadsAllocationController@deleteLeadAllocations');
Route::get('leads_allocation_view/{id}','leadsAllocationController@view_lead_allocation');

Route::get('un_allocated_countries','leadsAllocationController@un_allocated_countries');

// Users
Route::get('users','usersController@getUsersList');
Route::get('add_user','usersController@addNewUser');
Route::get('user_duplicate_check','usersController@user_duplicate_check');
Route::post('save_user','usersController@save_user');
Route::get('edit_user/{id}','usersController@edit_user');
Route::post('update_user','usersController@update_user');
Route::get('get_role_supervisors','usersController@get_role_supervisors');
Route::get('view_user/{id}','usersController@view_user');
Route::post('update_user_status','usersController@update_user_status');

// Roles
Route::get('roles','rolesController@getRoles');
Route::get('add_role','rolesController@addNewRole');
Route::post('save_role_permissions','rolesController@save_role_permissions');
Route::get('edit_role_permissions/{id}','rolesController@edit_role_permissions');
Route::post('update_role_permissions','rolesController@update_role_permissions');
Route::get('view_role_permissions/{id}','rolesController@view_role_permissions');

// Reports
Route::get('reports','reportsController@getReports');
Route::get('add_report','reportsController@addNewReport');
Route::get('get-subcategories','reportsController@getCatSubcategories');
Route::post('save_report','reportsController@save_report');
Route::get('edit_report/{id}','reportsController@edit_report');
Route::post('update_report','reportsController@update_report');
Route::get('view_report/{id}','reportsController@view_report');
Route::post('update_report_status','reportsController@update_report_status');
Route::get('reports_csv_download','downloadController@reports_csv_download');

// Custom Reports
Route::get('custom_reports','customReportsController@getCustomReports');
Route::get('add_custom_report','customReportsController@addNewCustomReport');
Route::post('save_custom_report','customReportsController@save_custom_report');
Route::get('edit_custom_report/{id}','customReportsController@edit_custom_report');
Route::post('update_custom_report','customReportsController@update_custom_report');
Route::get('view_custom_report/{id}','customReportsController@view_custom_report');
Route::post('update_custom_report_status','customReportsController@update_custom_report_status');
Route::get('custom_reports_csv_download','downloadController@custom_reports_csv_download');

// Leads
Route::get('leads','leadsController@getLeads');
Route::get('lead-details/{id}','leadsController@getLeadDetails');
Route::post('save_lead_comment','leadsController@save_lead_comment');
Route::post('save_remainder','leadsController@save_remainder');
Route::get('change_assignedUser_LeadStatus','leadsController@change_assignedUser_LeadStatus');
Route::get('leads_csv_download','downloadController@leads_csv_download');

// Orders
Route::get('orders','ordersController@getOrdersData');

// Articles
Route::get('articles','articlesController@getArticlesData');
Route::get('add_article','articlesController@addNewArticle');
Route::post('save_article','articlesController@save_article');
Route::get('edit_article/{id}','articlesController@edit_article');
Route::post('update_article','articlesController@update_article');
Route::post('update_article_status','articlesController@update_article_status');
Route::get('view_article/{id}','articlesController@view_article');

// Case Studies
Route::get('case_studies','caseStudyController@getCaseStudyData');
Route::get('add_case_study','caseStudyController@addNewCaseStudy');
Route::post('save_case_study','caseStudyController@save_case_study');
Route::get('edit_case_study/{id}','caseStudyController@edit_case_study');
Route::post('update_case_study','caseStudyController@update_case_study');
Route::post('update_cs_status','caseStudyController@update_cs_status');

// Infographics
Route::get('infographics','infographicController@getInfographicData');
Route::get('add_infographic','infographicController@addNewInfographic');
Route::post('save_infographic','infographicController@save_infographic');
Route::get('edit_infographic/{id}','infographicController@edit_infographic');
Route::post('update_infographic','infographicController@update_infographic');
Route::post('update_infographic_status','infographicController@update_infographic_status');

// News
Route::get('news','newsController@getNewsData');
Route::get('add_news','newsController@addNewNews');
Route::post('save_news','newsController@save_news');
Route::get('edit_news/{id}','newsController@edit_news');
Route::post('update_news','newsController@update_news');
Route::post('update_news_status','newsController@update_news_status');
Route::get('view_news/{id}','newsController@view_news');

// Press Release
Route::get('press-releases','pressReleaseController@getPressReleases');
Route::get('add_press_release','pressReleaseController@addPressRelease');
Route::post('save_press_release','pressReleaseController@save_press_release');
Route::get('edit_press_release/{id}','pressReleaseController@edit_press_release');
Route::post('update_press_release','pressReleaseController@update_press_release');
Route::post('update_press_release_status','pressReleaseController@update_press_release_status');
Route::get('view_press_release/{id}','pressReleaseController@view_press_release');

// Dashboard
Route::get('dashboard','homeController@getHomeData');
Route::get('getLeadStatus','homeController@getLeadStatus');
Route::get('get_mnth_wise_lead_data','homeController@get_mnth_wise_lead_data');
Route::get('get_mnth_wise_report_data','homeController@get_mnth_wise_report_data');
Route::get('get_mnth_wise_orders_data','homeController@get_mnth_wise_orders_data');
Route::get('get_mnth_wise_articles_data','homeController@get_mnth_wise_articles_data');

// Profile
Route::get('profile','LoginController@getProfileData');
Route::post('update_password','LoginController@update_password');

// Mail Config
Route::get('mail_config','mailController@mail_config');
Route::get('add_mail_config','mailController@add_mail_config');
Route::post('save_mail_config/','mailController@save_mail_config');
Route::get('edit_email_config/{id}','mailController@edit_email_config');
Route::post('update_mail_config','mailController@update_mail_config');
Route::post('update_email_status','mailController@update_email_status');

Route::get('mail_template_new','mailController@mail_template_new');