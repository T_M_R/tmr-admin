@extends('layouts.header')
@section('body-content')
<section class="regions-list">
    <div class="block-header">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 my-auto">
                <h4 class="page-title m-0">Mail</h4>
            </div>
        </div>
    </div>
    <div class="row clearfix mt-3">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <p class="center status_change_msg text-danger"></p>
            <div class="card">
                <div class="body p-0">

                </div>
            </div>
        </div>
    </div>
</section>
@endsection