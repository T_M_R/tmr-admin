@extends('layouts.header')
@section('body-content')
<section class="sub-category-list">
    <div class="block-header">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 my-auto">
                <h4 class="page-title m-0">Sub Categories</h4>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5 my-auto">
                <div class="input-group m-0">
                    <form method="GET" action="{{url('/sub-categories')}}" enctype="multipart/form-data" role="form" autocomplete="off">
                        <input type="search" id="search_key" name="search_key" class="search-field m-0" value="{{$search_key}}" placeholder="Search">
                        <div class="input-group-append">
                            <button type="submit" class="btn btn-primary"><i class="fa fa-search mr-1"></i> Search</button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-1 col-lg-1 pl-0">
                <form method="GET" action="{{url('/sub-categories')}}" enctype="multipart/form-data" role="form" autocomplete="off">
                    <button type="submit" class="btn btn-clear">Clear</button>
                </form>
            </div>
            <?php 
                $sessionUser=Session::get('subMenuArray');  
                $val=[];
                $allRoutes=[];
                $addButtonStatus=0;
                $editButtonStatus=0;
                $viewButtonStatus=0;
                foreach($sessionUser as $PagePermission)
                {
                    if(isset($PagePermission['route'])){
                        if($PagePermission['route'] == "sub-categories")
                        {
                            $addButtonStatus=$PagePermission['is_add'];
                            $editButtonStatus=$PagePermission['is_edit'];
                            $viewButtonStatus=$PagePermission['is_view'];
                        }
                    }
                }
            ?>
            <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                @if($addButtonStatus == 1)
                <a href="{{URL:: to('/add_subcategory')}}">
                    <button type="button" class="btn btn-info float-right">Add New</button>
                </a>
                @endif
            </div>
        </div>
    </div>
    <div class="row clearfix mt-3">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            @if(isset($getSubCategoriesList) && sizeof($getSubCategoriesList)>0)
            <div class="card">
                <div class="body p-0">
                    <div class="table-responsive">
                        <!-- <table class="table table-bordered table-striped table-hover js-basic-example dataTable"> -->
                        <table class="table table-bordered mb-0">
                            <thead>
                                <tr>
                                    <th># S.No</th>
                                    <th>Sub Category</th>
                                    <th>Category</th>
                                    <th>Status</th>
                                    @if($editButtonStatus == 1)
                                    <th>Action</th>
                                    @endif
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                    $page_id = $getSubCategoriesList->currentPage();
                                    if($page_id == 1 || $page_id == '')
                                    {
                                        $i=1; 
                                    }
                                    else
                                    {
                                        $i=($page_id-1)*30;
                                        $i= $i+1;
                                    }
                                ?>
                                @foreach($getSubCategoriesList as $value)
                                    <tr>
                                        <td>{{$i}}</td>
                                        <td>{{$value->sub_category_name}}</td>
                                        <td>{{$value->cat_master_name}}</td>
                                        <td>
                                            @if($value->status == 'Y')
                                                <input class="checkbox sub_category_toggle_event" type="checkbox" data-toggle="toggle" data-style="ios" checked>
                                                <input type="hidden" name="hidden_status" class="hidden_status" value="1" />
                                            @else
                                                <input class="checkbox sub_category_toggle_event" type="checkbox" data-toggle="toggle" data-style="ios">
                                                <input type="hidden" name="hidden_status" class="hidden_status" value="0"/>
                                            @endif
                                            <input type="hidden" class="sub_category_id" value="{{$value->sub_category_id}}">
                                        @if($editButtonStatus == 1)
                                        <td>
                                            <a href="{{URL:: to('/edit_sub_category/'.$value->sub_category_id)}}" class="text-decoration-none">
                                                <button class="btn tblActnBtn">
                                                    <i class="material-icons">mode_edit</i>
                                                </button>
                                            </a>
                                        </td>
                                        @endif
                                    </tr>
                                    <?php $i++;?>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            @else
                <div class="card">
                    <p class="text-center py-5" style="font-size:25px">No Data Available To Show</p>  
                </div>
            @endif
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 paginate-sec">
            @if(sizeof($getSubCategoriesList)>0)
                {{$getSubCategoriesList->links()}}
            @endif
        </div>
    </div>
</section>
@endsection