$('.report_sub_category_id').attr('disabled', true);
$('.report_category_id').on('change', function () {
    report_category_id = $('.report_category_id option:selected').val();
    crm_user_unit = $('.crm_user_unit option:selected').val();
    if (report_category_id != '' && report_category_id != null && report_category_id != undefined) {
        $.ajax({
            url: '/get-subcategories',
            method: 'GET',
            data: {
                report_category_id: report_category_id,
            },
            success: function (result) {
                console.log(result);
                $('#report_sub_category_id').find('option').remove();
                var result_array = [];
                // result_array.push('<option value="">Select</option>');
                result_array.push('<option value="" selected disabled></option>');
                for (i = 0; i < result.length; i++) {
                    result_array.push('<option value="' + result[i].sub_category_id + '">' + result[i].sub_category_name + '</option>');
                }
                $("#report_sub_category_id").append(result_array.join(''));
                $('.report_sub_category_id').attr('disabled', false);
            }
        })
    }
})

$('.save_report').on('click', function () {
    var report_name = $('.report_name').val();
    var report_bread_crumb = $('.report_bread_crumb').val();
    var report_url = $('.report_url').val();
    var report_short_desc = CKEDITOR.instances['report_short_desc'].getData();
    var report_long_desc = CKEDITOR.instances['report_long_desc'].getData();
    var report_table_of_content = CKEDITOR.instances['report_table_of_content'].getData();
    var report_list_of_tables = CKEDITOR.instances['report_list_of_tables'].getData();
    var report_analysis = CKEDITOR.instances['report_analysis'].getData();
    var report_analysis_2 = CKEDITOR.instances['report_analysis_2'].getData();
    var report_mkt_sgmt = CKEDITOR.instances['report_mkt_sgmt'].getData();
    var rep_list_chart = CKEDITOR.instances['rep_list_chart'].getData();
    var rep_mar_overview = CKEDITOR.instances['rep_mar_overview'].getData();
    var rep_part2 = CKEDITOR.instances['rep_part2'].getData();
    var report_preview_part_2 = CKEDITOR.instances['report_preview_part_2'].getData();
    var report_category_id = $('.report_category_id option:selected').val();
    var report_sub_category_id = $('.report_sub_category_id option:selected').val();
    var report_publish_date = $('.report_publish_date').val();
    var report_sul = $('.report_sul').val();
    var report_el = $('.report_el').val();
    var report_cul = $('.report_cul').val();
    var report_pages = $('.report_pages').val();
    var report_type = $('.report_type option:selected').val();
    var rep_top_selling = $('.rep_top_selling option:selected').val();
    var rep_schema = $('.rep_schema option:selected').val();
    var top_selling = $('.top_selling option:selected').val();
    var is_updated = $('.is_updated option:selected').val();
    var meta_title = $('.meta_title').val();
    var meta_keywords = $('.meta_keywords').val();
    var meta_desc = $('.meta_desc').val();
    var dc_desc = $('.dc_desc').val();

    if (report_name != '' && report_name != null && report_name != undefined &&
        // report_bread_crumb != '' && report_bread_crumb != null && report_bread_crumb != undefined &&
        report_url != '' && report_url != null && report_url != undefined &&
        report_short_desc != '' && report_short_desc != null && report_short_desc != undefined &&
        report_long_desc != '' && report_long_desc != null && report_long_desc != undefined &&
        report_table_of_content != '' && report_table_of_content != null && report_table_of_content != undefined &&
        report_list_of_tables != '' && report_list_of_tables != null && report_list_of_tables != undefined &&
        report_analysis != '' && report_analysis != null && report_analysis != undefined &&
        report_analysis_2 != '' && report_analysis_2 != null && report_analysis_2 != undefined &&
        report_mkt_sgmt != '' && report_mkt_sgmt != null && report_mkt_sgmt != undefined &&
        rep_mar_overview != '' && rep_mar_overview != null && rep_mar_overview != undefined &&
        report_category_id != '' && report_category_id != null && report_category_id != undefined &&
        report_sub_category_id != '' && report_sub_category_id != null && report_sub_category_id != undefined &&
        report_publish_date != '' && report_publish_date != null && report_publish_date != undefined &&
        report_sul != '' && report_sul != null && report_sul != undefined &&
        report_el != '' && report_el != null && report_el != undefined &&
        report_cul != '' && report_cul != null && report_cul != undefined &&
        report_pages != '' && report_pages != null && report_pages != undefined &&
        report_type != '' && report_type != null && report_type != undefined &&
        // rep_top_selling != '' && rep_top_selling != null && rep_top_selling != undefined &&
        // rep_schema != '' && rep_schema != null && rep_schema != undefined &&
        // top_selling != '' && top_selling != null && top_selling != undefined &&
        meta_title != '' && meta_title != null && meta_title != undefined &&
        meta_keywords != '' && meta_keywords != null && meta_keywords != undefined &&
        meta_desc != '' && meta_desc != null && meta_desc != undefined) {
        $.ajax({
            url: '/save_report',
            type: "POST",
            data: {
                report_name: report_name,
                report_bread_crumb: report_bread_crumb,
                report_url: report_url,
                report_short_desc: report_short_desc,
                report_long_desc: report_long_desc,
                report_table_of_content: report_table_of_content,
                report_list_of_tables: report_list_of_tables,
                report_analysis: report_analysis,
                report_analysis_2: report_analysis_2,
                report_mkt_sgmt: report_mkt_sgmt,
                rep_list_chart: rep_list_chart,
                rep_mar_overview: rep_mar_overview,
                rep_part2: rep_part2,
                report_preview_part_2: report_preview_part_2,
                report_category_id: report_category_id,
                report_sub_category_id: report_sub_category_id,
                report_publish_date: report_publish_date,
                report_sul: report_sul,
                report_el: report_el,
                report_cul: report_cul,
                report_pages: report_pages,
                report_type: report_type,
                rep_top_selling: rep_top_selling,
                rep_schema: rep_schema,
                top_selling: top_selling,
                is_updated: is_updated,
                meta_title: meta_title,
                meta_keywords: meta_keywords,
                meta_desc: meta_desc,
                dc_desc: dc_desc
            },
            success: function (result) {
                console.log(result);
                // return false
                if (result == 1) {
                    $('.report_success_msg').css({ 'display': 'block' });
                    $('.report_success_msg').text('Report Saved successfully.');
                    setTimeout(() => {
                        $('.report_success_msg').css({ 'display': 'none' });
                        window.location.href = document.location.origin + '/reports';
                    }, 1500);
                }
                else {
                    $('.report_error_msg').css({ 'display': 'block' });
                    $('.report_error_msg').text('Something Went Wrong.Pls try again later..!!');
                    setTimeout(() => {
                        $('.report_error_msg').css({ 'display': 'none' });
                    }, 3000);
                    return false;
                }
            }
        });
    }
    else {
        $('.report_error_msg').css({ 'display': 'block' });
        $('.report_error_msg').text('All Fields Are Required.');
        setTimeout(() => {
            $('.report_error_msg').css({ 'display': 'none' });
            // location.reload();
        }, 3000);
        return false;
    }

})

$('.update_report').on('click', function () {
    var report_id = $('.report_id').val();
    var report_name = $('.report_name').val();
    var report_bread_crumb = $('.report_bread_crumb').val();
    var report_url = $('.report_url').val();
    var report_short_desc = CKEDITOR.instances['report_short_desc'].getData();
    var report_long_desc = CKEDITOR.instances['report_long_desc'].getData();
    var report_table_of_content = CKEDITOR.instances['report_table_of_content'].getData();
    var report_list_of_tables = CKEDITOR.instances['report_list_of_tables'].getData();
    var report_analysis = CKEDITOR.instances['report_analysis'].getData();
    var report_analysis_2 = CKEDITOR.instances['report_analysis_2'].getData();
    var report_mkt_sgmt = CKEDITOR.instances['report_mkt_sgmt'].getData();
    var rep_list_chart = CKEDITOR.instances['rep_list_chart'].getData();
    var rep_mar_overview = CKEDITOR.instances['rep_mar_overview'].getData();
    var rep_part2 = CKEDITOR.instances['rep_part2'].getData();
    var report_preview_part_2 = CKEDITOR.instances['report_preview_part_2'].getData();
    var report_category_id = $('.report_category_id option:selected').val();
    var report_sub_category_id = $('.report_sub_category_id option:selected').val();
    var report_publish_date = $('.report_publish_date').val();
    var report_sul = $('.report_sul').val();
    var report_el = $('.report_el').val();
    var report_cul = $('.report_cul').val();
    var report_pages = $('.report_pages').val();
    var report_type = $('.report_type option:selected').val();
    var rep_top_selling = $('.rep_top_selling option:selected').val();
    var rep_schema = $('.rep_schema option:selected').val();
    var top_selling = $('.top_selling option:selected').val();
    var is_updated = $('.is_updated option:selected').val();
    var meta_title = $('.meta_title').val();
    var meta_keywords = $('.meta_keywords').val();
    var meta_desc = $('.meta_desc').val();
    var dc_desc = $('.dc_desc').val();

    if (report_id != '' && report_id != null && report_id != undefined &&
        report_name != '' && report_name != null && report_name != undefined &&
        report_url != '' && report_url != null && report_url != undefined &&
        report_short_desc != '' && report_short_desc != null && report_short_desc != undefined &&
        report_long_desc != '' && report_long_desc != null && report_long_desc != undefined &&
        report_table_of_content != '' && report_table_of_content != null && report_table_of_content != undefined &&
        report_list_of_tables != '' && report_list_of_tables != null && report_list_of_tables != undefined &&
        report_analysis != '' && report_analysis != null && report_analysis != undefined &&
        report_analysis_2 != '' && report_analysis_2 != null && report_analysis_2 != undefined &&
        report_mkt_sgmt != '' && report_mkt_sgmt != null && report_mkt_sgmt != undefined &&
        rep_mar_overview != '' && rep_mar_overview != null && rep_mar_overview != undefined &&
        report_category_id != '' && report_category_id != null && report_category_id != undefined &&
        report_sub_category_id != '' && report_sub_category_id != null && report_sub_category_id != undefined &&
        report_publish_date != '' && report_publish_date != null && report_publish_date != undefined &&
        report_sul != '' && report_sul != null && report_sul != undefined &&
        report_el != '' && report_el != null && report_el != undefined &&
        report_cul != '' && report_cul != null && report_cul != undefined &&
        report_pages != '' && report_pages != null && report_pages != undefined &&
        report_type != '' && report_type != null && report_type != undefined &&
        // rep_top_selling != '' && rep_top_selling != null && rep_top_selling != undefined &&
        // rep_schema != '' && rep_schema != null && rep_schema != undefined &&
        // top_selling != '' && top_selling != null && top_selling != undefined &&
        meta_title != '' && meta_title != null && meta_title != undefined &&
        meta_keywords != '' && meta_keywords != null && meta_keywords != undefined &&
        meta_desc != '' && meta_desc != null && meta_desc != undefined) {
        $.ajax({
            url: '/update_report',
            type: "POST",
            data: {
                report_id: report_id,
                report_name: report_name,
                report_bread_crumb: report_bread_crumb,
                report_url: report_url,
                report_short_desc: report_short_desc,
                report_long_desc: report_long_desc,
                report_table_of_content: report_table_of_content,
                report_list_of_tables: report_list_of_tables,
                report_analysis: report_analysis,
                report_analysis_2: report_analysis_2,
                report_mkt_sgmt: report_mkt_sgmt,
                rep_list_chart: rep_list_chart,
                rep_mar_overview: rep_mar_overview,
                rep_part2: rep_part2,
                report_preview_part_2: report_preview_part_2,
                report_category_id: report_category_id,
                report_sub_category_id: report_sub_category_id,
                report_publish_date: report_publish_date,
                report_sul: report_sul,
                report_el: report_el,
                report_cul: report_cul,
                report_pages: report_pages,
                report_type: report_type,
                rep_top_selling: rep_top_selling,
                rep_schema: rep_schema,
                top_selling: top_selling,
                is_updated: is_updated,
                meta_title: meta_title,
                meta_keywords: meta_keywords,
                meta_desc: meta_desc,
                dc_desc: dc_desc
            },
            success: function (result) {
                console.log(result);
                // return false
                if (result == 1) {
                    $('.report_success_msg').css({ 'display': 'block' });
                    $('.report_success_msg').text('Report Updated successfully.');
                    setTimeout(() => {
                        $('.report_success_msg').css({ 'display': 'none' });
                        window.location.href = document.location.origin + '/reports';
                    }, 1500);
                }
                else {
                    $('.report_error_msg').css({ 'display': 'block' });
                    $('.report_error_msg').text('Something Went Wrong.Pls try again later..!!');
                    setTimeout(() => {
                        $('.report_error_msg').css({ 'display': 'none' });
                    }, 3000);
                    return false;
                }
            }
        });
    }
    else {
        $('.report_error_msg').css({ 'display': 'block' });
        $('.report_error_msg').text('All Fields Are Required.');
        setTimeout(() => {
            $('.report_error_msg').css({ 'display': 'none' });
            // location.reload();
        }, 3000);
        return false;
    }

})

$('.report_toggle_event').change(function (e) {
    // $('.console-event').html('Toggle: ' $(this).prop('checked'))
    var status = $(this).prop('checked');
    console.log(status)
    if (status == true) {
        var hidden_status = $(this).parent().parent().find('.hidden_status').val(1);
    }
    else {
        var hidden_status = $(this).parent().parent().find('.hidden_status').val(0);
    }

    var report_id = $(this).parent().parent().find('.report_id').val();
    var hidden_status = $(this).parent().parent().find('.hidden_status').val();
    console.log(report_id);
    console.log(hidden_status);
    if (report_id != '' && report_id != undefined && report_id != null) {
        $.ajax({
            url: '/update_report_status',
            type: "POST",
            data: {
                report_id: report_id,
                hidden_status: hidden_status,
            },
            success: function (result) {
                console.log(result);
                // return false
                if (result == 1) {
                    showSuccessMessage();
                }
                else {
                    showCancelMessage();
                }
            }
        });
    } else {
        $('.status_change_msg').css({ 'display': 'block' });
        $('.status_change_msg').text('Record id not found');
        setTimeout(() => {
            $('.status_change_msg').css({ 'display': 'none' });
            // location.reload();
        }, 3000);
        return false;
    }
})

function showSuccessMessage() {
    swal("Status Updated Successfully!", "", "success");
}

function showCancelMessage() {
    swal("Oops", "Something went wrong!", "error")
}