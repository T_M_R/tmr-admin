<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Session;
use Config;
use Carbon\Carbon;

class caseStudy extends Model
{
    public function getCaseStudyData($search_key){
        $query = DB::table('case_study')
                ->join('category','category.category_id','=','case_study.category_id')
                ->select('case_study.cs_id','case_study.cs_heading','case_study.url','case_study.category_id','case_study.cs_status','category.cat_name')
                ->orderBy('case_study.cs_id','DESC');

        if($search_key != ''){
            $query->where('case_study.cs_heading','like','%'.$search_key.'%');
        }

        $getCaseStudyData = $query->paginate(30);
        if(sizeof($getCaseStudyData)>0){
            return $getCaseStudyData;
        }
        else{
            return [];
        }
    }

    public function save_case_study($data){
        $date = Carbon::now();
        // print_r($data);
        // exit;
        $save_case_study = DB::table('case_study')->insert([
            'cs_heading' => $data['cs_name'],
            'url' => $data['cs_url'],
            'category_id' => $data['cs_category_id'],
            'cs_description' => $data['cs_challenge'],
            'insight' => $data['cs_insight'],
            'solution' => $data['cs_solution'],
            'result' => $data['cs_result'],
            // 'cs_image' => $data['cs_image'],
            'cs_meta_title' => $data['meta_title'],
            'cs_meta_keywords' => $data['meta_keywords'],
            'cs_meta_description' => $data['meta_desc'],
            'cs_dc_description' => $data['dc_desc'],
            'seo_title' => $data['seo_title'],
            'cs_status' => $data['cs_status'],
            'created_at' => $date
        ]);
        
        if($save_case_study == 1){
            return 1;
        }
        else{
            return 0;
        }
    }

    public function getCaseStudyDataById($id){
        $getCaseStudyDataById = DB::table('case_study')
                            ->where('cs_id','=',$id)
                            ->get();
        if(sizeof($getCaseStudyDataById)>0){
            return $getCaseStudyDataById;
        }
        else{
            return [];
        }
    }

    public function update_case_study($data){
        $date = Carbon::now();
        $update_case_study = DB::table('case_study')->where('cs_id',$data['cs_id'])->update([
            'cs_heading' => $data['cs_name'],
            'url' => $data['cs_url'],
            'category_id' => $data['cs_category_id'],
            'cs_description' => $data['cs_challenge'],
            'insight' => $data['cs_insight'],
            'solution' => $data['cs_solution'],
            'result' => $data['cs_result'],
            // 'cs_image' => $data['cs_image'],
            'cs_meta_title' => $data['meta_title'],
            'cs_meta_keywords' => $data['meta_keywords'],
            'cs_meta_description' => $data['meta_desc'],
            'cs_dc_description' => $data['dc_desc'],
            'seo_title' => $data['seo_title'],
            'cs_status' => $data['cs_status'],
            'updated_at' => $date
        ]);
        
        if($update_case_study == 1){
            return 1;
        }
        else{
            return 0;
        }
    }

    public function update_cs_status($data){
        $date = Carbon::now();
        $status='';
        if($data['hidden_status'] == 1){
            $status=1;
        }
        else{
            $status=0;
        }
        $update_cs_status = DB::table('case_study')->where('cs_id','=',$data['cs_id'])->update([
            'cs_status' => $status,
            'updated_at' => $date
        ]);
        
        if($update_cs_status == 1){
            return 1;
        }
        else{
            return 0;
        }
    }
}
