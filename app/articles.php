<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Session;
use Config;
use Carbon\Carbon;

class articles extends Model
{
    public function getArticlesData($search_key){
        $query = DB::table('tbl_articles')
                ->select('id','name','url','status')
                ->orderBy('id','DESC');

        if($search_key != ''){
            $query->where('name','like','%'.$search_key.'%');
        }

        $getArticlesData = $query->paginate(30);
        if(sizeof($getArticlesData)>0){
            return $getArticlesData;
        }
        else{
            return [];
        }
    }

    public function save_article($data){
        $date = Carbon::now();
        $month_year = $date->format("Y-m");
        // print_r($data);
        // exit;
        // $upload_date = strtotime( $data['article_publish_date']);
        $save_article = DB::table('tbl_articles')->insert([
            'rep_id' => 0,
            'name' => $data['article_name'],
            'heading_h1' => $data['article_heading'],
            'url' => $data['article_url'],
            'category_id' => $data['article_category_id'],
            'short_desc' => $data['article_short_desc'],
            'full_desc' => $data['article_long_desc'],
            'upload_date' => $data['article_publish_date'],
            'meta_title' => $data['meta_title'],
            'meta_keywords' => $data['meta_keywords'],
            'meta_description' => $data['meta_desc'],
            'status' => $data['article_status'],
            'creation' => $date,
            'modification' => $date,
            'month_year'=>$month_year
        ]);
        
        if($save_article == 1){
            return 1;
        }
        else{
            return 0;
        }
    }

    public function getArticleDataById($id){
        $getArticleDataById = DB::table('tbl_articles')
                            ->where('id','=',$id)
                            ->get();
        if(sizeof($getArticleDataById)>0){
            return $getArticleDataById;
        }
        else{
            return [];
        }
    }

     public function update_article($data){
        $date = Carbon::now();
        // $upload_date = strtotime( $data['article_publish_date']);
        // $upload_date = date('Y-m-d H:i:s',$data['article_publish_date']);
        // print_r($upload_date);
        // exit;
        $update_article = DB::table('tbl_articles')->where('id',$data['article_id'])->update([
            'rep_id' => 0,
            'name' => $data['article_name'],
            'heading_h1' => $data['article_heading'],
            'url' => $data['article_url'],
            'category_id' => $data['article_category_id'],
            'short_desc' => $data['article_short_desc'],
            'first_desc' => $data['article_first_desc'],
            'full_desc' => $data['article_long_desc'],
            'upload_date' => $data['article_publish_date'],
            'meta_title' => $data['meta_title'],
            'meta_keywords' => $data['meta_keywords'],
            'meta_description' => $data['meta_desc'],
            'status' => $data['article_status'],
            'modification' => $date
        ]);
        
        if($update_article == 1){
            return 1;
        }
        else{
            return 0;
        }
    }

    public function update_article_status($data){
        $date = Carbon::now();
        $status='';
        if($data['hidden_status'] == 1){
            $status='Y';
        }
        else{
            $status='N';
        }
        $update_article_status = DB::table('tbl_articles')->where('id','=',$data['article_id'])->update([
            'status' => $status,
            // 'updated_at' => $date
        ]);
        
        if($update_article_status == 1){
            return 1;
        }
        else{
            return 0;
        }
    }
}
