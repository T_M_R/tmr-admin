$('.save_press_release').on('click', function () {
    var press_release_name = $('.press_release_name').val();
    var press_release_url = $('.press_release_url').val();
    var press_release_heading = $('.press_release_heading').val();
    var press_release_category_id = $('.press_release_category_id option:selected').val();
    var press_release_short_desc = CKEDITOR.instances['press_release_short_desc'].getData();
    var press_release_first_desc = CKEDITOR.instances['press_release_first_desc'].getData();
    var press_release_long_desc = CKEDITOR.instances['press_release_long_desc'].getData();
    var press_release_publish_date = $('.press_release_publish_date').val();
    var meta_title = $('.meta_title').val();
    var meta_keywords = $('.meta_keywords').val();
    var meta_desc = $('.meta_desc').val();
    var press_release_status = $('.press_release_status option:selected').val();

    if (press_release_name != '' && press_release_name != null && press_release_name != undefined &&
        press_release_heading != '' && press_release_heading != null && press_release_heading != undefined &&
        press_release_url != '' && press_release_url != null && press_release_url != undefined &&
        press_release_short_desc != '' && press_release_short_desc != null && press_release_short_desc != undefined &&
        press_release_long_desc != '' && press_release_long_desc != null && press_release_long_desc != undefined &&
        press_release_category_id != '' && press_release_category_id != null && press_release_category_id != undefined &&
        // press_release_publish_date != '' && press_release_publish_date != null && press_release_publish_date != undefined &&
        press_release_status != '' && press_release_status != null && press_release_status != undefined) {
        $.ajax({
            url: '/save_press_release',
            type: "POST",
            data: {
                press_release_name: press_release_name,
                press_release_heading: press_release_heading,
                press_release_url: press_release_url,
                press_release_category_id: press_release_category_id,
                press_release_short_desc: press_release_short_desc,
                press_release_first_desc: press_release_first_desc,
                press_release_long_desc: press_release_long_desc,
                press_release_publish_date: press_release_publish_date,
                press_release_status: press_release_status,
                meta_title: meta_title,
                meta_keywords: meta_keywords,
                meta_desc: meta_desc
            },
            success: function (result) {
                console.log(result);
                // return false
                if (result == 1) {
                    $('.press_release_success_msg').css({ 'display': 'block' });
                    $('.press_release_success_msg').text('Press Release Data Saved successfully.');
                    setTimeout(() => {
                        $('.press_release_success_msg').css({ 'display': 'none' });
                        window.location.href = document.location.origin + '/press-releases';
                    }, 1500);
                }
                else {
                    $('.press_release_error_msg').css({ 'display': 'block' });
                    $('.press_release_error_msg').text('Something Went Wrong.Pls try again later..!!');
                    setTimeout(() => {
                        $('.press_release_error_msg').css({ 'display': 'none' });
                    }, 3000);
                    return false;
                }
            }
        });
    }
    else {
        $('.press_release_error_msg').css({ 'display': 'block' });
        $('.press_release_error_msg').text('All Fields Are Required.');
        setTimeout(() => {
            $('.press_release_error_msg').css({ 'display': 'none' });
            // location.reload();
        }, 3000);
        return false;
    }

})

$('.update_press_release').on('click', function () {
    var press_release_id = $('.press_release_id').val();
    var press_release_name = $('.press_release_name').val();
    var press_release_heading = $('.press_release_heading').val();
    var press_release_url = $('.press_release_url').val();
    var press_release_category_id = $('.press_release_category_id option:selected').val();
    var press_release_short_desc = CKEDITOR.instances['press_release_short_desc'].getData();
    var press_release_first_desc = CKEDITOR.instances['press_release_first_desc'].getData();
    var press_release_long_desc = CKEDITOR.instances['press_release_long_desc'].getData();
    var press_release_publish_date = $('.press_release_publish_date').val();
    var meta_title = $('.meta_title').val();
    var meta_keywords = $('.meta_keywords').val();
    var meta_desc = $('.meta_desc').val();
    var press_release_status = $('.press_release_status option:selected').val();

    if (press_release_id != '' && press_release_id != null && press_release_id != undefined &&
        press_release_heading != '' && press_release_heading != null && press_release_heading != undefined &&
        press_release_name != '' && press_release_name != null && press_release_name != undefined &&
        press_release_url != '' && press_release_url != null && press_release_url != undefined &&
        press_release_short_desc != '' && press_release_short_desc != null && press_release_short_desc != undefined &&
        press_release_long_desc != '' && press_release_long_desc != null && press_release_long_desc != undefined &&
        press_release_category_id != '' && press_release_category_id != null && press_release_category_id != undefined &&
        // press_release_publish_date != '' && press_release_publish_date != null && press_release_publish_date != undefined &&
        press_release_status != '' && press_release_status != null && press_release_status != undefined) {
        $.ajax({
            url: '/update_press_release',
            type: "POST",
            data: {
                press_release_id: press_release_id,
                press_release_heading: press_release_heading,
                press_release_name: press_release_name,
                press_release_url: press_release_url,
                press_release_category_id: press_release_category_id,
                press_release_short_desc: press_release_short_desc,
                press_release_first_desc: press_release_first_desc,
                press_release_long_desc: press_release_long_desc,
                press_release_publish_date: press_release_publish_date,
                press_release_status: press_release_status,
                meta_title: meta_title,
                meta_keywords: meta_keywords,
                meta_desc: meta_desc
            },
            success: function (result) {
                console.log(result);
                // return false
                if (result == 1) {
                    $('.press_release_success_msg').css({ 'display': 'block' });
                    $('.press_release_success_msg').text('Press Release Data Updated successfully.');
                    setTimeout(() => {
                        $('.press_release_success_msg').css({ 'display': 'none' });
                        window.location.href = document.location.origin + '/press-releases';
                    }, 1500);
                }
                else {
                    $('.press_release_error_msg').css({ 'display': 'block' });
                    $('.press_release_error_msg').text('Something Went Wrong.Pls try again later..!!');
                    setTimeout(() => {
                        $('.press_release_error_msg').css({ 'display': 'none' });
                    }, 3000);
                    return false;
                }
            }
        });
    }
    else {
        $('.press_release_error_msg').css({ 'display': 'block' });
        $('.press_release_error_msg').text('All Fields Are Required.');
        setTimeout(() => {
            $('.press_release_error_msg').css({ 'display': 'none' });
            // location.reload();
        }, 3000);
        return false;
    }

})

$('.press_release_toggle_event').change(function (e) {
    // $('.console-event').html('Toggle: ' $(this).prop('checked'))
    var status = $(this).prop('checked');
    console.log(status)
    if (status == true) {
        var hidden_status = $(this).parent().parent().find('.hidden_status').val(1);
    }
    else {
        var hidden_status = $(this).parent().parent().find('.hidden_status').val(0);
    }

    var press_release_id = $(this).parent().parent().find('.press_release_id').val();
    var hidden_status = $(this).parent().parent().find('.hidden_status').val();
    console.log(press_release_id);
    console.log(hidden_status);
    if (press_release_id != '' && press_release_id != undefined && press_release_id != null) {
        $.ajax({
            url: '/update_press_release_status',
            type: "POST",
            data: {
                press_release_id: press_release_id,
                hidden_status: hidden_status,
            },
            success: function (result) {
                console.log(result);
                // return false
                if (result == 1) {
                    showSuccessMessage();
                }
                else {
                    showCancelMessage();
                }
            }
        });
    } else {
        $('.status_change_msg').css({ 'display': 'block' });
        $('.status_change_msg').text('Record id not found');
        setTimeout(() => {
            $('.status_change_msg').css({ 'display': 'none' });
            // location.reload();
        }, 3000);
        return false;
    }
})

function showSuccessMessage() {
    swal("Status Updated Successfully!", "", "success");
}

function showCancelMessage() {
    swal("Oops", "Something went wrong!", "error")
}