@extends('layouts.header')
@section('body-content')
<section class="sub-category-list">
    <div class="block-header">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 my-auto">
                <h4 class="page-title m-0">Edit Compnay</h4>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <a href="{{URL:: to('/companies')}}">
                    <button type="button" class="btn btn-info float-right">Back</button>
                </a>
            </div>
        </div>
    </div>
    <div class="row clearfix mt-3">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="card project_widget">
                <div class="body mt-4 mb-4">
                    @foreach($edit_company as $value)
                    <div class="row">
                        <div class="col-12 col-md-12 mb-0">
                            <div class="input-group">  
                                <input type="text" class="company_name" autocomplete="off" value="{{$value->company_name}}" required/>
                                <label>Company Name <span class="text-danger">*</span></label>
                            </div>
                        </div>
                        
                        <div class="col-12 col-md-12 mb-0">
                            <label class="" style="color:#414244;"><b>Company Description</b><span class="text-danger">*</span></label>
                            <div class="input-group mt-5">  
                                <textarea class="company_desc ckeditor" id="company_desc" name="sub_category_desc"> {{$value->company_description}} </textarea>
                            </div>
                        </div>
                        <div class="col-12 col-md-12 mb-0">
                            <div class="input-group">  
                                <input type="text" class="meta_title" autocomplete="off" value="{{$value->meta_title}}" required/>
                                <label>Meta Title </label>
                            </div>
                        </div>
                        <div class="col-12 col-md-12 mb-0">
                            <div class="input-group">  
                                <input type="text" class="meta_keywords" autocomplete="off" value="{{$value->meta_keyword}}" required/>
                                <label>Meta Keywords</label>
                            </div>
                        </div>
                        <div class="col-12 col-md-12 mb-0">
                            <label class="" style="color:#414244;"><b>Meta Description</b></label>
                            <div class="form-group mt-5"> 
                                <div class="form-line">  
                                    <textarea class="form-control meta_desc p-3" name="meta_desc" rows="4" placeholder="Meta Description"> {{$value->meta_description}} </textarea>
                                </div>
                            </div>
                        </div>
                        <input type="hidden" class="compnay_id" value="{{$value->company_id}}">
                        <div class="col-12 col-md-4 mb-0">
                            <p class="text-danger mb-0">* All fields are Mandatory</p>
                        </div>
                    </div>
                    @endforeach
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb-0 text-center">
                            <div class="form-group">
                                <div class="form-line">
                                    <div class="company_error_msg col-red w-100" style="display:none"></div>
                                    <div class="company_success_msg col-green w-100" style="display:none"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 text-center">
                            <button type="submit" class="btn btn-primary waves-effect update_company">UPDATE</button>                                                                
                        </div>
                    </div>    
                </div>
            </div>
        </div>
    </div>
</section>
@endsection