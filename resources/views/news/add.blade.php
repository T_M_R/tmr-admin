@extends('layouts.header')
@section('body-content')
<section class="sub-category-list">
    <div class="block-header">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 my-auto">
                <h4 class="page-title m-0">Add News</h4>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <a href="{{URL:: to('/news')}}">
                    <button type="button" class="btn btn-info float-right">Back</button>
                </a>
            </div>
        </div>
    </div>
    <div class="row clearfix mt-3">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="card project_widget">
                <div class="body mt-4 mb-4">
                    <form method="POST" action="{{url('save_news')}}" enctype="multipart/form-data">   
                        <div class="row">
                            <div class="col-12 col-md-6 mb-0">
                                <div class="input-group">  
                                    <input type="text" class="news_name" name="news_name" autocomplete="off" value="" required/>
                                    <label>Title <span class="text-danger">*</span></label>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 mb-0">
                                <div class="input-group">  
                                    <input type="text" class="news_url" name="news_url" autocomplete="off" value="" required/>
                                    <label>URL <span class="text-danger">*</span></label>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 mb-0">
                                <div class="input-group">  
                                    <input type="text" class="news_by" name="news_by" autocomplete="off" value="" required/>
                                    <label>News By <span class="text-danger">*</span></label>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 mb-0">
                                <div class="input-group">  
                                    <input type="file" class="fileToUpload" name="fileToUpload" autocomplete="off" value="" />
                                    <input class="form-control" name="hiddenfileToUpload" type="hidden" id="oldImage" class="fileToUpload" style="border-bottom: 0px;"/>
                                    <!-- <label>Image <span class="text-danger">*</span></label> -->
                                </div>
                            </div>
                            <div class="col-12 col-md-12 mb-0">
                                <label class="" style="color:#414244;"><b>Short Description</b><span class="text-danger">*</span></label>
                                <div class="input-group mt-5">
                                    <textarea class="news_short_desc ckeditor" name="news_short_desc" id="news_short_desc" >  </textarea>
                                </div>
                            </div>
                            <div class="col-12 col-md-12 mb-0 d-none">
                                <label class="" style="color:#414244;"><b>Long Description</b><span class="text-danger">*</span></label>
                                <div class="input-group mt-5">  
                                    <textarea class="news_long_desc ckeditor" id="news_long_desc" name="news_long_desc">  </textarea>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 mb-0">
                                <div class="input-group">
                                    <select class="floating-select news_category_id" name="news_category_id" onclick="this.setAttribute('value', this.value);" value="" required>
                                        @if(sizeof($getCategories)>0)
                                            <option value=""></option>
                                            @foreach($getCategories as $val)
                                                <option value="{{$val->category_id}}">{{$val->cat_name}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                    <label>Category <span class="text-danger">*</span></label>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 mb-0">
                                <div class="input-group">  
                                    <input type="text" class="datepicker news_publish_date" name="news_publish_date" autocomplete="off" value="" required/>
                                    <span style="position: absolute;top: 8px;right: 0;bottom: 0;"><i class="icon-calendar" style="font-size: 26px;"></i></span>
                                    <label>Publish Date <span class="text-danger">*</span></label>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 mb-0">
                                <div class="input-group">  
                                    <input type="text" class="meta_title" name="meta_title" autocomplete="off" value="" />
                                    <label>Meta Title </label>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 mb-0">
                                <div class="input-group">  
                                    <input type="text" class="meta_keywords" name="meta_keywords" autocomplete="off" value="" />
                                    <label>Meta Keywords</label>
                                </div>
                            </div>
                            <div class="col-12 col-md-12 mb-0">
                                <label class="" style="color:#414244;"><b>Meta Description</b></label>
                                <div class="form-group mt-5"> 
                                    <div class="form-line">  
                                        <textarea class="form-control meta_desc p-3" name="meta_desc" rows="4" placeholder="Meta Description">  </textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 mb-0">
                                <div class="input-group">
                                    <select class="floating-select news_status" name="news_status" onclick="this.setAttribute('value', this.value);" value="" required>
                                        <option value=""></option>
                                        <option value="Y">Active</option>
                                        <option value="N">In-Active</option>
                                    </select>
                                    <label>Status <span class="text-danger">*</span></label>
                                </div>
                            </div>
                            <div class="col-12 col-md-12 mb-0">
                                <p class="text-danger mb-0">* All fields are Mandatory</p>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb-0 text-center">
                                <div class="form-group">
                                    <div class="form-line">
                                        <div class="article_error_msg col-red w-100" style="display:none"></div>
                                        <div class="article_success_msg col-green w-100" style="display:none"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 text-center">
                                <button type="submit" class="btn btn-primary waves-effect">SUBMIT</button>                                                                
                            </div>
                        </div> 
                    </form>   
                </div>
            </div>
        </div>
    </div>
</section>
@endsection