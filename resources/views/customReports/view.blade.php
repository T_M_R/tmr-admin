@extends('layouts.header')
@section('body-content')
<section class="sub-category-list">
    <div class="block-header">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 my-auto">
                <h4 class="page-title m-0">View Custom Report</h4>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <a href="{{URL:: to('/custom_reports')}}">
                    <button type="button" class="btn btn-info float-right">Back</button>
                </a>
            </div>
        </div>
    </div>
    <div class="row clearfix mt-3">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="card project_widget">
                <div class="body mt-4 mb-4">
                    @if(isset($view_custom_report) && sizeof($view_custom_report)>0)
                        @foreach($view_custom_report as $vreport)
                            <div class="row">
                                <div class="col-12 col-md-12 mb-0">
                                    <div class="input-group">  
                                        <input type="text" class="custom_report_name" autocomplete="off" value="{{$vreport->rep_name}}" disabled required/>
                                        <label>Report Name <span class="text-danger">*</span></label>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6 mb-0">
                                    <div class="input-group">  
                                        <input type="text" class="custom_report_url" autocomplete="off" value="{{$vreport->rep_url}}" disabled required/>
                                        <label>Report URL <span class="text-danger">*</span></label>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6 mb-0">
                                    <div class="input-group">  
                                        <input type="number" class="custom_report_sul" autocomplete="off" value="{{$vreport->rep_price_sul}}" disabled required/>
                                        <label>Price-SUL <span class="text-danger">*</span></label>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6 mb-0">
                                    <div class="input-group">  
                                        <input type="number" class="custom_report_el" autocomplete="off" value="{{$vreport->rep_price_el}}" disabled required/>
                                        <label>Price-EL<span class="text-danger">*</span></label>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6 mb-0">
                                    <div class="input-group">  
                                        <input type="number" class="custom_report_cul" autocomplete="off" value="{{$vreport->rep_price_cul}}" disabled required/>
                                        <label>Price-CUL <span class="text-danger">*</span></label>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6 mb-0">
                                    <div class="input-group">
                                        <select class="floating-select custom_report_category_id" onclick="this.setAttribute('value', this.value);" value="" disabled required>
                                            @if(sizeof($getCategories)>0)
                                                <option value=""></option>
                                                @foreach($getCategories as $val)
                                                    @if($val->category_id == $vreport->cat_id)
                                                        <option value="{{$val->category_id}}" selected>{{$val->cat_name}}</option>
                                                    @else
                                                        <option value="{{$val->category_id}}">{{$val->cat_name}}</option>
                                                    @endif
                                                @endforeach
                                            @endif
                                        </select>
                                        <label>Category <span class="text-danger">*</span></label>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6 mb-0">
                                    <div class="input-group">  
                                        <input type="text" class="datepicker custom_report_publish_date" autocomplete="off" value="{{$vreport->rep_pub_date}}" disabled required/>
                                        <span style="position: absolute;top: 8px;right: 0;bottom: 0;"><i class="icon-calendar" style="font-size: 26px;"></i></span>
                                        <label>Publish Date <span class="text-danger">*</span></label>
                                    </div>
                                </div>
                                <input type="hidden" class="custom_report_id" value="{{$vreport->rep_id}}">
                            </div>
                        @endforeach
                    @else
                        <div class="row">
                            <div class="col-md-12">
                                <h5 class="text-danger">Something went wrong while fetching Data from DB</h5>
                            </div>
                        </div>
                    @endif   
                </div>
            </div>
        </div>
    </div>
</section>
@endsection