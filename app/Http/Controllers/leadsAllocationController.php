<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\leadsAllocation;
use App\master;
use App\mail;
use App\users;
use Carbon\Carbon;
use Redirect;
use Session;
use DB;
use Config;

class leadsAllocationController extends Controller
{
    public function __construct(){
        $this->leads = new leadsAllocation();
        $this->master = new master();
        $this->users = new users();
    }

    public function get_lead_allocations(Request $request){
        if (!session('username')) {
	    	return redirect('/');
        }  

        $user_id='';
        $category_id='';
        $country_id='';
        $filterFlag= 0;

        if(isset($_GET['user'])){
            $filterFlag= 1;
            $user_id=$_GET['user'];
        }
        if(isset($_GET['category_id'])){
            $filterFlag= 1;
            $category_id=$_GET['category_id'];
        }
        if(isset($_GET['country_id'])){
            $filterFlag= 1;
            $country_id=$_GET['country_id'];
        }

        $get_my_team = $this->users->get_my_team();

        $getCountriesList = $this->leads->getCountriesList();
        $getCategories = $this->leads->getCategories();
        $getUsers = $this->leads->getUsersForFilter();
        
        $users = $this->leads->getUsers($get_my_team,$user_id);
        $leadAllocations = $this->leads->getLeadAllocations($get_my_team,$user_id,$category_id,$country_id,$filterFlag);
        if(sizeof($leadAllocations)>0){
            $leadAllocations = $this->leads->prepareAllocationsList($leadAllocations, $users);
        }
        else if($filterFlag == 0){
            $leadAllocations = $this->leads->prepareAllocationsList($leadAllocations, $users);
        }else{
            $leadAllocations = [];
        }

        return view('leadAllocations.list')->with('leadAllocations',$leadAllocations)->with('getCountriesList',$getCountriesList)->with('getCategories',$getCategories)->with('getUsers',$getUsers);
    }

    public function edit_lead_allocations(Request $request,$id){
        if (!session('username')) {
	    	return redirect('/');
        } 

        $getRegionsList = $this->leads->getRegionsList();
        $getCountriesList = $this->leads->getCountriesList();
        $getCategories = $this->leads->getCategories();
        $getUserName =  $this->leads->getUserName($id);
        $lead_allocations =  $this->leads->getAllocationsGroupByRegion($id);
        
        return view('leadAllocations.edit')->with('getRegionsList',$getRegionsList)->with('getCountriesList',$getCountriesList)->with('getCategories',$getCategories)->with('getUserName',$getUserName)->with('lead_allocations',$lead_allocations);
    }

    public function view_lead_allocation(Request $request,$id){
        if (!session('username')) {
	    	return redirect('/');
        } 

        $getRegionsList = $this->leads->getRegionsList();
        $getCountriesList = $this->leads->getCountriesList();
        $getCategories = $this->leads->getCategories();
        $getUserName =  $this->leads->getUserName($id);
        $lead_allocations =  $this->leads->getAllocationsGroupByRegion($id);
        // print_r($getUserName);
        // exit;
        return view('leadAllocations.view')->with('getRegionsList',$getRegionsList)->with('getCountriesList',$getCountriesList)->with('getCategories',$getCategories)->with('getUserName',$getUserName)->with('lead_allocations',$lead_allocations);
    }

    public function get_regions_by_category(Request $request){
        if (!session('username')) {
	    	return redirect('/');
        } 

        $data = $request->all();
        $result =  $this->leads->get_regions_by_category($data);
        return $result;
    }

    public function get_countries_by_region(Request $request){
        if (!session('username')) {
	    	return redirect('/');
        } 

        $data = $request->all();
        $result =  $this->leads->get_countries_by_region($data);
        return $result;
    }

    public function updateLeadAllocations(Request $request){
        if (!session('username')) {
	    	return redirect('/');
        } 

        $data = $request->all();
        $userid = $data['userid'];
        $cat_id = $data['cat_id'];
        $region_id = $data['region_id'];
        $country_id = $data['country_id'];

        $getUserMail = $this->leads->getUserMail($userid);
        $getCatName = $this->leads->getCatName($cat_id);

        $result = $this->leads->updateLeadAllocations($userid, $cat_id, $region_id, $country_id);

        if($result == 1){
            // For Mail 
            $loopCount = 0;
            $success_Mails_Array = [];
            $failed_MailsArray = [];

            $email_subject = "Lead Allocation"; 
            $email_body = "You have been allocated with new Category - " .$getCatName[0]->cat_name . " to manage sales";
            // $email_to = $getUserMail[0]->email; 
            $email_to = "rakeshm@auroraelabs.com"; 
            $email_from = "rakeshm@auroraelabs.com";

            if($email_to != '' || $email_to != null){
                $mailto="$email_to";
                
                if (filter_var($mailto, FILTER_VALIDATE_EMAIL)) 
                {
                    $email = new \SendGrid\Mail\Mail(); 
                    $email->setFrom($email_from, "TMR");
                    $email->setSubject($email_subject);
                    $email->addTo($mailto);
                    $email->addContent(
                        "text/html", "$email_body"
                    );
            
                    $sendgrid = new \SendGrid(getenv('SENDGRID_API_KEY'));
                    try {
                        $response = $sendgrid->send($email);
                        array_push($success_Mails_Array,$email_to);
                    } catch (Exception $e) {
                        array_push($failed_MailsArray,$email_to);
                        echo 'Caught exception: '. $e->getMessage() ."\n";
                    }
                } 
                else{
                    array_push($failed_MailsArray,$email_to);
                }
            }
        }
        return $result;
        
    }

    public function deleteLeadAllocations(Request $request){
        if (!session('username')) {
	    	return redirect('/');
        } 
        
        $data = $request->all();
       
        $country_id = $data['country_id'];
        $result = $this->leads->deleteLeadAllocations($data,$country_id);
        return $result;
    }

    public function un_allocated_countries(Request $request){
        if (!session('username')) {
	    	return redirect('/');
        }  

        $category_id='';
        if(isset($_GET['category_id'])){
            $category_id=$_GET['category_id'];
        }

        $getCategories = $this->leads->getCategories();
        if(empty($category_id)){   
            $category_id = $getCategories[0]->category_id;
        }
        $get_un_allocated_countries = $this->leads->get_un_allocated_countries($category_id);

        return view('unAllocatedCountries.list')->with('getCategories',$getCategories)->with('get_un_allocated_countries',$get_un_allocated_countries);
    }
    
}
