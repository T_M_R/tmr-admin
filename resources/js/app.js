require('./bootstrap');

domain = window.location.origin;
url_path = window.location.pathname;

$('.user_profile .dropdown-toggle').on('click', function () {
    $(this).parent().find('.dropdown-menu').addClass('show');
})

$(document).on('click', function (event) {
    var $trigger = $('.dropdown');
    if ($trigger !== event.target && !$trigger.has(event.target).length) {
        $('.dropdown-menu').removeClass('show');
    }
});

$('.onchange-select').on('change', function () {
    var optionvalue = $(this).val();
    if (optionvalue != '') {
        location = optionvalue;
    }
    else {

    }
})

$('.update_password').on('click', function () {
    let current_pw = $('.current_pw').val();
    let new_pw = $('.new_pw').val();
    let re_enter_new_pw = $('.re_enter_new_pw').val();

    if (current_pw != '' && new_pw != '' && re_enter_new_pw != '') {
        $.ajax({
            url: '/update_password',
            type: "POST",
            data: {
                current_pw: current_pw,
                new_pw: new_pw,
                re_enter_new_pw: re_enter_new_pw
            },
            success: function (result) {
                console.log(result);
                if (result == 1) {
                    $('.pw_change_success_msg').css({ 'display': 'block' });
                    $('.pw_change_success_msg').text('Password Updated successfully.');
                    setTimeout(() => {
                        $('.pw_change_success_msg').css({ 'display': 'none' });
                        // window.location.href = document.location.origin + '/profile';
                        location.reload();
                    }, 3000);
                }
                else if (result == 2) {
                    $('.pw_change_error_msg').css({ 'display': 'block' });
                    $('.pw_change_error_msg').text("Current Password doesn't match with our records");
                    setTimeout(() => {
                        $('.pw_change_error_msg').css({ 'display': 'none' });
                    }, 3000);
                    return false;
                }
                else {
                    $('.pw_change_error_msg').css({ 'display': 'block' });
                    $('.pw_change_error_msg').text('Something Went Wrong.Pls try again later..!!');
                    setTimeout(() => {
                        $('.pw_change_error_msg').css({ 'display': 'none' });
                    }, 3000);
                    return false;
                }
            }
        });
    }
});

import('./dashboard.js');
import('./toggle_switch.js');
import('./master.js');
import('./lead_allocation.js');
import('./roles.js');
import('./users.js');
import('./reports.js');
import('./custom_reports.js');
import('./leads.js');
import('./articles.js');
import('./case_study.js');
import('./infographic.js');
import('./news.js');
import('./mail.js');
import('./press_release.js');