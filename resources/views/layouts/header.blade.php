<!-- Top Bar -->
@extends('main')
@section('content')
<nav class="navbar">
    <div class="container-fluid">
        <div class="navbar-header">
            <a href="#" onClick="return false;" class="navbar-toggle collapsed" data-toggle="collapse"
                data-target="#navbar-collapse" aria-expanded="false"></a>
            <a href="#" onClick="return false;" class="bars"></a>
            <a class="navbar-brand" href="index.html">
                <!-- <img src="assets/images/logo.png" alt="" /> -->
                <img src="{{URL::asset('assets/images/project/tmr-logo.png')}}" alt="tmr-logo" style="height:100%">
                <!-- <span class="logo-name">Blize</span> -->
            </a>
        </div>
        <div class="collapse navbar-collapse" id="navbar-collapse">
            <ul class="nav navbar-nav navbar-left">
                <li>
                    <a href="#" onClick="return false;" class="sidemenu-collapse">
                        <i data-feather="align-justify"></i>
                    </a>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <!-- Full Screen Button -->
                <li class="fullscreen">
                    <a href="javascript:;" class="fullscreen-btn">
                        <i data-feather="maximize"></i>
                    </a>
                </li>
                <!-- #END# Full Screen Button -->
                <!-- #START# Notifications-->
                
                <!-- #END# Notifications-->
                <li class="dropdown user_profile">
                    <div class="chip dropdown-toggle" data-toggle="dropdown">
                        <!-- <img src="assets/images/user.jpg" alt="Contact Person"> -->
                        {{Session::get('username')}}
                    </div>
                    <ul class="dropdown-menu pullDown">
                        <li class="body">
                            <ul class="user_dw_menu">
                                <li>
                                    <a href="{{URL:: to('/profile')}}">
                                        <i class="material-icons">person</i>Profile
                                    </a>
                                </li>
                                <li>
                                    <a href="{{URL:: to('/logout')}}" class="js-right-sidebar">
                                        <i class="material-icons">power_settings_new</i>Logout
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <!-- #END# Tasks -->
            </ul>
        </div>
    </div>
</nav>
@include('layouts.sidebar')
<section class="content">
    <div class="container-fluid">
        @yield('body-content')
    </div>
</section>
@endsection
<!-- #Top Bar -->