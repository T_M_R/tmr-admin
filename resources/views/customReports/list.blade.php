@extends('layouts.header')
@section('body-content')
<section class="report-list">
    <div class="block-header">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 my-auto">
                <h4 class="page-title m-0">Custom Reports</h4>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5 my-auto">
                <div class="input-group m-0">
                    <form method="GET" action="{{url('/custom_reports')}}" enctype="multipart/form-data" role="form" autocomplete="off">
                        <input type="search" id="search_key" name="search_key" class="search-field m-0" value="{{$search_key}}" placeholder="Search Report Name">
                        <div class="input-group-append">
                            <button type="submit" class="btn btn-primary"><i class="fa fa-search mr-1"></i> Search</button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-1 col-lg-1 pl-0">
                <form method="GET" action="{{url('/custom_reports')}}" enctype="multipart/form-data" role="form" autocomplete="off">
                    <button type="submit" class="btn btn-clear">Clear</button>
                </form>
            </div>
            <?php 
                $sessionUser=Session::get('mainMenuArray');  
                $val=[];
                $allRoutes=[];
                $addButtonStatus=0;
                $editButtonStatus=0;
                $viewButtonStatus=0;
                $downloadButtonStatus=0;
                foreach($sessionUser as $PagePermission)
                {
                    if(isset($PagePermission['route'])){
                        if($PagePermission['route'] == "custom_reports")
                        {
                            $addButtonStatus=$PagePermission['is_add'];
                            $editButtonStatus=$PagePermission['is_edit'];
                            $viewButtonStatus=$PagePermission['is_view'];
                            $downloadButtonStatus=$PagePermission['is_download'];
                        }
                    }
                }
            ?>
            <div class="col-xs-12 col-sm-12 col-md-1 col-lg-1 my-auto pl-0">
                @if($downloadButtonStatus == 1)
                <form method="GET" action="{{url('/custom_reports_csv_download')}}" enctype="multipart/form-data" class="mb-0" role="form" autocomplete="off">
                    <button type="submit" class="btn btn-info h-100"><i data-feather="download"></i></bitton>
                </form>
                @endif
            </div>
            <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
                @if($addButtonStatus == 1)
                <a href="{{URL:: to('/add_custom_report')}}">
                    <button type="button" class="btn btn-info float-right">Add New</button>
                </a>
                @endif
            </div>
        </div>
    </div>
    <div class="row clearfix mt-3">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            @if(sizeof($getCustomReports)>0)
            <p class="status_change_msg text-center"></p>
            <div class="card">
                <div class="body p-0">
                    <div class="table-responsive">
                        <!-- <table class="table table-bordered table-striped table-hover js-basic-example dataTable"> -->
                        <table class="table table-bordered mb-0">
                            <thead>
                                <tr>
                                    <th>S.No</th>
                                    <th>Report</th>
                                    <th>Report URL</th>
                                    <th>Publish Date</th>
                                    <th>Status</th>
                                    @if($editButtonStatus == 1 || $viewButtonStatus == 1)
                                    <th>Action</th>
                                    @endif
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                    $page_id = $getCustomReports->currentPage();
                                    if($page_id == 1 || $page_id == '')
                                    {
                                        $i=1; 
                                    }
                                    else
                                    {
                                        $i=($page_id-1)*30;
                                        $i= $i+1;
                                    }
                                ?>
                                @foreach($getCustomReports as $value)
                                    <tr>
                                        <td>{{$i}}</td>
                                        <td>{{$value->rep_name}}</td>
                                        <td><a href="{{'https://www.transparencymarketresearch.com/'.$value->rep_url}}" target="_blank">{{$value->rep_url}}</a></td>
                                        <td>
                                            @if($value->rep_pub_date != '')
                                                <?php $rep_pub_date = date("d-m-Y", strtotime($value->rep_pub_date));?>
                                                {{$rep_pub_date}}
                                            @else
                                                -----
                                            @endif
                                        </td>
                                        <td>
                                            @if($value->rep_status == 'Y')
                                                <input class="checkbox custom_report_toggle_event" type="checkbox" data-toggle="toggle" data-style="ios" checked>
                                                <input type="hidden" name="hidden_status" class="hidden_status" value="1" />
                                            @else
                                                <input class="checkbox custom_report_toggle_event" type="checkbox" data-toggle="toggle" data-style="ios">
                                                <input type="hidden" name="hidden_status" class="hidden_status" value="0"/>
                                            @endif
                                            <input type="hidden" class="custom_report_id" value="{{$value->rep_id}}">
                                        @if($editButtonStatus == 1 || $viewButtonStatus == 1)
                                        <td>
                                            @if($editButtonStatus == 1)
                                            <a href="{{URL:: to('/edit_custom_report/'.$value->rep_id)}}" class="text-decoration-none">
                                                <button class="btn tblActnBtn">
                                                    <i class="material-icons">mode_edit</i>
                                                </button>
                                            </a>
                                            @endif
                                            @if($viewButtonStatus == 1)
                                            <a href="{{URL:: to('/view_custom_report/'.$value->rep_id)}}" class="text-decoration-none">
                                                <button class="btn tblActnBtn">
                                                <i class="fa fa-eye" aria-hidden="true"></i>
                                                </button>
                                            </a>
                                            @endif
                                        </td>
                                        @endif
                                    </tr>
                                    <?php $i++;?>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            @else
                <div class="card">
                    <p class="text-center py-5" style="font-size:25px">No Data Available To Show</p>  
                </div>
            @endif
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 paginate-sec d-none">
            @if(sizeof($getCustomReports)>0)
                {{$getCustomReports->links()}}
            @endif
        </div>
    </div>
</section>
@endsection