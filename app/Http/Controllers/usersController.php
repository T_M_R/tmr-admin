<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\login;
use App\users;
use Carbon\Carbon;
use Redirect;
use Session;
use DB;
use Config;

class usersController extends Controller
{
    public function __construct(){
        $this->login = new login();
        $this->users = new users();
    }

    public function getUsersList(Request $request){
        if (!session('username')) {
	    	return redirect('/');
        } 
        $search_key = '';
        if(isset($_GET['search_key'])){
            $search_key=$_GET['search_key'];
        }
        $getUsersList = $this->users->getUsersList($search_key);
        return view('users.list')->with('getUsersList',$getUsersList)->with('search_key',$search_key);
    }

    public function addNewUser(Request $request){
        if (!session('username')) {
	    	return redirect('/');
        } 

        $getRoles = $this->users->getRoles();
        return view('users.add')->with('getRoles',$getRoles);
    }

    public function user_duplicate_check(Request $request){
        if (!session('username')) {
	    	return redirect('/');
        } 
        
        $data = $request->all();
        $user_duplicate_check = $this->users->user_duplicate_check($data);
        return $user_duplicate_check;
    }

    public function save_user(Request $request){
        if (!session('username')) {
	    	return redirect('/');
        } 
        $data = $request->all();
        $save_user = $this->users->save_user($data);
        return $save_user;
    }

    public function edit_user(Request $request,$id){
        if (!session('username')) {
	    	return redirect('/');
        } 
        $data = $request->all();
        $getRoles = $this->users->getRoles();
        $edit_user = $this->users->edit_user($id);
        $get_role_supervisors = $this->users->get_role_supervisors($edit_user[0]->role_id);
        return view('users.edit')->with('edit_user',$edit_user)->with('getRoles',$getRoles)->with('get_role_supervisors',$get_role_supervisors);
    }

    public function view_user(Request $request,$id){
        if (!session('username')) {
	    	return redirect('/');
        } 
        $data = $request->all();
        $getRoles = $this->users->getRoles();
        $edit_user = $this->users->edit_user($id);
        $get_role_supervisors = $this->users->get_role_supervisors($edit_user[0]->role_id);
        return view('users.view')->with('edit_user',$edit_user)->with('getRoles',$getRoles)->with('get_role_supervisors',$get_role_supervisors);
    }

    public function update_user(Request $request){
        if (!session('username')) {
	    	return redirect('/');
        } 
        $data = $request->all();
        $update_user = $this->users->update_user($data);
        return $update_user;
    }

    public function get_role_supervisors(Request $request){
        if (!session('username')) {
	    	return redirect('/');
        } 
        $data = $request->all();
        $get_role_supervisors = $this->users->get_role_supervisors($data);
        return $get_role_supervisors;
    }

    public function update_user_status(Request $request){
        if (!session('username')) {
	    	return redirect('/');
        } 
        
        $data = $request->all();
        $update_user_status = $this->users->update_user_status($data);
        return $update_user_status;
    }
}
