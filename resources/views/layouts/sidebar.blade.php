<div>
    <!-- Left Sidebar -->
    <aside id="leftsidebar" class="sidebar">
        <!-- Menu -->
        <div class="menu">
            <ul class="list">
                <!-- <li class="header">-- Main</li> -->
                <?php $mainMenuArray = session('mainMenuArray');?>
                <?php $subMenuArray = session('subMenuArray');?>
                @foreach($mainMenuArray as $mainMenu)
                    <?php $main_menu_name = $mainMenu['name']; $main_menu_route = $mainMenu['route']; $main_menu_id = $mainMenu['id']; $main_menu_parent_id = $mainMenu['parent_page_id']; $main_menu_view = $mainMenu['is_view'] ?>    
                    @if($main_menu_view != 0)
                        @if($main_menu_route != '#')
                            @if( \Request::is($main_menu_route) )
                                <li class="active">
                                    <a href="{{URL:: to($main_menu_route)}}">
                                        <i class="{{$mainMenu['page_icon']}}"></i>
                                        <span>{{$main_menu_name}}</span>
                                    </a>
                                </li>
                            @else
                                <li>
                                    <a href="{{URL:: to($main_menu_route)}}">
                                        <i class="{{$mainMenu['page_icon']}}"></i>
                                        <span>{{$main_menu_name}}</span>
                                    </a>
                                </li>
                            @endif
                        @else
                            <?php $i = 0;?>
                            @foreach($subMenuArray as $subMenu)
                                <?php $subMenu_name = $subMenu['name']; $subMenu_route = $subMenu['route']; $subMenu_id = $subMenu['id']; $subMenu_parent_id = $subMenu['parent_page_id'];?>
                                @if($mainMenu['id'] == $subMenu['parent_page_id'])
                                    @if( \Request::is($subMenu_route) )
                                        <?php $i++; ?>
                                    @endif
                                @endif
                            @endforeach
                            <?php $li=''; if($i>0){ $li='active'; } ?>
                            @if($i>0)
                                <li class="<?php echo $li; ?> ">
                                    <a href="#" class="menu-toggle">
                                        <i class="{{$mainMenu['page_icon']}}"></i>
                                        <span>{{$main_menu_name}}</span>
                                    </a>
                                    <ul class="ml-menu">
                                        @foreach($subMenuArray as $subMenu)
                                            @if($subMenu['is_view'] != 0)
                                                @if($mainMenu['id'] == $subMenu['parent_page_id'])
                                                    @if( \Request::is($subMenu['route']) )
                                                        <li class="active">
                                                            <a href="{{URL:: to($subMenu['route'])}}">
                                                                <img class="icon-white" src="{{URL::asset('assets/images/icons/'.$subMenu['page_icon'])}}" alt="">
                                                                <span>{{$subMenu['name']}}</span>
                                                            </a>
                                                        </li>
                                                    @else
                                                        <li>
                                                            <a href="{{URL:: to($subMenu['route'])}}">
                                                                <img class="icon-white" src="{{URL::asset('assets/images/icons/'.$subMenu['page_icon'])}}" alt="">
                                                                <span>{{$subMenu['name']}}</span>
                                                            </a>
                                                        </li>
                                                    @endif
                                                @endif
                                            @endif
                                        @endforeach
                                    </ul>
                                </li>
                            @else
                                <li class="<?php echo $li; ?> ">
                                    <a href="#" class="menu-toggle">
                                        <i class="{{$mainMenu['page_icon']}}"></i>
                                        <span>{{$main_menu_name}}</span>
                                    </a>
                                    <ul class="ml-menu">
                                        @foreach($subMenuArray as $subMenu)
                                            @if($subMenu['is_view'] != 0)
                                                @if($mainMenu['id'] == $subMenu['parent_page_id'])
                                                    @if( \Request::is($subMenu['route']) )
                                                        <li class="active">
                                                            <a href="{{URL:: to($subMenu['route'])}}">
                                                                <img class="icon-white" src="{{URL::asset('assets/images/icons/'.$subMenu['page_icon'])}}" alt="">
                                                                <span>{{$subMenu['name']}}</span>
                                                            </a>
                                                        </li>
                                                    @else
                                                        <li>
                                                            <a href="{{URL:: to($subMenu['route'])}}">
                                                                <img class="icon-white" src="{{URL::asset('assets/images/icons/'.$subMenu['page_icon'])}}" alt="">
                                                                <span>{{$subMenu['name']}}</span>
                                                            </a>
                                                        </li>
                                                    @endif
                                                @endif
                                            @endif
                                        @endforeach
                                    </ul>
                                </li>
                            @endif
                        @endif
                    @endif
                @endforeach
            </ul>
        </div>
        <!-- #Menu -->
    </aside>
    <!-- #END# Left Sidebar -->
</div>