@extends('layouts.header')
<style>
    td{
        background: #fff !important;
    }
</style>
@section('body-content')
<section class="regions-list">
    <div class="block-header">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 my-auto">
                <h4 class="page-title m-0">View Lead Allocations</h4>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <a href="{{URL:: to('/lead_allocations')}}">
                    <button type="button" class="btn btn-info float-right">Back</button>
                </a>
            </div>
        </div>
    </div>
    <div class="row clearfix mt-3">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="card project_widget">
                <div class="card-header">
                    <h4 styles="width:fit-content;" class="m-0"><i class="fa fa-user-circle"></i> Lead Allocations <span> for </span> <span style="text-transform: capitalize;"><?php echo $getUserName[0]->name; ?></span></h4>
                </div><br>
                <div class="body">
                    <div class="row">
                        <div class="col-md-12 mb-0">
                            <div class="row">
                                <div class="col-md-2">
                                    <strong>Name</strong>
                                </div>
                                <div class="col-md-1">:</div>
                                <div class="col-md-3">
                                    <p class="text-muted">{{$getUserName[0]->name}}</p>
                                </div>
                                <div class="col-md-2">
                                    <strong>Mobile</strong>
                                </div>
                                <div class="col-md-1">:</div>
                                <div class="col-md-3">
                                    <p class="text-muted">{{$getUserName[0]->mobile_phone}}</p>
                                </div>
                                <div class="col-md-2">
                                    <strong>Role</strong>
                                </div>
                                <div class="col-md-1">:</div>
                                <div class="col-md-3">
                                    <p class="text-muted">{{$getUserName[0]->role_name}}</p>
                                </div>
                                <div class="col-md-2">
                                    <strong>Supervisor</strong>
                                </div>
                                <div class="col-md-1">:</div>
                                <div class="col-md-3">
                                    <p class="text-muted">{{$getUserName[0]->supervisor_name}}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive">     
                        <table  class="table table-striped table-bordered table-condensed table-lead-allocations" >
                            <thead>
                                <th>Category</th>
                                <th>Region</th>
                                <th>Country</th>
                            </thead>
                            <tbody class="tbody-lead-allocations">
                                @if(sizeof($lead_allocations)>0)
                                    @foreach ($lead_allocations as $name => $category)
                                    <tr class="colored">
                                        <td class="align-middle" rowspan="">{{$category->cat_name}}</td>
                                        @foreach ($category->regions as $region)
                                            <td class="align-middle" rowspan="">{{$region->region_name}}</td>
                                            <td rowspan="1" class="align-middle">{{$region->countries_for_view}}</td>
                                            <!-- </tr> -->
                                        @endforeach
                                    </tr>    
                                    @endforeach
                                @else
                                    <tr><td colspan="4" class="text-center"><b>No Allocations</b></td></tr>
                                @endif
                            </tbody>
                        </table>
                    </div>   
                </div>
            </div>
        </div>
    </div>
</section>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Message</h5>
        <button type="button" class="close text-right close-la-modal" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
            <p class="message"></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary close-la-modal" data-dismiss="modal">Close</button>
        <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
      </div>
    </div>
  </div>
</div>
@endsection