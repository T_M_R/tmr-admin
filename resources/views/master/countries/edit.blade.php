@extends('layouts.header')
@section('body-content')
<section class="regions-list">
    <div class="block-header">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 my-auto">
                <h4 class="page-title m-0">Add Country</h4>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <a href="{{URL:: to('/countries')}}">
                    <button type="button" class="btn btn-info float-right">Back</button>
                </a>
            </div>
        </div>
    </div>
    <div class="row clearfix mt-3">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="card project_widget">
                <div class="body mt-4 mb-4">
                    @foreach($edit_country as $value)
                        <div class="row">
                            <div class="col-12 col-md-4 mb-0">
                                <div class="input-group">  
                                    <input type="text" class="country_name" autocomplete="off" value="{{$value->name}}" required/>
                                    <label>Country Name <span class="text-danger">*</span></label>
                                </div>
                            </div>
                            <div class="col-12 col-md-4 mb-0">
                                <div class="input-group">  
                                    <input type="text" class="country_code" autocomplete="off" value="{{$value->country_code}}" required/>
                                    <label>Country Code <span class="text-danger">*</span></label>
                                </div>
                            </div>
                            <div class="col-12 col-md-4 mb-0">
                                <div class="input-group">
                                    <select class="floating-select country_region_id" onclick="this.setAttribute('value', this.value);" value="" required>
                                        @if(sizeof($getRegionsList)>0)
                                            <option value=""></option>
                                            @foreach($getRegionsList as $val)
                                                @if($val->id == $value->region_id)
                                                    <option value="{{$val->id}}" selected>{{$val->name}}</option>
                                                @else
                                                    <option value="{{$val->id}}">{{$val->name}}</option>
                                                @endif
                                            @endforeach
                                        @endif
                                    </select>
                                    <label>Region <span class="text-danger">*</span></label>
                                </div>
                            </div>
                            <input type="hidden" class="add_update_flag" value="2">
                            <input type="hidden" class="country_id" value="{{$value->id}}">
                            <div class="col-12 col-md-4 mb-0">
                                <p class="text-danger mb-0">* All fields are Mandatory</p>
                            </div>
                        </div>
                    @endforeach
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb-0 text-center">
                            <div class="form-group">
                                <div class="form-line">
                                    <div class="country_error_msg col-red w-100" style="display:none"></div>
                                    <div class="country_success_msg col-green w-100" style="display:none"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 text-center">
                            <button type="submit" class="btn btn-info waves-effect update_country">UPDATE</button>                                                                
                        </div>
                    </div>    
                </div>
            </div>
        </div>
    </div>
</section>
@endsection