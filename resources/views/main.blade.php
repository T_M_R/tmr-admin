<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=Edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <title>TMR ADMIN</title>
        <!-- Favicon-->
        <link rel="icon" href="{{URL::asset('assets/images/project/favicon.ico')}}" type="image/x-icon">
        <!-- Plugins Core Css -->
        <link href="{{URL::asset('assets/css/app.min.css')}}" rel="stylesheet">
        
        <link href="{{URL::asset('assets/css/form.min.css')}}" rel="stylesheet">
        <!-- Custom Css -->
        <link href="{{URL::asset('assets/css/style.css')}}" rel="stylesheet" />
        <!-- Theme style. You can choose a theme from css/themes instead of get all themes -->
        <!-- <link href="{{URL::asset('assets/css/styles/all-themes.css')}}" rel="stylesheet" /> -->
        <link href="{{URL::asset('assets/css/styles/theme-white.css')}}" rel="stylesheet" />
        <link href="{{URL::asset('css/app.css')}}" rel="stylesheet">
        <link href="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/css/select2.min.css" rel="stylesheet" />
        <!-- <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous"> -->

        <!-- <link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/css/bootstrap4-toggle.min.css" rel="stylesheet"> -->
        <style>
            .ios {
                width: 68px !important;
                height: 28px !important;
            }
            .toggle{
                border-radius: 28px !important;
            }

            .btn-light{
               background-color: #f8f9fa;
            }
        </style>
    </head>

    <body class="theme-white">
        <div class="page-loader-wrapper">
            <!-- <div class="loader"> -->
                <div class="loader_Sec">
                    <div class="boxes">
                        <div class="box">
                            <div></div>
                            <div></div>
                            <div></div>
                            <div></div>
                        </div>
                        <div class="box">
                            <div></div>
                            <div></div>
                            <div></div>
                            <div></div>
                        </div>
                        <div class="box">
                            <div></div>
                            <div></div>
                            <div></div>
                            <div></div>
                        </div>
                        <div class="box">
                            <div></div>
                            <div></div>
                            <div></div>
                            <div></div>
                        </div>
                    </div>
                </div> 
            <!-- </div>  -->
        </div>
        <?php 
            $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; 
            $path = $_SERVER['REQUEST_URI'];
            $urlPath = preg_replace('#/+#','',$path);
        ?>
        @yield('content')
        
        <!-- <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.min.js" integrity="sha384-Atwg2Pkwv9vp0ygtn1JAojH0nYbwNJLPhwyoVbhoPwBhjQPR5VtM2+xf0Uwh9KtT" crossorigin="anonymous"></script> -->

        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

        <script type="text/javascript" src="{{URL::asset('js/app.js')}}"></script>
        <script type="text/javascript"  src="{{URL::asset('assets/js/app.min.js')}}"></script>
        <script type="text/javascript" src="{{URL::asset('assets/js/form.min.js')}}"></script>
        <script type="text/javascript" src="{{URL::asset('assets/js/bundles/ckeditor/ckeditor.js')}}"></script>
        <!-- <script type="text/javascript" src="{{URL::asset('assets/js/pages/ui/dialogs.js')}}"></script> -->
        <!-- <script type="text/javascript"  src="{{URL::asset('assets/js/chart.min.js')}}"></script> -->
        <script type="text/javascript"  src="{{URL::asset('assets/js/table.min.js')}}"></script>
        <script  type="text/javascript"  src="{{URL::asset('/assets/js/pages/forms/basic-form-elements.js')}}"></script>
        <!-- <script type="text/javascript"  src="{{URL::asset('assets/js/bundles/apexcharts/apexcharts.min.js')}}"></script> -->
        <!-- <script type="text/javascript"  src="{{URL::asset('assets/js/pages/tables/jquery-datatable.js')}}"></script> -->
        <!-- Custom Js -->
        <script type="text/javascript"  src="{{URL::asset('assets/js/admin.js')}}"></script>
        <!-- <script type="text/javascript"  src="{{URL::asset('assets/js/pages/ui/notifications.js')}}"></script> -->
        <!-- <script type="text/javascript"  src="{{URL::asset('assets/js/pages/forms/editors.js')}}"></script> -->
        <!-- <script type="text/javascript"  src="{{URL::asset('assets/js/pages/index.js')}}"></script> -->
        
        <script src="https://code.highcharts.com/highcharts.js"></script>
        <script src="https://code.highcharts.com/modules/funnel.js"></script>
        <script src="https://code.highcharts.com/highcharts-3d.js"></script>

        <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

        <!-- Toggle Switch -->
    <!-- <script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js"></script> -->

        <script>
            $(document).ready(function() {
                $(".js-example-placeholder-multiple").select2({
                    // placeholder: "Follow-up By"
                });
            });
            
            //For flaoting labels on page load
            $("input").each(function(){
                var input = $(this).val(); 
                // console.log(input);
                // This is the jquery object of the input, do what you will
                if($(this).val() != ''){
                    $(this).parent().find('label').addClass('dynamic-floating-label');
                }
            });

            $("floating-select").each(function(){
                var select = $('option:selected',this).val(); // This is the jquery object of the input, do what you will
                // console.log(select);
                if( select != ''){
                    $(this).parent().find('label').addClass('dynamic-floating-label');
                }
                else{
                    $(this).parent().find('label').removeClass('dynamic-floating-label');
                }
            });

            $('.modal-close').click(function() {
                $('.modal').modal('hide');
            });

            $('.page-loader-wrapper').css({'display':'none'});

            $('.assigned_to_input_sec').css({ 'display': 'none' });
            $('.assignto-edit-remove-btn').css({ 'display': 'none' });
            function openAssignInput(conId) {
                var recordId = conId;
                console.log(recordId);
                $('.assigned_to_name_' + recordId).css({ 'display': 'none' });
                $('.assignto-edit-btn_' + recordId).css({ 'display': 'none' });
                $('.assigned_to_input_sec_' + recordId).toggle({ 'display': 'inline-block' });
                $('.assignto-edit-remove-btn_' + recordId).css({ 'display': 'inline-block' });
            }

            function closeAssignInput(conId) {
                var recordId = conId;
                console.log(recordId);
                $('.assigned_to_name_' + recordId).toggle({ 'display': 'inline-block' });
                $('.assignto-edit-btn_' + recordId).css({ 'display': 'inline-block' });
                $('.assigned_to_input_sec_' + recordId).toggle({ 'display': 'none' });
                $('.assignto-edit-remove-btn_' + recordId).css({ 'display': 'none' });
            }

            $('.status_input_sec').css({'display':'none'});
            $('.status-edit-remove-btn').css({'display':'none'});
            function openStatusInput(conId){
                var recordId = conId;
                console.log(recordId);
                $('.status_name_'+recordId).css({'display':'none'});
                $('.status-edit-btn_'+recordId).css({'display':'none'});
                $('.status_input_sec_'+recordId).toggle({'display':'inline-block'});
                $('.status-edit-remove-btn_'+recordId).css({'display':'inline-block'});
            }

            function closeStatusInput(conId){
                var recordId = conId;
                console.log(recordId);
                $('.status_name_'+recordId).toggle({'display':'inline-block'});
                $('.status-edit-btn_'+recordId).css({'display':'inline-block'});
                $('.status_input_sec_'+recordId).toggle({'display':'none'});
                $('.status-edit-remove-btn_'+recordId).css({'display':'none'});
            }

            $('.success-msg').css({'display':'none'});
            $('.error-msg').css({'display':'none'});
            function changeAssigned_User_LeadStatus (leadId){
                var acname = 'change_lead_status';
                var status = $('.changeStatus_' + leadId).val();
                // var comment = $('#comment_' + conId).val();
                var userId = $('.userAssign_' + leadId).val();
                //var catType = $('#catType_' + conId).val();
                //var tierType = $('#tierType_' + conId).val();
                //var valStatus = false;
                /*if (status == 'N') {
                    valStatus = true;
                } else if (catType != 'CAT-0' && tierType != 'TIER-0') {
                    valStatus = true;
                } else {
                    valStatus = false;
                }*/
                if(userId == null){
                    userId = '';
                }

                valStatus = true;
                console.log(leadId);
                console.log(userId);
                console.log(status);
                // return false;
                if ((leadId !='' && leadId != null && leadId != undefined) && ((userId != null && userId > 0 )|| valStatus))	
                {
                    $.ajax({
                        url: '/change_assignedUser_LeadStatus',
                        type: "GET",
                        //data: "id=" + conId + "&status=" + status + "&comment=" + comment + "&userId=" + userId + "&catType=" + catType + "&tierType=" + tierType,
                        // data: "id=" + conId + "&status=" + status + "&userId=" + userId,
                        data: {
                            leadId: leadId,
                            userId: userId,
                            status: status
                        },
                        success: function (result) {
                            console.log(result);
                            // return false;
                            result = JSON.parse(result);
                            console.log(result);
                            // return;
                            if (result.status == 'success')
                            {
                                $('.success-msg').css({'display':'block'});
                                $('.success-msg').text('Successfully Updated');
                                $('.success-error-msg-modal').modal('show');
                                showSuccessMessage();
                                if(result.commentStatus == 1){
                                    setTimeout(() => {
                                    $('.success-error-msg-modal').modal('hide');
                                        $('.status_name_'+leadId).toggle({'display':'inline-block'});
                                        $('.status_name_'+leadId).html(result.status_name);
                                        $('.status-edit-btn_'+leadId).css({'display':'inline-block'});
                                        $('.status_input_sec_'+leadId).toggle({'display':'none'});
                                        $('.status-edit-remove-btn_'+leadId).css({'display':'none'});
                                    // location.reload();
                                    }, 1500);
                                }
                                else{
                                    setTimeout(() => {
                                        $('.success-error-msg-modal').modal('hide');
                                        $('.assigned_to_name_'+leadId).toggle({'display':'inline-block'});
                                        $('.assigned_to_name_'+leadId).html(result.assigned_user_name);
                                        $('.assignto-edit-btn_'+leadId).css({'display':'inline-block'});
                                        $('.assigned_to_input_sec_'+leadId).toggle({'display':'none'});
                                        $('.assignto-edit-remove-btn_'+leadId).css({'display':'none'});
                                        // location.reload();
                                    }, 1500);
                                }
                            } 
                            else
                            {
                                $('.error-msg').css({'display':'block'});
                                $('.error-msg').text('Oops..!!, Something went wrong.');
                                $('.success-error-msg-modal').modal('show');
                                setTimeout(() => {
                                    $('.success-error-msg-modal').modal('hide');
                                    location.reload();
                                }, 1500);
                            }
                        }
                    });
                } else {
                    alert("All Fields Are Required");
                    return false;
                }
            }

            function showSuccessMessage() {
                swal("Status Updated Successfully!", "", "success");
            }

            function showCancelMessage() {
                swal("Oops", "Something went wrong!", "error")
            }
        </script>
    </body>
</html>