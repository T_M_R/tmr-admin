@extends('layouts.header')
@section('body-content')
<section class="regions-list">
    <div class="block-header">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 my-auto">
                <h4 class="page-title m-0">Email Configuration</h4>
            </div>
            <?php 
                $sessionUser=Session::get('subMenuArray');  
                $val=[];
                $allRoutes=[];
                $addButtonStatus=0;
                $editButtonStatus=0;
                $viewButtonStatus=0;
                foreach($sessionUser as $PagePermission)
                {
                    if(isset($PagePermission['route'])){
                        if($PagePermission['route'] == "roles")
                        {
                            $addButtonStatus=$PagePermission['is_add'];
                            $editButtonStatus=$PagePermission['is_edit'];
                            $viewButtonStatus=$PagePermission['is_view'];
                        }
                    }
                }
            ?>
            <div class="col-xs-12 col-sm-12 col-md-3 col-lg-9">
                @if($addButtonStatus == 1)
                <a href="{{URL:: to('/add_mail_config')}}">
                    <button type="button" class="btn btn-primary float-right">Add New</button>
                </a>
                @endif
            </div>
        </div>
    </div>
    <div class="row clearfix mt-3">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            @if(session()->has('message'))
                <div class="badge col-green w-100">
                    {{ session()->get('message') }}
                    <?php session()->forget('message'); ?>
                </div>
            @endif
            @if(session()->has('error'))
                <div class="badge col-red w-100">
                    {{ session()->get('error') }}
                    <?php session()->forget('error'); ?>
                </div>
            @endif
            <p class="center status_change_msg text-danger"></p>
            @if(isset($mail_config) && sizeof($mail_config)>0)
            <div class="card">
                <div class="body p-0">
                    <div class="table-responsive">
                        <!-- <table class="table table-bordered table-striped table-hover js-basic-example dataTable"> -->
                        <table class="table table-bordered mb-0">
                            <thead>
                                <tr>
                                    <th>S.No</th>
                                    <th>Category</th>
                                    <th>Email To</th>
                                    <th>Status</th>
                                    @if($editButtonStatus == 1)
                                    <th>Action</th>
                                    @endif
                                </tr>
                            </thead>
                            <tbody>
                                <?php $i=1;?>
                                @foreach($mail_config as $value)
                                    <tr>
                                        <td>{{$i}}</td>
                                        <td>{{$value->email_cat}}</td>
                                        <td>{{$value->email_to}}</td>
                                        <td>
                                            @if($value->email_status == 'Y')  
                                                <input class="checkbox email_toggle_event" type="checkbox" data-toggle="toggle" data-style="ios" checked>
                                                <input type="hidden" name="hidden_status" class="hidden_status" value="1" />
                                            @else
                                                <input class="checkbox email_toggle_event" type="checkbox" data-toggle="toggle" data-style="ios">
                                                <input type="hidden" name="hidden_status" class="hidden_status" value="0"/>
                                            @endif
                                            <input type="hidden" class="email_id" value="{{$value->email_id}}">
                                        </td>
                                        @if($editButtonStatus == 1)
                                        <td>
                                            <a href="{{URL:: to('/edit_email_config/'.$value->email_id)}}" class="text-decoration-none">
                                                <button class="btn tblActnBtn">
                                                    <i class="material-icons">mode_edit</i>
                                                </button>
                                            </a>
                                        </td>
                                        @endif
                                    </tr>
                                    <?php $i++;?>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            @else
                <div class="card">
                    <p class="text-center py-5" style="font-size:25px">No Data Available To Show</p>  
                </div>
            @endif
        </div>
    </div>
</section>
@endsection