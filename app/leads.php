<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Session;
use Config;
use Carbon\Carbon;

class leads extends Model
{
    public function getLeads($get_my_team,$user_id,$category_id,$country_id,$filterFlag){
        $myteam = "" . implode(", ", $get_my_team) . "";
            
        $query = DB::table('leads as l')
                ->join('reports as r','r.rep_id','=','l.repId')
                ->join('category as c','c.category_id','=','r.cat_id')
                ->join('users as u','u.id','=','l.userId')
                ->select('l.*','u.id','u.name as username','r.rep_name','r.rep_type','r.cat_id','c.cat_name')
                // ->where('l.leadStatus','=',1)
                ->orderBy('l.contactId','DESC');
                
        if($user_id != ''){
            $query->where('l.userId','=',$user_id);
        }

        if($category_id != ''){
            $query->where('r.cat_id','=',$category_id);
        }

        // if($country_id != ''){
        //     $query->where('r.country','=',$country_id);
        // }
        
        if($user_id != ''){
            $getLeads = $query->paginate(30);
        }
        else{
            $getLeads = $query->whereIn('l.userId',$get_my_team)->paginate(30);
        }
     
        if(sizeof($getLeads)>0){
            return $getLeads;
        }
        else{
            return [];
        }
    }

    public function getLeads_For_Excel($get_my_team,$user_id,$category_id,$country_id,$filterFlag){
        $myteam = "" . implode(", ", $get_my_team) . "";
            
        $query = DB::table('leads as l')
                ->join('reports as r','r.rep_id','=','l.repId')
                ->join('category as c','c.category_id','=','r.cat_id')
                ->join('users as u','u.id','=','l.userId')
                ->select('l.*','u.id','u.name as username','r.rep_name','r.rep_type','r.cat_id','c.cat_name')
                // ->where('l.leadStatus','=',1)
                ->orderBy('l.contactId','DESC');
                
        if($user_id != ''){
            $query->where('l.userId','=',$user_id);
        }

        if($category_id != ''){
            $query->where('r.category_id','=',$category_id);
        }

        // if($country_id != ''){
        //     $query->where('r.country','=',$country_id);
        // }
        
        if($user_id != ''){
            $getLeads_For_Excel = $query->paginate(30);
        }
        else{
            $getLeads_For_Excel = $query->whereIn('l.userId',$get_my_team)->paginate(30);
        }
     
        if(sizeof($getLeads_For_Excel)>0){
            return $getLeads_For_Excel;
        }
        else{
            return [];
        }
    }

    public function getLeadDetails($id){
        $getLeadDetails = DB::table('leads as l')
                        ->join('reports as r','r.rep_id','=','l.repId')
                        ->join('category as c','c.category_id','=','r.cat_id')
                        ->join('users as u','u.id','=','l.userId')
                        ->select('l.*','u.id','u.name as username','r.rep_name','r.cat_id','r.rep_type','c.cat_name')
                        ->where('l.contactId','=',$id)
                        ->orderBy('l.contactId','DESC')
                        ->get();
        if(sizeof($getLeadDetails)>0){
            return $getLeadDetails;
        }
        else{
            return [];
        }
    }

     public function getLeadComments($id){
        $getLeadComments = DB::table('lead_comments')
                            ->join('users','users.id','=','lead_comments.activity_by')
                            ->select('lead_comments.*','users.name as username')
                            ->where('lead_comments.lead_id','=',$id)
                            ->orderBy('lead_comments.id','DESC')
                            ->get();
        if(sizeof($getLeadComments)>0){
            return $getLeadComments;
        }
        else{
            return [];
        }
    }

    public function save_lead_comment($data){
        $date = Carbon::now();
        $month_year = $date->format("Y-m");
        $user_id = Session::get('user_id');
        // print_r($data);
        // print_r($user_id);
        // exit;
        $save_lead_comment = DB::table('lead_comments')->insert([
            'lead_id' => $data['leadId'],
            'commented_at_stage' => 0,
            'comment' => $data['comment'],
            'comment_type' => 1,
            'activity_by' => $user_id,
            'created_at' => $date
            // 'month_year' => $month_year
        ]);
    
        if($save_lead_comment){
            return $save_lead_comment;
        }
        else{
            return 0;
        }
    }

    public function save_remainder($data){
        $date = Carbon::now();
        $user_id = Session::get('user_id');
        
        $save_remainder = DB::table('lead_followup')->insert([
            'lead_id' => $data['leadId'],
            'user_id' => $user_id,
            'followup_text' => $data['remainder_comments'],
            'next_followup_date' => $data['remainder_date'],
            'created_date' => $date
        ]);
    
        if($save_remainder){
            $comment = 'Added Remainder ->  '.$data['remainder_comments'].' on  '.$data['remainder_date'] ;

            $save_lead_comment = DB::table('lead_comments')->insert([
                'lead_id' => $data['leadId'],
                'commented_at_stage' => 0,
                'comment' => $comment,
                'comment_type' => 4,
                'activity_by' => $user_id,
                'created_at' => $date
            ]);

            return $save_remainder;
        }
        else{
            return 0;
        }
    }

    public function change_assignedUser_LeadStatus($data){
        $date = Carbon::now();
        $user_id = Session::get('user_id');
        $commentStatus = 0;

        $getLeadData = DB::table('leads')
                        ->join('users as u','u.id','=','leads.userId')
                        ->select('leads.*','u.name as username')
                        ->where('contactId', $data['leadId'])
                        ->get();
        if($data['userId'] == ''){
            $data['userId'] = $getLeadData[0]->userId;
        }
        $statusFrom = $this->getleadStatusName($getLeadData[0]->leadStatus);
        $userFrom = $getLeadData[0]->userId;

        $userName = DB::table('users')->select('name')->where('id','=',$data['userId'])->get();
        $assignedUser = $userName[0]->name;

        $change_assignedUser_LeadStatus = DB::table('leads')->where('contactId', $data['leadId'])->update([
            'userId' => $data['userId'],
            'leadStatus' => $data['status']
        ]);
        if($change_assignedUser_LeadStatus){
            $commentStatus = 1;
            if($data['status'] != '' && $statusFrom != $data['status']){
                $statusTo = $this->getleadStatusName($data['status']);
                $comment = 'Changed the lead status from ' .$statusFrom .' to '. $statusTo;

                $save_lead_comment = DB::table('lead_comments')->insert([
                    'lead_id' => $data['leadId'],
                    'commented_at_stage' => 0,
                    'comment' => $comment,
                    'comment_type' => 2,
                    'activity_by' => $user_id,
                    'created_at' => $date
                ]);
            }
            
            if($data['userId'] != '' && $userFrom != $data['userId']){
                $commentStatus = 2;
                $comment = 'Assigned the lead to '.$assignedUser;

                $save_lead_comment = DB::table('lead_comments')->insert([
                    'lead_id' => $data['leadId'],
                    'commented_at_stage' => 0,
                    'comment' => $comment,
                    'comment_type' => 3,
                    'activity_by' => $user_id,
                    'created_at' => $date
                ]);
            }
            return json_encode(array(
                'status' => 'success',
                'status_name' => $statusTo,
                'assigned_user_name' => $assignedUser,
                'commentStatus' => $commentStatus
            ));
        }
        else{
            return 0;
        }
    }
    
    function getleadStatusName($currentStatus) {
        if ($currentStatus == "N") {
            return "Active";
        }
        if ($currentStatus == "P") {
            return "Pipeline";
        }
        if ($currentStatus == "S") {
            return "Student";
        }
        if ($currentStatus == "J") {
            return "Junk";
        }
        if ($currentStatus == "NB") {
            return "No Budget";
        }
        if ($currentStatus == "NI") {
            return "Not Interested";
        }
        if ($currentStatus == "FU") {
            return "Follow Up";
        }
        if ($currentStatus == "IP") {
            return "In Process";
        }
        if ($currentStatus == "NR") {
            return "No Response";
        }
        if ($currentStatus == "C") {
            return "Close";
        }
        if ($currentStatus == "SALE") {
            return "Sale";
        }
    }
}
