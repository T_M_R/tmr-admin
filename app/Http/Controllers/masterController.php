<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\master;
use Carbon\Carbon;
use Redirect;
use Session;
use DB;
use Config;

class masterController extends Controller
{
    public function __construct(){
        $this->master = new master();
    }

    public function getRegionsList(Request $request){
        if (!session('username')) {
	    	return redirect('/');
        } 
        $search_key = '';
        if(isset($_GET['search_key'])){
            $search_key=$_GET['search_key'];
        }
        $getRegionsList = $this->master->getRegionsList($search_key);
        $getRegionsList = $getRegionsList->appends(['search_key'=>$search_key]);
        return view('master.regions.list')->with('getRegionsList',$getRegionsList)->with('search_key',$search_key);
    }

    public function addNewRegion(Request $request){
        if (!session('username')) {
	    	return redirect('/');
        } 
    
        return view('master.regions.add');
    }

    public function region_duplicate_check(Request $request){
        if (!session('username')) {
	    	return redirect('/');
        } 
        
        $data = $request->all();
        $region_duplicate_check = $this->master->region_duplicate_check($data);
        return $region_duplicate_check;
    }

    public function save_region(Request $request){
        if (!session('username')) {
	    	return redirect('/');
        } 
        
        $data = $request->all();
        $save_region = $this->master->save_region($data);
        return $save_region;
    }

    public function edit_region(Request $request,$id){
        if (!session('username')) {
	    	return redirect('/');
        } 
        
        $data = $request->all();
        $edit_region = $this->master->getregionDataById($id);
        return view('master.regions.edit')->with('edit_region',$edit_region);
    }

    public function update_region(Request $request){
        if (!session('username')) {
	    	return redirect('/');
        } 
        
        $data = $request->all();
        $update_region = $this->master->update_region($data);
        return $update_region;
    }

    public function update_region_status(Request $request){
        if (!session('username')) {
	    	return redirect('/');
        } 
        
        $data = $request->all();
        $update_region_status = $this->master->update_region_status($data);
        return $update_region_status;
    }

    // Countries
    public function getCountriesList(Request $request){
        if (!session('username')) {
	    	return redirect('/');
        } 
        $search_key = '';
        if(isset($_GET['search_key'])){
            $search_key=$_GET['search_key'];
        }
        $getCountriesList = $this->master->getCountriesList($search_key);
        $getCountriesList = $getCountriesList->appends(['search_key'=>$search_key]);
        return view('master.countries.list')->with('getCountriesList',$getCountriesList)->with('search_key',$search_key);
    }

    public function addNewCountry(Request $request){
        if (!session('username')) {
	    	return redirect('/');
        } 
        
        $getRegionsList = $this->master->getRegionsListForCountries();
        return view('master.countries.add')->with('getRegionsList',$getRegionsList);
    }

    public function country_duplicate_check(Request $request){
        if (!session('username')) {
	    	return redirect('/');
        } 
        
        $data = $request->all();
        $country_duplicate_check = $this->master->country_duplicate_check($data);
        return $country_duplicate_check;
    }

    public function save_country(Request $request){
        if (!session('username')) {
	    	return redirect('/');
        } 
        
        $data = $request->all();
        $save_country = $this->master->save_country($data);
        return $save_country;
    }

    public function edit_country(Request $request,$id){
        if (!session('username')) {
	    	return redirect('/');
        } 
        
        $data = $request->all();
        $getRegionsList = $this->master->getRegionsListForCountries();
        $edit_country = $this->master->getcountryDataById($id);
        return view('master.countries.edit')->with('getRegionsList',$getRegionsList)->with('edit_country',$edit_country);
    }

    public function update_country(Request $request){
        if (!session('username')) {
	    	return redirect('/');
        } 
        
        $data = $request->all();
        $update_country = $this->master->update_country($data);
        return $update_country;
    }

    public function update_country_status(Request $request){
        if (!session('username')) {
	    	return redirect('/');
        } 
        
        $data = $request->all();
        $update_country_status = $this->master->update_country_status($data);
        return $update_country_status;
    }

    // Categories====================================
    public function getCategoriesList(Request $request){
        if (!session('username')) {
	    	return redirect('/');
        } 
        $search_key = '';
        if(isset($_GET['search_key'])){
            $search_key=$_GET['search_key'];
        }
        $getCategoriesList = $this->master->getCategoriesList($search_key);
        $getCategoriesList = $getCategoriesList->appends(['search_key'=>$search_key]);
        return view('master.categories.list')->with('getCategoriesList',$getCategoriesList)->with('search_key',$search_key);
    }

    public function addNewCategory(Request $request){
        if (!session('username')) {
	    	return redirect('/');
        } 
        
        return view('master.categories.add');
    }

    public function category_duplicate_check(Request $request){
        if (!session('username')) {
	    	return redirect('/');
        } 
        
        $data = $request->all();
        $category_duplicate_check = $this->master->category_duplicate_check($data);
        return $category_duplicate_check;
    }

    public function save_category(Request $request){
        if (!session('username')) {
	    	return redirect('/');
        } 
        
        $data = $request->all();
        $save_category = $this->master->save_category($data);
        return $save_category;
    }

    public function edit_category(Request $request,$id){
        if (!session('username')) {
	    	return redirect('/');
        } 
        
        $data = $request->all();
        $edit_category = $this->master->getCategoryDataById($id);
        return view('master.categories.edit')->with('edit_category',$edit_category);
    }
    
    public function update_category(Request $request){
        if (!session('username')) {
	    	return redirect('/');
        } 
        
        $data = $request->all();
        $update_category = $this->master->update_category($data);
        return $update_category;
    }

    public function update_category_status(Request $request){
        if (!session('username')) {
	    	return redirect('/');
        } 
        
        $data = $request->all();
        $update_category_status = $this->master->update_category_status($data);
        return $update_category_status;
    }

    // Sub-Categories====================================
    public function getSubCategoriesList(Request $request){
        if (!session('username')) {
	    	return redirect('/');
        } 
        $search_key = '';
        if(isset($_GET['search_key'])){
            $search_key=$_GET['search_key'];
        }
        $getSubCategoriesList = $this->master->getSubCategoriesList($search_key);
        $getSubCategoriesList = $getSubCategoriesList->appends(['search_key'=>$search_key]);
        return view('master.SubCategories.list')->with('getSubCategoriesList',$getSubCategoriesList)->with('search_key',$search_key);
    }

    public function addNewSubCategory(Request $request){
        if (!session('username')) {
	    	return redirect('/');
        } 
        $getCategories = $this->master->getCategories();
        return view('master.SubCategories.add')->with('getCategories',$getCategories);
    }

    public function save_sub_category(Request $request){
        if (!session('username')) {
	    	return redirect('/');
        } 
        
        $data = $request->all();
        $save_sub_category = $this->master->save_sub_category($data);
        return $save_sub_category;
    }

    public function edit_sub_category(Request $request,$id){
        if (!session('username')) {
	    	return redirect('/');
        } 
        
        $data = $request->all();
        $getCategories = $this->master->getCategories();
        $edit_sub_category = $this->master->getSubCategoryDataById($id);
        return view('master.SubCategories.edit')->with('getCategories',$getCategories)->with('edit_sub_category',$edit_sub_category);
    }

    public function update_sub_category(Request $request){
        if (!session('username')) {
	    	return redirect('/');
        } 
        
        $data = $request->all();
        $update_sub_category = $this->master->update_sub_category($data);
        return $update_sub_category;
    }

    public function update_sub_category_status(Request $request){
        if (!session('username')) {
	    	return redirect('/');
        } 
        
        $data = $request->all();
        $update_sub_category_status = $this->master->update_sub_category_status($data);
        return $update_sub_category_status;
    }

    // companies=================================

    public function getCompaniesList(Request $request){
        if (!session('username')) {
	    	return redirect('/');
        } 
        $search_key = '';
        if(isset($_GET['search_key'])){
            $search_key=$_GET['search_key'];
        }
        $getCompaniesList = $this->master->getCompaniesList($search_key);
        $getCompaniesList = $getCompaniesList->appends(['search_key'=>$search_key]);
        return view('master.companies.list')->with('getCompaniesList',$getCompaniesList)->with('search_key',$search_key);
    }

    public function addNewCompany(Request $request){
        if (!session('username')) {
	    	return redirect('/');
        } 
    
        return view('master.companies.add');
    }

    public function company_duplicate_check(Request $request){
        if (!session('username')) {
	    	return redirect('/');
        } 
        
        $data = $request->all();
        $company_duplicate_check = $this->master->company_duplicate_check($data);
        return $company_duplicate_check;
    }

    public function save_company(Request $request){
        if (!session('username')) {
	    	return redirect('/');
        } 
        
        $data = $request->all();
        $save_company = $this->master->save_company($data);
        return $save_company;
    }

    public function edit_company(Request $request,$id){
        if (!session('username')) {
	    	return redirect('/');
        } 
        
        $data = $request->all();
        $edit_company = $this->master->getCompanyDataById($id);
        return view('master.companies.edit')->with('edit_company',$edit_company);
    }

    public function update_company(Request $request){
        if (!session('username')) {
	    	return redirect('/');
        } 
        
        $data = $request->all();
        $update_company = $this->master->update_company($data);
        return $update_company;
    }

    public function update_company_status(Request $request){
        if (!session('username')) {
	    	return redirect('/');
        } 
        
        $data = $request->all();
        $update_company_status = $this->master->update_company_status($data);
        return $update_company_status;
    }

}
