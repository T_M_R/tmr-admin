<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\home;
use App\users;
use App\leadsAllocation;
use App\news;
use Carbon\Carbon;
use Redirect;
use Session;
use DB;
use Config;

class homeController extends Controller
{
    public function __construct(){
        $this->home = new home();
        $this->users = new users();
        $this->news = new news();
        $this->leadsAllocation = new leadsAllocation();
    }

    public function getHomeData(Request $request){
        if (!session('username')) {
	    	return redirect('/');
        }

        $category_id = '';
        $user_id = '';
        $from_date = '';
        $to_date = '';
        $get_my_team = [];

        if(isset($_GET['from_date'])){
            $from_date=$_GET['from_date'];
        }
        if(isset($_GET['to_date'])){
            $to_date=$_GET['to_date'];
        }
        if(isset($_GET['user_id'])){
            $user_id=$_GET['user_id'];
        }
        if(isset($_GET['category_id'])){
            $category_id=$_GET['category_id'];
        }
        if($category_id == 'ALL'){
            $category_id = '';
        }
        if($user_id == 'ALL'){
            $user_id = '';
        }
        if($user_id == ''){
            if(Session::get('user_id') !== '1'){
                // $get_my_team = $this->home->get_my_team();
                $get_my_team = $this->users->get_my_team();
            }
        }
        
        if($from_date === $to_date){ $from_date = ''; $to_date = ''; }
        
        $getCategories = $this->leadsAllocation->getCategories();
        $getUsers = $this->leadsAllocation->getUsersForFilter();
        
        $get_my_team = $this->users->get_my_team();
        
        $getleadsCount = $this->home->getleadsCount($get_my_team,$user_id, $category_id);
        $getLatestLeads = $this->home->getLatestLeads($get_my_team,$user_id, $category_id);
        $getLeadRemainders = $this->home->getLeadRemainders($get_my_team);

        $getLatestReports = $this->home->getLatestReports($get_my_team,$user_id, $category_id);

        $getLatestOrders = $this->home->getLatestOrders($get_my_team,$user_id, $category_id);

        $getLatestArticles = $this->home->getLatestArticles($get_my_team,$user_id, $category_id);

        $getLatestNews = $this->home->getLatestNews($get_my_team,$user_id, $category_id);
        
        $getReportsCount = $this->home->getReportsCount();
        $getOrdersCount = $this->home->getOrdersCount();
        
        return view('Home.index')->with('getUsers',$getUsers)->with('getCategories',$getCategories)->with('getleadsCount',$getleadsCount)->with('getReportsCount',$getReportsCount)->with('getOrdersCount',$getOrdersCount)->with('getLatestLeads',$getLatestLeads)->with('getLeadRemainders',$getLeadRemainders)->with('getLatestReports',$getLatestReports)->with('getLatestOrders',$getLatestOrders)->with('getLatestArticles',$getLatestArticles)->with('getLatestNews',$getLatestNews);
    }

    public function getLeadStatus(Request $request){
        $get_my_team = [];
        $category_id = '';
        $user_id = '';
        $from_date = '';
        $to_date = '';
        if(isset($_GET['from_date'])){
            $from_date=$_GET['from_date'];
        }
        if(isset($_GET['to_date'])){
            $to_date=$_GET['to_date'];
        }
        if(isset($_GET['user_id'])){
            $user_id=$_GET['user_id'];
        }
        if(isset($_GET['category_id'])){
            $category_id=$_GET['category_id'];
        }
        if($category_id == 'ALL'){
            $category_id = '';
        }
        if($user_id == 'ALL'){
            $user_id = '';
        }
        if($user_id == ''){
            if(Session::get('user_id') !== '1'){
                // $get_my_team = $this->home->get_my_team();
                $get_my_team = $this->users->get_my_team();
            }
        }
        
        if($from_date === $to_date){ $from_date = ''; $to_date = ''; }
        $data = $this->home->getLeadStatus($from_date, $to_date, $user_id, $category_id, $get_my_team);

        return $data;
        // if ($data) {
        //     echo $data;
        // } else {
        //     echo "error";
        // }
    }

    public function get_mnth_wise_lead_data(Request $request){
        $get_my_team = [];
        $category_id = '';
        $user_id = '';
        $from_date = '';
        $to_date = '';
        $time = '';
        if(isset($_GET['from_date'])){
            $from_date=$_GET['from_date'];
        }
        if(isset($_GET['to_date'])){
            $to_date=$_GET['to_date'];
        }
        if(isset($_GET['user_id'])){
            $user_id=$_GET['user_id'];
        }
        if(isset($_GET['category_id'])){
            $category_id=$_GET['category_id'];
        }
        if($category_id == 'ALL'){
            $category_id = '';
        }
        if($user_id == 'ALL'){
            $user_id = '';
        }
        if($user_id == ''){
            if(Session::get('user_id') !== '1'){
                // $get_my_team = $this->home->get_my_team();
                $get_my_team = $this->users->get_my_team();
            }
        }
        if($from_date === $to_date){ $from_date = ''; $to_date = ''; }

        $monthsArray=[];
        $monthNamesArray=[];
        $date_range_flag=0;
        $monthsResult=$this->home->prepareMonths($time, $from_date, $to_date);
        if(sizeof($monthsResult['month_numbers'])>0){
            if(isset($monthsResult['date_range_flag']) && $monthsResult['date_range_flag'] == 1){
                $givenmonths = $monthsResult['month_numbers'];
                $no_of_days = $monthsResult['no_of_days'];
                $months = "'" . implode ( "', '", $givenmonths ) . "'";
                $date_range_flag=1;
            }
            else{
                $givenmonths = $monthsResult['month_numbers'];
                $no_of_days = $monthsResult['no_of_days'];
                $months = "'" . implode ( "', '", $givenmonths ) . "'";
            }
        }
        else{
            $givenmonths = '';
            $months='';
            $no_of_days = '';        
        }

        if(sizeof($monthsResult)>0){
            $monthsArray=$monthsResult['month_numbers'];
            $monthNamesArray=$monthsResult['month_names'];
        }

        $data = $this->home->get_mnth_wise_lead_data($givenmonths,$monthNamesArray, $no_of_days, $user_id, $category_id, $get_my_team);
        // print_r($data);
        // exit;
        return $data;
    }

    public function get_mnth_wise_report_data(Request $request){
        $get_my_team = [];
        $category_id = '';
        $user_id = '';
        $from_date = '';
        $to_date = '';
        $time = '';
        if(isset($_GET['from_date'])){
            $from_date=$_GET['from_date'];
        }
        if(isset($_GET['to_date'])){
            $to_date=$_GET['to_date'];
        }
        if(isset($_GET['user_id'])){
            $user_id=$_GET['user_id'];
        }
        if(isset($_GET['category_id'])){
            $category_id=$_GET['category_id'];
        }
        if($category_id == 'ALL'){
            $category_id = '';
        }
        if($user_id == 'ALL'){
            $user_id = '';
        }
        if($user_id == ''){
            if(Session::get('user_id') !== '1'){
                // $get_my_team = $this->home->get_my_team();
                $get_my_team = $this->users->get_my_team();
            }
        }
        
        if($from_date === $to_date){ $from_date = ''; $to_date = ''; }

        $monthsArray=[];
        $monthNamesArray=[];
        $date_range_flag=0;
        $monthsResult=$this->home->prepareMonths($time, $from_date, $to_date);
        if(sizeof($monthsResult['month_numbers'])>0){
            if(isset($monthsResult['date_range_flag']) && $monthsResult['date_range_flag'] == 1){
                $givenmonths = $monthsResult['month_numbers'];
                $no_of_days = $monthsResult['no_of_days'];
                $months = "'" . implode ( "', '", $givenmonths ) . "'";
                $date_range_flag=1;
            }
            else{
                $givenmonths = $monthsResult['month_numbers'];
                $no_of_days = $monthsResult['no_of_days'];
                $months = "'" . implode ( "', '", $givenmonths ) . "'";
            }
        }
        else{
            $givenmonths = '';
            $months='';
            $no_of_days = '';        
        }

        $data = $this->home->get_mnth_wise_report_data($givenmonths, $monthNamesArray, $no_of_days,$user_id, $category_id, $get_my_team);
        // print_r($data);
        // exit;
        return $data;
        // if ($data) {
        //     echo $data;
        // } else {
        //     echo "error";
        // }
    }

    public function get_mnth_wise_orders_data(Request $request){
        $category_id = '';
        $user_id = '';
        $from_date = '';
        $to_date = '';
        $time = '';
        $get_my_team = [];

        if(isset($_GET['from_date'])){
            $from_date=$_GET['from_date'];
        }
        if(isset($_GET['to_date'])){
            $to_date=$_GET['to_date'];
        }
        if(isset($_GET['user_id'])){
            $user_id=$_GET['user_id'];
        }
        if(isset($_GET['category_id'])){
            $category_id=$_GET['category_id'];
        }
        if($category_id == 'ALL'){
            $category_id = '';
        }
        if($user_id == 'ALL'){
            $user_id = '';
        }
        if($user_id == ''){
            if(Session::get('user_id') !== '1'){
                // $get_my_team = $this->home->get_my_team();
                $get_my_team = $this->users->get_my_team();
            }
        }
        
        if($from_date === $to_date){ $from_date = ''; $to_date = ''; }

        $monthsArray=[];
        $monthNamesArray=[];
        $date_range_flag=0;
        $monthsResult=$this->home->prepareMonths($time, $from_date, $to_date);
        if(sizeof($monthsResult['month_numbers'])>0){
            if(isset($monthsResult['date_range_flag']) && $monthsResult['date_range_flag'] == 1){
                $givenmonths = $monthsResult['month_numbers'];
                $no_of_days = $monthsResult['no_of_days'];
                $months = "'" . implode ( "', '", $givenmonths ) . "'";
                $date_range_flag=1;
            }
            else{
                $givenmonths = $monthsResult['month_numbers'];
                $no_of_days = $monthsResult['no_of_days'];
                $months = "'" . implode ( "', '", $givenmonths ) . "'";
            }
        }
        else{
            $givenmonths = '';
            $months='';
            $no_of_days = '';        
        }

        if(sizeof($monthsResult)>0){
            $monthsArray=$monthsResult['month_numbers'];
            $monthNamesArray=$monthsResult['month_names'];
        }

        $data = $this->home->get_mnth_wise_orders_data($givenmonths,$monthNamesArray, $no_of_days, $user_id, $category_id, $get_my_team);
        
        return $data;
    }

    public function get_mnth_wise_articles_data(Request $request){
        $category_id = '';
        $user_id = '';
        $from_date = '';
        $to_date = '';
        $time = '';
        $get_my_team = [];

        if(isset($_GET['from_date'])){
            $from_date=$_GET['from_date'];
        }
        if(isset($_GET['to_date'])){
            $to_date=$_GET['to_date'];
        }
        if(isset($_GET['user_id'])){
            $user_id=$_GET['user_id'];
        }
        if(isset($_GET['category_id'])){
            $category_id=$_GET['category_id'];
        }
        if($category_id == 'ALL'){
            $category_id = '';
        }
        if($user_id == 'ALL'){
            $user_id = '';
        }
        if($user_id == ''){
            if(Session::get('user_id') !== '1'){
                // $get_my_team = $this->home->get_my_team();
                $get_my_team = $this->users->get_my_team();
            }
        }
        
        if($from_date === $to_date){ $from_date = ''; $to_date = ''; }

        $monthsArray=[];
        $monthNamesArray=[];
        $date_range_flag=0;
        $monthsResult=$this->home->prepareMonths($time, $from_date, $to_date);
        if(sizeof($monthsResult['month_numbers'])>0){
            if(isset($monthsResult['date_range_flag']) && $monthsResult['date_range_flag'] == 1){
                $givenmonths = $monthsResult['month_numbers'];
                $no_of_days = $monthsResult['no_of_days'];
                $months = "'" . implode ( "', '", $givenmonths ) . "'";
                $date_range_flag=1;
            }
            else{
                $givenmonths = $monthsResult['month_numbers'];
                $no_of_days = $monthsResult['no_of_days'];
                $months = "'" . implode ( "', '", $givenmonths ) . "'";
            }
        }
        else{
            $givenmonths = '';
            $months='';
            $no_of_days = '';        
        }

        if(sizeof($monthsResult)>0){
            $monthsArray=$monthsResult['month_numbers'];
            $monthNamesArray=$monthsResult['month_names'];
        }

        $data = $this->home->get_mnth_wise_articles_data($givenmonths, $monthNamesArray, $no_of_days, $user_id, $category_id, $get_my_team);
        
        return $data;
    }
}
