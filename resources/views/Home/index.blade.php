@extends('layouts.header')
@section('body-content')
<section class="dasboard">
    <div class="block-header">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 my-auto">
                <h4 class="page-title m-0">Dashboard</h4>
            </div>
        </div>
    </div>
    <?php 
        $user_id='';
        $category_id='';
        $from_date='';
        $to_date='';

        if(isset($_GET['user'])){
            $user_id=$_GET['user'];
        }
        if(isset($_GET['category_id'])){
            $category_id=$_GET['category_id'];
        }
        
        if($from_date == '' && $to_date == ''){
            $from_date = date('Y-m-d H:i:s');
            $from_date = strtotime($from_date);
            $from_date = date("d-m-Y", strtotime("-3 Months",$from_date));
            $from_date = date("d-m-Y", strtotime($from_date));

            $to_date = date('d-m-Y');
        }
        else{
            if(isset($_GET['from_date'])){
                $from_date=$_GET['from_date'];
            }
            if(isset($_GET['to_date'])){
                $to_date=$_GET['to_date'];
            }
        }
        
    ?>
    <div class="row mt-4">
        <div class="col-sm-12 col-md-3 col-lg-3 d-none">
            <div class="from-group m-0">
                <select class="floating-select user" id="user" >
                    <option value="" disabled selected>User</option>
                        @foreach($getUsers as $user)
                            @if($user->id == $user_id)
                                <option value="{{$user->id}}" selected>{{$user->name}}</option>
                            @else
                                <option value="{{$user->id}}">{{$user->name}}</option>
                            @endif
                        @endforeach
                </select>
            </div>
        </div>
        <div class="col-sm-12 col-md-6 col-lg-6">
            <div class="from-group m-0">
                <select class="floating-select category_id" id="category_id" >
                    <option value="" disabled selected>Category</option>
                        @foreach($getCategories as $val)
                            @if($val->category_id == $category_id)
                                <option value="{{$val->category_id}}" selected>{{$val->cat_name}}</option>
                            @else
                                <option value="{{$val->category_id}}">{{$val->cat_name}}</option>
                            @endif
                        @endforeach
                </select>
            </div>
        </div>
        <div class="col-12 col-md-2 col-lg-2">
            <div class="input-group">  
                <input type="text" class="datepicker from_date" name="from_date" id="from_date" autocomplete="off" value="{{$from_date}}" placeholder="From Date"/>
                <span style="position: absolute;top: 8px;right: 0;bottom: 0;"><i class="icon-calendar" style="font-size: 26px;"></i></span>
            </div>
        </div>
        <div class="col-12 col-md-2 col-lg-2">
            <div class="input-group">  
                <input type="text" class="datepicker to_date" name="to_date" id="to_date" autocomplete="off" value="{{$to_date}}" placeholder="To Date"/>
                <span style="position: absolute;top: 8px;right: 0;bottom: 0;"><i class="icon-calendar" style="font-size: 26px;"></i></span>
            </div>
        </div>
        <div class="col-12 col-md-2 col-lg-2">
            <button type="button" class="btn btn-info float-right get-custom-funnel-leads">GET DATA</button>
        </div>
    </div>
    <div class="row mt-4">
        <div class="col-md-12 col-xl-4">
            <div class="card comp-card">
                <div class="card-body">
                    <div class="row align-items-center">
                        <div class="col">
                            <h4 class="m-t-5 m-b-20">Leads</h4>
                        </div>
                        <div class="col-auto">
                            <h3 class="f-w-700 col-green">{{$getleadsCount}}</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12 col-xl-4">
            <div class="card comp-card">
                <div class="card-body">
                    <div class="row align-items-center">
                        <div class="col">
                            <h4 class="m-t-5 m-b-20">Reports</h4>
                        </div>
                        <div class="col-auto">
                            <h3 class="f-w-700 col-orange">{{$getReportsCount}}</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12 col-xl-4">
            <div class="card comp-card">
                <div class="card-body">
                    <div class="row align-items-center">
                        <div class="col">
                            <h4 class="m-t-5 m-b-20">Orders</h4>
                        </div>
                        <div class="col-auto">
                            <h3 class="f-w-700 col-cyan">{{$getOrdersCount}}</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 col-lg-6">
            <div class="card" style="height: 500px;">
                <div class="row">
                    <div class="col-md-12 col-lg-12">
                        <div id="lineChart" style="width: 100%; min-height: 500px; margin: 0 auto"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-lg-6 dasboard-sec">
            <div class="card" style="height: 500px;">
                <div class="header fixed">
                    <h5 class="page-title center m-0">Latest Leads</h5>
                </div>
                @if(isset($getLatestLeads) && sizeof($getLatestLeads)>0)
                    <div class="body pt-0 style-15 mt-2" style="height: 438px;overflow-y:scroll">
                        @foreach($getLatestLeads as $value)
                        <a href="{{URL:: to('/lead-details/'.$value->contactId)}}">
                            <div class="card border mb-3">
                                <div class="body p-2">
                                    <div class="row">
                                        <div class="col-md-12 mb-0">
                                        <?php $report= substr("$value->rep_name",0,50)."...";?>
                                            <p class="" data-toggle="tooltip" data-placement="top" title="{{$value->rep_name}}"><Strong>Report :</Strong>{{$report}}</p>
                                        </div>
                                        <div class="col-md-12 mb-1">
                                            <div class="row pl-2">
                                                <div class="col">
                                                    <strong>User</strong>
                                                </div>
                                                <div class="col">
                                                <strong>Mobile</strong>
                                                </div>
                                                <div class="col">
                                                    <strong>Category</strong>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12 m-0">
                                            <div class="row pl-2">
                                                <div class="col">
                                                    <p class="m-0">{{$value->name}}</p>
                                                </div>
                                                <div class="col">
                                                <p class="m-0">{{$value->phoneNo}}</p>
                                                </div>
                                                <div class="col">
                                                    <?php $category= strlen($value->cat_master_name) > 15 ? substr($value->cat_master_name,0,15)."..." : $value->cat_master_name;?>
                                                    <p class="m-0">{{$category}}</strong>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12 mb-0 d-none">
                                            <div class="table-responsive">
                                                <table class="table table-bordered mb-0">
                                                    <thead>
                                                        <tr>
                                                            <th>User</th>
                                                            <th>Mobile</th>
                                                            <th>Categoty</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>{{$value->name}}</td>
                                                            <td>{{$value->phoneNo}}</td>
                                                            <?php $category= strlen($value->cat_master_name) > 15 ? substr($value->cat_master_name,0,15)."..." : $value->cat_master_name;?>
                                                            <td>{{$category}}</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                        @endforeach
                    </div>
                @else
                    <div class="body">
                        <div class="row">
                            <div class="col-md-12">
                                <p class="center">No Data Available</p>
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="card">
                <div class="body">
                    <div class="row">
                        <div class="col-md-12 col-lg-12 mb-0">
                            <div id="funnelcontainer" style="width: 100%; min-height: 500px; margin: 0 auto"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-lg-6 dasboard-sec">
            <div class="card" style="height: 530px">
                <div class="header">
                    <h5 class="page-title center m-0">Reminders</h5>
                </div>
                @if(isset($getLeadRemainders) && sizeof($getLeadRemainders)>0)
                    <div class="body pt-0 style-15 mt-2" style="height: 450px;overflow-y:scroll">
                        @foreach($getLeadRemainders as $remainder)
                        <a href="{{URL:: to('/lead-details/'.$remainder->lead_id)}}">
                            <div class="card border mb-3">
                                <div class="body p-2">
                                    <div class="row">
                                        <div class="col-md-12 mb-1">
                                            <div class="row pl-2">
                                                <div class="col">
                                                    <strong>Name</strong>
                                                </div>
                                                <div class="col">
                                                <strong>text</strong>
                                                </div>
                                                <div class="col">
                                                    <strong>Follow Up Date</strong>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12 m-0">
                                            <div class="row pl-2">
                                                <div class="col">
                                                    <p class="m-0">{{$value->name}}</p>
                                                </div>
                                                <div class="col">
                                                    <?php $followup_text= substr("$remainder->followup_text",0,35)."...";?>
                                                    <p class="m-0" data-toggle="tooltip" data-placement="top" title="{{$remainder->followup_text}}">{{$followup_text}}</p>
                                                </div>
                                                <div class="col">
                                                    <p class="m-0">{{$remainder->next_followup_date}}</strong>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                        @endforeach
                    </div>
                @else
                    <div class="body">
                        <div class="row">
                            <div class="col-md-12">
                                <p class="center">No Data Available</p>
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>

    <!-- Reports -->
    <div class="row">
        <div class="col-md-12">
            <div class="card-header border-bottom-0 p-3">
                <h4>Reports</h4>
            </div>
        </div>
    </div>
    <div class="row mt-4">
        <div class="col-md-6 col-lg-6">
            <div class="card" style="height: 500px;">
                <div class="row">
                    <div class="col-md-12 col-lg-12">
                        <div id="reportLineChart" style="width: 100%; min-height: 500px; margin: 0 auto"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-lg-6 dasboard-sec">
            <div class="card" style="height: 500px;">
                <div class="header fixed">
                    <h5 class="page-title center m-0">Latest Reports</h5>
                </div>
                @if(isset($getLatestReports) && sizeof($getLatestReports)>0)
                    <div class="body pt-0 style-15 mt-2" style="height: 438px;overflow-y:scroll">
                        @foreach($getLatestReports as $value)
                        <a href="{{URL:: to('/view_report/'.$value->rep_id)}}">
                            <div class="card border mb-3">
                                <div class="body p-2">
                                    <div class="row">
                                        <div class="col-md-12 mb-0">
                                        <?php $report= substr("$value->rep_name",0,50)."...";?>
                                            <p class="" data-toggle="tooltip" data-placement="top" title="{{$value->rep_name}}"><Strong>Report :</Strong>{{$report}}</p>
                                        </div>
                                        <div class="col-md-12 mb-1">
                                            <div class="row pl-2">
                                                <div class="col">
                                                    <?php $category= strlen($value->cat_master_name) > 15 ? substr($value->cat_master_name,0,15)."..." : $value->cat_master_name;?>
                                                    <p class="m-0"><strong>Category :</strong> {{$category}}</p>
                                                </div>
                                                <div class="col">
                                                    <?php $rep_pub_date = date("d-m-Y", strtotime($value->rep_pub_date));?>
                                                    <p class="m-0"><strong>Publish Date :</strong> {{$rep_pub_date}}</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                        @endforeach
                    </div>
                @else
                    <div class="body">
                        <div class="row">
                            <div class="col-md-12">
                                <p class="center">No Data Available</p>
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>

    <!-- Orders -->
    <div class="row">
        <div class="col-md-12">
            <div class="card-header border-bottom-0 p-3">
                <h4>Orders</h4>
            </div>
        </div>
    </div>
    <div class="row mt-4">
        <div class="col-md-6 col-lg-6">
            <div class="card" style="height: 500px;">
                <div class="row">
                    <div class="col-md-12 col-lg-12">
                        <div id="ordersLineChart" style="width: 100%; min-height: 500px; margin: 0 auto"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-lg-6 dasboard-sec">
            <div class="card" style="height: 500px;">
                <div class="header fixed">
                    <h5 class="page-title center m-0">Latest Orders</h5>
                </div>
                @if(isset($getLatestOrders) && sizeof($getLatestOrders)>0)
                    <div class="body pt-0 style-15 mt-2" style="height: 438px;overflow-y:scroll">
                        @foreach($getLatestOrders as $value)
                        <div class="card border mb-3">
                            <div class="body p-2">
                                <div class="row">
                                    <div class="col-md-12 mb-0">
                                    <?php $report= substr("$value->rep_name",0,50)."...";?>
                                        <p class="" data-toggle="tooltip" data-placement="top" title="{{$value->rep_name}}"><Strong>Report :</Strong>{{$report}}</p>
                                    </div>
                                    <div class="col-md-12 mb-1">
                                        <div class="row pl-2">
                                            <div class="col">
                                                <p class="m-0"><strong>Transaction Status: </strong> {{$value->transaction_status}}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                @else
                    <div class="body">
                        <div class="row">
                            <div class="col-md-12">
                                <p class="center">No Data Available</p>
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>

    <!-- Articles -->
    <div class="row">
        <div class="col-md-12">
            <div class="card-header border-bottom-0 p-3">
                <h4>Articles</h4>
            </div>
        </div>
    </div>
    <div class="row mt-4">
        <div class="col-md-6 col-lg-6 d-none">
            <div class="card" style="height: 500px;">
                <div class="row">
                    <div class="col-md-12 col-lg-12">
                        <div id="articlesLineChart" style="width: 100%; min-height: 500px; margin: 0 auto"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12 col-lg-12 dasboard-sec">
            <div class="card" style="height: 500px;">
                <div class="header fixed">
                    <h5 class="page-title center m-0">Latest Articles</h5>
                </div>
                @if(isset($getLatestArticles) && sizeof($getLatestArticles)>0)
                    <div class="body pt-0 style-15 mt-2" style="height: 438px;overflow-y:scroll">
                        @foreach($getLatestArticles as $value)
                        <a href="{{URL:: to('/view_article/'.$value->id)}}">
                            <div class="card border mb-3">
                                <div class="body p-2">
                                    <div class="row">
                                        <div class="col-md-12 mb-0">
                                            <?php $aname= $value->name ? substr($value->name,0,140)."..." : $value->name;?>
                                            <p class="" data-toggle="tooltip" data-placement="top" title="{{$value->name}}"><Strong>Article :</Strong>{{$aname}}</p>
                                        </div>
                                        <div class="col-md-12 mb-1">
                                            <div class="row pl-2">
                                                <div class="col">
                                                    <p class="m-0"><strong>URL : </strong> {{$value->url}}</p>
                                                </div>
                                                <div class="col">
                                                    <?php $rep_name= $value->rep_name ? substr($value->rep_name,0,30)."..." : $value->rep_name;?>
                                                    <p class="m-0" data-toggle="tooltip" data-placement="top" title="{{$value->rep_name}}"><strong>Report : </strong> {{$rep_name}}</p>
                                                </div>
                                                <div class="col">
                                                    <p class="m-0"><strong>Category : </strong> {{$value->cat_name}}</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                        @endforeach
                    </div>
                @else
                    <div class="body">
                        <div class="row">
                            <div class="col-md-12">
                                <p class="center">No Data Available</p>
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>

    <!-- Articles -->
    <div class="row">
        <div class="col-md-12">
            <div class="card-header border-bottom-0 p-3">
                <h4>News</h4>
            </div>
        </div>
    </div>
    <div class="row mt-4">
        <div class="col-md-12 col-lg-12 dasboard-sec">
            <div class="card" style="height: 500px;">
                <div class="header fixed">
                    <h5 class="page-title center m-0">Latest News</h5>
                </div>
                @if(isset($getLatestNews) && sizeof($getLatestNews)>0)
                    <div class="body pt-0 style-15 mt-2" style="height: 438px;overflow-y:scroll">
                        @foreach($getLatestNews as $value)
                        <a href="{{URL:: to('/view_news/'.$value->id)}}">
                            <div class="card border mb-3">
                                <div class="body p-2">
                                    <div class="row">
                                        <div class="col-md-12 mb-0">
                                            <?php $gnews_title= substr("$value->gnews_title",0,150)."...";?>
                                            <p class="" data-toggle="tooltip" data-placement="top" title="{{$value->gnews_title}}"><Strong>News :</Strong>{{$gnews_title}}</p>
                                        </div>
                                        <div class="col-md-12 mb-1">
                                            <div class="row pl-2">
                                                <div class="col">
                                                    <p class="m-0"><strong>News By : </strong> {{$value->news_by}}</p>
                                                </div>
                                                <div class="col">
                                                    <p class="m-0"><strong>Publish Date : </strong> {{$value->publish_date}}</p>
                                                </div>
                                                <div class="col">
                                                    <p class="m-0"><strong>Category : </strong> {{$value->cat_name}}</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                        @endforeach
                    </div>
                @else
                    <div class="body">
                        <div class="row">
                            <div class="col-md-12">
                                <p class="center">No Data Available</p>
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
</section>
@endsection