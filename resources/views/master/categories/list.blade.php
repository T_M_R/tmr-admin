@extends('layouts.header')
@section('body-content')
<section class="category-list">
    <div class="block-header">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 my-auto">
                <h4 class="page-title m-0">Categories</h4>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5 my-auto">
                <div class="input-group m-0">
                    <form method="GET" action="{{url('/categories')}}" enctype="multipart/form-data" role="form" autocomplete="off">
                        <input type="search" id="search_key" name="search_key" class="search-field m-0" value="{{$search_key}}" placeholder="Search">
                        <div class="input-group-append">
                            <button type="submit" class="btn btn-primary"><i class="fa fa-search mr-1"></i> Search</button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-1 col-lg-1 pl-0">
                <form method="GET" action="{{url('/categories')}}" enctype="multipart/form-data" role="form" autocomplete="off">
                    <button type="submit" class="btn btn-clear">Clear</button>
                </form>
            </div>
            <?php 
                $sessionUser=Session::get('subMenuArray');  
                $val=[];
                $allRoutes=[];
                $addButtonStatus=0;
                $editButtonStatus=0;
                $viewButtonStatus=0;
                foreach($sessionUser as $PagePermission)
                {
                    if(isset($PagePermission['route'])){
                        if($PagePermission['route'] == "categories")
                        {
                            $addButtonStatus=$PagePermission['is_add'];
                            $editButtonStatus=$PagePermission['is_edit'];
                            $viewButtonStatus=$PagePermission['is_view'];
                        }
                    }
                }
            ?>
            <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                @if($addButtonStatus == 1)
                <a href="{{URL:: to('/add_category')}}">
                    <button type="button" class="btn btn-info float-right">Add New</button>
                </a>
                <!-- <button type="button" class="btn btn-primary float-right add_category_modal_btn">Add New</button> -->
                @endif
            </div>
        </div>
    </div>
    <div class="row clearfix mt-3">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            @if(isset($getCategoriesList) && sizeof($getCategoriesList)>0)
            <div class="card">
                <div class="body p-0">
                    <div class="table-responsive">
                        <!-- <table class="table table-bordered table-striped table-hover js-basic-example dataTable"> -->
                        <table class="table table-bordered mb-0">
                            <thead>
                                <tr>
                                    <th>S.No</th>
                                    <th>Category Name</th>
                                    <th>Status</th>
                                    @if($editButtonStatus == 1)
                                    <th>Action</th>
                                    @endif
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                    $page_id = $getCategoriesList->currentPage();
                                    if($page_id == 1 || $page_id == '')
                                    {
                                        $i=1; 
                                    }
                                    else
                                    {
                                        $i=($page_id-1)*30;
                                        $i= $i+1;
                                    }
                                ?>
                                @foreach($getCategoriesList as $value)
                                    <tr>
                                        <td>{{$i}}</td>
                                        <td>{{$value->cat_name}}</td>
                                        <td>
                                            @if($value->cat_status == 'Y')
                                                <input class="checkbox category_toggle_event" type="checkbox" data-toggle="toggle" data-style="ios" checked>
                                                <input type="hidden" name="hidden_status" class="hidden_status" value="Y" />
                                            @else
                                                <input class="checkbox category_toggle_event" type="checkbox" data-toggle="toggle" data-style="ios">
                                                <input type="hidden" name="hidden_status" class="hidden_status" value="N"/>
                                            @endif
                                            <input type="hidden" class="category_id" value="{{$value->category_id}}">
                                        @if($editButtonStatus == 1)
                                        <td>
                                            <a href="{{URL:: to('/edit_category/'.$value->category_id)}}" class="text-decoration-none">
                                                <button class="btn tblActnBtn">
                                                    <i class="material-icons">mode_edit</i>
                                                </button>
                                            </a>
                                        </td>
                                        @endif
                                    </tr>
                                    <?php $i++;?>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            @else
                <div class="card">
                    <p class="text-center py-5" style="font-size:25px">No Data Available To Show</p>  
                </div>
            @endif
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 paginate-sec">
            @if(sizeof($getCategoriesList)>0)
                {{$getCategoriesList->links()}}
            @endif
        </div>
    </div>
</section>

<div class="modal fade addCategoryModal" id="addCategoryModal" tabindex="-1" role="dialog" aria-labelledby="formModal" aria-hidden="true">    
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title modal-title" id="formModal">Add Category</h5>
                <button type="button" class="close modal-close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12 p-0">
                        <div class="col-12 col-md-12 mb-0">
                            <div class="input-group">  
                                <input type="text" class="category_name" autocomplete="off" value="" required/>
                                <label>Category Name <span class="text-danger">*</span></label>
                            </div>
                        </div>
                        <input type="hidden" class="category_id">
                        <div class="col-12 col-md-6 mb-0">
                            <p class="text-danger mb-0">* All fields are Mandatory</p>
                        </div>
                        <p class="cat_success_msg mb-0 text-success text-center"></p>
                        <p class="cat_error_msg mb-0 text-danger text-center"></p>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger modal-close" data-dismiss="modal">CLOSE</button>
                <button type="button" class="btn btn-primary save_category">SUBMIT</button>
            </div>
        </div>
    </div>
</div>
@endsection