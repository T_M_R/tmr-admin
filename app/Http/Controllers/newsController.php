<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\news;
use App\master;
use Carbon\Carbon;
use Redirect;
use Session;
use DB;
use Config;

class newsController extends Controller
{
    public function __construct(){
        $this->news = new news();
        $this->master = new master();
    }

    public function getNewsData(Request $request){
        if (!session('username')) {
	    	return redirect('/');
        } 
        $search_key = '';
        if(isset($_GET['search_key'])){
            $search_key=$_GET['search_key'];
        }
        $getNewsData = $this->news->getNewsData($search_key);
        $getNewsData = $getNewsData->appends(['search_key'=>$search_key]);
       
        return view('news.list')->with('getNewsData',$getNewsData)->with('search_key',$search_key);
    }

    public function addNewNews(){
        if (!session('username')) {
	    	return redirect('/');
        }

        $getSubCategories = $this->master->getSubCategories();
        $getCategories = $this->master->getCategories();

        return view('news.add')->with('getCategories',$getCategories)->with('getSubCategories',$getSubCategories);
    }

     public function save_news(Request $request){
        if (!session('username')) {
	    	return redirect('/');
        } 
        
        $data = $request->all();
        $img_name='';
        if ($request->hasFile('fileToUpload')) 
        {
            $image    = $request->file('fileToUpload');
            $imgname  = $image->getClientOriginalName();
            $img_name = $imgname;
            $destinationPath = public_path('/assets/images/project');
            $image->move($destinationPath, $imgname);
        }
        else
        {
            $img_name=$request->input('hiddenfileToUpload');
        }

        $save_news = $this->news->save_news($data,$img_name);
        // return redirect('/news')->with('message','News Saved Successfully');
        if($save_news == 1){
            return redirect('/news')->with('message','News Updated Successfully');
        }
        else{
            return redirect('/news')->with('error','Something Went Wrong..!!, Please try again.');
        }
    }

     public function edit_news($id){
        if (!session('username')) {
	    	return redirect('/');
        }

        $getSubCategories = $this->master->getSubCategories();
        $edit_news = $this->news->getNewsDataById($id);
        $getCategories = $this->master->getCategories();

        return view('news.edit')->with('getSubCategories',$getSubCategories)->with('getCategories',$getCategories)->with('edit_news',$edit_news);
    }

    public function update_news(Request $request){
        if (!session('username')) {
	    	return redirect('/');
        } 
        
        $data = $request->all();
        $img_name='';
        if ($request->hasFile('fileToUpload')) 
        {
            $image    = $request->file('fileToUpload');
            $imgname  = $image->getClientOriginalName();
            $img_name = $imgname;
            $destinationPath = public_path('/assets/images/project');
            $image->move($destinationPath, $imgname);
        }
        else
        {
            $img_name=$request->input('hiddenfileToUpload');
        }
        
        $update_news = $this->news->update_news($data,$img_name);
        // return redirect('/news')->with('message','News Updated Successfully');
        if($update_news == 1){
            return redirect('/news')->with('message','News Updated Successfully');
        }
        else{
            return redirect('/news')->with('error','Something Went Wrong..!!, Please try again.');
        }
    }

    public function update_news_status(Request $request){
        if (!session('username')) {
	    	return redirect('/');
        } 
        
        $data = $request->all();
        $update_news_status = $this->news->update_news_status($data);
        return $update_news_status;
    }

    public function view_news($id){
        if (!session('username')) {
	    	return redirect('/');
        }

        $getSubCategories = $this->master->getSubCategories();
        $view_news = $this->news->getNewsDataById($id);
        $getCategories = $this->master->getCategories();

        return view('news.view')->with('getSubCategories',$getSubCategories)->with('getCategories',$getCategories)->with('view_news',$view_news);
    }
}
