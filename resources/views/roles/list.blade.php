@extends('layouts.header')
@section('body-content')
<section class="regions-list">
    <div class="block-header">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 my-auto">
                <h4 class="page-title m-0">Roles</h4>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5 my-auto">
                <div class="input-group m-0  d-none">
                    <form method="GET" action="{{url('/roles')}}" enctype="multipart/form-data" role="form" autocomplete="off">
                        <input type="search" id="search_key" name="search_key" class="search-field m-0" value="{{$search_key}}" placeholder="Search">
                        <div class="input-group-append">
                            <button type="submit" class="btn btn-primary"><i class="fa fa-search mr-1"></i> Search</button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-1 col-lg-1 pl-0">
                <form method="GET" action="{{url('/roles')}}" enctype="multipart/form-data" role="form" autocomplete="off"  class="d-none">
                    <button type="submit" class="btn btn-clear">Clear</button>
                </form>
            </div>
            <?php 
                $sessionUser=Session::get('subMenuArray');  
                $val=[];
                $allRoutes=[];
                $addButtonStatus=0;
                $editButtonStatus=0;
                $viewButtonStatus=0;
                foreach($sessionUser as $PagePermission)
                {
                    if(isset($PagePermission['route'])){
                        if($PagePermission['route'] == "roles")
                        {
                            $addButtonStatus=$PagePermission['is_add'];
                            $editButtonStatus=$PagePermission['is_edit'];
                            $viewButtonStatus=$PagePermission['is_view'];
                        }
                    }
                }
            ?>
            <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                @if($addButtonStatus == 1)
                <a href="{{URL:: to('/add_role')}}" >
                    <button type="button" class="btn btn-primary float-right">Add New</button>
                </a>
                @endif
            </div>
        </div>
    </div>
    <div class="row clearfix mt-3">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            @if(session()->has('message'))
                <div class="badge col-green w-100">
                    {{ session()->get('message') }}
                    <?php session()->forget('message'); ?>
                </div>
            @endif
            @if(session()->has('error'))
                <div class="badge col-red w-100">
                    {{ session()->get('error') }}
                    <?php session()->forget('error'); ?>
                </div>
            @endif
            @if(sizeof($getRoles)>0)
            <div class="card">
                <div class="body p-0">
                    <div class="table-responsive">
                        <table class="table table-bordered mb-0">
                            <thead>
                                <tr>
                                    <th>S.No</th>
                                    <th>Role Name</th>
                                    @if($editButtonStatus == 1 || $viewButtonStatus == 1)
                                    <th>Action</th>
                                    @endif
                                </tr>
                            </thead>
                            <tbody>
                                <?php $i=1;?>
                                @foreach($getRoles as $value)
                                    <tr>
                                        <td>{{$i}}</td>
                                        <td>{{$value->role_name}}</td>
                                        @if($editButtonStatus == 1 || $viewButtonStatus == 1)
                                        <td>
                                            @if($editButtonStatus == 1)
                                            <a href="{{URL:: to('/edit_role_permissions/'.$value->role_id)}}" class="text-decoration-none">
                                                <button class="btn tblActnBtn">
                                                    <i class="material-icons">mode_edit</i>
                                                </button>
                                            </a>
                                            @endif
                                            @if($viewButtonStatus == 1)
                                            <a href="{{URL:: to('/view_role_permissions/'.$value->role_id)}}" class="text-decoration-none">
                                                <button class="btn tblActnBtn">
                                                <i class="fa fa-eye" aria-hidden="true"></i>
                                                </button>
                                            </a>
                                            @endif
                                        </td>
                                        @endif
                                    </tr>
                                    <?php $i++;?>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            @else
                <div class="card">
                    <p class="text-center py-5" style="font-size:25px">No Data Available To Show</p>  
                </div>
            @endif
        </div>
    </div>
</section>
@endsection