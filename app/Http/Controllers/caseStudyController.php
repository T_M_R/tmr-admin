<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\caseStudy;
use App\master;
use Carbon\Carbon;
use Redirect;
use Session;
use DB;
use Config;

class caseStudyController extends Controller
{
    public function __construct(){
        $this->caseStudy = new caseStudy();
        $this->master = new master();
    }

    public function getCaseStudyData(Request $request){
        if (!session('username')) {
	    	return redirect('/');
        } 
        $search_key = '';
        if(isset($_GET['search_key'])){
            $search_key=$_GET['search_key'];
        }
        $getCaseStudyData = $this->caseStudy->getCaseStudyData($search_key);
        $getCaseStudyData = $getCaseStudyData->appends(['search_key'=>$search_key]);

        return view('caseStudy.list')->with('getCaseStudyData',$getCaseStudyData)->with('search_key',$search_key);
    }

    public function addNewCaseStudy(){
        if (!session('username')) {
	    	return redirect('/');
        }

        $getCategories = $this->master->getCategories();
        $getSubCategories = $this->master->getSubCategories();
        $getAuthors = $this->master->getAuthors();

        return view('caseStudy.add')->with('getCategories',$getCategories)->with('getSubCategories',$getSubCategories)->with('getAuthors',$getAuthors);
    }

    public function save_case_study(Request $request){
        if (!session('username')) {
	    	return redirect('/');
        } 
        
        $data = $request->all();
        
        $save_case_study = $this->caseStudy->save_case_study($data);
        // return $save_case_study;
        if($save_case_study == 1){
            return redirect('/case_studies')->with('message','Case Study saved successfully');
        }
        else{
            return redirect('/case_studies')->with('error','Something Went Wrong..!!, Please try again.');
        }
    }

    public function edit_case_study($id){
        if (!session('username')) {
	    	return redirect('/');
        }

        $getCategories = $this->master->getCategories();
        $getSubCategories = $this->master->getSubCategories();
        $getAuthors = $this->master->getAuthors();
        $edit_case_study = $this->caseStudy->getCaseStudyDataById($id);

        return view('caseStudy.edit')->with('getCategories',$getCategories)->with('getSubCategories',$getSubCategories)->with('getAuthors',$getAuthors)->with('edit_case_study',$edit_case_study);
    }

    public function update_case_study(Request $request){
        if (!session('username')) {
	    	return redirect('/');
        } 
        
        $data = $request->all();
        
        $update_case_study = $this->caseStudy->update_case_study($data);
        // return $update_case_study;
        if($update_case_study == 1){
            return redirect('/case_studies')->with('message','Case Study updated successfully');
        }
        else{
            return redirect('/case_studies')->with('error','Something Went Wrong..!!, Please try again.');
        }
        // return redirect('/case_studies');
    }

    public function update_cs_status(Request $request){
        if (!session('username')) {
	    	return redirect('/');
        } 
        
        $data = $request->all();
        $update_cs_status = $this->caseStudy->update_cs_status($data);
        return $update_cs_status;
    }
}
