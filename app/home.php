<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Session;
use Config;
use Carbon\Carbon;
use DateTime;
use DateInterval;
use DatePeriod;

class home extends Model
{
    public function prepareMonths($time=null, $from=null, $to=null){
        if($from != null && $to != null){
            $fromdate = date('Y-m-d', strtotime($from));
            $todate = date('Y-m-d', strtotime($to));
            // for diff b/n two dates------------------------------------------------
            $date1=$fromdate;
            $date2=$todate; 
            function dateDiff($date1, $date2) 
            { 
                $date1_ts = strtotime($date1); 
                $date2_ts = strtotime($date2); 
                $diff = $date2_ts - $date1_ts;
                return round($diff / 86400); 
            } 
            $no_of_days= dateDiff($date1, $date2);

            $monthNumbers=[];
            $monthNames=[];
            $dates_array=[];
            for($i=1;$i<=$no_of_days+1;$i++){
                $start_date=date("Y-m-d H:i:s",strtotime($fromdate));
                $end_date=date("Y-m-d H:i:s",strtotime($fromdate.' +1 day'));
                // echo "date1: ".$start_date." and date2 ".$end_date."<br>";
                $fromdate = $end_date;
                array_push($dates_array, $start_date);
            }
            foreach($dates_array as $dates){
                $monthNames[] = date('M-Y', strtotime($dates));
            }
            $monthNames = array_values(array_unique($monthNames));
            $firstValue = reset($dates_array);
            $lastvalue = end($dates_array);
            $monthNumbers = array($firstValue,$lastvalue);
            $finalMonthsList=["month_names"=>$monthNames, "month_numbers"=>$monthNumbers, "no_of_days" => $no_of_days,"date_range_flag"=>1];
            Session::put('date_range_flag', 1);
            return $finalMonthsList;
        }
    }

    public function no_of_Days($givenmonths){
        $fromdate = date('Y-m-d', strtotime($givenmonths[0]));
        $todate = date('Y-m-d', strtotime($givenmonths[1]));

        $date1=$fromdate;
        $date2=$todate;

        function dateDiff($date1, $date2) 
        { 
            $date1_ts = strtotime($date1); 
            $date2_ts = strtotime($date2); 
            $diff = $date2_ts - $date1_ts;
            return round($diff / 86400); 
        } 
        $no_of_days= dateDiff($date1, $date2);
    }

    public function getleadsCount($get_my_team,$user_id, $category_id){
        $getleadsCount = DB::table('leads')->whereIn('userId',$get_my_team)->count();

        return $getleadsCount;
    }

    public function getReportsCount(){
        $getReportsCount = DB::table('reports')->count();

        return $getReportsCount;
    }

    public function getOrdersCount(){
        $getOrdersCount = DB::table('order_amex_transaction')->count();

        return $getOrdersCount;
    }

    public function get_mnth_wise_lead_data($givenmonths,$monthNamesArray, $no_of_days, $user_id, $category_id, $get_my_team){
        $dataArray=[];
        $dates_array=[];
        
        $status = false;
        $fromdate = $givenmonths[0];
        $todate = $givenmonths[1];
        $no_of_months = sizeof($monthNamesArray);

        for($i=1;$i<=$no_of_days+1;$i++){
            
            $from_date=date("Y-m-d",strtotime($fromdate));
            $to_date=date("Y-m-d",strtotime($fromdate.' +1 day'));

            $get_mnth_wise_lead_data = DB::table('leads')
                                    ->leftjoin('reports as r','r.rep_id','=','leads.repId')
                                    ->select('contactId');
                                    
            if($from_date != '' && $to_date != ''){
                $get_mnth_wise_lead_data->whereRaw("leads.created_date BETWEEN '$from_date' AND '$to_date' AND leads.created_date is not null");
            }

            if($user_id != ''){
                $get_mnth_wise_lead_data->where('leads.userId','=',$user_id);
            }
            else{
                $get_mnth_wise_lead_data->whereIn('leads.userId',$get_my_team);
            }

            if($category_id != ''){
                $get_mnth_wise_lead_data->where('r.cat_id','=',$category_id);
            }

            $get_mnth_wise_lead_data = $get_mnth_wise_lead_data->count();

            $fromdate = $to_date;
            array_push($dataArray,$get_mnth_wise_lead_data);
            array_push($dates_array, $from_date);
        }

        $status = true;
        return json_encode(array(
            'status' => $status,
            'dates_array' => $dates_array,
            // 'monthNamesArray' => $monthNamesArray,
            'get_mnth_wise_lead_data' => $dataArray
        ));
    }

    public function getLatestLeads($get_my_team,$user_id, $category_id){
        $getLatestLeads = DB::table('leads')
                        ->join('reports as r','r.rep_id','=','leads.repId')
                        ->join('category_master as cm','cm.cat_master_id','=','r.cat_id')
                        ->select('leads.contactId','leads.name','leads.emailId','leads.phoneNo','r.rep_name','cm.cat_master_name')
                        // ->whereIn('leads.userId',$get_my_team)
                        ->orderBy('leads.contactId','DESC')
                        ->limit(10);
                        // ->get();
        if($user_id != ''){
            $getLatestLeads->where('leads.userId','=',$user_id);
        }
        else{
            $getLatestLeads->whereIn('leads.userId',$get_my_team);
        }

        if($category_id != ''){
            $getLatestLeads->where('r.cat_id','=',$category_id);
        }

        $getLatestLeads = $getLatestLeads->get();

        return $getLatestLeads;
    }

    public function getLeadStatus($from_date, $to_date, $user_id, $category_id, $get_my_team){

        $query = "SELECT  SUM(if(leads.leadStatus = 'N', 1, 0)) as active, SUM(if(leads.leadStatus = 'P', 1, 0)) as pipeline, SUM(if(leads.leadStatus = 'IP', 1, 0)) as inprocess, SUM(if(leads.leadStatus = 'C', 1, 0)) as closed, SUM(if(leads.leadStatus = 'SALE', 1, 0)) as sale FROM leads left join reports r on r.rep_id = leads.repId WHERE leads.contactId IS NOT NULL";
        
        if(!empty($from_date) && !empty($to_date)){
            $from_date = date("Y-m-d H:i:s", strtotime($from_date));
            $to_date = date("Y-m-d H:i:s", strtotime($to_date));
            $date_filter = " AND (leads.created_date BETWEEN '$from_date' AND '$to_date')";
            $query = $query.$date_filter;
        }

        if(!empty($user_id)){
            $user_filter = " AND leads.userId = $user_id ";
            $query = $query.$user_filter;
        }
        else{
            if(count($get_my_team)>0){
                $ids = "(" . implode(",", $get_my_team) . ")";
                $user_filter = " AND leads.userId IN $ids ";
                $query = $query.$user_filter;
            }
        }

        if($category_id != ''){
            $category_filter = " AND r.cat_id = $category_id ";
            $query = $query.$category_filter;
        }

        // if(!empty($category_id)){
        //     $user_filter = " AND category_id = $category_id ";
        //     $query = $query.$user_filter;
        // }
        
        $getLeadStatus = DB::Select($query);
        
        $status = false;
        if(count($getLeadStatus)>0){
            $status = true;
        }
        return json_encode(array(
            'status' => $status,
            'leadStatus' => $getLeadStatus
        ));
    }

    public function getLeadRemainders($get_my_team){
        $date= Carbon::now();
        $date = date("d-m-Y", strtotime($date));
        // print_r($date);
        // exit;
        $getLeadRemainders = DB::table('lead_followup')
                        ->leftjoin('users','users.id','=','lead_followup.user_id')
                        ->select('lead_followup.id','lead_followup.lead_id','lead_followup.followup_text','lead_followup.next_followup_date','users.name')
                        ->where('lead_followup.next_followup_date','>',$date.' - 00:00')
                        ->where('lead_followup.next_followup_date','<',$date.' - 23:59')
                        // ->whereIn('user_id',$get_my_team)
                        ->orderBy('lead_followup.id','DESC')
                        ->get();

        return $getLeadRemainders;
    }

    // Reports Data
    public function get_mnth_wise_report_data($givenmonths, $monthNamesArray, $no_of_days, $user_id, $category_id, $get_my_team){
        $dataArray=[];
        $dates_array=[];
        
        $status = false;
        $fromdate = $givenmonths[0];
        $todate = $givenmonths[1];
        $no_of_months = sizeof($monthNamesArray);

        for($i=1;$i<=$no_of_days+1;$i++){
            
            $from_date=date("Y-m-d",strtotime($fromdate));
            $to_date=date("Y-m-d",strtotime($fromdate.' +1 day'));

            $get_mnth_wise_report_data = DB::table('reports')
                                    // ->select(DB::raw("count(rep_id) as count"),'month_year');
                                    ->select('rep_id');
                                    
            if($from_date != '' && $to_date != ''){
                $get_mnth_wise_report_data->whereRaw("created_at BETWEEN '$from_date' AND '$to_date' AND created_at is not null");
            }

            if($category_id != ''){
                $get_mnth_wise_report_data->where('cat_id','=',$category_id);
            }

            $get_mnth_wise_report_data = $get_mnth_wise_report_data->count();

            $fromdate = $to_date;
            array_push($dataArray,$get_mnth_wise_report_data);
            array_push($dates_array, $from_date);
        }
        
        $status = true;
        return json_encode(array(
            'status' => $status,
            'dates_array' => $dates_array,
            'get_mnth_wise_report_data' => $dataArray
        ));
    }

    public function getLatestReports($search_key){
        $getLatestReports = DB::table('reports')
                        ->join('category_master as cm','cm.cat_master_id','=','reports.cat_id')
                        ->select('rep_id','rep_name','rep_url','rep_type','rep_pub_date','rep_status','cm.cat_master_name')
                        ->orderBy('rep_id','DESC')
                        ->limit(8)
                        ->get();
        if(sizeof($getLatestReports)>0){
            return $getLatestReports;
        }
        else{
            return [];
        }
    }

    // Orders Data
    public function get_mnth_wise_orders_data($givenmonths, $monthNamesArray, $no_of_days, $user_id, $category_id, $get_my_team){
        $dataArray=[];
        $dates_array=[];
        
        $status = false;
        $fromdate = $givenmonths[0];
        $todate = $givenmonths[1];
        $no_of_months = sizeof($monthNamesArray);

        for($i=1;$i<=$no_of_days+1;$i++){
            
            $from_date=date("Y-m-d",strtotime($fromdate));
            $to_date=date("Y-m-d",strtotime($fromdate.' +1 day'));

            $get_mnth_wise_orders_data = DB::table('order_amex_transaction as oat')
                                    ->leftjoin('order_master_live as oml','oml.id','=','oat.order_id')
                                    ->leftjoin('reports as r','r.rep_id','=','oat.report_id')
                                    // ->select(DB::raw("count(rep_id) as count"),'month_year');
                                    ->select('oat.order_id');
                                    
            if($from_date != '' && $to_date != ''){
                $get_mnth_wise_orders_data->whereRaw("oat.transaction_date BETWEEN '$from_date' AND '$to_date' AND oat.transaction_date is not null");
            }

            if($category_id != ''){
                $get_mnth_wise_orders_data->where('r.cat_id','=',$category_id);
            }

            $get_mnth_wise_orders_data = $get_mnth_wise_orders_data->count();

            $fromdate = $to_date;
            array_push($dataArray,$get_mnth_wise_orders_data);
            array_push($dates_array, $from_date);
        }

        $status = true;
        return json_encode(array(
            'status' => $status,
            'dates_array' => $dates_array,
            'get_mnth_wise_orders_data' => $dataArray
        ));
    }

    public function getLatestOrders($search_key){
        $getLatestOrders = DB::table('order_amex_transaction as oat')
                    ->join('reports','reports.rep_id','=','oat.report_id')
                    ->select('reports.rep_name','oat.order_id','oat.report_purchase_attempt_id','oat.order_amount','oat.transaction_status','oat.transaction_date')
                    ->orderBy('order_id','DESC')
                    ->limit(8)
                    ->get();
        if(sizeof($getLatestOrders)>0){
            return $getLatestOrders;
        }
        else{
            return [];
        }
    }

    // Articles Data
    public function get_mnth_wise_articles_data($givenmonths, $monthNamesArray, $no_of_days, $user_id, $category_id, $get_my_team){
        $status = false;
        $get_mnth_wise_articles_data = DB::table('tbl_articles')
                                    ->select(DB::raw("count(id) as count"),'month_year')
                                    // ->whereIn('userId',$get_my_team)
                                    ->groupBy('month_year')
                                    ->get();
        if(sizeof($get_mnth_wise_articles_data)>0){
            // return $get_mnth_wise_articles_data;
            $status = true;
            return json_encode(array(
                'status' => $status,
                'get_mnth_wise_articles_data' => $get_mnth_wise_articles_data
        ));
        }
        else{
            return [];
        }

        // $dataArray=[];
        // $dates_array=[];
        
        // $status = false;
        // $fromdate = $givenmonths[0];
        // $todate = $givenmonths[1];
        // $no_of_months = sizeof($monthNamesArray);

        // for($i=1;$i<=$no_of_days+1;$i++){
            
        //     $from_date=date("Y-m-d",strtotime($fromdate));
        //     $to_date=date("Y-m-d",strtotime($fromdate.' +1 day'));

        //     $get_mnth_wise_articles_data = DB::table('articles')
        //                             ->leftjoin('reports as r','r.rep_id','=','articles.rep_id')
        //                             // ->select(DB::raw("count(rep_id) as count"),'month_year');
        //                             ->select('id');
                                    
        //     if($from_date != '' && $to_date != ''){
        //         $get_mnth_wise_articles_data->whereRaw("date(Y-m-d, articles.created_at) BETWEEN '$from_date' AND '$to_date' AND articles.created_at is not null");
        //     }

        //     if($category_id != ''){
        //         $get_mnth_wise_articles_data->where('r.cat_id','=',$category_id);
        //     }

        //     $get_mnth_wise_articles_data = $get_mnth_wise_articles_data->count();

        //     $fromdate = $to_date;
        //     array_push($dataArray,$get_mnth_wise_articles_data);
        //     array_push($dates_array, $from_date);
        // }
        // print_r($dataArray);
        // print_r($dates_array);
        // exit;
        // $status = true;
        // return json_encode(array(
        //     'status' => $status,
        //     'dates_array' => $dates_array,
        //     'get_mnth_wise_articles_data' => $dataArray
        // ));
    }

    public function getLatestArticles($search_key){
       $getLatestArticles = DB::table('tbl_articles')
                        ->leftjoin('reports as r','r.rep_id','=','tbl_articles.rep_id')
                        ->leftjoin('category','category.category_id','=','tbl_articles.category_id')
                        ->select('tbl_articles.id','tbl_articles.name','tbl_articles.url','tbl_articles.status','r.rep_name','category.cat_name')
                        ->orderBy('tbl_articles.id','DESC')
                        ->limit(8)
                        ->get();
        if(sizeof($getLatestArticles)>0){
            return $getLatestArticles;
        }
        else{
            return [];
        }
    }

    // News
    public function getLatestNews($search_key){
        $getLatestNews = DB::table('gnews')
                    ->leftjoin('category','category.category_id','=','gnews.category_id')
                    ->select('gnews.id','gnews.gnews_title','gnews.gnews_url','gnews.category_id','gnews.status','gnews.news_by','gnews.publish_date','gnews.status','gnews.status','category.cat_name')
                    ->orderBy('gnews.id','DESC')
                    ->limit(10)
                    ->get();

        if(sizeof($getLatestNews)>0){
            return $getLatestNews;
        }
        else{
            return [];
        }
    }
}
