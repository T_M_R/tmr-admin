<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\login;
use App\master;
use Carbon\Carbon;
use Redirect;
use Session;
use DB;
use Config;

class loginController extends Controller
{
    public function __construct(){
        $this->login = new login();
        $this->master = new master();
    }

    public function login(Request $request){
        return view('login');
    }

    public function validatelogin(Request $request){
        $data = $request->all();
        if(!isset($data['username']) || !isset($data['password'])){
            return redirect('/')->with('error', 'Incorrect credentials');
        }
        elseif($data['username']=='' || $data['password']==''){
            return redirect('/')->with('error', 'Username or Password should not empty');
        }
        else{
            session()->flush();
            $getUserDetails=$this->login->validatelogin($data);

            if(sizeof($getUserDetails)>0) {
                $sectionPermissions=$this->login->sectionPermissions($getUserDetails[0]->role_id); 
                Session::put('user_id', $getUserDetails[0]->id);
                Session::put('username', $getUserDetails[0]->username);
                Session::put('roleid', $getUserDetails[0]->role_id);
                Session::put('email', $getUserDetails[0]->email); 
                Session::put('sectionPermissions',array($sectionPermissions));
                $user_id = $getUserDetails[0]->id;
                $roleid = $getUserDetails[0]->role_id;
                $main_menu = array();
                
                // foreach($getUserDetails as $val){
                //     if($val->parent_page_id==''){
                //         //$main_menu[$val->master_page_id][$val->master_page_name] = $val->master_page_name;
                //         $main_menu[$val->master_page_id][] = $val->master_page_name;
                //         $main_menu[$val->master_page_id][] = $val->master_page_route;
                //     }
                // }

                // foreach($getUserDetails as $val){
                //     if($val->parent_page_id!=''){
                //         //$main_menu[$val->parent_page_id][$val->master_page_id]=$val->master_page_name;
                //         $main_menu[$val->parent_page_id][]=$val->master_page_name;
                //         $main_menu[$val->parent_page_id][]=$val->master_page_route;
                //     }
                // }
                
                $mainPages=[];
                $childPages=[];
                $mainMenuArray=[];
                $subMenuArray=[];
                foreach($getUserDetails as $rolePermissions){
                    if($rolePermissions->parent_page_id=='' || $rolePermissions->parent_page_id==null || $rolePermissions->parent_page_id==0)
                    {
                        $mainPages['name']=$rolePermissions->master_page_name;
                        $mainPages['route']=$rolePermissions->master_page_route;
                        $mainPages['parent_page_id']=$rolePermissions->parent_page_id;
                        $mainPages['id']=$rolePermissions->master_page_id;
                        $mainPages['page_icon']=$rolePermissions->page_icon;
                        $mainPages['is_view']=$rolePermissions->is_view;
                        $mainPages['is_add']=$rolePermissions->is_add;
                        $mainPages['is_edit']=$rolePermissions->is_edit;
                        $mainPages['is_download']=$rolePermissions->is_download;
                        array_push($mainMenuArray, $mainPages);
                    }else{
                        $childPages['id']=$rolePermissions->master_page_id;
                        $childPages['name']=$rolePermissions->master_page_name;
                        $childPages['route']=$rolePermissions->master_page_route;
                        $childPages['parent_page_id']=$rolePermissions->parent_page_id;
                        $childPages['page_icon']=$rolePermissions->page_icon;
                        $childPages['is_view']=$rolePermissions->is_view;
                        $childPages['is_add']=$rolePermissions->is_add;
                        $childPages['is_edit']=$rolePermissions->is_edit;
                        $childPages['is_download']=$rolePermissions->is_download;
                        array_push($subMenuArray, $childPages);
                    }

                    // if($val->parent_page_id==''){
                    //     //$main_menu[$val->master_page_id][$val->master_page_name] = $val->master_page_name;
                    //     $main_menu[$val->master_page_id][] = $val->master_page_name;
                    //     $main_menu[$val->master_page_id][] = $val->master_page_route;
                    // }
                }
                Session::put('mainMenuArray',$mainMenuArray);
                Session::put('subMenuArray',$subMenuArray);

                return redirect('dashboard');
            }
            else{
                return redirect('/')->with('error', 'Incorrect credentials');
            }
        }
    }

    public function logout(){
        session()->flush();
        return redirect('/');
    }

    public function getProfileData(Request $request){
        $data = $request->all();
        
        $getProfileData=$this->login->getProfileData($data);
        
        return view('profile.index')->with('getProfileData',$getProfileData);
    }

    public function update_password(Request $request){
        $data = $request->all();
        
        $update_password=$this->login->update_password($data);
        
        return $update_password;
    }
}
