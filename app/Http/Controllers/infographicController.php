<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\infographic;
use App\master;
use Carbon\Carbon;
use Redirect;
use Session;
use DB;
use Config;

class infographicController extends Controller
{
    public function __construct(){
        $this->infographic = new infographic();
        $this->master = new master();
    }

    public function getInfographicData(Request $request){
        if (!session('username')) {
	    	return redirect('/');
        } 
        $search_key = '';
        if(isset($_GET['search_key'])){
            $search_key=$_GET['search_key'];
        }
        $getInfographicData = $this->infographic->getInfographicData($search_key);
        $getInfographicData = $getInfographicData->appends(['search_key'=>$search_key]);

        return view('infographic.list')->with('getInfographicData',$getInfographicData)->with('search_key',$search_key);
    }

    public function addNewInfographic(){
        if (!session('username')) {
	    	return redirect('/');
        }

        $getSubCategories = $this->master->getSubCategories();
        $getCategories = $this->master->getCategories();

        return view('infographic.add')->with('getCategories',$getCategories)->with('getSubCategories',$getSubCategories);
    }

     public function save_infographic(Request $request){
        if (!session('username')) {
	    	return redirect('/');
        } 
        
        $data = $request->all();
        $img_name='';
        $infograhic_thumb_image='';
        if ($request->hasFile('fileToUpload')) 
        {
            $image    = $request->file('fileToUpload');
            $imgname  = $image->getClientOriginalName();
            $img_name = $imgname;
            $destinationPath = public_path('/assets/images/project');
            $image->move($destinationPath, $imgname);
        }
        else
        {
            $img_name=$request->input('hiddenfileToUpload');
        }

        if ($request->hasFile('infograhic_thumb_image')) 
        {
            $image    = $request->file('infograhic_thumb_image');
            $imgname  = $image->getClientOriginalName();
            $infograhic_thumb_image = $imgname;
            $destinationPath = public_path('/assets/images/project');
            $image->move($destinationPath, $imgname);
        }
        else
        {
            $infograhic_thumb_image=$request->input('hidden_infograhic_thumb_image');
        }

        $save_infographic = $this->infographic->save_infographic($data,$img_name,$infograhic_thumb_image);
        // return redirect('/infographics')->with('message','Infographic Saved Successfully');
        if($save_infographic == 1){
            return redirect('/infographics')->with('message','Infographic Saved Successfully');
        }
        else{
            return redirect('/infographics')->with('error','Something Went Wrong..!!, Please try again.');
        }
    }

    public function edit_infographic($id){
        if (!session('username')) {
	    	return redirect('/');
        }

        $getSubCategories = $this->master->getSubCategories();
        $getCategories = $this->master->getCategories();
        $edit_infographic = $this->infographic->getInfographicDataById($id);

        return view('infographic.edit')->with('getSubCategories',$getSubCategories)->with('getCategories',$getCategories)->with('edit_infographic',$edit_infographic);
    }

    public function update_infographic(Request $request){
        if (!session('username')) {
	    	return redirect('/');
        } 
        
        $data = $request->all();
        $img_name='';
        $infograhic_thumb_image='';
        if ($request->hasFile('fileToUpload')) 
        {
            $image    = $request->file('fileToUpload');
            $imgname  = $image->getClientOriginalName();
            $img_name = $imgname;
            $destinationPath = public_path('/assets/images/project');
            $image->move($destinationPath, $imgname);
        }
        else
        {
            $img_name=$request->input('hiddenfileToUpload');
        }

        if ($request->hasFile('infograhic_thumb_image')) 
        {
            $image    = $request->file('infograhic_thumb_image');
            $imgname  = $image->getClientOriginalName();
            $infograhic_thumb_image = $imgname;
            $destinationPath = public_path('/assets/images/project');
            $image->move($destinationPath, $imgname);
        }
        else
        {
            $infograhic_thumb_image=$request->input('hidden_infograhic_thumb_image');
        }
        
        $update_infographic = $this->infographic->update_infographic($data,$img_name,$infograhic_thumb_image);
        // return redirect('/infographics')->with('message','Infographic Updated Successfully');
        if($update_infographic == 1){
            return redirect('/infographics')->with('message','Infographic Updated Successfully');
        }
        else{
            return redirect('/infographics')->with('error','Something Went Wrong..!!, Please try again.');
        }
    }

    public function update_infographic_status(Request $request){
        if (!session('username')) {
	    	return redirect('/');
        } 
        
        $data = $request->all();
        $update_infographic_status = $this->infographic->update_infographic_status($data);
        return $update_infographic_status;
    }
}
