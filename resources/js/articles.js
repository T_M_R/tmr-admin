$('.save_article').on('click', function () {
    var article_name = $('.article_name').val();
    var article_url = $('.article_url').val();
    var article_heading = $('.article_heading').val();
    var article_category_id = $('.article_category_id option:selected').val();
    var article_short_desc = CKEDITOR.instances['article_short_desc'].getData();
    var article_first_desc = CKEDITOR.instances['article_first_desc'].getData();
    var article_long_desc = CKEDITOR.instances['article_long_desc'].getData();
    var article_publish_date = $('.article_publish_date').val();
    var meta_title = $('.meta_title').val();
    var meta_keywords = $('.meta_keywords').val();
    var meta_desc = $('.meta_desc').val();
    var article_status = $('.article_status option:selected').val();

    if (article_name != '' && article_name != null && article_name != undefined &&
        article_heading != '' && article_heading != null && article_heading != undefined &&
        article_url != '' && article_url != null && article_url != undefined &&
        article_short_desc != '' && article_short_desc != null && article_short_desc != undefined &&
        article_long_desc != '' && article_long_desc != null && article_long_desc != undefined &&
        article_category_id != '' && article_category_id != null && article_category_id != undefined &&
        // article_publish_date != '' && article_publish_date != null && article_publish_date != undefined &&
        article_status != '' && article_status != null && article_status != undefined) {
        $.ajax({
            url: '/save_article',
            type: "POST",
            data: {
                article_name: article_name,
                article_heading: article_heading,
                article_url: article_url,
                article_category_id: article_category_id,
                article_short_desc: article_short_desc,
                article_first_desc: article_first_desc,
                article_long_desc: article_long_desc,
                article_publish_date: article_publish_date,
                article_status: article_status,
                meta_title: meta_title,
                meta_keywords: meta_keywords,
                meta_desc: meta_desc
            },
            success: function (result) {
                console.log(result);
                // return false
                if (result == 1) {
                    $('.article_success_msg').css({ 'display': 'block' });
                    $('.article_success_msg').text('Article Saved successfully.');
                    setTimeout(() => {
                        $('.article_success_msg').css({ 'display': 'none' });
                        window.location.href = document.location.origin + '/articles';
                    }, 1500);
                }
                else {
                    $('.article_error_msg').css({ 'display': 'block' });
                    $('.article_error_msg').text('Something Went Wrong.Pls try again later..!!');
                    setTimeout(() => {
                        $('.article_error_msg').css({ 'display': 'none' });
                    }, 3000);
                    return false;
                }
            }
        });
    }
    else {
        $('.article_error_msg').css({ 'display': 'block' });
        $('.article_error_msg').text('All Fields Are Required.');
        setTimeout(() => {
            $('.article_error_msg').css({ 'display': 'none' });
            // location.reload();
        }, 3000);
        return false;
    }

})

$('.update_article').on('click', function () {
    var article_id = $('.article_id').val();
    var article_name = $('.article_name').val();
    var article_heading = $('.article_heading').val();
    var article_url = $('.article_url').val();
    var article_category_id = $('.article_category_id option:selected').val();
    var article_short_desc = CKEDITOR.instances['article_short_desc'].getData();
    var article_first_desc = CKEDITOR.instances['article_first_desc'].getData();
    var article_long_desc = CKEDITOR.instances['article_long_desc'].getData();
    var article_publish_date = $('.article_publish_date').val();
    var meta_title = $('.meta_title').val();
    var meta_keywords = $('.meta_keywords').val();
    var meta_desc = $('.meta_desc').val();
    var article_status = $('.article_status option:selected').val();

    if (article_id != '' && article_id != null && article_id != undefined &&
        article_heading != '' && article_heading != null && article_heading != undefined &&
        article_name != '' && article_name != null && article_name != undefined &&
        article_url != '' && article_url != null && article_url != undefined &&
        article_short_desc != '' && article_short_desc != null && article_short_desc != undefined &&
        article_long_desc != '' && article_long_desc != null && article_long_desc != undefined &&
        article_category_id != '' && article_category_id != null && article_category_id != undefined &&
        // article_publish_date != '' && article_publish_date != null && article_publish_date != undefined &&
        article_status != '' && article_status != null && article_status != undefined) {
        $.ajax({
            url: '/update_article',
            type: "POST",
            data: {
                article_id: article_id,
                article_heading: article_heading,
                article_name: article_name,
                article_url: article_url,
                article_category_id: article_category_id,
                article_short_desc: article_short_desc,
                article_first_desc: article_first_desc,
                article_long_desc: article_long_desc,
                article_publish_date: article_publish_date,
                article_status: article_status,
                meta_title: meta_title,
                meta_keywords: meta_keywords,
                meta_desc: meta_desc
            },
            success: function (result) {
                console.log(result);
                // return false
                if (result == 1) {
                    $('.article_success_msg').css({ 'display': 'block' });
                    $('.article_success_msg').text('Article Updated successfully.');
                    setTimeout(() => {
                        $('.article_success_msg').css({ 'display': 'none' });
                        window.location.href = document.location.origin + '/articles';
                    }, 1500);
                }
                else {
                    $('.article_error_msg').css({ 'display': 'block' });
                    $('.article_error_msg').text('Something Went Wrong.Pls try again later..!!');
                    setTimeout(() => {
                        $('.article_error_msg').css({ 'display': 'none' });
                    }, 3000);
                    return false;
                }
            }
        });
    }
    else {
        $('.article_error_msg').css({ 'display': 'block' });
        $('.article_error_msg').text('All Fields Are Required.');
        setTimeout(() => {
            $('.article_error_msg').css({ 'display': 'none' });
            // location.reload();
        }, 3000);
        return false;
    }

})

$('.article_toggle_event').change(function (e) {
    // $('.console-event').html('Toggle: ' $(this).prop('checked'))
    var status = $(this).prop('checked');
    console.log(status)
    if (status == true) {
        var hidden_status = $(this).parent().parent().find('.hidden_status').val(1);
    }
    else {
        var hidden_status = $(this).parent().parent().find('.hidden_status').val(0);
    }

    var article_id = $(this).parent().parent().find('.article_id').val();
    var hidden_status = $(this).parent().parent().find('.hidden_status').val();
    console.log(article_id);
    console.log(hidden_status);
    if (article_id != '' && article_id != undefined && article_id != null) {
        $.ajax({
            url: '/update_article_status',
            type: "POST",
            data: {
                article_id: article_id,
                hidden_status: hidden_status,
            },
            success: function (result) {
                console.log(result);
                // return false
                if (result == 1) {
                    showSuccessMessage();
                }
                else {
                    showCancelMessage();
                }
            }
        });
    } else {
        $('.status_change_msg').css({ 'display': 'block' });
        $('.status_change_msg').text('Record id not found');
        setTimeout(() => {
            $('.status_change_msg').css({ 'display': 'none' });
            // location.reload();
        }, 3000);
        return false;
    }
})

function showSuccessMessage() {
    swal("Status Updated Successfully!", "", "success");
}

function showCancelMessage() {
    swal("Oops", "Something went wrong!", "error")
}