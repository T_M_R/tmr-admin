@extends('layouts.header')
@section('body-content')
<section class="regions-list">
    <div class="block-header">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 my-auto">
                <h4 class="page-title m-0">Edit Category</h4>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <a href="{{URL:: to('/categories')}}">
                    <button type="button" class="btn btn-primary float-right">Back</button>
                </a>
            </div>
        </div>
    </div>
    <div class="row clearfix mt-3">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="card project_widget">
                <div class="body mt-4 mb-4">
                    @foreach($edit_category as $value)
                        <div class="row">
                            <div class="col-12 col-md-6 mb-0">
                                <div class="input-group">  
                                    <input type="text" class="category_name" autocomplete="off" value="{{$value->cat_name}}" required/>
                                    <label>Category Name <span class="text-danger">*</span></label>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 mb-0">
                                <div class="input-group">  
                                    <input type="text" class="category_path" autocomplete="off" value="{{$value->cat_path}}" required/>
                                    <label>Category Path <span class="text-danger">*</span></label>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 mb-0">
                                <div class="input-group">  
                                    <input type="text" class="category_heading" autocomplete="off" value="{{$value->cat_page_heading}}" required/>
                                    <label>Category Page Heading<span class="text-danger">*</span></label>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 mb-0">
                                <div class="input-group">  
                                    <input type="text" class="seo_pagename" autocomplete="off" value="{{$value->seo_pagename}}" required/>
                                    <label>SEO Page Name<span class="text-danger">*</span></label>
                                </div>
                            </div>
                            <!-- <div class="col-12 col-md-6 mb-0">
                                <div class="input-group">  
                                    <input type="file" class="thumb_img" autocomplete="off" value="{{$value->cat_name}}" required/>
                                </div>
                            </div> -->
                            <div class="col-12 col-md-6 mb-0">
                                <div class="input-group">  
                                    <input type="text" class="thumb_alt_text" autocomplete="off" value="{{$value->thumb_alt_text}}" required/>
                                    <label>Thumb Alt Text<span class="text-danger">*</span></label>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 mb-0">
                                <div class="input-group">  
                                    <input type="text" class="thumb_title_text" autocomplete="off" value="{{$value->thumb_title_text}}" required/>
                                    <label>Thumb Title Text<span class="text-danger">*</span></label>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 mb-0">
                                <div class="input-group">
                                    <select class="floating-select thumb_no_follow" onclick="this.setAttribute('value', this.value);" required>
                                        <option value=""></option>
                                        @if($value->thumb_no_follow == 'Y')
                                            <option value="Y" selected>Yes</option>
                                            <option value="N">No</option>
                                        @elseif($value->thumb_no_follow == 'N')
                                            <option value="Y">Yes</option>
                                            <option value="N" selected>No</option>
                                        @else
                                            <option value="Y">Yes</option>
                                            <option value="N">No</option>
                                        @endif
                                    </select>
                                    <label>Thumb No Flow<span class="text-danger">*</span></label>
                                </div>
                            </div>
                            <!-- <div class="col-12 col-md-6 mb-0">
                                <div class="input-group">  
                                    <input type="file" class="med_img" autocomplete="off" value="{{$value->cat_name}}" required/>
                                </div>
                            </div> -->
                            <div class="col-12 col-md-6 mb-0">
                                <div class="input-group">  
                                    <input type="text" class="med_alt_text" autocomplete="off" value="{{$value->med_alt_text}}" required/>
                                    <label>Media Alt Text<span class="text-danger">*</span></label>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 mb-0">
                                <div class="input-group">  
                                    <input type="text" class="med_title_text" autocomplete="off" value="{{$value->med_title_text}}" required/>
                                    <label>Media Title Text<span class="text-danger">*</span></label>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 mb-0">
                                <div class="input-group">
                                    <select class="floating-select med_no_follow" onclick="this.setAttribute('value', this.value);"  required>
                                        <option value=""></option>
                                        @if($value->med_no_follow == 'Y')
                                            <option value="Y" selected>Yes</option>
                                            <option value="N">No</option>
                                        @elseif($value->med_no_follow == 'N')
                                            <option value="Y">Yes</option>
                                            <option value="N" selected>No</option>
                                        @else
                                            <option value="Y">Yes</option>
                                            <option value="N">No</option>
                                        @endif
                                    </select>
                                    <label>Thumb No Flow<span class="text-danger">*</span></label>
                                </div>
                            </div>
                            <div class="col-12 col-md-12 mb-0">
                                <label class="" style="color:#414244;"><b>Top  Description</b><span class="text-danger">*</span></label>
                                <div class="input-group mt-5">
                                    <textarea class="cat_top_descr ckeditor" name="cat_top_descr" id="cat_top_descr" >{{$value->cat_top_descr}}  </textarea>
                                </div>
                            </div>
                            <div class="col-12 col-md-12 mb-0">
                                <label class="" style="color:#414244;"><b>Bottom Description</b><span class="text-danger">*</span></label>
                                <div class="input-group mt-5">  
                                    <textarea class="cat_bottom_descr ckeditor" id="cat_bottom_descr" name="cat_bottom_descr">{{$value->cat_bottom_descr}}  </textarea>
                                </div>
                            </div>
                            <div class="col-12 col-md-12 mb-0">
                                <label class="" style="color:#414244;"><b>Home Description</b><span class="text-danger">*</span></label>
                                <div class="input-group mt-5">  
                                    <textarea class="cat_home_descr ckeditor" id="cat_home_descr" name="cat_home_descr"> {{$value->cat_home_descr}} </textarea>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 mb-0">
                                <div class="input-group">  
                                    <input type="text" class="meta_title" autocomplete="off" value="{{$value->meta_title}}" required/>
                                    <label>Meta Title <span class="text-danger">*</span></label>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 mb-0">
                                <div class="input-group">  
                                    <input type="text" class="meta_keywords" autocomplete="off" value="{{$value->meta_keyword}}" required/>
                                    <label>Meta Keywords<span class="text-danger">*</span></label>
                                </div>
                            </div>
                            <div class="col-12 col-md-12 mb-0">
                                <label class="" style="color:#414244;"><b>Meta Description</b><span class="text-danger">*</span></label>
                                <div class="form-group mt-5"> 
                                    <div class="form-line">  
                                        <textarea class="form-control meta_desc p-3" name="meta_desc" rows="4" placeholder="Meta Description"> {{$value->meta_descr}} </textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 mb-0">
                                <div class="input-group">  
                                    <input type="text" class="seo_header" autocomplete="off" value="{{$value->seo_header}}" required/>
                                    <label>SEO Header<span class="text-danger">*</span></label>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 mb-0">
                                <div class="input-group">  
                                    <input type="text" class="seo_footer" autocomplete="off" value="{{$value->seo_footer}}" required/>
                                    <label>SEO Footer<span class="text-danger">*</span></label>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 mb-0">
                                <div class="input-group">
                                    <select class="floating-select is_homepage" name="is_homepage" onclick="this.setAttribute('value', this.value);">
                                        <option value=""></option>
                                        @if($value->is_homepage == 'Y')
                                            <option value="Y" selected>Active</option>
                                            <option value="N">In-Active</option>
                                        @elseif($value->is_homepage == 'N')
                                            <option value="Y">Active</option>
                                            <option value="N" selected>In-Active</option>
                                        @else
                                            <option value="Y">Active</option>
                                            <option value="N">In-Active</option>
                                        @endif
                                    </select>
                                    <label>Is Homepage</label>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 mb-0">
                                <div class="input-group">  
                                    <input type="text" class="link_title" name="link_title" autocomplete="off" value="{{$value->link_title}}"/>
                                    <label>Link Title</label>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 mb-0">
                                <div class="input-group">  
                                    <input type="text" class="qweb_logo_code" name="qweb_logo_code" autocomplete="off" value="{{$value->qweb_logo_code}}"/>
                                    <label>Qweb Logo Code</label>
                                </div>
                            </div>
                            <input type="hidden" class="category_id" value="{{$value->category_id}}">
                            <div class="col-12 col-md-12 mb-0">
                                <p class="text-danger mb-0">* All fields are Mandatory</p>
                            </div>
                        </div>
                    @endforeach
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb-0 text-center">
                            <div class="form-group">
                                <div class="form-line">
                                    <div class="cat_error_msg col-red w-100" style="display:none"></div>
                                    <div class="cat_success_msg col-green w-100" style="display:none"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 text-center">
                            <button type="submit" class="btn btn-info waves-effect update_category">UPDATE</button>                                                                
                        </div>
                    </div>    
                </div>
            </div>
        </div>
    </div>
</section>
@endsection