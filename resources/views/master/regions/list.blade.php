@extends('layouts.header')
@section('body-content')
<section class="regions-list">
    <div class="block-header">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 my-auto">
                <h4 class="page-title m-0">Regions</h4>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5 my-auto">
                <div class="input-group m-0">
                    <form method="GET" action="{{url('/regions')}}" enctype="multipart/form-data" role="form" autocomplete="off">
                        <input type="search" id="search_key" name="search_key" class="search-field m-0" value="{{$search_key}}" placeholder="Search">
                        <div class="input-group-append">
                            <button type="submit" class="btn btn-primary"><i class="fa fa-search mr-1"></i> Search</button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-1 col-lg-1 pl-0">
                <form method="GET" action="{{url('/regions')}}" enctype="multipart/form-data" role="form" autocomplete="off">
                    <button type="submit" class="btn btn-clear">Clear</button>
                </form>
            </div>
            <?php 
                $sessionUser=Session::get('subMenuArray');  
                $val=[];
                $allRoutes=[];
                $addButtonStatus=0;
                $editButtonStatus=0;
                $viewButtonStatus=0;
                foreach($sessionUser as $PagePermission)
                {
                    if(isset($PagePermission['route'])){
                        if($PagePermission['route'] == "regions")
                        {
                            $addButtonStatus=$PagePermission['is_add'];
                            $editButtonStatus=$PagePermission['is_edit'];
                            $viewButtonStatus=$PagePermission['is_view'];
                        }
                    }
                }
            ?>
            <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                @if($addButtonStatus == 1)
                    <button type="button" class="btn btn-primary float-right add_region_modal_btn">Add New</button>
                @endif
            </div>
        </div>
    </div>
    <div class="row clearfix mt-3">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <p class="center status_change_msg text-danger"></p>
            @if(isset($getRegionsList) && sizeof($getRegionsList)>0)
            <div class="card">
                <div class="body p-0">
                    <div class="table-responsive">
                        <!-- <table class="table table-bordered table-striped table-hover js-basic-example dataTable"> -->
                        <table class="table table-bordered mb-0">
                            <thead>
                                <tr>
                                    <th>S.No</th>
                                    <th>Region Name</th>
                                    <th>Status</th>
                                    @if($editButtonStatus == 1)
                                    <th>Action</th>
                                    @endif
                                </tr>
                            </thead>
                            <tbody>
                                <?php $i=1;?>
                                @foreach($getRegionsList as $value)
                                    <tr>
                                        <td>{{$i}}</td>
                                        <td>{{$value->name}}</td>
                                        <td>
                                            @if($value->status == 'Y')  
                                                <input class="checkbox region_toggle_event" type="checkbox" data-toggle="toggle" data-style="ios" checked>
                                                <input type="hidden" name="hidden_status" class="hidden_status" value="1" />
                                            @else
                                                <input class="checkbox region_toggle_event" type="checkbox" data-toggle="toggle" data-style="ios">
                                                <input type="hidden" name="hidden_status" class="hidden_status" value="0"/>
                                            @endif
                                            <input type="hidden" class="region_id" value="{{$value->id}}">
                                        @if($editButtonStatus == 1)
                                        <td>
                                            <a href="{{URL:: to('/edit_region/'.$value->id)}}" class="text-decoration-none">
                                                <button class="btn tblActnBtn">
                                                    <i class="material-icons">mode_edit</i>
                                                </button>
                                            </a>
                                        </td>
                                        @endif
                                    </tr>
                                    <?php $i++;?>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            @else
                <div class="card">
                    <p class="text-center py-5" style="font-size:25px">No Data Available To Show</p>  
                </div>
            @endif
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 paginate-sec">
            @if(sizeof($getRegionsList)>0)
                {{$getRegionsList->links()}}
            @endif
        </div>
    </div>
</section>

<div class="modal fade addRegionModal" id="addRegionModal" tabindex="-1" role="dialog" aria-labelledby="formModal" aria-hidden="true">    
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title modal-title" id="formModal">Add Region</h5>
                <button type="button" class="close modal-close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12 p-0">
                        <div class="col-12 col-md-12 mb-0">
                            <div class="input-group">  
                                <input type="text" class="region_name" autocomplete="off" value="" required/>
                                <label>Region Name <span class="text-danger">*</span></label>
                            </div>
                        </div>
                        <div class="form-group d-none">
                            <label class="col-sm-6 control-label"> Region Short Name</label>
                            <div class="col-sm-12">
                                <input class="form-control add_continent_short_name" type="text" name="add_continent_short_name" id="add_continent_short_name" placeholder="Region Short name" required />
                            </div>
                        </div>
                        <input type="hidden" class="region_id">
                        <div class="col-12 col-md-6 mb-0">
                            <p class="text-danger mb-0">* All fields are Mandatory</p>
                        </div>
                        <p class="region_success_msg mb-0 text-success text-center"></p>
                        <p class="region_error_msg mb-0 text-danger text-center"></p>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger modal-close" data-dismiss="modal">CLOSE</button>
                <button type="button" class="btn btn-primary save_region">SUBMIT</button>
            </div>
        </div>
    </div>
</div>
@endsection