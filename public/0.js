(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[0],{

/***/ "./resources/js/master.js":
/*!********************************!*\
  !*** ./resources/js/master.js ***!
  \********************************/
/*! no static exports found */
/***/ (function(module, exports) {

// for regions=====================================
$('.add_region_modal_btn').click(function () {
  $('.region_name').val(''); // $('.add_continent_short_name').val('');

  $('.region_id').val('');
  $('.add_update_flag').val(1);
  $('#addRegionModal').modal('show');
});
$('.save_region').on('click', function () {
  var region_name = $('.region_name').val(); // var region_short_name = $('.region_short_name').val();

  var add_update_flag = 1;
  var region_id = $('.region_id').val();
  console.log(region_id);
  console.log(region_name); // console.log(region_short_name);

  console.log(add_update_flag); // return false;

  if (region_name != '' && region_name != undefined && region_name != null) {
    $.ajax({
      url: '/region_duplicate_check',
      type: "GET",
      data: {
        region_name: region_name,
        region_id: region_id,
        add_update_flag: add_update_flag
      },
      success: function success(result) {
        console.log(result); // return false;

        if (result == 0) {
          $.ajax({
            url: '/save_region',
            type: "POST",
            data: {
              region_name: region_name,
              // region_short_name : region_short_name,
              add_update_flag: add_update_flag
            },
            success: function success(result) {
              console.log(result); // return false

              if (result == 1) {
                $('.region_success_msg').css({
                  'display': 'block'
                });
                $('.region_success_msg').text('Region Saved successfully.');
                setTimeout(function () {
                  $('.region_success_msg').css({
                    'display': 'none'
                  });
                  window.location.href = document.location.origin + '/regions';
                }, 3000);
              } else {
                $('.region_error_msg').css({
                  'display': 'block'
                });
                $('.region_error_msg').text('Something Went Wrong.Pls try again later..!!');
                setTimeout(function () {
                  $('.region_error_msg').css({
                    'display': 'none'
                  });
                }, 3000);
                return false;
              }
            }
          });
        } else {
          $('.region_error_msg').css({
            'display': 'block'
          });
          $('.region_error_msg').text('Region Name already exists.');
          setTimeout(function () {
            $('.region_error_msg').css({
              'display': 'none'
            });
          }, 3000);
          return false;
        }
      }
    });
  } else {
    $('.region_error_msg').css({
      'display': 'block'
    });
    $('.region_error_msg').text('All Fields Are Required.');
    setTimeout(function () {
      $('.region_error_msg').css({
        'display': 'none'
      }); // location.reload();
    }, 3000);
    return false;
  }
});
$('.edit_region_modal_btn').click(function () {
  var region_id = $(this).val();
  var region_name = $(this).parent().find('.region_name').val(); // var continent_short_name = $(this).parent().find('.continent_short_name').val();

  console.log(region_id);
  console.log($(this).parent().find('.region_name').val());
  console.log(region_name);
  $('.region_name').val('');

  if (region_id != '' && region_name != '') {
    $('.modal-title').text('Edit Region');
    $('.region_name').val(region_name);
    $('.region_id').val(region_id);
    $('.add_update_flag').val(2);
    $('.addRegionModal').modal('show');
  } else {}
});
$('.update_region').on('click', function () {
  var region_name = $('.region_name').val(); // var region_short_name = $('.region_short_name').val();

  var add_update_flag = 2;
  var region_id = $('.region_id').val();
  console.log(region_id);
  console.log(region_name); // console.log(region_short_name);

  console.log(add_update_flag); // return false;

  if (region_name != '' && region_name != undefined && region_name != null) {
    $.ajax({
      url: '/region_duplicate_check',
      type: "GET",
      data: {
        region_name: region_name,
        region_id: region_id,
        add_update_flag: add_update_flag
      },
      success: function success(result) {
        console.log(result); // return false;

        if (result == 0) {
          $.ajax({
            url: '/update_region',
            type: "POST",
            data: {
              region_name: region_name,
              // region_short_name : region_short_name,
              region_id: region_id,
              add_update_flag: add_update_flag
            },
            success: function success(result) {
              console.log(result); // return false

              if (result == 1) {
                $('.region_success_msg').css({
                  'display': 'block'
                });
                $('.region_success_msg').text('Region Updated successfully.');
                setTimeout(function () {
                  $('.region_success_msg').css({
                    'display': 'none'
                  });
                  window.location.href = document.location.origin + '/regions';
                }, 3000);
              } else {
                $('.region_error_msg').css({
                  'display': 'block'
                });
                $('.region_error_msg').text('Something Went Wrong.Pls try again later..!!');
                setTimeout(function () {
                  $('.region_error_msg').css({
                    'display': 'none'
                  });
                }, 3000);
                return false;
              }
            }
          });
        } else {
          $('.region_error_msg').css({
            'display': 'block'
          });
          $('.region_error_msg').text('Region Name already exists.');
          setTimeout(function () {
            $('.region_error_msg').css({
              'display': 'none'
            });
          }, 3000);
          return false;
        }
      }
    });
  } else {
    $('.region_error_msg').css({
      'display': 'block'
    });
    $('.region_error_msg').text('All Fields Are Required.');
    setTimeout(function () {
      $('.region_error_msg').css({
        'display': 'none'
      }); // location.reload();
    }, 3000);
    return false;
  }
});
$('.region_toggle_event').change(function (e) {
  // $('.console-event').html('Toggle: ' $(this).prop('checked'))
  var status = $(this).prop('checked');
  console.log(status);

  if (status == true) {
    var hidden_status = $(this).parent().parent().find('.hidden_status').val(1);
  } else {
    var hidden_status = $(this).parent().parent().find('.hidden_status').val(0);
  }

  var region_id = $(this).parent().parent().find('.region_id').val();
  var hidden_status = $(this).parent().parent().find('.hidden_status').val();
  console.log(region_id);
  console.log(hidden_status);

  if (region_id != '' && region_id != undefined && region_id != null) {
    $.ajax({
      url: '/update_region_status',
      type: "POST",
      data: {
        region_id: region_id,
        hidden_status: hidden_status
      },
      success: function success(result) {
        console.log(result); // return false

        if (result == 1) {
          // $('.region_success_msg').css({'display':'block'});
          // $('.region_success_msg').text('Region Updated successfully.');
          showSuccessMessage(); // setTimeout(() => {
          //     $('.region_success_msg').css({'display':'none'});
          //     window.location.href = document.location.origin+'/regions';
          // }, 1500);
        } else {
          // $('.region_error_msg').css({'display':'block'});
          // $('.region_error_msg').text('Something Went Wrong.Pls try again later..!!');
          showCancelMessage();
          setTimeout(function () {
            $('.region_error_msg').css({
              'display': 'none'
            });
          }, 3000);
          return false;
        }
      }
    });
  } else {
    $('.status_change_msg').css({
      'display': 'block'
    });
    $('.status_change_msg').text('Record id not found');
    setTimeout(function () {
      $('.status_change_msg').css({
        'display': 'none'
      }); // location.reload();
    }, 3000);
    return false;
  }
});

function showSuccessMessage() {
  swal("Status Updated Successfully!", "", "success");
}

function showCancelMessage() {
  swal("Oops", "Something went wrong!", "error");
} // For Country =============================


$('.save_country').on('click', function () {
  var country_name = $('.country_name').val();
  var country_code = $('.country_code').val();
  var region_id = $('.country_region_id option:selected').val();
  var add_update_flag = 1;
  var country_id = $('.country_id').val();
  console.log(country_id);
  console.log(country_name);
  console.log(region_id);
  console.log(add_update_flag); // return false;

  if (country_name != '' && country_name != undefined && country_name != null && region_id != '' && region_id != undefined && region_id != null) {
    $.ajax({
      url: '/country_duplicate_check',
      type: "GET",
      data: {
        country_name: country_name,
        country_id: country_id,
        add_update_flag: add_update_flag
      },
      success: function success(result) {
        console.log(result); // return false;

        if (result == 0) {
          $.ajax({
            url: '/save_country',
            type: "POST",
            data: {
              country_name: country_name,
              country_code: country_code,
              region_id: region_id,
              add_update_flag: add_update_flag
            },
            success: function success(result) {
              console.log(result); // return false

              if (result == 1) {
                $('.country_success_msg').css({
                  'display': 'block'
                });
                $('.country_success_msg').text('Country Saved successfully.');
                setTimeout(function () {
                  $('.country_success_msg').css({
                    'display': 'none'
                  });
                  window.location.href = document.location.origin + '/countries';
                }, 3000);
              } else {
                $('.country_error_msg').css({
                  'display': 'block'
                });
                $('.country_error_msg').text('Something Went Wrong.Pls try again later..!!');
                setTimeout(function () {
                  $('.country_error_msg').css({
                    'display': 'none'
                  });
                }, 3000);
                return false;
              }
            }
          });
        } else {
          $('.country_error_msg').css({
            'display': 'block'
          });
          $('.country_error_msg').text('Country Name already exists.');
          setTimeout(function () {
            $('.country_error_msg').css({
              'display': 'none'
            });
          }, 3000);
          return false;
        }
      }
    });
  } else {
    $('.country_error_msg').css({
      'display': 'block'
    });
    $('.country_error_msg').text('All Fields Are Required.');
    setTimeout(function () {
      $('.country_error_msg').css({
        'display': 'none'
      }); // location.reload();
    }, 3000);
    return false;
  }
});
$('.update_country').on('click', function () {
  var country_name = $('.country_name').val();
  var country_code = $('.country_code').val();
  var region_id = $('.country_region_id option:selected').val();
  var add_update_flag = $('.add_update_flag').val();
  var country_id = $('.country_id').val();
  console.log(country_id);
  console.log(country_name);
  console.log(region_id);
  console.log(add_update_flag); // return false;

  if (country_name != '' && country_name != undefined && country_name != null && region_id != '' && region_id != undefined && region_id != null) {
    $.ajax({
      url: '/country_duplicate_check',
      type: "GET",
      data: {
        country_name: country_name,
        country_id: country_id,
        add_update_flag: add_update_flag
      },
      success: function success(result) {
        console.log(result); // return false;

        if (result == 0) {
          $.ajax({
            url: '/update_country',
            type: "POST",
            data: {
              country_name: country_name,
              country_code: country_code,
              region_id: region_id,
              country_id: country_id,
              add_update_flag: add_update_flag
            },
            success: function success(result) {
              console.log(result); // return false

              if (result == 1) {
                $('.country_success_msg').css({
                  'display': 'block'
                });
                $('.country_success_msg').text('Country Updated Successfully.');
                setTimeout(function () {
                  $('.country_success_msg').css({
                    'display': 'none'
                  });
                  window.location.href = document.location.origin + '/countries';
                }, 3000);
              } else {
                $('.country_error_msg').css({
                  'display': 'block'
                });
                $('.country_error_msg').text('Something Went Wrong.Pls try again later..!!');
                setTimeout(function () {
                  $('.country_error_msg').css({
                    'display': 'none'
                  });
                }, 3000);
                return false;
              }
            }
          });
        } else {
          $('.country_error_msg').css({
            'display': 'block'
          });
          $('.country_error_msg').text('Country Name already exists.');
          setTimeout(function () {
            $('.country_error_msg').css({
              'display': 'none'
            });
          }, 3000);
          return false;
        }
      }
    });
  } else {
    $('.country_error_msg').css({
      'display': 'block'
    });
    $('.country_error_msg').text('All Fields Are Required.');
    setTimeout(function () {
      $('.country_error_msg').css({
        'display': 'none'
      }); // location.reload();
    }, 2000);
    return false;
  }
});
$('.country_toggle_event').change(function (e) {
  // $('.console-event').html('Toggle: ' $(this).prop('checked'))
  var status = $(this).prop('checked');
  console.log(status);

  if (status == true) {
    var hidden_status = $(this).parent().parent().find('.hidden_status').val(1);
  } else {
    var hidden_status = $(this).parent().parent().find('.hidden_status').val(0);
  }

  var country_id = $(this).parent().parent().find('.country_id').val();
  var hidden_status = $(this).parent().parent().find('.hidden_status').val();
  console.log(country_id);
  console.log(hidden_status);

  if (country_id != '' && country_id != undefined && country_id != null) {
    $.ajax({
      url: '/update_country_status',
      type: "POST",
      data: {
        country_id: country_id,
        hidden_status: hidden_status
      },
      success: function success(result) {
        console.log(result); // return false

        if (result == 1) {
          showSuccessMessage();
        } else {
          showCancelMessage();
        }
      }
    });
  } else {
    $('.status_change_msg').css({
      'display': 'block'
    });
    $('.status_change_msg').text('Record id not found');
    setTimeout(function () {
      $('.status_change_msg').css({
        'display': 'none'
      }); // location.reload();
    }, 3000);
    return false;
  }
}); // For Categories==================

$('.add_category_modal_btn').click(function () {
  $('.category_name').val('');
  $('.category_id').val('');
  $('.add_update_flag').val(1);
  $('#addCategoryModal').modal('show');
});
$('.save_category').on('click', function () {
  var category_name = $('.category_name').val();
  var add_update_flag = 1;
  var category_id = $('.category_id').val();
  console.log(category_id);
  console.log(category_name);
  console.log(add_update_flag);

  if (category_name != '' && category_name != undefined && category_name != null) {
    $.ajax({
      url: '/category_duplicate_check',
      type: "GET",
      data: {
        category_name: category_name,
        category_id: category_id,
        add_update_flag: add_update_flag
      },
      success: function success(result) {
        console.log(result);

        if (result == 0) {
          $.ajax({
            url: '/save_category',
            type: "POST",
            data: {
              category_name: category_name,
              add_update_flag: add_update_flag
            },
            success: function success(result) {
              console.log(result);

              if (result == 1) {
                $('.cat_success_msg').css({
                  'display': 'block'
                });
                $('.cat_success_msg').text('Category Saved successfully.');
                setTimeout(function () {
                  $('.cat_success_msg').css({
                    'display': 'none'
                  });
                  window.location.href = document.location.origin + '/categories';
                }, 1500);
              } else {
                $('.cat_error_msg').css({
                  'display': 'block'
                });
                $('.cat_error_msg').text('Something Went Wrong.Pls try again later..!!');
                setTimeout(function () {
                  $('.cat_error_msg').css({
                    'display': 'none'
                  });
                }, 3000);
                return false;
              }
            }
          });
        } else {
          $('.cat_error_msg').css({
            'display': 'block'
          });
          $('.cat_error_msg').text('Category Name already exists.');
          setTimeout(function () {
            $('.cat_error_msg').css({
              'display': 'none'
            });
          }, 3000);
          return false;
        }
      }
    });
  } else {
    $('.cat_error_msg').css({
      'display': 'block'
    });
    $('.cat_error_msg').text('All Fields Are Required.');
    setTimeout(function () {
      $('.cat_error_msg').css({
        'display': 'none'
      }); // location.reload();
    }, 3000);
    return false;
  }
});
$('.update_category').on('click', function () {
  var category_name = $('.category_name').val();
  var add_update_flag = 2;
  var category_id = $('.category_id').val();
  console.log(category_id);
  console.log(category_name);
  console.log(add_update_flag);

  if (category_name != '' && category_name != undefined && category_name != null) {
    $.ajax({
      url: '/category_duplicate_check',
      type: "GET",
      data: {
        category_name: category_name,
        category_id: category_id,
        add_update_flag: add_update_flag
      },
      success: function success(result) {
        console.log(result);

        if (result == 0) {
          $.ajax({
            url: '/update_category',
            type: "POST",
            data: {
              category_name: category_name,
              category_id: category_id,
              add_update_flag: add_update_flag
            },
            success: function success(result) {
              console.log(result);

              if (result == 1) {
                $('.cat_success_msg').css({
                  'display': 'block'
                });
                $('.cat_success_msg').text('Category Updated successfully.');
                setTimeout(function () {
                  $('.cat_success_msg').css({
                    'display': 'none'
                  });
                  window.location.href = document.location.origin + '/categories';
                }, 1500);
              } else {
                $('.cat_error_msg').css({
                  'display': 'block'
                });
                $('.cat_error_msg').text('Something Went Wrong.Pls try again later..!!');
                setTimeout(function () {
                  $('.cat_error_msg').css({
                    'display': 'none'
                  });
                }, 3000);
                return false;
              }
            }
          });
        } else {
          $('.cat_error_msg').css({
            'display': 'block'
          });
          $('.cat_error_msg').text('Category Name already exists.');
          setTimeout(function () {
            $('.cat_error_msg').css({
              'display': 'none'
            });
          }, 3000);
          return false;
        }
      }
    });
  } else {
    $('.cat_error_msg').css({
      'display': 'block'
    });
    $('.cat_error_msg').text('All Fields Are Required.');
    setTimeout(function () {
      $('.cat_error_msg').css({
        'display': 'none'
      }); // location.reload();
    }, 3000);
    return false;
  }
});
$('.category_toggle_event').change(function (e) {
  // $('.console-event').html('Toggle: ' $(this).prop('checked'))
  var status = $(this).prop('checked');
  console.log(status);

  if (status == true) {
    var hidden_status = $(this).parent().parent().find('.hidden_status').val(1);
  } else {
    var hidden_status = $(this).parent().parent().find('.hidden_status').val(0);
  }

  var category_id = $(this).parent().parent().find('.category_id').val();
  var hidden_status = $(this).parent().parent().find('.hidden_status').val();
  console.log(category_id);
  console.log(hidden_status);

  if (category_id != '' && category_id != undefined && category_id != null) {
    $.ajax({
      url: '/update_category_status',
      type: "POST",
      data: {
        category_id: category_id,
        hidden_status: hidden_status
      },
      success: function success(result) {
        console.log(result); // return false

        if (result == 1) {
          showSuccessMessage();
        } else {
          showCancelMessage();
        }
      }
    });
  } else {
    $('.status_change_msg').css({
      'display': 'block'
    });
    $('.status_change_msg').text('Record id not found');
    setTimeout(function () {
      $('.status_change_msg').css({
        'display': 'none'
      }); // location.reload();
    }, 3000);
    return false;
  }
}); // $('.region_toggle_event').change(function(e) {
//     // $('.console-event').html('Toggle: ' $(this).prop('checked'))
//     var status = $(this).prop('checked');
//     console.log(status)
//     if(status == true)
//     {
//         var hidden_status = $(this).parent().parent().find('.hidden_status').val(1);
//     }
//     else
//     {
//         var hidden_status = $(this).parent().parent().find('.hidden_status').val(0);
//     }
// })
// For Sub Category=======================

$('.save_sub_category').on('click', function () {
  var category_id = $('.category_id option:selected').val();
  var sub_category_name = $('.sub_category_name').val();
  var sub_category_url = $('.sub_category_url').val();
  var sub_category_heading_h2 = $('.sub_category_heading_h2').val();
  var sub_category_short_desc = CKEDITOR.instances['sub_category_short_desc'].getData();
  var sub_category_desc = CKEDITOR.instances['sub_category_desc'].getData();
  var meta_title = $('.meta_title').val();
  var meta_keywords = $('.meta_keywords').val();
  var meta_desc = $('.meta_desc').val();
  var add_update_flag = 1;
  console.log(category_id);
  console.log(sub_category_name);
  console.log(sub_category_url);
  console.log(sub_category_heading_h2);
  console.log(sub_category_short_desc);
  console.log(sub_category_desc);
  console.log(meta_title);
  console.log(meta_keywords);
  console.log(meta_desc); // return false;

  if (category_id != '' && category_id != undefined && category_id != null && sub_category_name != '' && sub_category_name != undefined && sub_category_name != null && sub_category_url != '' && sub_category_url != undefined && sub_category_url != null && sub_category_heading_h2 != '' && sub_category_heading_h2 != undefined && sub_category_heading_h2 != null && sub_category_short_desc != '' && sub_category_short_desc != undefined && sub_category_short_desc != null && sub_category_desc != '' && sub_category_desc != undefined && sub_category_desc != null && meta_title != '' && meta_title != undefined && meta_title != null && meta_keywords != '' && meta_keywords != undefined && meta_keywords != null && meta_desc != '' && meta_desc != undefined && meta_desc != null) {
    $.ajax({
      url: '/save_sub_category',
      type: "POST",
      data: {
        category_id: category_id,
        sub_category_name: sub_category_name,
        sub_category_url: sub_category_url,
        sub_category_heading_h2: sub_category_heading_h2,
        sub_category_short_desc: sub_category_short_desc,
        sub_category_desc: sub_category_desc,
        meta_title: meta_title,
        meta_keywords: meta_keywords,
        meta_desc: meta_desc
      },
      success: function success(result) {
        console.log(result);

        if (result == 1) {
          $('.cat_success_msg').css({
            'display': 'block'
          });
          $('.cat_success_msg').text('SubCategory Saved successfully.');
          setTimeout(function () {
            $('.cat_success_msg').css({
              'display': 'none'
            });
            window.location.href = document.location.origin + '/sub-categories';
          }, 1500);
        } else {
          $('.cat_error_msg').css({
            'display': 'block'
          });
          $('.cat_error_msg').text('Something Went Wrong.Pls try again later..!!');
          setTimeout(function () {
            $('.cat_error_msg').css({
              'display': 'none'
            });
          }, 3000);
          return false;
        }
      }
    });
  } else {
    $('.cat_error_msg').css({
      'display': 'block'
    });
    $('.cat_error_msg').text('All Fields Are Required.');
    setTimeout(function () {
      $('.cat_error_msg').css({
        'display': 'none'
      }); // location.reload();
    }, 3000);
    return false;
  }
});
$('.update_sub_category').on('click', function () {
  var sub_category_id = $('.sub_category_id').val();
  var category_id = $('.category_id option:selected').val();
  var sub_category_name = $('.sub_category_name').val();
  var sub_category_url = $('.sub_category_url').val();
  var sub_category_heading_h2 = $('.sub_category_heading_h2').val();
  var sub_category_short_desc = CKEDITOR.instances['sub_category_short_desc'].getData();
  var sub_category_desc = CKEDITOR.instances['sub_category_desc'].getData();
  var meta_title = $('.meta_title').val();
  var meta_keywords = $('.meta_keywords').val();
  var meta_desc = $('.meta_desc').val();
  var add_update_flag = 2;
  console.log(sub_category_id);
  console.log(category_id);
  console.log(sub_category_name);
  console.log(sub_category_url);
  console.log(sub_category_heading_h2);
  console.log(sub_category_short_desc);
  console.log(sub_category_desc);
  console.log(meta_title);
  console.log(meta_keywords);
  console.log(meta_desc); // return false;

  if (sub_category_id != '' && sub_category_id != undefined && sub_category_id != null && category_id != '' && category_id != undefined && category_id != null && sub_category_name != '' && sub_category_name != undefined && sub_category_name != null && sub_category_url != '' && sub_category_url != undefined && sub_category_url != null && sub_category_heading_h2 != '' && sub_category_heading_h2 != undefined && sub_category_heading_h2 != null && sub_category_short_desc != '' && sub_category_short_desc != undefined && sub_category_short_desc != null && sub_category_desc != '' && sub_category_desc != undefined && sub_category_desc != null && meta_title != '' && meta_title != undefined && meta_title != null && meta_keywords != '' && meta_keywords != undefined && meta_keywords != null && meta_desc != '' && meta_desc != undefined && meta_desc != null) {
    $.ajax({
      url: '/update_sub_category',
      type: "POST",
      data: {
        sub_category_id: sub_category_id,
        category_id: category_id,
        sub_category_name: sub_category_name,
        sub_category_url: sub_category_url,
        sub_category_heading_h2: sub_category_heading_h2,
        sub_category_short_desc: sub_category_short_desc,
        sub_category_desc: sub_category_desc,
        meta_title: meta_title,
        meta_keywords: meta_keywords,
        meta_desc: meta_desc
      },
      success: function success(result) {
        console.log(result);

        if (result == 1) {
          $('.cat_success_msg').css({
            'display': 'block'
          });
          $('.cat_success_msg').text('SubCategory Updated successfully.');
          setTimeout(function () {
            $('.cat_success_msg').css({
              'display': 'none'
            });
            window.location.href = document.location.origin + '/sub-categories';
          }, 1500);
        } else {
          $('.cat_error_msg').css({
            'display': 'block'
          });
          $('.cat_error_msg').text('Something Went Wrong.Pls try again later..!!');
          setTimeout(function () {
            $('.cat_error_msg').css({
              'display': 'none'
            });
          }, 3000);
          return false;
        }
      }
    });
  } else {
    $('.cat_error_msg').css({
      'display': 'block'
    });
    $('.cat_error_msg').text('All Fields Are Required.');
    setTimeout(function () {
      $('.cat_error_msg').css({
        'display': 'none'
      }); // location.reload();
    }, 3000);
    return false;
  }
});
$('.sub_category_toggle_event').change(function (e) {
  // $('.console-event').html('Toggle: ' $(this).prop('checked'))
  var status = $(this).prop('checked');
  console.log(status);

  if (status == true) {
    var hidden_status = $(this).parent().parent().find('.hidden_status').val(1);
  } else {
    var hidden_status = $(this).parent().parent().find('.hidden_status').val(0);
  }

  var sub_category_id = $(this).parent().parent().find('.sub_category_id').val();
  var hidden_status = $(this).parent().parent().find('.hidden_status').val();
  console.log(sub_category_id);
  console.log(hidden_status);

  if (sub_category_id != '' && sub_category_id != undefined && sub_category_id != null) {
    $.ajax({
      url: '/update_sub_category_status',
      type: "POST",
      data: {
        sub_category_id: sub_category_id,
        hidden_status: hidden_status
      },
      success: function success(result) {
        console.log(result); // return false

        if (result == 1) {
          showSuccessMessage();
        } else {
          showCancelMessage();
        }
      }
    });
  } else {
    $('.status_change_msg').css({
      'display': 'block'
    });
    $('.status_change_msg').text('Record id not found');
    setTimeout(function () {
      $('.status_change_msg').css({
        'display': 'none'
      }); // location.reload();
    }, 3000);
    return false;
  }
}); // Companies==============================================
// For Sub Category=======================

$('.save_company').on('click', function () {
  var company_name = $('.company_name').val();
  var company_desc = CKEDITOR.instances['company_desc'].getData();
  var meta_title = $('.meta_title').val();
  var meta_keywords = $('.meta_keywords').val();
  var meta_desc = $('.meta_desc').val();
  var add_update_flag = 1;
  console.log(company_name);
  console.log(company_desc);
  console.log(meta_title);
  console.log(meta_keywords);
  console.log(meta_desc); // return false;

  if (company_name != '' && company_name != undefined && company_name != null && company_desc != '' && company_desc != undefined && company_desc != null && meta_title != '' && meta_title != undefined && meta_title != null && meta_keywords != '' && meta_keywords != undefined && meta_keywords != null && meta_desc != '' && meta_desc != undefined && meta_desc != null) {
    $.ajax({
      url: '/save_company',
      type: "POST",
      data: {
        company_name: company_name,
        company_desc: company_desc,
        meta_title: meta_title,
        meta_keywords: meta_keywords,
        meta_desc: meta_desc
      },
      success: function success(result) {
        console.log(result);

        if (result == 1) {
          $('.company_success_msg').css({
            'display': 'block'
          });
          $('.company_success_msg').text('Company Saved successfully.');
          setTimeout(function () {
            $('.company_success_msg').css({
              'display': 'none'
            });
            window.location.href = document.location.origin + '/companies';
          }, 1500);
        } else {
          $('.company_error_msg').css({
            'display': 'block'
          });
          $('.company_error_msg').text('Something Went Wrong.Pls try again later..!!');
          setTimeout(function () {
            $('.company_error_msg').css({
              'display': 'none'
            });
          }, 3000);
          return false;
        }
      }
    });
  } else {
    $('.company_error_msg').css({
      'display': 'block'
    });
    $('.company_error_msg').text('All Fields Are Required.');
    setTimeout(function () {
      $('.company_error_msg').css({
        'display': 'none'
      }); // location.reload();
    }, 3000);
    return false;
  }
});
$('.update_company').on('click', function () {
  var company_id = $('.compnay_id').val();
  var company_name = $('.company_name').val();
  var company_desc = CKEDITOR.instances['company_desc'].getData();
  var meta_title = $('.meta_title').val();
  var meta_keywords = $('.meta_keywords').val();
  var meta_desc = $('.meta_desc').val();
  var add_update_flag = 1;
  console.log(company_id);
  console.log(company_name);
  console.log(company_desc);
  console.log(meta_title);
  console.log(meta_keywords);
  console.log(meta_desc); // return false;

  if (company_id != '' && company_id != undefined && company_id != null && company_name != '' && company_name != undefined && company_name != null && company_desc != '' && company_desc != undefined && company_desc != null && meta_title != '' && meta_title != undefined && meta_title != null && meta_keywords != '' && meta_keywords != undefined && meta_keywords != null && meta_desc != '' && meta_desc != undefined && meta_desc != null) {
    $.ajax({
      url: '/update_company',
      type: "POST",
      data: {
        company_id: company_id,
        company_name: company_name,
        company_desc: company_desc,
        meta_title: meta_title,
        meta_keywords: meta_keywords,
        meta_desc: meta_desc
      },
      success: function success(result) {
        console.log(result);

        if (result == 1) {
          $('.company_success_msg').css({
            'display': 'block'
          });
          $('.company_success_msg').text('Company Updated successfully.');
          setTimeout(function () {
            $('.company_success_msg').css({
              'display': 'none'
            });
            window.location.href = document.location.origin + '/companies';
          }, 1500);
        } else {
          $('.company_error_msg').css({
            'display': 'block'
          });
          $('.company_error_msg').text('Something Went Wrong.Pls try again later..!!');
          setTimeout(function () {
            $('.company_error_msg').css({
              'display': 'none'
            });
          }, 3000);
          return false;
        }
      }
    });
  } else {
    $('.company_error_msg').css({
      'display': 'block'
    });
    $('.company_error_msg').text('All Fields Are Required.');
    setTimeout(function () {
      $('.company_error_msg').css({
        'display': 'none'
      }); // location.reload();
    }, 3000);
    return false;
  }
});
$('.company_toggle_event').change(function (e) {
  // $('.console-event').html('Toggle: ' $(this).prop('checked'))
  var status = $(this).prop('checked');
  console.log(status);

  if (status == true) {
    var hidden_status = $(this).parent().parent().find('.hidden_status').val(1);
  } else {
    var hidden_status = $(this).parent().parent().find('.hidden_status').val(0);
  }

  var company_id = $(this).parent().parent().find('.company_id').val();
  var hidden_status = $(this).parent().parent().find('.hidden_status').val();
  console.log(company_id);
  console.log(hidden_status);

  if (company_id != '' && company_id != undefined && company_id != null) {
    $.ajax({
      url: '/update_company_status',
      type: "POST",
      data: {
        company_id: company_id,
        hidden_status: hidden_status
      },
      success: function success(result) {
        console.log(result); // return false

        if (result == 1) {
          showSuccessMessage();
        } else {
          showCancelMessage();
        }
      }
    });
  } else {
    $('.status_change_msg').css({
      'display': 'block'
    });
    $('.status_change_msg').text('Record id not found');
    setTimeout(function () {
      $('.status_change_msg').css({
        'display': 'none'
      }); // location.reload();
    }, 3000);
    return false;
  }
});

/***/ })

}]);