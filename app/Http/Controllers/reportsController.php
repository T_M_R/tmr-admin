<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\master;
use App\reports;
use Carbon\Carbon;
use Redirect;
use Session;
use DB;
use Config;

class reportsController extends Controller
{
    public function __construct(){
        $this->master = new master();
        $this->reports = new reports();
    }

    public function getReports(Request $request){
        if (!session('username')) {
	    	return redirect('/');
        } 
        $search_key = '';
        if(isset($_GET['search_key'])){
            $search_key=$_GET['search_key'];
        }
        $getReports = $this->reports->getReports($search_key);
        $getReports = $getReports->appends(['search_key'=>$search_key]);

        $getReports_For_Download = $this->reports->getReports_For_Download($search_key);
        Session::put('getReports',$getReports_For_Download);
        return view('reports.list')->with('getReports',$getReports)->with('search_key',$search_key);
    } 
    
    public function addNewReport(Request $request){
        if (!session('username')) {
	    	return redirect('/');
        } 

        $getCategories = $this->master->getCategories();
        $getSubCategories = $this->master->getSubCategories();
        $getCompanies = $this->master->getCompanies();
        return view('reports.add')->with('getCategories',$getCategories)->with('getSubCategories',$getSubCategories)->with('getCompanies',$getCompanies);
    }

    public function getCatSubcategories(Request $request){
        if (!session('username')) {
	    	return redirect('/');
        } 
        
        $data = $request->all();
        $getCatSubcategories = $this->master->getCatSubcategories($data);
        return $getCatSubcategories;
    }

    public function save_report(Request $request){
        if (!session('username')) {
	    	return redirect('/');
        } 
        
        $data = $request->all();
        $save_report = $this->reports->save_report($data);
        return $save_report;
    }

    public function edit_report(Request $request,$id){
        if (!session('username')) {
	    	return redirect('/');
        } 
        
        $data = $request->all();
        $getCategories = $this->master->getCategories();
        $getSubCategories = $this->master->getSubCategories();
        $getCompanies = $this->master->getCompanies();
        $edit_report = $this->reports->getReportDataById($id);
        return view('reports.edit')->with('getCategories',$getCategories)->with('getSubCategories',$getSubCategories)->with('getCompanies',$getCompanies)->with('edit_report',$edit_report);
    }

    public function update_report(Request $request){
        if (!session('username')) {
	    	return redirect('/');
        } 
        
        $data = $request->all();
        $update_report = $this->reports->update_report($data);
        return $update_report;
    }

    public function update_report_status(Request $request){
        if (!session('username')) {
	    	return redirect('/');
        } 
        
        $data = $request->all();
        $update_report_status = $this->reports->update_report_status($data);
        return $update_report_status;
    }

    public function view_report(Request $request,$id){
        if (!session('username')) {
	    	return redirect('/');
        } 
        
        $data = $request->all();
        $getCategories = $this->master->getCategories();
        $getSubCategories = $this->master->getSubCategories();
        $getCompanies = $this->master->getCompanies();
        $view_report = $this->reports->getReportDataById($id);
        return view('reports.view')->with('getCategories',$getCategories)->with('getSubCategories',$getSubCategories)->with('getCompanies',$getCompanies)->with('view_report',$view_report);
    }
    
}
