<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Session;
use Config;
use Carbon\Carbon;

class reports extends Model
{
    public function getReports($search_key){
        $query = DB::table('tbl_reports')
                    ->select('rep_id','rep_name','rep_url','rep_type','rep_pub_date','rep_status')
                    ->orderBy('rep_id','DESC');
        if($search_key != ''){
            $query->where('rep_name','like','%'.$search_key.'%');
        }

        $getReports = $query->paginate(30);

        if(sizeof($getReports)>0){
            return $getReports;
        }
        else{
            return [];
        }
    }

    public function getReports_For_Download($search_key){
        $query = DB::table('tbl_reports')
                    ->select('rep_id','rep_name','rep_url','rep_type','rep_pub_date','rep_status')
                    ->orderBy('rep_id','DESC');
        if($search_key != ''){
            $query->where('rep_name','like','%'.$search_key.'%');
        }

        $getReports = $query->get();

        if(sizeof($getReports)>0){
            return $getReports;
        }
        else{
            return [];
        }
    }

    public function save_report($data){
        $date = Carbon::now();
        $month_year = $date->format("Y-m");
        // print_r($data);
        // exit;
        $save_report = DB::table('tbl_reports')->insertGetId([
            'rep_name' => $data['report_name'],
            'rep_breadcrumb' => $data['report_bread_crumb'],
            'rep_url' => $data['report_url'],
            'rep_sdesc' => $data['report_short_desc'],
            'cat_id' => $data['report_category_id'],
            'sub_cat_id' => $data['report_sub_category_id'],
            'rep_pub_date' => $data['report_publish_date'],
            'rep_price_sul' => $data['report_sul'],
            'rep_price_cul' => $data['report_cul'],
            'rep_price_el' => $data['report_el'],
            'rep_pages' => $data['report_pages'],
            'rep_type' => $data['report_type'],
            'rep_top_selling' => $data['rep_top_selling'],
            'meta_title' => $data['meta_title'],
            'meta_keywords' => $data['meta_keywords'],
            'meta_desc' => $data['meta_desc'],
            'dc_desc' => $data['dc_desc'],
            'rep_schema' => $data['rep_schema'],
            'is_updated' => $data['is_updated'],
            'top_selling' => $data['top_selling'],
            'rep_status' => 'Y',
            'month_year' => $month_year
        ]);
        
        if($save_report){
            $save_report_details = DB::table('tbl_reports_details')->insert([
                'rep_id' => $save_report,
                'rep_mar_overview' => $data['rep_mar_overview'],
                'rep_free_analysis' => $data['report_analysis'],
                'rep_free_analysis_2' => $data['report_analysis_2'],
                'rep_desc' => $data['report_long_desc'],
                'rep_mar_segment' => $data['report_mkt_sgmt'],
                'rep_tab_content' => $data['report_table_of_content'],
                'rep_list_table' => $data['report_list_of_tables'],
                'rep_list_chart' => $data['rep_list_chart'],
                'rep_part2' => $data['rep_part2'],
                'report_preview_part_2' => $data['report_preview_part_2'],
                'created_on' => $date,
                'updated_on' => $date
            ]);
            if($save_report_details == 1){
                return 1;
            }
            else{
                return 0;
            }
            // return 1;
        }
        else{
            return 0;
        }
    }

    public function getReportDataById($id){
        $getReportDataById = DB::table('tbl_reports')
                            ->leftjoin('tbl_reports_details','tbl_reports_details.rep_id','=','tbl_reports.rep_id')
                            ->select('tbl_reports.rep_id as report_id','tbl_reports.*','tbl_reports_details.*')
                            ->where('tbl_reports.rep_id','=',$id)
                            ->get();
        if(sizeof($getReportDataById)>0){
            return $getReportDataById;
        }
        else{
            return [];
        }
    }

    public function update_report($data){
        $date = Carbon::now();
        //  print_r($data);
        $update_report = DB::table('tbl_reports')->where('rep_id','=',$data['report_id'])->update([
            'rep_name' => $data['report_name'],
            'rep_breadcrumb' => $data['report_bread_crumb'],
            'rep_url' => $data['report_url'],
            'rep_sdesc' => $data['report_short_desc'],
            'cat_id' => $data['report_category_id'],
            'sub_cat_id' => $data['report_sub_category_id'],
            'rep_pub_date' => $data['report_publish_date'],
            'rep_price_sul' => $data['report_sul'],
            'rep_price_cul' => $data['report_cul'],
            'rep_price_el' => $data['report_el'],
            'rep_pages' => $data['report_pages'],
            'rep_type' => $data['report_type'],
            'rep_top_selling' => $data['rep_top_selling'],
            'meta_title' => $data['meta_title'],
            'meta_keywords' => $data['meta_keywords'],
            'meta_desc' => $data['meta_desc'],
            'dc_desc' => $data['dc_desc'],
            'rep_schema' => $data['rep_schema'],
            'is_updated' => $data['is_updated'],
            'top_selling' => $data['top_selling'],
        ]);
        // print_r($update_report);
        // exit;
        if($update_report){
            $delete =  DB::table('tbl_reports_details')->where('rep_id','=',$data['report_id'])->delete();
            $save_report_details = DB::table('tbl_reports_details')->insert([
                'rep_id' => $data['report_id'],
                'rep_mar_overview' => $data['rep_mar_overview'],
                'rep_free_analysis' => $data['report_analysis'],
                'rep_free_analysis_2' => $data['report_analysis_2'],
                'rep_desc' => $data['report_long_desc'],
                'rep_mar_segment' => $data['report_mkt_sgmt'],
                'rep_tab_content' => $data['report_table_of_content'],
                'rep_list_table' => $data['report_list_of_tables'],
                'rep_list_chart' => $data['rep_list_chart'],
                'rep_part2' => $data['rep_part2'],
                'report_preview_part_2' => $data['report_preview_part_2'],
                'created_on' => $date,
                'updated_on' => $date,
            ]);
            if($save_report_details == 1){
                return 1;
            }
            else{
                return 0;
            }
        }
        else{
            return 0;
        }
    }

    public function update_report_status($data){
        $date = Carbon::now();
        $status='';
        if($data['hidden_status'] == 1){
            $status='Y';
        }
        else{
            $status='N';
        }
        $update_report_status = DB::table('tbl_reports')->where('rep_id','=',$data['report_id'])->update([
            'rep_status' => $status,
            // 'updated_at' => $date
        ]);
        
        if($update_report_status == 1){
            return 1;
        }
        else{
            return 0;
        }
    }
}
