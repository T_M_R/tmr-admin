<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Session;
use Config;
use Carbon\Carbon;

class users extends Model
{
    public function getUsersList($search_key){
        $getUsersList = DB::table('users')
                        ->leftjoin('roles_new','roles_new.role_id','=','users.role_id')
                        ->leftjoin('users as u','u.id','=','users.supervisor_id')
                        ->select('users.id','users.role_id','users.username','users.name','users.email','users.mobile_phone','users.status','roles_new.role_name','u.name as reporting_user')
                        ->orderBy('users.id','DESC');
                        // ->paginate(30);
        if($search_key != ''){
            $getUsersList->where('users.name','like','%'.$search_key.'%');
        }
        $getUsersList = $getUsersList->paginate(30);
        
        if($getUsersList){
            return $getUsersList;
        }
        else{
            return [];
        }
    }

    public function user_duplicate_check($data){

        $user_name = $data['user_name'];
        $add_update_flag = $data['add_update_flag'];
        $mobile = $data['mobile'];
        $user_id = $data['user_id'];

        if($add_update_flag == 1){
            $user_duplicate_check = DB::table('users')
                                    ->where('mobile_phone','like','%'.$mobile.'%')
                                    ->count();
        }
        else{
            $user_duplicate_check = DB::table('users')
                                    ->where('mobile_phone','like','%'.$mobile.'%')
                                    ->where('id','!=',$user_id)
                                    ->count();
        }
        if($user_duplicate_check == 0){
            return 0;
        }
        else{
            return 1;
        }
    }

    public function save_user($data){
        $date = Carbon::now();
        if(isset($data['user_supervisor'])){
            $supervisorId = $data['user_supervisor'];
        }
        else{
            $supervisorId = null;
        }
        // $getUserRoleId = DB::table('roles_new')->select('role_id')->where('id','=',$data['user_role'])->get();

        $save_user = DB::table('users')->insert([
            'name' => $data['full_name'],
            'username' => $data['user_name'],
            'password' => $data['password'],
            'email' => $data['email'],
            'mobile_phone' => $data['mobile'],
            'role_id' => $data['user_role'],
            'parent_id' => $data['user_supervisor'],
            'supervisor_id'=>$data['user_supervisor'],
            'status' => 'Y',
            // 'user_name' => $data['user_name'],
            'created_at' => $date
        ]);
        
        if($save_user == 1){
            return 1;
        }
        else{
            return 0;
        }
    }

    public function edit_user($id){
        $edit_user = DB::table('users')
                        // ->join('roles','roles.role_id','=','users.role_id')
                        ->select('users.id','users.role_id','users.username','users.name','users.email','users.mobile_phone','users.status','users.password','users.parent_id','users.supervisor_id')
                        ->where('users.id','=',$id)
                        ->orderBy('users.id','DESC')
                        ->get();
        if($edit_user){
            return $edit_user;
        }
        else{
            return [];
        }
    }

    public function update_user($data){
        $date = Carbon::now();
        // $getUserRoleId = DB::table('roles_new')->select('role_id')->where('id','=',$data['user_role'])->get();

        $update_user = DB::table('users')->where('id',$data['user_id'])->update([
            'name' => $data['full_name'],
            'username' => $data['user_name'],
            'password' => $data['password'],
            'email' => $data['email'],
            'mobile_phone' => $data['mobile'],
            'role_id' => $data['user_role'],
            'parent_id' => $data['user_supervisor'],
            'supervisor_id'=>$data['user_supervisor'],
            'updated_at' => $date
        ]);
        
        if($update_user == 1){
            return 1;
        }
        else{
            return 0;
        }
    }

    public function getRoles(){
        $getRoles = DB::table('roles_new')
                    ->orderBy('id','ASC')
                    ->get();
        if($getRoles){
            return $getRoles;
        }
        else{
            return [];
        }
    }

    public function getUsers(){
        $getUsers = DB::table('users')
                    ->select('id','name')
                    ->where('status','Y')
                    ->orderBy('id','ASC')
                    ->get();
        if($getUsers){
            return $getUsers;
        }
        else{
            return [];
        }
    }

    public function get_role_supervisors($data){
        if(isset($data['user_role'])){
            $role = $data['user_role'];
        }
        else{
           $role = $data;
        }
        $rolesArray=[];
        $getRoleData = DB::table('roles_new')
                    ->select('id','role_id')
                    ->where('role_id','=',$role)
                    ->get();
        $getHigherRoles = DB::table('roles_new')
                        ->select('id','role_id')
                        ->where('id','<',$getRoleData[0]->id)
                        ->get();
        foreach($getHigherRoles as $role){
            $rolesArray[]=$role->role_id;
        }
        
        if(sizeof($rolesArray)>0){
            $get_role_supervisors = DB::table('users')
                                ->select('id','name')
                                ->whereIn('role_id',$rolesArray)
                                ->get();
            if(sizeof($get_role_supervisors)>0){
                return $get_role_supervisors;
            }
            else{
                return 0;
            }
        }
        else{
            return [];
        }
    }

    public function update_user_status($data){
        $date = Carbon::now();
        $status='';
        if($data['hidden_status'] == 1){
            $status='Y';
        }
        else{
            $status='N';
        }
        $update_user_status = DB::table('users')->where('id','=',$data['user_id'])->update([
            'status' => $status,
            'updated_at' => $date
        ]);
        
        if($update_user_status == 1){
            return 1;
        }
        else{
            return 0;
        }
    }

    public function get_my_team(){
        $user_id = Session::get('user_id');

        $users = DB::table('users')
                ->select('id','name','role_id','supervisor_id')
                ->whereNotNull('users.supervisor_id')
                ->orderby('users.id','ASC')
                ->get();
        
        $userViseArray=[];
        $finalArray=[];
        $liste = array();
        foreach($users as $subordinates){
            $value = $subordinates->id;
            if( $subordinates->supervisor_id == $user_id ){
                if(!in_array($value, $liste, true) ){
                    // $userViseArray['id']=$subordinates->id;
                    // $userViseArray['name']=$subordinates->name;
                    // $userViseArray['internal_user_id']=$subordinates->internal_user_id;
                    $userViseArray=$subordinates->id;
                }
                if( !empty($userViseArray) ){
                    array_push($finalArray, $userViseArray);
                }
                $get_my_team_loop = $this->get_my_team_loop($users,$value);
                if( !empty($userViseArray) ){
                    foreach( $get_my_team_loop as $member ){
                        array_push($finalArray, $member);
                    }
                }
            }
            $liste[] = $subordinates->id;
        }
        foreach($users as $subordinates){
            if($subordinates->id == $user_id){
                $userViseArray=$subordinates->id;
                array_push($finalArray, $userViseArray);
            }
        }
        // $finalArray = implode(', ', $finalArray);
        return $finalArray;
    }

    function get_my_team_loop($users,$user_id){
        $userViseArray=[];
        $finalArray=[];
        $liste = array();
        foreach($users as $subordinates){
            $value = $subordinates->id;
            if( $subordinates->supervisor_id == $user_id ){
                if(!in_array($value, $liste, true) ){
                    // $userViseArray['id']=$subordinates->id;
                    // $userViseArray['name']=$subordinates->name;
                    // $userViseArray['internal_user_id']=$subordinates->internal_user_id;
                    $userViseArray=$subordinates->id;
                }
                if( !empty($userViseArray) ){
                    array_push($finalArray, $userViseArray);
                }
                $get_my_team_loop = $this->get_my_team_loop($users,$value);
                if( !empty($userViseArray) ){
                    foreach( $get_my_team_loop as $member ){
                        array_push($finalArray, $member);
                    }
                }
            }
            $liste[] = $subordinates->id;
        }
        return $finalArray;
    }
}
