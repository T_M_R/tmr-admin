@extends('layouts.header')
@section('body-content')
<section class="users-add">
    <div class="block-header">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 my-auto">
                <h4 class="page-title m-0">Add User</h4>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <a href="{{URL:: to('/users')}}">
                    <button type="button" class="btn btn-info float-right">Back</button>
                </a>
            </div>
        </div>
    </div>
    <div class="row clearfix mt-3">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="card project_widget">
                <div class="body mt-4 mb-4">
                    <div class="row">
                        <div class="col-12 col-md-4 mb-0">
                            <div class="input-group">  
                                <input type="text" class="full_name" autocomplete="off" value="" required/>
                                <label>Full Name <span class="text-danger">*</span></label>
                            </div>
                        </div>
                        <div class="col-12 col-md-4 mb-0">
                            <div class="input-group">  
                                <input type="text" class="mobile" autocomplete="off" value="" required/>
                                <label>Mobile <span class="text-danger">*</span></label>
                            </div>
                        </div>
                        <div class="col-12 col-md-4 mb-0">
                            <div class="input-group">  
                                <input type="text" class="email" autocomplete="off" value="" required/>
                                <label>Email <span class="text-danger">*</span></label>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 mb-0">
                            <div class="input-group">  
                                <input type="text" class="user_name" autocomplete="off" value="" required/>
                                <label>Username <span class="text-danger">*</span></label>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 mb-0">
                            <div class="input-group">  
                                <input type="text" class="password" autocomplete="off" value="" required/>
                                <label>Password <span class="text-danger">*</span></label>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 mb-0">
                            <div class="input-group">
                                <select class="floating-select user_role" name="user_role" id="user_role" required >
                                    <option value=""></option>
                                        @foreach($getRoles as $role)
                                            <option value="{{$role->role_id}}">{{$role->role_name}}</option>
                                        @endforeach
                                </select>
                                <label>Role <span class="text-danger">*</span></label>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 mb-0">
                            <div class="input-group">
                                <select class="floating-select user_supervisor" name="user_supervisor" id="user_supervisor" required >
                                    <option value=""><option>
                                </select>
                                <label>Supervisor <span class="text-danger">*</span></label>
                            </div>
                        </div>
                        <input type="hidden" class="user_id">
                        <div class="col-12 col-md-12 mb-0">
                            <p class="text-danger mb-0">* All fields are Mandatory</p>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb-0 text-center">
                            <div class="form-group">
                                <div class="form-line">
                                    <div class="user_error_msg col-red w-100" style="display:none"></div>
                                    <div class="user_success_msg col-green w-100" style="display:none"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 text-center">
                            <button type="submit" class="btn btn-primary waves-effect save_user">SUBMIT</button>                                                                
                        </div>
                    </div>    
                </div>
            </div>
        </div>
    </div>
</section>
@endsection