@extends('layouts.header')
@section('body-content')
<section class="sub-category-list">
    <div class="block-header">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 my-auto">
                <h4 class="page-title m-0">Add New Aritcles</h4>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <a href="{{URL:: to('/articles')}}">
                    <button type="button" class="btn btn-info float-right">Back</button>
                </a>
            </div>
        </div>
    </div>
    <div class="row clearfix mt-3">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="card project_widget">
                <div class="body mt-4 mb-4">
                    <div class="row">
                        <div class="col-12 col-md-12 mb-0">
                            <div class="input-group">  
                                <input type="text" class="article_name" autocomplete="off" value="" required/>
                                <label>Title <span class="text-danger">*</span></label>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 mb-0">
                            <div class="input-group">  
                                <input type="text" class="article_heading" autocomplete="off" value="" required/>
                                <label>Heading (h1) <span class="text-danger">*</span></label>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 mb-0">
                            <div class="input-group">  
                                <input type="text" class="article_url" autocomplete="off" value="" required/>
                                <label>URL <span class="text-danger">*</span></label>
                            </div>
                        </div>
                        <div class="col-12 col-md-12 mb-0">
                            <div class="input-group">
                                <select class="floating-select article_category_id" onclick="this.setAttribute('value', this.value);" value="" required>
                                    @if(sizeof($getCategories)>0)
                                        <option value=""></option>
                                        @foreach($getCategories as $val)
                                            <option value="{{$val->category_id}}">{{$val->cat_name}}</option>
                                        @endforeach
                                    @endif
                                </select>
                                <label>Category <span class="text-danger">*</span></label>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 mb-0 d-none">
                            <div class="input-group">
                                <select class="floating-select article_author_id" onclick="this.setAttribute('value', this.value);" value="" required>
                                    @if(sizeof($getSubCategories)>0)
                                        <option value=""></option>
                                        @foreach($getSubCategories as $val)
                                            <option value="{{$val->sub_category_id}}">{{$val->sub_category_name}}</option>
                                        @endforeach
                                    @endif
                                </select>
                                <label>Author <span class="text-danger">*</span></label>
                            </div>
                        </div>
                        <div class="col-12 col-md-12 mb-0">
                            <label class="" style="color:#414244;"><b>Short  Description</b><span class="text-danger">*</span></label>
                            <div class="input-group mt-5">
                                <textarea class="article_short_desc ckeditor" name="article_short_desc" id="article_short_desc" >  </textarea>
                            </div>
                        </div>
                        <div class="col-12 col-md-12 mb-0">
                            <label class="" style="color:#414244;"><b>First Description</b></label>
                            <div class="input-group mt-5">  
                                <textarea class="article_first_desc ckeditor" id="article_first_desc" name="article_first_desc">  </textarea>
                            </div>
                        </div>
                        <div class="col-12 col-md-12 mb-0">
                            <label class="" style="color:#414244;"><b>Long Description</b><span class="text-danger">*</span></label>
                            <div class="input-group mt-5">  
                                <textarea class="article_long_desc ckeditor" id="article_long_desc" name="article_long_desc">  </textarea>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 mb-0">
                            <div class="input-group">  
                                <input type="text" class="meta_title" autocomplete="off" value="" required/>
                                <label>Meta Title </label>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 mb-0">
                            <div class="input-group">  
                                <input type="text" class="meta_keywords" autocomplete="off" value="" required/>
                                <label>Meta Keywords</label>
                            </div>
                        </div>
                        <div class="col-12 col-md-12 mb-0">
                            <label class="" style="color:#414244;"><b>Meta Description</b></label>
                            <div class="form-group mt-5"> 
                                <div class="form-line">  
                                    <textarea class="form-control meta_desc p-3" name="meta_desc" rows="4" placeholder="Meta Description">  </textarea>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 mb-0">
                            <div class="input-group">  
                                <input type="text" class="datepicker article_publish_date" autocomplete="off" value="" required/>
                                <span style="position: absolute;top: 8px;right: 0;bottom: 0;"><i class="icon-calendar" style="font-size: 26px;"></i></span>
                                <label>Publish Date <span class="text-danger">*</span></label>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 mb-0">
                            <div class="input-group">
                                <select class="floating-select article_status" onclick="this.setAttribute('value', this.value);" value="" required>
                                    <option value=""></option>
                                    <option value="Y">Active</option>
                                    <option value="N">In-Active</option>
                                </select>
                                <label>Status <span class="text-danger">*</span></label>
                            </div>
                        </div>
                        <input type="hidden" class="sub_category_id">
                        <div class="col-12 col-md-4 mb-0">
                            <p class="text-danger mb-0">* All fields are Mandatory</p>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb-0 text-center">
                            <div class="form-group">
                                <div class="form-line">
                                    <div class="article_error_msg col-red w-100" style="display:none"></div>
                                    <div class="article_success_msg col-green w-100" style="display:none"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 text-center">
                            <button type="submit" class="btn btn-primary waves-effect save_article">SUBMIT</button>                                                                
                        </div>
                    </div>    
                </div>
            </div>
        </div>
    </div>
</section>
@endsection