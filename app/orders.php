<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Session;
use Config;
use Carbon\Carbon;

class orders extends Model
{
    public function getOrdersData($search_key){
        $query = DB::table('order_amex_transaction as oat')
                    ->join('reports','reports.rep_id','=','oat.report_id')
                    ->select('reports.rep_name','oat.order_id','oat.report_purchase_attempt_id','oat.order_amount','oat.transaction_status','oat.transaction_date')
                    ->orderBy('order_id','DESC');
        if($search_key != ''){
            $query->where('reports.rep_name','like','%'.$search_key.'%');
        }

        $getOrdersData = $query->paginate(30);

        if(sizeof($getOrdersData)>0){
            return $getOrdersData;
        }
        else{
            return [];
        }
    }
}
