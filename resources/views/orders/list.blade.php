@extends('layouts.header')
@section('body-content')
<section class="report-list">
    <div class="block-header">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 my-auto">
                <h4 class="page-title m-0">Orders</h4>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5 my-auto">
                <div class="input-group m-0">
                    <form method="GET" action="{{url('/orders')}}" enctype="multipart/form-data" role="form" autocomplete="off">
                        <input type="search" id="search_key" name="search_key" class="search-field m-0" value="{{$search_key}}" placeholder="Search Report Name">
                        <div class="input-group-append">
                            <button type="submit" class="btn btn-primary"><i class="fa fa-search mr-1"></i> Search</button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-1 col-lg-1 pl-0">
                <form method="GET" action="{{url('/orders')}}" enctype="multipart/form-data" role="form" autocomplete="off">
                    <button type="submit" class="btn btn-clear">Clear</button>
                </form>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3"></div>
        </div>
    </div>
    <div class="row clearfix mt-3">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            @if(isset($getOrdersData) && sizeof($getOrdersData)>0)
            <p class="status_change_msg text-center"></p>
            <div class="card">
                <div class="body p-0">
                    <div class="table-responsive">
                        <!-- <table class="table table-bordered table-striped table-hover js-basic-example dataTable"> -->
                        <table class="table table-bordered mb-0">
                            <thead>
                                <tr>
                                    <th>S.No</th>
                                    <th>Report</th>
                                    <th>Attempt Id</th>
                                    <th>Order Amount</th>
                                    <th>Status</th>
                                    <th>Transaction Date</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $i=1;?>
                                @foreach($getOrdersData as $value)
                                    <tr>
                                        <td>{{$i}}</td>
                                        <?php $rep_name = wordwrap($value->rep_name,5). "\n"?>
                                        <td>{{$rep_name}}</td>
                                        <td>
                                            @if($value->report_purchase_attempt_id != '')
                                                {{$value->report_purchase_attempt_id}}
                                            @else
                                                -----
                                            @endif
                                        </td>
                                        <td>
                                            @if($value->order_amount != '')
                                                {{$value->order_amount}}
                                            @else
                                                -----
                                            @endif
                                        </td>
                                        <td>
                                            @if($value->transaction_status != '')
                                                {{$value->transaction_status}}
                                            @else
                                                -----
                                            @endif
                                        </td>
                                        <td>
                                            @if($value->transaction_date != '')
                                                <?php $transaction_date = date("d-m-Y h:m", strtotime($value->transaction_date));?>
                                                {{$transaction_date}}
                                            @else
                                                -----
                                            @endif
                                        </td>
                                    </tr>
                                    <?php $i++;?>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            @else
                <div class="card">
                    <p class="text-center py-5" style="font-size:25px">No Data Available To Show</p>  
                </div>
            @endif
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 paginate-sec">
            @if(sizeof($getOrdersData)>0)
                {{$getOrdersData->links()}}
            @endif
        </div>
    </div>
</section>
@endsection