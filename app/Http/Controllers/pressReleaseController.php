<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\pressRelease;
use App\master;
use Carbon\Carbon;
use Redirect;
use Session;
use DB;
use Config;

class pressReleaseController extends Controller
{
    public function __construct(){
        $this->pressRelease = new pressRelease();
        $this->master = new master();
    }

    public function getPressReleases(Request $request){
        if (!session('username')) {
	    	return redirect('/');
        } 
        $search_key = '';
        if(isset($_GET['search_key'])){
            $search_key=$_GET['search_key'];
        }
        $getPressReleases = $this->pressRelease->getPressReleases($search_key);
        $getPressReleases = $getPressReleases->appends(['search_key'=>$search_key]);

        return view('pressRelease.list')->with('getPressReleases',$getPressReleases)->with('search_key',$search_key);
    }

    public function addPressRelease(){
        if (!session('username')) {
	    	return redirect('/');
        }

        $getCategories = $this->master->getCategories();
        $getSubCategories = $this->master->getSubCategories();
        $getAuthors = $this->master->getAuthors();

        return view('pressRelease.add')->with('getCategories',$getCategories)->with('getSubCategories',$getSubCategories)->with('getAuthors',$getAuthors);
    }

    public function save_press_release(Request $request){
        if (!session('username')) {
	    	return redirect('/');
        } 
        
        $data = $request->all();
        $save_press_release = $this->pressRelease->save_press_release($data);
        return $save_press_release;
    }

    public function edit_press_release($id){
        if (!session('username')) {
	    	return redirect('/');
        }

        $getCategories = $this->master->getCategories();
        $getSubCategories = $this->master->getSubCategories();
        $getAuthors = $this->master->getAuthors();
        $edit_press_release = $this->pressRelease->getPressReleaseDataById($id);

        return view('pressRelease.edit')->with('getSubCategories',$getSubCategories)->with('getAuthors',$getAuthors)->with('edit_press_release',$edit_press_release)->with('getCategories',$getCategories);
    }

    public function update_press_release(Request $request){
        if (!session('username')) {
	    	return redirect('/');
        } 
        
        $data = $request->all();
        $update_press_release = $this->pressRelease->update_press_release($data);
        return $update_press_release;
    }

    public function update_press_release_status(Request $request){
        if (!session('username')) {
	    	return redirect('/');
        } 
        
        $data = $request->all();
        $update_press_release_status = $this->pressRelease->update_press_release_status($data);
        return $update_press_release_status;
    }

    public function view_press_release($id){
        if (!session('username')) {
	    	return redirect('/');
        }

        $getCategories = $this->master->getCategories();
        $getSubCategories = $this->master->getSubCategories();
        $getAuthors = $this->master->getAuthors();
        $view_press_release = $this->pressRelease->getArticleDataById($id);

        return view('pressRelease.view')->with('getSubCategories',$getSubCategories)->with('getAuthors',$getAuthors)->with('view_press_release',$view_press_release);
    }
}
