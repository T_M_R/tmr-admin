@extends('layouts.header')
@section('body-content')
<section class="sub-category-list">
    <div class="block-header">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 my-auto">
                <h4 class="page-title m-0">Add New Case Study</h4>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <a href="{{URL:: to('/case_studies')}}">
                    <button type="button" class="btn btn-info float-right">Back</button>
                </a>
            </div>
        </div>
    </div>
    <div class="row clearfix mt-3">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="card project_widget">
                <div class="body mt-4 mb-4">
                    <form method="POST" action="{{url('update_case_study')}}" enctype="multipart/form-data">
                        @foreach($edit_case_study as $value)   
                            <div class="row">
                                <div class="col-12 col-md-6 mb-0">
                                    <div class="input-group">  
                                        <input type="text" class="cs_name" name="cs_name" autocomplete="off" value="{{$value->cs_heading}}" required/>
                                        <label>Title <span class="text-danger">*</span></label>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6 mb-0">
                                    <div class="input-group">  
                                        <input type="text" class="cs_url" name="cs_url" autocomplete="off" value="{{$value->url}}" required/>
                                        <label>URL <span class="text-danger">*</span></label>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6 mb-0">
                                    <div class="input-group">
                                        <select class="floating-select cs_category_id" name="cs_category_id" onclick="this.setAttribute('value', this.value);" required>
                                            @if(sizeof($getCategories)>0)
                                                <option value=""></option>
                                                @foreach($getCategories as $val)
                                                    @if($val->category_id == $value->category_id)
                                                        <option value="{{$val->category_id}}" selected>{{$val->cat_name}}</option>
                                                    @else
                                                        <option value="{{$val->category_id}}">{{$val->cat_name}}</option>
                                                    @endif
                                                @endforeach
                                            @endif
                                        </select>
                                        <label>Category <span class="text-danger">*</span></label>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6 mb-0">
                                    <div class="input-group">  
                                        <input type="file" class="cs_image" name="cs_image" autocomplete="off" value=""/>
                                        <!-- <label>Image <span class="text-danger">*</span></label> -->
                                    </div>
                                </div>
                                <div class="col-12 col-md-12 mb-0">
                                    <label class="" style="color:#414244;"><b>Challenge</b><span class="text-danger">*</span></label>
                                    <div class="input-group mt-5">
                                        <textarea class="cs_challenge ckeditor" name="cs_challenge" id="cs_challenge" > {{$value->cs_description}} </textarea>
                                    </div>
                                </div>
                                <div class="col-12 col-md-12 mb-0">
                                    <label class="" style="color:#414244;"><b>Insight</b><span class="text-danger">*</span></label>
                                    <div class="input-group mt-5">  
                                        <textarea class="cs_insight ckeditor" id="cs_insight" name="cs_insight">  {{$value->insight}}</textarea>
                                    </div>
                                </div>
                                <div class="col-12 col-md-12 mb-0">
                                    <label class="" style="color:#414244;"><b>Solution</b><span class="text-danger">*</span></label>
                                    <div class="input-group mt-5">
                                        <textarea class="cs_solution ckeditor" name="cs_solution" id="cs_solution" > {{$value->solution}} </textarea>
                                    </div>
                                </div>
                                <div class="col-12 col-md-12 mb-0">
                                    <label class="" style="color:#414244;"><b>Result</b><span class="text-danger">*</span></label>
                                    <div class="input-group mt-5">
                                        <textarea class="cs_result ckeditor" name="cs_result" id="cs_result" > {{$value->result}} </textarea>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6 mb-0">
                                    <div class="input-group">  
                                        <input type="text" class="meta_title" name="meta_title" autocomplete="off" value="{{$value->cs_meta_title}}" required/>
                                        <label>Meta Title </label>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6 mb-0">
                                    <div class="input-group">  
                                        <input type="text" class="meta_keywords" name="meta_keywords" autocomplete="off" value="{{$value->cs_meta_keywords}}" required/>
                                        <label>Meta Keywords</label>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6 mb-0">
                                    <label class="" style="color:#414244;"><b>Meta Description</b></label>
                                    <div class="form-group mt-5"> 
                                        <div class="form-line">  
                                            <textarea class="form-control meta_desc p-3" name="meta_desc" rows="4" placeholder="Meta Description"> {{$value->cs_meta_description}} </textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6 mb-0">
                                    <label class="" style="color:#414244;"><b>DC Description</b></label>
                                    <div class="form-group mt-5"> 
                                        <div class="form-line">  
                                            <textarea class="form-control dc_desc p-3" name="dc_desc" rows="4" placeholder="DC Description"> {{$value->cs_dc_description}} </textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6 mb-0">
                                    <div class="input-group">  
                                        <input type="text" class="seo_title" name="seo_title" autocomplete="off" value="{{$value->seo_title}}" required/>
                                        <label>Seo Title </label>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6 mb-0">
                                    <div class="input-group">
                                        <select class="floating-select cs_status" name="cs_status" onclick="this.setAttribute('value', this.value);" required>
                                            <option value=""></option>
                                            @if($value->cs_status == 1)
                                                <option value="1" selected>Active</option>
                                                <option value="0">In-Active</option>
                                            @elseif($value->cs_status == 0)
                                                <option value="1">Active</option>
                                                <option value="0" selected>In-Active</option>
                                            @else
                                                <option value="1">Active</option>
                                                <option value="0">In-Active</option>
                                            @endif
                                        </select>
                                        <label>Status <span class="text-danger">*</span></label>
                                    </div>
                                </div>
                                <input type="hidden" class="cs_id" name="cs_id" value="{{$value->cs_id}}">
                                <div class="col-12 col-md-4 mb-0">
                                    <p class="text-danger mb-0">* All fields are Mandatory</p>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb-0 text-center">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <div class="cs_error_msg col-red w-100" style="display:none"></div>
                                            <div class="cs_success_msg col-green w-100" style="display:none"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 text-center">
                                    <button type="submit" class="btn btn-primary waves-effect update_cs">UPDATE</button>                                                                
                                </div>
                            </div> 
                        @endforeach
                    </form>   
                </div>
            </div>
        </div>
    </div>
</section>
@endsection