@extends('layouts.header')
@section('body-content')
<section class="regions-list">
    <div class="block-header">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 my-auto">
                <h4 class="page-title m-0">Edit Role</h4>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <a href="{{URL:: to('/roles')}}">
                    <button type="button" class="btn btn-info float-right">Back</button>
                </a>
            </div>
        </div>
    </div>
    <div class="row clearfix mt-3">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="card project_widget">
                @if(sizeof($getPagesList)>0)
                    <form method="POST" action="{{url('update_role_permissions')}}" enctype="multipart/form-data">   
                        <div class="body mt-4 mb-4">
                            <div class="row">
                                <div class="col-12 col-md-6 mb-0">
                                    <div class="input-group">  
                                        <input type="text" class="role_name" name="role_name" autocomplete="off" value="{{$getRoleName[0]->role_name}}" disabled required/>
                                        <label>Role Name <span class="text-danger">*</span></label>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6 mb-0">
                                    <div class="input-group">  
                                        <select class="floating-select reporting_role_id" name="reporting_role_id" id="reporting_role_id" >
                                            <option value=""></option>
                                                @foreach($roles as $role)
                                                    @if($role->role_id == $getRoleName[0]->reporting_role_id)
                                                        <option value="{{$role->role_id}}" selected>{{$role->role_name}}</option>
                                                    @else
                                                        <option value="{{$role->role_id}}">{{$role->role_name}}</option>
                                                    @endif
                                                @endforeach
                                        </select>
                                        <label>Reporting Role <span class="text-danger">*</span></label>
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb-0 text-center">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <div class="role_error_msg col-red w-100" style="display:none"></div>
                                            <div class="role_success_msg col-green w-100" style="display:none"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb-0">
                                    <div class="table-responsive">
                                        <table class="table table-bordered mb-0">
                                            <thead>
                                                <tr>
                                                    <th>Page Name</th>
                                                    <th>View</th>
                                                    <th>Edit</th>
                                                    <th>Add</th>
                                                    <th>Delete</th>
                                                    <th>Download</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php  
                                                    $i=0;
                                                    function checkBoxStatus($userPermissions, $prname, $pageId, $type){
                                                        $needToCheck=false;
                                                        foreach ($userPermissions as $key) {
                                                            if( $key->page_id == $pageId && $key->{$type} == $prname  ){
                                                                $needToCheck=true;
                                                            }
                                                        }
                                                        return $needToCheck;
                                                    }
                                                ?>
                                                @foreach($getPagesList as $main_menu)
                                                    @if($main_menu->parent_page_id == 0)
                                                        <tr>
                                                            <td>{{$main_menu->name}}</td>
                                                            <td class="center">
                                                                @if($main_menu->is_view == 1)
                                                                    <?php $viewpage = checkBoxStatus($editPermissions,$main_menu->is_view,$main_menu->id,'is_view');  ?>
                                                                    @if($viewpage==true)
                                                                        <p class="m-0">
                                                                            <label class="table-labels">
                                                                                <input type="checkbox" checked="checked"  class="permission-check-box"/>
                                                                                <span></span>
                                                                                <input type="hidden" class="hidden-val" value="1"  name="view[]">
                                                                            </label>
                                                                        </p>
                                                                    @else
                                                                        <p>
                                                                            <label class="table-labels">
                                                                                <input type="checkbox" class="permission-check-box" />
                                                                                <span></span>
                                                                                <input type="hidden"  class="hidden-val" value="0"  name="view[]">
                                                                            </label>
                                                                        </p>
                                                                    @endif
                                                                @else
                                                                    <p class="d-none m-0">
                                                                        <label class="table-labels">
                                                                            <input type="checkbox" disabled="disabled"/>
                                                                            <span></span>
                                                                            <input type="hidden"  class="hidden-val" value="0"  name="view[]">
                                                                        </label>
                                                                    </p>
                                                                @endif
                                                            </td>
                                                            <td class="center">
                                                                @if($main_menu->is_edit == 1)
                                                                    <?php $editpage = checkBoxStatus($editPermissions,$main_menu->is_edit,$main_menu->id,'is_edit');  ?>
                                                                    @if($editpage == true)
                                                                        <p class="m-0">
                                                                            <label class="table-labels">
                                                                                <input type="checkbox" checked="checked" class="permission-check-box"/>
                                                                                <span></span>
                                                                                <input type="hidden" class="hidden-val" value="1"  name="edit[]">
                                                                            </label>
                                                                        </p>
                                                                    @else
                                                                        <p class="m-0">
                                                                            <label class="table-labels">
                                                                                <input type="checkbox"  class="permission-check-box"/>
                                                                                <span></span>
                                                                                <input type="hidden" class="hidden-val" value="0"  name="edit[]">
                                                                            </label>
                                                                        </p>
                                                                    @endif
                                                                @else
                                                                    <p class="d-none m-0">
                                                                        <label class="table-labels">
                                                                            <input type="checkbox" disabled="disabled"/>
                                                                            <span></span>
                                                                            <input type="hidden"  class="hidden-val" value="0"  name="edit[]">
                                                                        </label>
                                                                    </p>
                                                                @endif
                                                            </td>
                                                            <td class="center">
                                                                @if($main_menu->is_add == 1)
                                                                    <?php $addpage = checkBoxStatus($editPermissions,$main_menu->is_add,$main_menu->id,'is_add');  ?>
                                                                    @if($addpage == true)
                                                                        <p class="m-0">
                                                                            <label class="table-labels">
                                                                                <input type="checkbox" checked="checked" class="permission-check-box"/>
                                                                                <span></span>
                                                                                <input type="hidden" class="hidden-val" value="1"  name="add[]">
                                                                            </label>
                                                                        </p>
                                                                    @else
                                                                        <p class="m-0">
                                                                            <label class="table-labels">
                                                                                <input type="checkbox" class="permission-check-box"/>
                                                                                <span></span>
                                                                                <input type="hidden"  class="hidden-val" value="0"  name="add[]">
                                                                            </label>
                                                                        </p>
                                                                    @endif
                                                                @else
                                                                    <p class="d-none m-0">
                                                                        <label class="table-labels">
                                                                            <input type="checkbox" disabled="disabled"/>
                                                                            <span></span>
                                                                            <input type="hidden"  class="hidden-val" value="0"  name="add[]">
                                                                        </label>
                                                                    </p>
                                                                @endif
                                                            </td>
                                                            <td class="center">
                                                                @if($main_menu->is_delete == 1)
                                                                    <?php $deletepage = checkBoxStatus($editPermissions,$main_menu->is_delete,$main_menu->id,'is_delete');  ?>
                                                                    @if($deletepage == true)
                                                                        <p class="m-0">
                                                                            <label class="table-labels">
                                                                                <input type="checkbox" checked="checked" class="permission-check-box"/>
                                                                                <span></span>
                                                                                <input type="hidden" class="hidden-val" value="1"  name="delete[]">
                                                                            </label>
                                                                        </p>
                                                                    @else
                                                                        <p class="m-0">
                                                                            <label class="table-labels">
                                                                                <input type="checkbox" class="permission-check-box"/>
                                                                                <span></span>
                                                                                <input type="hidden"  class="hidden-val" value="0"  name="delete[]">
                                                                            </label>
                                                                        </p>
                                                                    @endif
                                                                @else
                                                                    <p class="d-none m-0">
                                                                        <label class="table-labels">
                                                                            <input type="checkbox" disabled="disabled"/>
                                                                            <span></span>
                                                                            <input type="hidden"  class="hidden-val" value="0"  name="delete[]">
                                                                        </label>
                                                                    </p>
                                                                @endif
                                                            </td>
                                                            <td class="center">
                                                                @if($main_menu->is_download == 1)
                                                                    <?php $downloadpage = checkBoxStatus($editPermissions,$main_menu->is_download,$main_menu->id,'is_download');  ?>
                                                                    @if($downloadpage == true)
                                                                        <p class="m-0">
                                                                            <label class="table-labels">
                                                                                <input type="checkbox" checked="checked" class="permission-check-box"/>
                                                                                <span></span>
                                                                                <input type="hidden" class="hidden-val" value="1"  name="download[]">
                                                                            </label>
                                                                        </p>
                                                                    @else
                                                                        <p class="m-0">
                                                                            <label class="table-labels">
                                                                                <input type="checkbox"class="permission-check-box"/>
                                                                                <span></span>
                                                                                <input type="hidden"  class="hidden-val" value="0"  name="download[]">
                                                                            </label>
                                                                        </p>
                                                                    @endif
                                                                @else
                                                                    <p class="d-none m-0">
                                                                        <label class="table-labels">
                                                                            <input type="checkbox" disabled="disabled"/>
                                                                            <span></span>
                                                                            <input type="hidden"  class="hidden-val" value="0"  name="download[]">
                                                                        </label>
                                                                    </p>
                                                                @endif
                                                            </td>
                                                            <td class="d-none">
                                                                <input type="text" class="d-none" name="page_id[]" value="{{$main_menu->id}}">
                                                                <input type="text" class="d-none" name="modulename[]" value="{{$main_menu->name}}">
                                                                <input type="text" class="d-none" name="parent_page_id[]" value="0">
                                                            </td>
                                                        </tr>
                                                        @foreach($getPagesList as $submenu)
                                                            @if($main_menu->id == $submenu->parent_page_id)
                                                                <tr>
                                                                    <td class=""  style="padding-left: 80px;">{{$submenu->name}}</td>
                                                                    <td class="center">
                                                                        @if($submenu->is_view == 1)
                                                                            <?php $viewpage = checkBoxStatus($editPermissions,$submenu->is_view,$submenu->id,'is_view');  ?>
                                                                            @if($viewpage==true)
                                                                                <p class="m-0">
                                                                                    <label class="table-labels">
                                                                                        <input type="checkbox" checked="checked" class="permission-check-box"/>
                                                                                        <span></span>
                                                                                        <input type="hidden" class="hidden-val" value="1"  name="view[]">
                                                                                    </label>
                                                                                </p>
                                                                            @else
                                                                                <p class="m-0">
                                                                                    <label class="table-labels">
                                                                                        <input type="checkbox" class="permission-check-box"/>
                                                                                        <span></span>
                                                                                        <input type="hidden"  class="hidden-val" value="0"  name="view[]">
                                                                                    </label>
                                                                                </p>
                                                                            @endif
                                                                        @else
                                                                            <p class="m-0">
                                                                                <label class="table-labels">
                                                                                    <input type="checkbox" disabled="disabled"/>
                                                                                    <span></span>
                                                                                    <input type="hidden"  class="hidden-val" value="0"  name="view[]">
                                                                                </label>
                                                                            </p>
                                                                        @endif
                                                                    </td>
                                                                    <td class="center">
                                                                        @if($submenu->is_edit == 1)
                                                                            <?php $editpage = checkBoxStatus($editPermissions,$submenu->is_edit,$submenu->id,'is_edit');  ?>
                                                                            @if($editpage==true)
                                                                                <p class="m-0">
                                                                                    <label class="table-labels">
                                                                                        <input type="checkbox" checked="checked" class="permission-check-box"/>
                                                                                        <span></span>
                                                                                        <input type="hidden" class="hidden-val" value="1"  name="edit[]">
                                                                                    </label>
                                                                                </p>
                                                                            @else
                                                                                <p class="m-0">
                                                                                    <label class="table-labels">
                                                                                        <input type="checkbox" class="permission-check-box"/>
                                                                                        <span></span>
                                                                                        <input type="hidden"  class="hidden-val" value="0"  name="edit[]">
                                                                                    </label>
                                                                                </p>
                                                                            @endif
                                                                        @else
                                                                            <p class="m-0">
                                                                                <label class="table-labels">
                                                                                    <input type="checkbox" disabled="disabled"/>
                                                                                    <span></span>
                                                                                    <input type="hidden"  class="hidden-val" value="0"  name="edit[]">
                                                                                </label>
                                                                            </p>
                                                                        @endif
                                                                    </td>
                                                                    <td class="center">
                                                                        @if($submenu->is_add == 1)
                                                                            <?php $addpage = checkBoxStatus($editPermissions,$submenu->is_add,$submenu->id,'is_add');  ?>
                                                                            @if($addpage==true)
                                                                                <p class="m-0">
                                                                                    <label class="table-labels">
                                                                                        <input type="checkbox" checked="checked" class="permission-check-box"/>
                                                                                        <span></span>
                                                                                        <input type="hidden" class="hidden-val" value="1"  name="add[]">
                                                                                    </label>
                                                                                </p>
                                                                            @else
                                                                                <p class="m-0">
                                                                                    <label class="table-labels">
                                                                                        <input type="checkbox" class="permission-check-box"/>
                                                                                        <span></span>
                                                                                        <input type="hidden"  class="hidden-val" value="0"  name="add[]">
                                                                                    </label>
                                                                                </p>
                                                                            @endif
                                                                        @else
                                                                            <p class="m-0">
                                                                                <label class="table-labels">
                                                                                    <input type="checkbox" disabled="disabled"/>
                                                                                    <span></span>
                                                                                    <input type="hidden"  class="hidden-val" value="0"  name="add[]">
                                                                                </label>
                                                                            </p>
                                                                        @endif
                                                                    </td>
                                                                    <td class="center">
                                                                        @if($submenu->is_delete == 1)
                                                                            <?php $deletepage = checkBoxStatus($editPermissions,$submenu->is_delete,$submenu->id,'is_delete');  ?>
                                                                            @if($deletepage==true)
                                                                                <p class="m-0">
                                                                                    <label class="table-labels">
                                                                                        <input type="checkbox" checked="checked" class="permission-check-box"/>
                                                                                        <span></span>
                                                                                        <input type="hidden" class="hidden-val" value="1"  name="delete[]">
                                                                                    </label>
                                                                                </p>
                                                                            @else
                                                                                <p class="m-0">
                                                                                    <label class="table-labels">
                                                                                        <input type="checkbox" class="permission-check-box"/>
                                                                                        <span></span>
                                                                                        <input type="hidden"  class="hidden-val" value="0"  name="delete[]">
                                                                                    </label>
                                                                                </p>
                                                                            @endif
                                                                        @else
                                                                            <p class="m-0">
                                                                                <label class="table-labels">
                                                                                    <input type="checkbox" disabled="disabled"/>
                                                                                    <span></span>
                                                                                    <input type="hidden"  class="hidden-val" value="0"  name="delete[]">
                                                                                </label>
                                                                            </p>
                                                                        @endif
                                                                    </td>
                                                                    <td class="center">
                                                                        @if($submenu->is_download == 1)
                                                                            <?php $downloadpage = checkBoxStatus($editPermissions,$submenu->is_download,$submenu->id,'is_download');  ?>
                                                                            @if($downloadpage==true)
                                                                                <p class="m-0">
                                                                                    <label class="table-labels">
                                                                                        <input type="checkbox" checked="checked" class="permission-check-box"/>
                                                                                        <span></span>
                                                                                        <input type="hidden" class="hidden-val" value="1"  name="download[]">
                                                                                    </label>
                                                                                </p>
                                                                            @else
                                                                                <p class="m-0">
                                                                                    <label class="table-labels">
                                                                                        <input type="checkbox" class="permission-check-box"/>
                                                                                        <span></span>
                                                                                        <input type="hidden"  class="hidden-val" value="0"  name="download[]">
                                                                                    </label>
                                                                                </p>
                                                                            @endif
                                                                        @else
                                                                            <p class="m-0">
                                                                                <label class="table-labels">
                                                                                    <input type="checkbox" disabled="disabled"/>
                                                                                    <span></span>
                                                                                    <input type="hidden"  class="hidden-val" value="0"  name="download[]">
                                                                                </label>
                                                                            </p>
                                                                        @endif
                                                                    </td>
                                                                    <td class="d-none">
                                                                        <input type="text" class="d-none" name="page_id[]" value="{{$submenu->id}}">
                                                                        <input type="text" class="d-none" name="modulename[]" value="{{$submenu->name}}">
                                                                        <input type="text" class="d-none" name="parent_page_id[]" value="{{$main_menu->id}}">
                                                                    </td>
                                                                </tr>
                                                            @endif
                                                        @endforeach
                                                    @endif
                                                    <?php $i++;?>
                                                @endforeach
                                            </tbody>
                                        </table>
                                        <input type="text" class="d-none" name="role_id" value="{{$fk_role_id}}">
                                    </div>
                                </div>
                                <div class="col-12 text-center mt-5">
                                    <button type="submit" class="btn btn-primary waves-effect save_role_permissions">UPDATE PERMISSIONS</button>                                                                
                                </div>
                            </div>    
                        </div>
                    </form>
                @else
                    <div>
                        <p class="text-center py-5" style="font-size:25px">No Roles Data Available To Show</p>
                    </div>
                @endif
            </div>
        </div>
    </div>
</section>
@endsection